namespace uMod.Plugins
{
    [Info("Mock Logging", "uMod", "0.0.1")]
    [Description("Mocking a plugin")]
    //[Log] // Uses uMod.Logging.Default logger (from global config)
    [Log("stack")] // Uses "stack" logger (from global config)
    [Log("my_mock_logger", "mocklog.log")] // Logs to logs/mocklog.log
    [LogDaily("my_mock_logger", "mocklog_{date|yyyy-MM-dd}.log")] // Logs to logs/mocklog_{yyyy-MM-dd}.log
    internal class MockLoggingPlugin : Plugin
    {
        [Hook("Loaded")]
        private void Loaded()
        {
            Logger.Info("MockLoggingPlugin.MockLoggingPluginLoaded");
        }
    }
}
