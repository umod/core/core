﻿using uMod.Tests.Mock;

namespace uMod.Plugins
{
    [Info("Mock Plugin", "uMod", "0.0.1")]
    [Description("Mocking a plugin")]
    internal class MockDeferredPlugin : Plugin
    {
        [Defer("MockDeferPlugin")]
        bool OnConflictedHook()
        {
            return true;
        }
    }
}
