using uMod.WebSockets;

namespace uMod.Plugins
{
    [Info("Mock RCON", "uMod", "0.0.1")]
    [Description("Mocks RCON handling for testing")]
    internal class MockRconPlugin : RconAdminPlugin
    {
        public bool started = false;

        [Hook("OnSocketServerStarted")]
        public void OnSocketServerStarted()
        {
            started = true;
        }
    }
}
