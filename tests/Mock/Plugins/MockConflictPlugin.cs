﻿namespace uMod.Plugins
{
    [Info("Mock Conflict", "uMod", "0.0.1")]
    [Description("Mocks hook conflicts for testing")]
    internal class MockConflictPlugin : Plugin
    {
        [Hook("OnConflictedHook")]
        public bool OnConflictedHook()
        {
            Logger.Info("ConflictedHookCalled");
            return true;
        }

        [Hook("OnConflictResolvedHook")]
        public bool OnConflictResolvedHook(HookEvent e)
        {
            Logger.Info("ConflictResolvedHookCalled");

            e.Context.Done(delegate (Plugin source, HookEvent e2)
            {
                if (e.Context.HasConflict)
                {
                    e.Context.FinalValue = true;
                }
            });

            return true;
        }
    }
}
