﻿namespace uMod.Plugins
{
    [Info("Mock Safety", "uMod", "0.0.1")]
    [Description("Mocks locking safety for testing")]
    internal class MockSafetyPlugin : Plugin
    {
        protected object Lock = new object();
        public int count = 0;

        [Hook("OnHookCountWithLock")]
        public void OnHookCountWithLock()
        {
            lock (Lock)
            {
                count++;
            }
        }

        [Hook("OnHookCountAsync"), Async]
        public void OnHookCountAsync()
        {
            lock (Lock)
            {
                count++;
            }
        }
    }
}
