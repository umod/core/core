﻿extern alias References;

using System.Collections.Generic;
using References::Newtonsoft.Json;
using uMod.Collections;
using uMod.Common;
using uMod.Configuration.Toml;
using uMod.Mock;

namespace uMod.Plugins
{
    [Info("Mock Player Dectorator", "uMod", "0.0.1")]
    [Description("Mocks a player decorator for testing")]
    internal class MockPlayerDecoratorPlugin : Plugin
    {
        public class PlayerDecorator
        {
            public IPlayer Player;
            public bool Special = false;
        }

        public class MockPlayerDecorator
        {
            public MockPlayer Player;
            public bool Special = false;
        }

        public class MockPlayerPersistedDataDecorator
        {
            [JsonIgnore, TomlIgnore]
            public MockPlayer Player {get; private set; }

            public bool Special = false;

        }

        [OnlinePlayers]
        public Hash<IPlayer, PlayerDecorator> MyPlayers = new Hash<IPlayer, PlayerDecorator>();

        [OnlinePlayers]
        public Hash<IPlayer, MockPlayerDecorator> MyMockPlayers = new Hash<IPlayer, MockPlayerDecorator>();

        [OnlinePlayers("persisted/{Id}_mydata.toml")]
        public Dictionary<IPlayer, MockPlayerPersistedDataDecorator> MyMockPersistedPlayers = new Dictionary<IPlayer, MockPlayerPersistedDataDecorator>();

        void MyTestHook(MockPlayerPersistedDataDecorator myTestPlayer)
        {
            myTestPlayer.Special = true;
        }
    }
}
