﻿namespace uMod.Plugins
{
    [Info("Mock Conflicted", "uMod", "0.0.1")]
    [Description("Mocks a plugin hook conflict for testing")]
    internal class MockConflictedPlugin : Plugin
    {
        [Hook("OnConflictedHook")]
        public bool OnConflictedHook()
        {
            Logger.Info("ConflictedHookCalled");
            return false;
        }

        [Hook("OnConflictResolvedHook")]
        public bool OnConflictResolvedHook()
        {
            Logger.Info("ConflictResolvedHookCalled");

            return false;
        }
    }
}
