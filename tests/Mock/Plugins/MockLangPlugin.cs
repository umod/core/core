﻿using System.Collections.Generic;

namespace uMod.Plugins
{
    [Info("Mock Lang", "uMod", "0.0.1")]
    [Description("Mocking a plugin")]
    internal class MockLangPlugin : Plugin
    {
        new void LoadDefaultMessages()
        {
            Lang.RegisterMessages (new Dictionary<string, string>
            {
                {"ExampleKey", "MockLangPlugin.ExampleEnglishMessage"}
            });

            Lang.RegisterMessages (new Dictionary<string, string>
            {
                {"ExampleKey", "MockLangPlugin.ExampleFrenchMesage"}
            }, "fr");
        }

        void Loaded()
        {
            Logger.Info(Lang.GetLangMessage("ExampleKey", "en"));
            Logger.Info(Lang.GetLangMessage("ExampleKey", "fr"));
        }
    }
}
