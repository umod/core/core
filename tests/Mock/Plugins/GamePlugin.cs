﻿using uMod.Plugins.Decorators;

namespace uMod.Plugins
{
    [Info("Mock Game", "uMod", "0.0.1")]
    [Description("Mocks a game plugin for testing")]
    [HookDecorator(typeof(ServerDecorator))]
    internal class GamePlugin : Plugin
    {
        void Loaded()
        {
            Interface.uMod.CallHook("IOnServerInitialized");
        }
    }
}
