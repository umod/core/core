﻿using uMod.Common;

namespace uMod.Plugins
{
    [Info("Mock Command Localized", "uMod", "0.0.1")]
    [Description("Mocks command localization for testing")]
    internal class MockCommandLocalizedPlugin : Plugin
    {
        [Localization]
        private interface MyLocale : ILocale
        {
            string MyCommand { get; }
            string MyCommandDescription { get; }
            string MyCommandHelp { get; }
        }

        [Locale]
        private class Default : MyLocale
        {
            public string MyCommand { get; } = "my_command";
            public string MyCommandDescription { get; } = "My command description";
            public string MyCommandHelp { get; } = "/my_command";
        }

        [Locale("ru")]
        private class RuDefault : MyLocale
        {
            public string MyCommand { get; } = "моя_команда";
            public string MyCommandDescription { get; } = "Описание моей команды";
            public string MyCommandHelp { get; } = "/моя_команда";
        }

        [Locale("fr")]
        private class FrDefault : MyLocale
        {
            public string MyCommand { get; } = "ma_commande";
            public string MyCommandDescription { get; } = "Ma description de commande";
            public string MyCommandHelp { get; } = "/ma_commande";
        }

        [Command("MyCommand {arg1?}")]
        [Locale(typeof(MyLocale))]
        [Description("MyCommandDescription")]
        [Help("MyCommandHelp")]
        private void cmdMockCommand(IPlayer player, ICommandInfo info)
        {
            Logger.Info("MockCommandLocalizedPlugin.MockCommandCalled");
            Logger.Info("MockCommandLocalizedPlugin.Commands:" + Commands.GetHelp(info.Definition.Name, Lang.GetLanguage(player)));
            Logger.Info("MockCommandLocalizedPlugin.Commands:" + Commands.GetDescription(info.Definition.Name, Lang.GetLanguage(player)));
            Logger.Info("MockCommandLocalizedPlugin.Info:" + info.GetHelp(player.Language.TwoLetterISOLanguageName));
            Logger.Info("MockCommandLocalizedPlugin.Info:" + info.GetDescription(player.Language.TwoLetterISOLanguageName));
        }
    }
}
