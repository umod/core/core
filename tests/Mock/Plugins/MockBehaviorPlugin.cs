namespace uMod.Plugins
{
    [Info("Mock Behavior", "uMod", "0.0.1")]
    [Description("Mocks a behavior for testing")]
    internal class MockBehaviorPlugin : Plugin
    {
        [Behavior]
        public class TestBehavior // : MonoBehaviour
        {
            public void Update()
            {
            }
        }
    }
}
