﻿using uMod.Common;

namespace uMod.Plugins
{
    [Info("Mock Chat Integration", "uMod", "0.0.1")]
    [Description("Mocks chat integration for testing")]
    internal class MockChatIntegrationPlugin : Plugin
    {
        [Hook("OnChat")]
        private void OnChat(IPlayer player, string message, HookEvent e)
        {
            Logger.Info("OnMockChatPluginChatCalled");
        }
    }
}
