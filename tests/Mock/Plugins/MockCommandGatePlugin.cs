using uMod.Auth;
using uMod.Common;

namespace uMod.Plugins
{
    [Info("Mock Command Gate", "uMod", "0.0.1")]
    [Description("Mocks command gates for testing")]
    internal class MockCommandGatePlugin : Plugin
    {
        private class MyActionGate : Gate
        {
            private readonly ILogger _logger;

            public MyActionGate(MockCommandGatePlugin plugin, ILogger logger) : base(plugin)
            {
                _logger = logger;
            }

            public bool Talking(IPlayer player)
            {
                _logger.Info("MockCommandGatePlugin.MyActionGate.TalkingCalled");
                return true;
            }

            public bool Whispering(IPlayer player)
            {
                _logger.Info("MockCommandGatePlugin.MyActionGate.WhisperingCalled");
                return false;
            }

            public bool Yelling(IPlayer player)
            {
                _logger.Info("MockCommandGatePlugin.MyActionGate.YellingCalled");
                return false;
            }
        }

        [Command("talk")]
        private void cmdTalk(IPlayer player, string cmd, string[] args)
        {
            Logger.Info("MockCommandGatePlugin.MockTalkCommand");
        }

        [Command("talk1"), Allows(typeof(MyActionGate), "talking")]
        private void cmdTalk1(IPlayer player, string cmd, string[] args)
        {
            Logger.Info("MockCommandGatePlugin.MockTalkCommand");
        }

        [Command("talk12"), Permission(typeof(MyActionGate), "talking")]
        private void cmdTalk12(IPlayer player, string cmd, string[] args)
        {
            Logger.Info("MockCommandGatePlugin.MockTalkCommand");
        }

        [Command("talk2")]
        [Denies(typeof(MyActionGate), "talking")]
        private void cmdTalk2(IPlayer player, string cmd, string[] args)
        {
            Logger.Info("MockCommandGatePlugin.MockTalk2Command");
        }

        [Command("yell")]
        [Denies(typeof(MyActionGate), "yelling")]
        private void cmdYell(IPlayer player, string cmd, string[] args)
        {
            Logger.Info("MockCommandGatePlugin.MockYellCommand");
        }

        [Command("yell2")]
        [None(typeof(MyActionGate), "yelling")]
        private void cmdYell2(IPlayer player, string cmd, string[] args)
        {
            Logger.Info("MockCommandGatePlugin.MockYell2Command");
        }

        [Command("talk3")]
        [All(typeof(MyActionGate), "talking", "yelling")]
        private void cmdTalk3(IPlayer player, string message, MyActionGate actionGate)
        {
            Logger.Info("MockCommandGatePlugin.MockTalk3Command");
        }

        [Command("talk4")]
        [Any(typeof(MyActionGate), "yelling", "talking")] // Called in order, yelling must be first to test both
        private void cmdTalk4(IPlayer player, string message, MyActionGate actionGate)
        {
            Logger.Info("MockCommandGatePlugin.MockTalk4Command");
        }

        [Command("talk5")]
        [None(typeof(MyActionGate), "talking", "yelling")]
        private void cmdTalk5(IPlayer player, string message, MyActionGate actionGate)
        {
            Logger.Info("MockCommandGatePlugin.MockTalk5Command");
        }
    }
}
