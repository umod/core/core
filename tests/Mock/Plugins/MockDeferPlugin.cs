﻿using uMod.Tests.Mock;

namespace uMod.Plugins
{
    [Info("Mock Defer", "uMod", "0.0.1")]
    [Description("Mocking a plugin")]
    internal class MockDeferPlugin : Plugin
    {
        bool OnConflictedHook()
        {
            return false;
        }
    }
}
