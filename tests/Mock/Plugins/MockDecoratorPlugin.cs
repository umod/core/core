﻿using uMod.Tests.Mock;

namespace uMod.Plugins
{
    [Info("Mock Decorator", "uMod", "0.0.1")]
    [Description("Mocks hook decorators for testing")]
    [HookDecorator(typeof(MockDecorator))]
    internal class MockDecoratorPlugin : Plugin
    {
        [HookDecorator(AutoRegister = false)]
        private class MyDecorator : HookDecorator
        {
            public MyDecorator(MockDecoratorPlugin plugin) : base(plugin)
            {
            }

            [Hook("OnAnyHook")]
            public void OnAnyHook()
            {
                Interface.uMod.LogInfo("MockDecoratorPlugin.MyDecorator.OnAnyHook");
            }
        }

        public void SubscribeMyDecorator()
        {
            Subscribe(nameof(MyDecorator));
        }

        public bool IsMyDecoratorSubscribed()
        {
            return IsSubscribed(nameof(MyDecorator));
        }
    }
}
