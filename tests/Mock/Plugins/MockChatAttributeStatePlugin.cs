﻿using uMod.Common;

namespace uMod.Plugins
{
    [Info("Mock Chat Attribute State", "uMod", "0.0.1")]
    [Description("Mocks chat attribute state for testing")]
    internal class MockChatAttributeStatePlugin : Plugin
    {
        [Hook("OnChat.Completed")]
        private void OnChat(IPlayer player, string message, HookEvent e)
        {
            if (e.State == EventState.Completed)
            {
                Logger.Info("OnMockChatPluginChatCompletedCalled");
            }

            Logger.Info("OnMockChatPluginChatUNCompletedCalled");
        }
    }
}
