﻿using uMod.Common;
using uMod.Libraries;

namespace uMod.Tests.Mock
{
    public class MockLibrary : Library
    {
        [LibraryProperty("TestProperty")]
        public bool TestProperty { get; } = true;

        public MockLibrary(IApplication application) : base(application)
        {
        }

        [LibraryFunction("GetTest")]
        public bool GetTest()
        {
            return true;
        }
    }
}
