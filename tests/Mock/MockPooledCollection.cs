﻿using System.Collections.Generic;
using System.Linq;
using uMod.Collections;

namespace uMod.Tests.Mock
{
    internal class MockPooledCollection<TValue> : ByRefPooledCollection<TValue>
    {
        public MockPooledCollection(ref ICollection<TValue> collection) : base(ref collection)
        {
        }

        protected override List<TValue> Pool()
        {
            return MockPool<TValue>.Pool(Collection);
        }

        protected override void Free(ref List<TValue> val)
        {
            MockPool<TValue>.Free(ref val);
        }

        protected override void Apply(ref List<TValue> items)
        {
            TryScope(out items);
        }

        private bool TryScope(out List<TValue> items)
        {
            items = Collection.Skip(1).ToList();
            return true;
        }
    }
}
