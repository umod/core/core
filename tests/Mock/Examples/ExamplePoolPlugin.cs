﻿using System.Collections.Generic;
using uMod.Libraries.Universal;
using uMod.Tests.Mock;

namespace uMod.Plugins
{
    [Info("Mock Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    class ExamplePoolPlugin : UniversalPlugin
    {
        public class ExamplePooledCollection<TValue> : MockPooledCollection<TValue>
        {
            public Position Position;
            public float Radius;

            public ExamplePooledCollection(ref ICollection<TValue> collection, Position position, float radius) : base(ref collection)
            {
                Position Position = position;
                Radius = radius;
            }

            protected override void Apply(ref List<TValue> items)
            {
                //Vis.Entities<Signage>(Position, radius, items); // TODO: Fix, not universal
            }
        }

        private void Loaded(MockServer server)
        {
            /*ExamplePooledCollection<Signage> collection = new ExamplePooledCollection<Signage>(server.World.Entities, new Position(0, 0, 0), 10); // TODO: Fix, not universal
            foreach (Signage entities in collection)
            {
                // All entities within a radius of 10 from 0,0,0
            }*/
        }
    }
}
