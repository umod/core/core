﻿extern alias References;

using References::Newtonsoft.Json.Linq;
using uMod.Libraries.Universal;

namespace uMod.Plugins
{
    [Info("Mock Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    class ExampleExtensionPlugin : UniversalPlugin
    {
        [Reference]
        public class MyOuterClass
        {
            public string TestProperty { get; }

            public bool Equals(JToken other)
            {
                return JToken.FromObject(this).Equals(other);
            }
        }

        public MyOuterClass outer = new MyOuterClass();

        [HookMethod("GenericHookArgument")]
        public void GenericHookArgument<TArg>(TArg someType, EventArgs e) where TArg : JToken
        {
        }

        [HookMethod("GenericHookArgument3WithResult")]
        public TResult GenericHookArgument3WithResult<TArg, TArg2, TResult>(TArg someType, bool test, TArg2 someOtherType, EventArgs e)
            where TArg : JToken
            where TArg2 : IPlugin
            where TResult : MyOuterClass
        {
            return default(TResult);
        }
    }
}
