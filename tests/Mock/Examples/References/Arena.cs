﻿using uMod.Libraries.Universal;

namespace uMod.Plugins
{
    [Info("Arena", "uMod", "1.0.0")]
    [Description("Mock arena")]
    class Arena : UniversalPlugin
    {
        [Optional]
        private Kits Kits;

        private void OnPlayerConnected(IPlayer player)
        {
            Kits?.GiveKit(player.Id, "default");
        }
    }

    namespace ArenaReferences
    {
        abstract class Kits : Plugin
        {
            public abstract void GiveKit(string userID, string kitName);
        }
    }
}
