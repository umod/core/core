﻿using uMod.Libraries.Universal;

namespace uMod.Plugins
{
    [Info("Event Plugin", "uMod", "1.0.0")]
    [Description("Mocking an event plugin")]
    class ExampleEventPlugin : UniversalPlugin
    {
        public void Loaded()
        {
            CallHook("MyHook");
        }

        public void MyHook(IServer server, EventArgs e)
        {
        }

        [After, Before, Canceled]
        public void MyHook(IEventContext sender, EventArgs e)
        {
            if (sender.Context is ExampleEventPlugin)
            {
                switch (e.State)
                {
                    case EventState.Completed:
                        break;

                    case EventState.Started:
                        break;

                    case EventState.Canceled:
                        break;
                }
            }
        }
    }
}
