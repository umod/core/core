﻿using uMod.Common;
using uMod.Extensions;
using uMod.Plugins;

namespace uMod.Tests.Mock
{
    internal class MockExtension : Extension
    {
        public MockExtension() : base()
        {
        }

        public override string Name => "MockExtension";

        public override string Title => "Mock Extension";

        public override string Author => "uMod Team";

        public override VersionNumber Version => new VersionNumber(1, 0, 0);

        public override bool SupportsReloading => true;

        public override void HandleAddedToManager(IExtensionManager manager)
        {
            manager.RegisterPluginLoader(new BatchPluginLoader(Interface.uMod.RootLogger, typeof(GamePlugin)));
        }
    }
}
