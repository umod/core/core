﻿using System.Collections.Generic;
using System.Linq;

namespace uMod.Tests.Mock
{
    internal class MockPool<T>
    {
        internal static List<T> Pool(ICollection<T> defaultValue)
        {
            if (defaultValue != null)
            {
                return defaultValue.ToList();
            }

            return Pool();
        }

        internal static List<T> Pool(ref ICollection<T> defaultValue)
        {
            if (defaultValue != null)
            {
                return defaultValue.ToList();
            }

            return Pool();
        }

        public static List<T> Pool()
        {
            return new List<T>();
        }

        internal static void Free(ref List<T> pool)
        {
            Interface.uMod.LogInfo("MockPoolFreeCalled");
        }
    }
}
