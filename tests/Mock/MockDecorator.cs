﻿using uMod.Plugins;

namespace uMod.Tests.Mock
{
    public class MockDecorator : HookDecorator
    {
        public MockDecorator(Plugin plugin) : base(plugin)
        {
        }

        [Hook("Init")]
        public void Init()
        {
            Interface.uMod.LogInfo("MockDecoratorInitCalled");
        }
    }
}
