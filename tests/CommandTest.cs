﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Command;
using uMod.Libraries;
using uMod.Mock;
using uMod.Plugins;

namespace uMod.Tests
{
    [TestClass]
    public class CommandTest : BaseTest
    {
        private Universal UniversalLibrary => Interface.uMod.Libraries.Get<Universal>();

        [TestMethod]
        [Plugin(typeof(MockCommandPlugin))]
        public void CommandHook()
        {
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);

            Interface.uMod.CallHook("cmdMockCommand", player, "/mockCommand", new string[] { });

            Assert.Await.IsLogged("MockCommandPlugin.MockCommandCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockCommandPlugin))]
        public void BasicCommand()
        {
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/mockCommand");

            Assert.Await.IsLogged("MockCommandPlugin.MockCommandCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockCommandPlugin))]
        public void AliasedCommand()
        {
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/mockAliasCommand1");

            Assert.Await.IsLogged("MockAliasedCommandCalledmockaliascommand1");

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/mockAliasCommand2");

            Assert.Await.IsLogged("MockAliasedCommandCalledmockaliascommand2");
        }

        [TestMethod]
        [Plugin(typeof(MockCommandPlugin))]
        public void ConsoleCommand()
        {
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);

            UniversalLibrary.Server.Command("mockCommand");

            Assert.Await.IsLogged("MockCommandPlugin.MockCommandCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockCommandPlugin))]
        public void AdvancedCommand()
        {
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/mockAdvancedCommand 1 hello");

            Assert.Await.IsLogged("MockCommandPlugin.MockAdvancedCommandCalled");

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/mockAdvancedCommand2 1 hello");

            Assert.Await.IsLogged("MockCommandPlugin.MockAdvancedCommand2Called");

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/mockAdvancedCommand3 1 hello");

            Assert.Await.IsLogged("MockCommandPlugin.MockAdvancedCommand3Called");

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/mockAdvancedCommand4 1 hello");

            Assert.Await.IsLogged("MockCommandPlugin.MockAdvancedCommand4Called");
        }

        [TestMethod]
        [Plugin(typeof(MockCommandLocalizedPlugin))]
        public void LocalizedCommand()
        {
            Lang LangLibrary = Interface.uMod.Libraries.Get<Lang>();

            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);
            LangLibrary.SetLanguage("en", player.Id);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/my_command");

            Assert.Await.IsLogged("MockCommandLocalizedPlugin.MockCommandCalled");
            Assert.Await.IsLogged("MockCommandLocalizedPlugin.Commands:/my_command");
            Assert.Await.IsLogged("MockCommandLocalizedPlugin.Commands:My command description");
            Assert.Await.IsLogged("MockCommandLocalizedPlugin.Info:/my_command");
            Assert.Await.IsLogged("MockCommandLocalizedPlugin.Info:My command description");

            ClearLog();

            LangLibrary.SetLanguage("ru", player.Id);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/моя_команда");

            Assert.Await.IsLogged("MockCommandLocalizedPlugin.MockCommandCalled");
            Assert.Await.IsLogged("MockCommandLocalizedPlugin.Commands:/моя_команда");
            Assert.Await.IsLogged("MockCommandLocalizedPlugin.Commands:Описание моей команды");
            Assert.Await.IsLogged("MockCommandLocalizedPlugin.Info:/моя_команда");
            Assert.Await.IsLogged("MockCommandLocalizedPlugin.Info:Описание моей команды");

            ClearLog();

            LangLibrary.SetLanguage("fr", player.Id);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/ma_commande");

            Assert.Await.IsLogged("MockCommandLocalizedPlugin.MockCommandCalled");
            Assert.Await.IsLogged("MockCommandLocalizedPlugin.Commands:/ma_commande");
            Assert.Await.IsLogged("MockCommandLocalizedPlugin.Commands:Ma description de commande");
            Assert.Await.IsLogged("MockCommandLocalizedPlugin.Info:/ma_commande");
            Assert.Await.IsLogged("MockCommandLocalizedPlugin.Info:Ma description de commande");
        }

        [TestMethod]
        [Plugin(typeof(MockCommandTomlLocalizedPlugin))]
        public void TomlLocalizedCommand()
        {
            Lang LangLibrary = Interface.uMod.Libraries.Get<Lang>();

            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);
            LangLibrary.SetLanguage("en", player.Id);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/my_command");

            Assert.Await.IsLogged("MockCommandTomlLocalizedPlugin.MockCommandCalled");
            Assert.Await.IsLogged("MockCommandTomlLocalizedPlugin.Commands:/my_command");
            Assert.Await.IsLogged("MockCommandTomlLocalizedPlugin.Commands:My command description");
            Assert.Await.IsLogged("MockCommandTomlLocalizedPlugin.Info:/my_command");
            Assert.Await.IsLogged("MockCommandTomlLocalizedPlugin.Info:My command description");

            ClearLog();

            LangLibrary.SetLanguage("ru", player.Id);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/моя_команда");

            Assert.Await.IsLogged("MockCommandTomlLocalizedPlugin.MockCommandCalled");
            Assert.Await.IsLogged("MockCommandTomlLocalizedPlugin.Commands:/моя_команда");
            Assert.Await.IsLogged("MockCommandTomlLocalizedPlugin.Commands:Описание моей команды");
            Assert.Await.IsLogged("MockCommandTomlLocalizedPlugin.Info:/моя_команда");
            Assert.Await.IsLogged("MockCommandTomlLocalizedPlugin.Info:Описание моей команды");

            ClearLog();

            LangLibrary.SetLanguage("fr", player.Id);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/ma_commande");

            Assert.Await.IsLogged("MockCommandTomlLocalizedPlugin.MockCommandCalled");
            Assert.Await.IsLogged("MockCommandTomlLocalizedPlugin.Commands:/ma_commande");
            Assert.Await.IsLogged("MockCommandTomlLocalizedPlugin.Commands:Ma description de commande");
            Assert.Await.IsLogged("MockCommandTomlLocalizedPlugin.Info:/ma_commande");
            Assert.Await.IsLogged("MockCommandTomlLocalizedPlugin.Info:Ma description de commande");
        }

        [TestMethod]
        [Plugin(typeof(MockCommandPlugin))]
        public void CommandDefinitionArguments()
        {
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/mockArgument 1");

            Assert.Await.IsLogged("MockCommandPlugin.MockArgumentCommand1Called");

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/mockArgument2 1");

            Assert.Await.IsLogged("MockCommandPlugin.MockArgumentCommand2Called");
        }

        [TestMethod]
        [Plugin(typeof(MockCommandPlugin))]
        public void CommandArgumentArray()
        {
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/mockMessage hello game world");

            Assert.Await.IsLogged("MockCommandPlugin.MockMessageCommandCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockCommandPlugin))]
        public void CommandArgumentArrayAlias()
        {
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/mm hello game world");

            Assert.Await.IsLogged("MockCommandPlugin.MockMessageCommandCalled");

            ClearLog();

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/mockMessage2 hello game world");

            Assert.Await.IsLogged("MockCommandPlugin.MockMessageCommandCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockCommandPlugin))]
        public void CommandDefinitionPositionArguments()
        {
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/mockPointArgument \"1 2\"");

            Assert.Await.IsLogged("MockCommandPlugin.MockArgumentCommandPointCalled");

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/mockPositionArgument \"1 2 3\"");

            Assert.Await.IsLogged("MockCommandPlugin.MockArgumentCommandPositionCalled");

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/mockPosition4Argument \"1 2 3 4\"");

            Assert.Await.IsLogged("MockCommandPlugin.MockArgumentCommandPosition4Called");
        }

        [TestMethod]
        [Plugin(typeof(MockCommandPlugin))]
        public void CommandDefinitionArgumentsInvalid()
        {
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/mockArgument");

            Assert.Await.IsLogged("MockCommandPlugin.MockArgumentCommand1Invalid");
        }

        [TestMethod]
        public void CommandArgs()
        {
            string[] commandParts = {
                "test_command",
                "yeS",
                "faLse",
                "1",
                "1.2",
                "\"hello world\"",
                "\"00:00:01.4\"",
                "\"05/01/2009 14:57:32.8\"",
                "360347",
                "984566",
                "65535"
            };

            CommandDefinition definition = new CommandDefinition("test_command {arg1} {arg2} {arg3} {arg4} {arg5} {arg6} {arg7} {arg8} {arg9} {arg10}");

            string cmd = string.Join(" ", commandParts);

            Args arg = new Args(definition, null, cmd);

            Assert.IsTrue(arg.GetBool("arg1"));
            Assert.IsFalse(arg.GetBool("arg2", true));
            Assert.AreEqual(1, arg.GetInt("arg3"));
            Assert.AreEqual(1.2f, arg.GetFloat("arg4"));
            Assert.AreEqual("hello world", arg.GetString("arg5"));
            Assert.AreEqual(TimeSpan.FromSeconds(1.4), arg.GetTimeSpan("arg6"));
            Assert.AreEqual(DateTime.Parse("05/01/2009 14:57:32.8"), arg.GetDateTime("arg7"));
            Assert.AreEqual((ulong)360347, arg.GetUInt64("arg8"));
            Assert.AreEqual(984566, arg.GetLong("arg9"));
            Assert.AreEqual((ushort)65535, arg.GetUShort("arg10"));
        }

        /// <summary>
        /// Determine if conversion correctly converts bound model
        /// </summary>
        [TestMethod]
        [Plugin(typeof(MockCommandBindingPlugin))]
        public void CommandWithConversion()
        {
            Plugin mockPlugin = GetPlugin<MockCommandBindingPlugin>();
            MockPlayer iPlayer = new MockPlayer(new GamePlayerIdentity());

            mockPlugin.Call("RegisterPlayer", iPlayer);

            UniversalLibrary.CommandSystem.HandleChatMessage(iPlayer, "/mockcommand");
            Assert.Await.IsLogged("MockChatBindingPlugin.MockCommand IPlayer");

            UniversalLibrary.CommandSystem.HandleChatMessage(iPlayer, "/mockbindcommand");
            Assert.Await.IsLogged("MockChatBindingPlugin.MockBindCommand TestPlayer");
        }

        /// <summary>
        /// Determine if command gets called in hookdecorator
        /// </summary>
        [TestMethod]
        [Plugin(typeof(MockCommandDecoratorPlugin))]
        public void DecoratorWithCommand()
        {
            Plugin mockPlugin = GetPlugin<MockCommandDecoratorPlugin>();

            MockPlayer iPlayer = new MockPlayer(new GamePlayerIdentity());

            UniversalLibrary.CommandSystem.HandleChatMessage(iPlayer, "/testcommand");
            Assert.Await.IsLogged("MockCommandDecoratorPlugin.TestDecorator.TestCommand");

            mockPlugin.Call("UnsubscribeDecorator");

            UniversalLibrary.CommandSystem.HandleChatMessage(iPlayer, "/testcommand2");
            Assert.Await.IsNotLogged("MockCommandDecoratorPlugin.TestDecorator.TestCommand2");
        }
    }
}
