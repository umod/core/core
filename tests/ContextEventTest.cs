﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Common;
using uMod.Mock;
using uMod.Plugins;

namespace uMod.Tests
{
    [TestClass]
    [Plugin(typeof(MockPlugin))]
    public class ContextEventTest : BaseTest
    {
        [TestMethod]
        public void BasicContextEvent()
        {
            MockPlugin mockPlugin = GetPlugin<MockPlugin>();

            EventArgs<Plugin> e = new EventArgs<Plugin>();
            bool result = false;
            bool done = false;
            ContextEventAsyncCallback<EventArgs<Plugin>, Plugin, Plugin> promise = new ContextEventAsyncCallback<EventArgs<Plugin>, Plugin, Plugin>(mockPlugin, delegate (Plugin sender, EventArgs<Plugin> e2)
            {
                result = true;
                return true;
            });

            Interface.uMod.Dispatcher.Dispatch(delegate ()
            {
                promise.Invoke(e, mockPlugin);
                done = true;
                return true;
            });

            Assert.Await.IsTrue(() => result, 50, 100);

            Assert.AreEqual(EventState.Completed, promise.State);

            Assert.Await.IsTrue(() => promise.State == EventState.Completed);
            //task.Wait();
            Assert.IsTrue(result);
            Assert.IsTrue(done);
        }

        [TestMethod]
        public void BasicFailedContextEvent()
        {
            MockPlugin mockPlugin = GetPlugin<MockPlugin>();

            EventArgs<Plugin> e = new EventArgs<Plugin>();
            bool result = false;
            bool fail = false;
            ContextEventAsyncCallback<EventArgs<Plugin>, Plugin, Plugin> promise = new ContextEventAsyncCallback<EventArgs<Plugin>, Plugin, Plugin>(mockPlugin, delegate (Plugin sender, EventArgs<Plugin> e2)
            {
                result = true;
                throw new Exception("Test");
            });

            Interface.uMod.Dispatcher.Dispatch(delegate ()
            {
                fail = !promise.Invoke(e, mockPlugin);
                return true;
            });

            Assert.Await.IsTrue(() => result, 50, 100);
            Assert.Await.IsTrue(() => fail, 50, 100);

            Assert.AreEqual(EventState.Failed, promise.State);

            Assert.Await.IsTrue(() => promise.State == EventState.Failed);

            Assert.AreEqual(EventState.Failed, promise.State);
            Assert.IsTrue(result);
            Assert.IsTrue(fail);
        }

        [TestMethod]
        public void ReusableContextEvent()
        {
            MockPlugin mockPlugin = GetPlugin<MockPlugin>();

            EventArgs<Plugin> e = new EventArgs<Plugin>();

            bool check = false;
            int calls = 0;
            ContextEventAsyncCallback<EventArgs<Plugin>, Plugin, Plugin> promise = new ContextEventAsyncCallback<EventArgs<Plugin>, Plugin, Plugin>(mockPlugin, delegate (Plugin sender, EventArgs<Plugin> e2)
            {
                calls++;
                return check;
            });

            Assert.IsFalse(promise.Invoke(e));

            Assert.Await.IsTrue(() => calls == 1);

            check = true;
            Assert.IsTrue(promise.Invoke(e));
            Assert.Await.IsTrue(() => calls == 2);

            Assert.IsTrue(promise.Invoke(e)); // Should return true without dispatching since promise is resolved
        }

        [TestMethod]
        public void ReusableAsyncContextEvent()
        {
            MockPlugin mockPlugin = GetPlugin<MockPlugin>();

            EventArgs<Plugin> e = new EventArgs<Plugin>();

            bool check = false;
            int calls = 0;
            ContextEventAsyncCallback<EventArgs<Plugin>, Plugin, Plugin> promise = new ContextEventAsyncCallback<EventArgs<Plugin>, Plugin, Plugin>(mockPlugin, delegate (Plugin sender, EventArgs<Plugin> e2)
             {
                 calls++;
                 return check;
             });

            Interface.uMod.Dispatcher.Dispatch(delegate ()
            {
                promise.Invoke(e);
                return true;
            });

            Assert.Await.IsTrue(() => calls == 1);

            check = true;
            Interface.uMod.Dispatcher.Dispatch(delegate ()
            {
                promise.Invoke(e);
                return true;
            });

            Assert.Await.IsTrue(() => calls == 2);
        }
    }
}
