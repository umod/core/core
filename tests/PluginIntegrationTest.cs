extern alias References;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using References::Newtonsoft.Json.Linq;
using uMod.Mock;
using uMod.Plugins;

namespace uMod.Tests
{
    [TestClass]
    public class PluginIntegrationTest : BaseTest
    {
        [TestMethod]
        [Plugin(typeof(MockDependentPlugin))]
        public void RequiresDependencyNotFound()
        {
            Assert.Await.IsLogged("Failed to initialize plugin");
        }

        [TestMethod]
        [Plugin(typeof(MockDependencyPlugin), typeof(MockDependentPlugin))]
        public void RequiresDependencyFound()
        {
            Assert.Await.IsLogged("MockDependentRequiredDependencyPluginFound");
        }

        [TestMethod]
        [Plugin(typeof(MockDependencyAdvancedPlugin), typeof(MockDependentPlugin))]
        public void RequiresDependencyAdvancedFound()
        {
            Assert.Await.IsLogged("MockDependentRequiredDependencyPluginFound");
        }

        [TestMethod]
        [Plugin(typeof(MockDependencyPlugin), typeof(MockPlugin), typeof(MockDependentPlugin))]
        public void OptionalDependencyFound()
        {
            Assert.Await.IsLogged("MockDependentOptionalDependencyPluginFound");
        }

        [TestMethod]
        [Plugin(typeof(MockIntegratedPlugin))]
        public void GenericHookArgument()
        {
            MockIntegratedPlugin mockPlugin = GetPlugin<MockIntegratedPlugin>();
            object test = new object();

            mockPlugin.CallHook("GenericHookArgument", JToken.FromObject(test));

            Assert.Await.IsLogged("OnMockIntegrationPluginGenericHookArgumentCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockIntegratedPlugin))]
        public void GenericHookArgumentWithResult()
        {
            MockIntegratedPlugin mockPlugin = GetPlugin<MockIntegratedPlugin>();

            object test = new object();

            object result = mockPlugin.CallHook("GenericHookArgumentWithResult", JToken.FromObject(test));

            Assert.IsTrue(result is MockIntegratedPlugin.MyOuterClass);

            Assert.Await.IsLogged("OnMockIntegrationPluginGenericHookArgumentWithResultCalled");

            Assert.IsTrue(typeof(MockIntegratedPlugin.MyOuterClass).IsAssignableFrom(result.GetType()));

            mockPlugin.CallHook("GenericHookArgument2WithResult", JToken.FromObject(test), mockPlugin);

            Assert.Await.IsLogged("OnMockIntegrationPluginGenericHookArgument2WithResultCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockIntegratedPlugin))]
        public void GenericHookArgument2WithResult()
        {
            MockIntegratedPlugin mockPlugin = GetPlugin<MockIntegratedPlugin>();
            object test = new object();

            object result = mockPlugin.CallHook("GenericHookArgument2WithResult", JToken.FromObject(test), mockPlugin);

            Assert.IsTrue(result is MockIntegratedPlugin.MyOuterClass);
            Assert.AreSame(typeof(MockIntegratedPlugin.MyOuterClass), result.GetType());

            Assert.Await.IsLogged("OnMockIntegrationPluginGenericHookArgument2WithResultCalled");
            Assert.Await.IsLogged("OnMockIntegrationPluginGenericHookArgument2.1WithResultCalled");
            Assert.Await.IsLogged("OnMockIntegrationPluginGenericHookArgument2.2WithResultCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockIntegratedPlugin))]
        public void GenericHookArgument3WithResult()
        {
            MockIntegratedPlugin mockPlugin = GetPlugin<MockIntegratedPlugin>();
            object test = new object();

            object result = mockPlugin.CallHook("GenericHookArgument3WithResult", JToken.FromObject(test), true, mockPlugin);

            Assert.IsTrue(result is MockIntegratedPlugin.MyOuterClass);

            Assert.Await.IsLogged("OnMockIntegrationPluginGenericHookArgument3WithResultCalled");
            Assert.Await.IsLogged("OnMockIntegrationPluginGenericHookArgument3.1WithResultCalled");
            Assert.Await.IsLogged("OnMockIntegrationPluginGenericHookArgument3.2WithResultCalled");

            Assert.IsTrue(typeof(MockIntegratedPlugin.MyOuterClass).IsAssignableFrom(result.GetType()));
        }
    }
}
