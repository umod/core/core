﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Auth;
using uMod.Common;
using uMod.Mock;
using uMod.Plugins;

namespace uMod.Tests
{
    [TestClass]
    public class GatePolicyTest : BaseTest
    {
        private MockPlayer mockPlayer;

        public MockPlayer MockPlayer
        {
            get
            {
                if (mockPlayer != null)
                {
                    return mockPlayer;
                }
                GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
                return mockPlayer = new MockPlayer(gamePlayer.Identity);
            }
        }

        private class JumpConfig
        {
            public bool JumpingAllowed = true;
        }

        private class TestConfigGate : Gate
        {
            private JumpConfig config = new JumpConfig();

            public TestConfigGate(IPlugin plugin) : base(plugin)
            {
            }

            public bool Jumping(IPlayer player)
            {
                return config.JumpingAllowed;
            }
        }

        private class TestGate : Gate
        {
            public TestGate(IPlugin plugin) : base(plugin)
            {
            }

            public bool Jumping(IPlayer player)
            {
                return true;
            }

            public bool Swimming(IPlayer player)
            {
                return false;
            }
        }

        private class TestPermissionGate : PermissionGate
        {
            public TestPermissionGate(IPlugin plugin) : base(plugin)
            {
            }

            public bool Jumping(IPlayer player)
            {
                return true;
            }

            public bool Swimming(IPlayer player)
            {
                return false;
            }
        }

        [TestMethod]
        public void GateAllowAllowed()
        {
            TestGate gate = new TestGate(null);
            Assert.IsTrue(gate.Allows("jumping"));
        }

        [TestMethod]
        public void GateAllowDenied()
        {
            TestGate gate = new TestGate(null);
            Assert.IsFalse(gate.Allows("swimming"));
        }

        [TestMethod]
        public void GateDenyAllowed()
        {
            TestGate gate = new TestGate(null);
            Assert.IsFalse(gate.Denies("jumping"));
        }

        [TestMethod]
        public void GateDenyDenied()
        {
            TestGate gate = new TestGate(null);
            Assert.IsTrue(gate.Denies("swimming"));
        }

        [TestMethod]
        public void ConfigGateAllowAllowed()
        {
            TestConfigGate gate = new TestConfigGate(null);
            Assert.IsTrue(gate.Allows("jumping"));
        }

        [TestMethod]
        public void ConfigGateDenyAllowed()
        {
            TestConfigGate gate = new TestConfigGate(null);
            Assert.IsFalse(gate.Denies("jumping"));
        }

        [TestMethod]
        public void PermissionGateAllowAllowed()
        {
            TestPermissionGate gate = new TestPermissionGate(null);
            Assert.IsTrue(gate.Allows("jumping"));
        }

        [TestMethod]
        public void PermissionGateAllowDenied()
        {
            TestPermissionGate gate = new TestPermissionGate(null);
            Assert.IsFalse(gate.Allows("swimming"));
        }

        [TestMethod]
        public void PermissionGateDenyAllowed()
        {
            TestPermissionGate gate = new TestPermissionGate(null);
            Assert.IsFalse(gate.Denies("jumping"));
        }

        [TestMethod]
        public void PermissionGateDenyDenied()
        {
            TestPermissionGate gate = new TestPermissionGate(null);
            Assert.IsTrue(gate.Denies("swimming"));
        }
    }
}
