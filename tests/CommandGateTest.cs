using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Libraries;
using uMod.Mock;
using uMod.Plugins;

namespace uMod.Tests
{
    [TestClass]
    public class CommandGateTest : BaseTest
    {
        private Universal UniversalLibrary => Interface.uMod.Libraries.Get<Universal>();

        [TestMethod]
        [Plugin(typeof(MockCommandGatePlugin))]
        public void CommandHook()
        {
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/talk");

            Assert.Await.IsLogged("MockCommandGatePlugin.MockTalkCommand");
        }

        [TestMethod]
        [Plugin(typeof(MockCommandGatePlugin))]
        public void CommandHookAllows()
        {
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/talk1");

            Assert.Await.IsLogged("MockCommandGatePlugin.MockTalkCommand");
        }

        [TestMethod]
        [Plugin(typeof(MockCommandGatePlugin))]
        public void CommandHookAllowsPermission()
        {
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/talk12");

            Assert.Await.IsLogged("MockCommandGatePlugin.MockTalkCommand");
        }

        [TestMethod]
        [Plugin(typeof(MockCommandGatePlugin))]
        public void CommandHookDenies()
        {
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/talk2");

            Assert.Await.IsLogged("MockCommandGatePlugin.MyActionGate.TalkingCalled");
            Assert.Await.IsNotLogged("MockCommandGatePlugin.MockTalk2Command");

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/yell");

            Assert.Await.IsLogged("MockCommandGatePlugin.MockYellCommand");
        }

        [TestMethod]
        [Plugin(typeof(MockCommandGatePlugin))]
        public void CommandHookAll()
        {
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/talk3");

            Assert.Await.IsLogged("MockCommandGatePlugin.MyActionGate.TalkingCalled");
            Assert.Await.IsLogged("MockCommandGatePlugin.MyActionGate.YellingCalled");
            Assert.Await.IsNotLogged("MockCommandGatePlugin.MockTalk3Command");
        }

        [TestMethod]
        [Plugin(typeof(MockCommandGatePlugin))]
        public void CommandHookAny()
        {
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/talk4");

            Assert.Await.IsLogged("MockCommandGatePlugin.MyActionGate.TalkingCalled");
            Assert.Await.IsLogged("MockCommandGatePlugin.MyActionGate.YellingCalled");
            Assert.Await.IsLogged("MockCommandGatePlugin.MockTalk4Command");
        }

        [TestMethod]
        [Plugin(typeof(MockCommandGatePlugin))]
        public void CommandHookNone()
        {
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            MockPlayer player = new MockPlayer(gamePlayer.Identity);

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/talk5");

            Assert.Await.IsLogged("MockCommandGatePlugin.MyActionGate.TalkingCalled");
            Assert.Await.IsNotLogged("MockCommandGatePlugin.MockTalk5Command");

            UniversalLibrary.CommandSystem.HandleChatMessage(player, "/yell2");

            Assert.Await.IsLogged("MockCommandGatePlugin.MyActionGate.YellingCalled");
            Assert.Await.IsLogged("MockCommandGatePlugin.MockYell2Command");
        }
    }
}
