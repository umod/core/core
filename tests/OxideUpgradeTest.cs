﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Common;
using uMod.Libraries;
using uMod.Mock;

namespace uMod.Tests
{
    [TestClass]
    [Environment("auth.driver", "protobuf")]
    public class OxideUpgradeTest : BaseTest
    {
        [TestInitialize]
        public override void SetupTest()
        {
            CleanFiles();

            string relativePath = Path.Combine("..", "..", "..", "umod", "data");
            DirectoryInfo srcFolder = new DirectoryInfo(Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, relativePath)));
            srcFolder.CopyTo(Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, "umod")));

            base.SetupTest();
        }

        private void CleanFiles()
        {
            string dataDir = Path.Combine(Environment.CurrentDirectory, Path.Combine("umod", "data"));
            string[] paths = {
                Path.Combine(dataDir, "umod.groups.data"),
                Path.Combine(dataDir, "umod.lang.data"),
                Path.Combine(dataDir, "umod.users.data")
            };

            foreach (string file in paths)
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
            }
        }

        [TestMethod]
        public void LegacyPlayerBob()
        {
            IPlayer player = CurrentScope.Module.Server.PlayerManager.FindPlayer("12345", PlayerFilter.All);
            Assert.IsNotNull(player);
            Assert.AreEqual("bob", player.Name);
            Assert.IsTrue(player.HasPermission("mockplugin.test4"));
            Assert.IsTrue(player.BelongsToGroup("default"));
            Assert.AreEqual("en", player.Language.Name);
        }

        [TestMethod]
        public void LegacyPlayerJoe()
        {
            IPlayer player = CurrentScope.Module.Server.PlayerManager.FindPlayer("54321", PlayerFilter.All);
            Assert.IsNotNull(player);
            Assert.AreEqual("joe", player.Name);
            Assert.IsTrue(player.HasPermission("mockplugin.test5"));
            Assert.IsTrue(player.BelongsToGroup("admin"));
            Assert.AreEqual("ru", player.Language.Name);
        }

        [TestMethod]
        public void LegacyGroups()
        {
            Permission permission = CurrentScope.Module.Libraries.Get<Permission>();

            Assert.IsTrue(permission.GroupExists("default"));
            Assert.IsTrue(permission.GroupHasPermission("default", "mockplugin.test1"));
            Assert.IsTrue(permission.GroupExists("admin"));
            Assert.IsTrue(permission.GroupHasPermission("admin", "mockplugin.test2"));
            Assert.IsTrue(permission.GroupExists("test"));
            Assert.IsTrue(permission.GroupHasPermission("test", "mockplugin.test3"));
        }
    }
}
