﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Common;
using uMod.Configuration;
using uMod.Mock;
using uMod.Plugins;
using uMod.Tests.Mock;

namespace uMod.Tests
{
    [TestClass]
    [Extension(typeof(MockExtension))]
    public class BaseTest : global::uMod.Mock.BaseTest
    {
        private static bool Loaded;

        public override IModule LoadModule()
        {
            Loaded = false;
            Interface.DebugCallback = delegate (string message)
            {
                lock (TestScope.LogLock)
                {
                    TestScope.Log.AppendLine(message);
                }
            };

            Module uMod = new Module(Interface.DebugCallback);
            
            CurrentScope.PluginLoader = new BatchPluginLoader(uMod.RootLogger);
            CurrentScope.Module = uMod;
            CurrentScope.Module.OnLoaded.Add(Load);

            return Interface.InitializeDebug(uMod, new InitializationInfo()
            {
                Applications = CurrentScope.LoadApps.ToArray(),
                Environment = GetEnvironment(),
                Extensions = GetExtensionTypes()
            });
        }

        public override void Register()
        {
            Assert.Await.IsTrue(() => Loaded, 250, 250);
        }

        public void Load()
        {
            Loaded = true;
        }

        protected override IPlugin GetPlugin(Type type)
        {
            return Utility.Plugins.Get(type.Name);
        }
    }
}
