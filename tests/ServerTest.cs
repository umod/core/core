﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Logging;

namespace uMod.Tests
{
    [TestClass]
    public class ServerTest : BaseTest
    {
        [TestMethod]
        public void ServerInitialized()
        {
            Assert.IsInstanceOfType(Interface.uMod, typeof(Module));
        }

        [TestMethod]
        public void ServerInstanceDirectory()
        {
            Assert.IsInstanceOfType(Interface.uMod.InstanceDirectory, typeof(string));
        }

        [TestMethod]
        public void DefaultConfigPosted()
        {
            Assert.IsTrue(Interface.uMod.Plugins.Configuration.Watchers.PluginWatchers);
        }

        [TestMethod]
        public void RootLoggerInitialized()
        {
            Assert.IsInstanceOfType(Interface.uMod.RootLogger, typeof(StackLogger));
        }

        [TestMethod]
        public void CanPostLog()
        {
            Interface.uMod.LogInfo("Test Log");
            Assert.Await.IsLogged("Test Log");
        }
    }
}
