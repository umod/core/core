﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Mock;
using uMod.Plugins;

namespace uMod.Tests
{
    [TestClass]
    [Plugin(typeof(MockGatePlugin))]
    public class PluginGateTest : BaseTest
    {
        public MockPlayer MockPlayer => new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);
        public MockPlayer MockTarget => new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);

        public Plugin MockGatePlugin => GetPlugin<MockGatePlugin>();

        [TestMethod]
        public void PluginLocalGateAllows()
        {
            MockPlayer mockPlayer = MockPlayer;
            MockGatePlugin.CallHook("OnPlayerConnected", mockPlayer);

            Assert.IsTrue(mockPlayer.HasPermission("mockgateplugin.test"));
            MockGatePlugin.CallHook("OnChat", mockPlayer, "hello world");
            Assert.Await.IsLogged("MockGatePlugin.OnChat.Allowed");
        }

        [TestMethod]
        public void PluginAttributeGateAllows()
        {
            MockPlayer mockPlayer = MockPlayer;
            MockGatePlugin.CallHook("OnPlayerConnected", mockPlayer);
            MockGatePlugin.CallHook("OnJump2", mockPlayer);
            Assert.Await.IsLogged("MockGatePlugin.MyActionGate.JumpingCalled");
            Assert.Await.IsLogged("MockGatePlugin.OnJump2.Allowed");
        }

        [TestMethod]
        public void PluginInjectedGateAllows()
        {
            MockPlayer mockPlayer = MockPlayer;
            MockGatePlugin.CallHook("OnPlayerConnected", mockPlayer);
            MockGatePlugin.CallHook("OnJump", mockPlayer);
            Assert.Await.IsLogged("MockGatePlugin.MyActionGate.JumpingCalled");
            Assert.Await.IsLogged("MockGatePlugin.OnJump.Allowed");
        }

        [TestMethod]
        public void PluginGateAny()
        {
            MockPlayer mockPlayer = MockPlayer;
            MockGatePlugin.CallHook("OnPlayerConnected", MockPlayer);
            Assert.AreEqual(true, MockGatePlugin.CallHook("CanSwimOrFly", MockPlayer));
        }

        [TestMethod]
        public void PluginGateAll()
        {
            MockPlayer mockPlayer = MockPlayer;
            MockGatePlugin.CallHook("OnPlayerConnected", MockPlayer);
            Assert.AreEqual(true, MockGatePlugin.CallHook("CanJumpAndFly", MockPlayer));
        }

        [TestMethod]
        public void PluginGateNone()
        {
            MockPlayer mockPlayer = MockPlayer;
            MockGatePlugin.CallHook("OnPlayerConnected", MockPlayer);
            Assert.AreEqual(true, MockGatePlugin.CallHook("CanNotSwimOrFall", MockPlayer));
        }

        [TestMethod]
        public void PluginGateAnyWithAttribute()
        {
            MockPlayer mockPlayer = MockPlayer;
            MockGatePlugin.CallHook("OnPlayerConnected", MockPlayer);
            Assert.AreEqual(true, MockGatePlugin.CallHook("CanSwimOrFlyWithAttribute", MockPlayer));
        }

        [TestMethod]
        public void PluginGateAllWithAttribute()
        {
            MockPlayer mockPlayer = MockPlayer;
            MockGatePlugin.CallHook("OnPlayerConnected", MockPlayer);
            Assert.AreEqual(true, MockGatePlugin.CallHook("CanJumpAndFlyWithAttribute", MockPlayer));
        }

        [TestMethod]
        public void PluginGateNoneWithAttribute()
        {
            MockPlayer mockPlayer = MockPlayer;
            MockGatePlugin.CallHook("OnPlayerConnected", MockPlayer);
            Assert.AreEqual(true, MockGatePlugin.CallHook("CanNotSwimOrFallWithAttribute", MockPlayer));
        }

        [TestMethod]
        public void PluginGateWithParameterShouldAllow()
        {
            MockPlayer mockPlayer = MockPlayer;
            MockGatePlugin.CallHook("OnPlayerConnected", MockPlayer);
            MockGatePlugin.CallHook("OnPlayerConnected", MockTarget);
            MockGatePlugin.CallHook("OnShoot", MockPlayer, MockTarget);
            Assert.Await.IsLogged("MockGatePlugin.MyActionGate.ShootingAtCalled");
            Assert.Await.IsLogged("MockGatePlugin.OnShoot.Allowed");
        }

        [TestMethod]
        public void PluginAttributeGateDenies()
        {
            MockPlayer mockPlayer = MockPlayer;
            MockGatePlugin.CallHook("OnSwim2", mockPlayer);
            Assert.Await.IsLogged("MockGatePlugin.MyActionGate.SwimmingCalled");
            Assert.Await.IsLogged("MockGatePlugin.OnSwim2.Denied");
        }

        [TestMethod]
        public void PluginInjectedGateDenies()
        {
            MockPlayer mockPlayer = MockPlayer;
            MockGatePlugin.CallHook("OnSwim", mockPlayer);
            Assert.Await.IsLogged("MockGatePlugin.MyActionGate.SwimmingCalled");
            Assert.Await.IsLogged("MockGatePlugin.OnSwim.Denied");
        }
    }
}
