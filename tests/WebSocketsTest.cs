﻿extern alias References;

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using References::Newtonsoft.Json.Linq;
using uMod.Common.Web;
using uMod.Common.WebSockets;
using uMod.Libraries;
using uMod.Mock;
using uMod.Plugins;
using Websocket.Client;

namespace uMod.Tests
{
    //TODO: enable after WebSockets app fully deployed
    //[TestClass]
    [Application("WebSockets")]
    public class WebSocketsTest : BaseTest
    {
        [TestMethod]
        [Plugin(typeof(MockRconPlugin))]
        public void SimpleWebsocketConnection()
        {
            MockRconPlugin mockPlugin = GetPlugin<MockRconPlugin>();

            Assert.Await.IsTrue(() => mockPlugin.started, 100, 200);

            int msgCount = 0;
            WebsocketClient client = new WebsocketClient(new Uri("ws://127.0.0.1:8081"));
            client.MessageReceived.Subscribe(delegate(ResponseMessage msg ) {
                msgCount++;
            });

            client.Start();

            var packet = new Packet()
            {
                Type = "groups"
            };

            string msg = JsonConvert.SerializeObject(packet);
            client.Send(msg);

            Assert.Await.IsTrue(() => msgCount > 0, 100, 200);
        }
    }
}
