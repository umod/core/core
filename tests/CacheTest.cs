using Microsoft.VisualStudio.TestTools.UnitTesting;
#if DEBUG
using System;
#endif

namespace uMod.Tests
{
    [TestClass]
    public class CacheTest : BaseTest
    {
        [TestMethod]
        public void CacheAdd()
        {
            Cache<string, int> cache = new Cache<string, int>();

            cache.Add("test", 1);

            Assert.AreEqual(cache["test"], 1);
        }

        [TestMethod]
        public void CacheRemove()
        {
            Cache<string, int> cache = new Cache<string, int>();

            cache.Add("test", 1);
            cache.Add("test2", 2);

            Assert.AreEqual(cache["test"], 1);
            Assert.AreEqual(cache["test2"], 2);

            cache.Forget("test");

            Assert.AreEqual(1, cache.Count);
        }

        [TestMethod]
        public void CacheClear()
        {
            Cache<string, int> cache = new Cache<string, int>();

            cache.Add("test", 1);

            cache.Clear();

            Assert.AreEqual(0, cache.Count);
        }

        [TestMethod]
        public void CacheTryGetValue()
        {
            Cache<string, int> cache = new Cache<string, int>();

            cache.Add("test", 1);

            Assert.IsTrue(cache.TryGetValue("test", out int result));
            Assert.AreEqual(1, result);
        }

#if DEBUG
        [TestMethod]
        public void CacheExpire()
        {
            Cache<string, int> cache = new Cache<string, int>();

            cache.Add("test2", 1);
            // Add cache item that is already expired (testing only)
            CacheItem<string, int> item = new CacheItem<string, int>("test", 2, DateTime.Now.Subtract(new TimeSpan(0, 5, 0)));

            cache.Add(item);
            Assert.IsFalse(cache.TryGetValue("test", out _));
        }

#endif
    }
}
