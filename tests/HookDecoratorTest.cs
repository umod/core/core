﻿using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Mock;
using uMod.Plugins;

namespace uMod.Tests
{
    [TestClass]
    public class HookDecoratorTest : BaseTest
    {
        [TestMethod]
        [Plugin(typeof(MockDecoratorPlugin))]
        public void PluginDecorator()
        {
            Assert.Await.IsLogged("MockDecoratorInitCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockPlayerDecoratorPlugin))]
        public void HookPlayerDecorator()
        {
            MockPlayerDecoratorPlugin mockPlugin = GetPlugin<MockPlayerDecoratorPlugin>();

            MockPlayer player = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);

            mockPlugin.CallHook("OnPlayerConnected", player);

            Assert.AreEqual(1, mockPlugin.MyPlayers.Count);

            mockPlugin.CallHook("OnPlayerDisconnected", player);

            Assert.Await.IsTrue(() => mockPlugin.MyPlayers.Count == 0);
        }

        [TestMethod]
        [Plugin(typeof(MockPlayerDecoratorPlugin))]
        public void HookMockPlayerDecorator()
        {
            MockPlayerDecoratorPlugin mockPlugin = GetPlugin<MockPlayerDecoratorPlugin>();

            MockPlayer player = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);

            mockPlugin.CallHook("OnPlayerConnected", player);

            Assert.AreEqual(1, mockPlugin.MyMockPlayers.Count);

            mockPlugin.CallHook("OnPlayerDisconnected", player);

            Assert.Await.IsTrue(() => mockPlugin.MyMockPlayers.Count == 0);
        }

        [TestMethod]
        [Plugin(typeof(MockPlayerDecoratorPlugin))]
        public void HookMockPlayerDecoratorRotate()
        {
            MockPlayerDecoratorPlugin mockPlugin = GetPlugin<MockPlayerDecoratorPlugin>();

            MockPlayer player = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);

            mockPlugin.CallHook("OnPlayerConnected", player);
            Assert.AreEqual(1, mockPlugin.MyMockPlayers.Count);
            mockPlugin.MyMockPlayers[player].Special = true;
            mockPlugin.CallHook("OnPlayerDisconnected", player);
            mockPlugin.CallHook("OnPlayerConnected", player);
            Assert.AreEqual(1, mockPlugin.MyMockPlayers.Count);
            Assert.IsTrue(mockPlugin.MyMockPlayers[player].Special);

            mockPlugin.CallHook("OnPlayerDisconnected", player);
            Assert.Await.IsTrue(() => mockPlugin.MyMockPlayers.Count == 0);
        }

        [TestMethod]
        [Plugin(typeof(MockIntegratedPlugin))]
        public void HookDecorator()
        {
            Plugin mockPlugin = GetPlugin<MockIntegratedPlugin>();

            Assert.IsInstanceOfType(mockPlugin, typeof(MockIntegratedPlugin));
            MockPlayer mockPlayer = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);
            Interface.uMod.CallHook("OnPlayerConnected", mockPlayer);

            Assert.Await.IsLogged("OnPlayerConnectedHookDecorator");
        }

        [TestMethod]
        [Plugin(typeof(MockPlayerDecoratorPlugin))]
        public void HookPersistedPlayerDecorator()
        {
            MockPlayerDecoratorPlugin mockPlugin = GetPlugin<MockPlayerDecoratorPlugin>();

            MockPlayer player = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);

            mockPlugin.CallHook("OnPlayerConnected", player);

            Assert.Await.IsTrue(() => mockPlugin.MyMockPersistedPlayers.Count == 1, 50, 50);

            mockPlugin.MyMockPersistedPlayers[player].Special = true;

            mockPlugin.CallHook("OnPlayerDisconnected", player);

            Assert.Await.IsTrue(() => mockPlugin.MyMockPersistedPlayers.Count == 0);

            string path = System.IO.Path.Combine(Interface.uMod.DataDirectory, "persisted", $"{player.Id}_mydata.toml");

            Assert.Await.IsTrue(() => System.IO.File.Exists(path));

            System.IO.File.Delete(path);
        }

        [TestMethod]
        [Plugin(typeof(MockPlayerDecoratorPlugin))]
        public void HookPersistedPlayerSubstituionDecorator()
        {
            MockPlayerDecoratorPlugin mockPlugin = GetPlugin<MockPlayerDecoratorPlugin>();

            MockPlayer player = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);

            mockPlugin.CallHook("OnPlayerConnected", player);

            Assert.Await.IsTrue(() => mockPlugin.MyMockPersistedPlayers.Count == 1, 50, 50);

            mockPlugin.CallHook("MyTestHook", player);

            Assert.Await.IsTrue(() => mockPlugin.MyMockPersistedPlayers[player].Special == true);

            string path = System.IO.Path.Combine(Interface.uMod.DataDirectory, "persisted", $"{player.Id}_myotherdata.toml");

            System.IO.File.Delete(path);
        }

        [TestMethod]
        [Plugin(typeof(MockDecoratorPlugin))]
        public void HookDecoratorSubscription()
        {
            MockDecoratorPlugin mockPlugin = GetPlugin<MockDecoratorPlugin>();

            Assert.IsInstanceOfType(mockPlugin, typeof(MockDecoratorPlugin));
            Assert.Await.IsFalse(() => mockPlugin.IsMyDecoratorSubscribed());
            mockPlugin.SubscribeMyDecorator();
            Assert.Await.IsTrue(() => mockPlugin.IsMyDecoratorSubscribed());
            Interface.uMod.CallHook("OnAnyHook");
            Assert.Await.IsLogged("MockDecoratorPlugin.MyDecorator.OnAnyHook");
        }
    }
}
