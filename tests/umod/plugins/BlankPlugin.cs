﻿using uMod.Tests.Mock;

namespace uMod.Plugins
{
    [Info("Blank Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    class BlankPlugin : Plugin
    {
    }
}
