﻿namespace uMod.Plugins
{
    [Info("Batch Compiled Plugin #1", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    public class BatchCompilePlugin1 : Plugin
    {
        public bool TestPluginCompiled()
        {
            return true;
        }
    }
}
