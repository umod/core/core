﻿//Requires: MockReferencedPlugin

namespace uMod.Plugins
{
    [Info( "MagicRequiresPlugin", "uMod", "1.0.0")]
    [Description("References a plugin using a magic reference")]
    public class MagicRequiresPlugin : Plugin
    {
        [Requires]
        private MockReferencedPlugin MockReferencedPlugin;

        [HookMethod("TestReferenceHook" )]
        public void TestReferenceHook()
        {
            Logger.Info( "TestReferenceHook Called" );
            MockReferencedPlugin?.TestMethod();
        }
    }
}
