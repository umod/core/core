﻿namespace uMod.Plugins
{
    [Info("Context Plugin", "uMod", "1.0.0")]
    [Description("Context Plugin")]
    public class ContextPlugin : Plugin
    {
        public interface TestInterface
        {
            bool Test { get; }
        }

        public class MyOuterClass : TestInterface
        {
            public bool Test { get; }
        }

        public void Loaded()
        {
            //Interface.uMod.CallHook("Context2Hook", new MyOuterClass());
        }
    }
}
