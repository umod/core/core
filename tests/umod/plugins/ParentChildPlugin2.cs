﻿using System.Linq;
using uMod.Common;

namespace uMod.Plugins
{
    [Info("Parent Child Plugin 2", "uMod", "0.0.1")]
    [Description("Parent Child Plugin")]
    public class ParentChildPlugin2 : Plugin
    {
        [Requires("ParentPlugin")] private ParentPlugin ParentPlugin;
    }
}
