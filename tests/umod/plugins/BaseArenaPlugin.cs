using uMod.Common;

namespace uMod.Plugins
{
    [Info("Base Arena", "uMod", "0.0.1")]
    [Description("Base arena test plugin")]
    public class BaseArenaPlugin : Plugin
    {
        [Optional("ArenaEventPlugin")]
        private IMessenger ArenaEventPlugin;

        [Hook("OnArenaLoaded")]
        private void OnArenaLoaded()
        {
            ArenaEventPlugin?.SayHi();
            /*var messengers = Utility.Plugins.Find<IMessenger>();
            foreach (var messenger in messengers)
            {
                messenger.SayHi();
            }*/
        }

        public class TestClass
        {
            public string Message = "BaseArenaPlugin.TestClass.Message";
        }

        public interface IMessenger : IPlugin
        {
            void SayHi();
        }
    }
}
