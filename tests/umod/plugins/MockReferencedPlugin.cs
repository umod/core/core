﻿namespace uMod.Plugins
{
    [Info("Mock Referenced Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    public class MockReferencedPlugin : Plugin
    {
        public bool TestMethod()
        {
            Logger.Info("ProxiedTestMethodCalled");
            return true;
        }
    }
}
