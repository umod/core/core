﻿namespace uMod.Plugins
{
    [Info("Mock Dynamic Dependency Plugin", "uMod", "1.0.0")]
    [Description("Fancy Test Module Plugin")]
    public class MockDynamicDependencyPlugin : Plugin
    {
        [Requires]
        private MockDynamicDependentPlugin MockDynamicDependentPlugin;

        public class TestClass : MockDynamicDependentPlugin.IMockInterface
        {
            public string Test { get; }
        }
    }
}
