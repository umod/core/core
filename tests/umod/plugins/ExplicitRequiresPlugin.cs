﻿namespace uMod.Plugins
{
    [Info("ExplicitRequiresPlugin", "uMod", "1.0.0")]
    [Description("References a plugin using a magic reference")]
    public class ExplicitRequiresPlugin : Plugin
    {
        [Requires]
        private MockReferencedPlugin MockReferencedPlugin;

        [HookMethod("TestReferenceHook")]
        public void TestReferenceHook()
        {
            Logger.Info("TestReferenceHook Called");
            MockReferencedPlugin?.TestMethod();
        }
    }
}
