﻿namespace uMod.Plugins
{
    [Info("Mock Reference Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    class MockReferencePlugin : Plugin
    {
        [Optional]
        private MockReferencedPlugin MockReferencedPlugin;

        [Requires]
        private MockReferencedPlugin2 MockReferencedPlugin2;

        [HookMethod("TestReferenceHook")]
        public void TestReferenceHook()
        {
            Logger.Info( "TestReferenceHook Called" );
            MockReferencedPlugin?.TestMethod();
        }
    }

    namespace MyMock
    {
        public abstract class MockReferencedPlugin : Plugin
        {
            public abstract bool TestMethod();
        }

        public abstract class MockReferencedPlugin2 : Plugin
        {
        }
    }
}
