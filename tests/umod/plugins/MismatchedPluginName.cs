﻿namespace uMod.Plugins
{
    [Info("Mismatched PluginName 4321", "uMod", "1.0.0")]
    [Description("A plugin that doesn't have the CS file line up with the class name")]
    public class MismatchedPluginName1234 : Plugin
    {
        public bool TestMethod()
        {
            Logger.Info("ProxiedTestMethodCalled");
            return true;
        }
    }
}
