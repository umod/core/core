﻿using System;
using System.Collections.Generic;
using uMod.Common.Database;

namespace uMod.Plugins
{
    [Info("Database Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    public class DatabasePlugin : Plugin
    {
        [Serializable]
        public class TestData
        {
            public int Number;
            public string String;
        }

        [Hook("Connect")]
        public void Connect()
        {
            string DataSource = $"{Interface.uMod.DataDirectory}/db.db";

            ConnectionInfo connectionInfo = new ConnectionInfo()
            {
                Type = ConnectionType.SQLite,
                Name = "MyPluginConnection",
                ConnectionString = $"Data Source={DataSource};Version=3;",
                Persistent = false
            };

            Logger.Info("Connecting...");
            DB.Open(connectionInfo)
                .Fail(delegate (Exception exception)
                {
                    Logger.Error(exception.Message);
                    return null;
                })
                .Done((Database.Client.Connection connection) =>
                {
                    Logger.Info($"Connection state {connection.State}");

                    connection.Execute("CREATE TABLE IF NOT EXISTS \"test_table\" (\"Number\" INTEGER, \"String\" TEXT, \"Key\" INTEGER PRIMARY KEY AUTOINCREMENT);")
                    .Done(delegate ()
                    {
                        Logger.Info("Created table");
                    });

                    connection.Execute("INSERT INTO test_table (Number, String) VALUES (1, \"hello\");")
                    .Done(delegate ()
                    {
                        Logger.Info("Inserted row");
                    }, delegate (Exception exception)
                    {
                        Logger.Error(exception.Message);
                    });

                    connection.Query<List<TestData>>("SELECT Number, String FROM test_table;")
                        .Done(delegate (List<TestData> result)
                        {
                            if (result is List<TestData> data)
                            {
                                Logger.Info("Result test success");
                            }
                            else
                            {
                                Logger.Error("Result is not List of TestData");
                            }
                        }, delegate (Exception exception)
                        {
                            Logger.Error(exception.Message);
                        });

                    connection.Close().Done((Database.Client.Connection c) =>
                    {
                        Logger.Info("Closing data connection");
                    });
                });
        }
    }
}
