﻿namespace uMod.Plugins
{
    [Info("Context2 Plugin", "uMod", "1.0.0")]
    [Description("Context2 Plugin")]
    class Context2Plugin : Plugin
    {
        [Requires]
        public ContextPlugin ContextPlugin;

        [Hook("Context2Hook")]
        public void Context2Hook(ContextPlugin.TestInterface outerClass)
        {
            if (outerClass != null)
            {
                Interface.uMod.LogInfo("Context2HookCalled");
            }
        }

        public void Loaded()
        {
            Interface.uMod.LogInfo("Context2HookLoadedCalled");
        }
    }
}
