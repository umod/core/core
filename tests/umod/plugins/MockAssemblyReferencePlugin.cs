﻿//Reference:FakeReference

namespace uMod.Plugins
{
    [Info("Mock Assembly Reference Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    public class MockAssemblyReferencePlugin : Plugin
    {
        void Loaded()
        {
            var fakeObject = new FakeReference.FakeClass();

            int result = fakeObject.FakeMethod();

            Logger.Info($"MockAssemblyReference_Loaded:{result}");
        }
    }
}
