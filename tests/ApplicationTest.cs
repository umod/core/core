using Microsoft.VisualStudio.TestTools.UnitTesting;
#if DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
#endif

namespace uMod.Tests
{
    [TestClass]
    public class ApplicationTest : BaseTest
    {
        private class TestType
        {
        }

        private class TestTypeOther
        {
        }

#if DEBUG
        [TestMethod]
        public void BindGenericObject()
        {
            Application app = new Application();

            TestType test = new TestType();

            app.When(typeof(ApplicationTest)).Needs(typeof(TestType)).Bind(test);

            app.TryInject<TestType>(this, null, out object test3);

            Assert.AreSame(test, test3);
        }

        [TestMethod]
        public void BindObject()
        {
            Application app = new Application();

            TestType test = new TestType();

            app.When(typeof(ApplicationTest)).Needs(typeof(TestType)).Bind(test);

            app.TryInject(typeof(TestType), this, null, out object test3);

            Assert.AreSame(test, test3);
        }

        private TestType GetTestType()
        {
            return new TestType();
        }

        [TestMethod]
        public void BindMethod()
        {
            Application app = new Application();

            MethodInfo method = GetType().GetMethod("GetTestType", BindingFlags.Instance | BindingFlags.NonPublic);

            app.When(typeof(ApplicationTest)).Needs(typeof(TestType)).Bind(method);

            app.TryInject(typeof(TestType), this, null, out object test3);

            Assert.IsInstanceOfType(test3, typeof(TestType));
        }

        [TestMethod]
        public void BindCallback()
        {
            Application app = new Application();

            app.When(typeof(ApplicationTest)).Needs(typeof(TestType)).Bind(() => new TestType());

            app.TryInject(typeof(TestType), this, null, out object test3);

            Assert.IsInstanceOfType(test3, typeof(TestType));
        }

        [TestMethod]
        public void BindCallbackWithParameter()
        {
            Application app = new Application();

            app.When(typeof(ApplicationTest)).Needs(typeof(TestType)).Bind(param => new TestType());

            app.TryInject(typeof(TestType), this, null, out object test3);

            Assert.IsInstanceOfType(test3, typeof(TestType));
        }

        [TestMethod]
        public void BindTryGetObject()
        {
            Application app = new Application();

            TestType inType = new TestType();
            app.Bind(inType);

            app.TryGetObject(typeof(TestType), out TestType outType);

            Assert.AreEqual(inType, outType);
        }

        [TestMethod]
        public void BindTryGetType()
        {
            Application app = new Application();

            TestType inType = new TestType();
            app.Bind(inType);

            app.TryGetType("uMod.Tests.ApplicationTest+TestType", out Type type);

            Assert.AreEqual(typeof(TestType), type);
        }

        [TestMethod]
        public void BindTryGetTypes()
        {
            Application app = new Application();

            TestType inType1 = new TestType();
            TestTypeOther inType2 = new TestTypeOther();
            app.Bind(inType1);
            app.Bind(inType2);

            app.TryGetTypes("*TestType*", out IEnumerable<Type> type);

            Assert.AreEqual(2, type.Count());
        }

        [TestMethod]
        public void BindAddedEvent()
        {
            Application app = new Application();

            int callbacks = 0;

            app.Added.Add(delegate (Type type, object obj)
            {
                Assert.AreEqual(typeof(TestType), type);
                callbacks++;
            });

            app.Bind(new TestType());

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void BindAddedTypeEvent()
        {
            Application app = new Application();

            int callbacks = 0;

            app.AddedType.Add(delegate (Type type)
            {
                Assert.AreEqual(typeof(TestType), type);
                callbacks++;
            });

            app.Bind(new TestType());
            app.Bind(new TestType());

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void BindRemovedTypeEvent()
        {
            Application app = new Application();

            int callbacks = 0;

            app.RemovedType.Add(delegate (Type type)
            {
                Assert.AreEqual(typeof(TestType), type);
                callbacks++;
            });

            TestType obj1 = new TestType();
            TestType obj2 = new TestType();
            app.Bind(obj1);
            app.Bind(obj2);

            app.Unbind(obj1);

            Assert.AreEqual(0, callbacks);

            app.Unbind(obj2);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void BindRemovedEvent()
        {
            Application app = new Application();

            int callbacks = 0;

            app.Removed.Add(delegate (Type type, object obj)
            {
                Assert.AreEqual(typeof(TestType), type);
                callbacks++;
            });

            TestType testObject = new TestType();
            app.Bind(testObject);

            app.Unbind(testObject);

            Assert.AreEqual(1, callbacks);
            app.TryGetObject(typeof(TestType), out object testObject2);
            Assert.IsNull(testObject2);
        }

        [TestMethod]
        public void ResolveTryGetTypes()
        {
            Application app = new Application();

            app.Resolve(typeof(TestType));
            app.Resolve(typeof(TestTypeOther));

            app.TryGetTypes("*TestType*", out IEnumerable<Type> type);

            Assert.AreEqual(2, type.Count());
        }

#endif
    }
}
