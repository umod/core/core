﻿using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Common;
using uMod.Mock;
using uMod.Tests.Mock;

namespace uMod.Tests
{
    [TestClass]
    [Library(typeof(MockLibrary))]
    public class LibraryTest : BaseTest
    {
        [TestMethod]
        public void LibraryFunctionInvoke()
        {
            ILibrary mockLibrary = GetLibrary<MockLibrary>();

            object result = mockLibrary.Invoke("GetTest");
            Assert.IsInstanceOfType(result, typeof(bool));
            Assert.IsTrue((bool)result);
        }

        [TestMethod]
        public void LibraryFunctionInvokeWithCast()
        {
            ILibrary mockLibrary = GetLibrary<MockLibrary>();

            bool result = mockLibrary.Invoke<bool>("GetTest");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void LibraryGetFunctionNames()
        {
            ILibrary mockLibrary = GetLibrary<MockLibrary>();

            string[] functionNames = mockLibrary.GetFunctionNames().ToArray();

            Assert.IsTrue(functionNames.Contains("GetTest"));
        }

        [TestMethod]
        public void LibraryGetFunctionInfo()
        {
            ILibrary mockLibrary = GetLibrary<MockLibrary>();

            MethodInfo method = mockLibrary.GetFunction("GetTest");

            Assert.IsTrue(method != null);
        }

        [TestMethod]
        public void LibraryGetPropertyNames()
        {
            ILibrary mockLibrary = GetLibrary<MockLibrary>();

            string[] propertyNames = mockLibrary.GetPropertyNames().ToArray();

            Assert.IsTrue(propertyNames.Contains("TestProperty"));
        }

        [TestMethod]
        public void LibraryGetPropertyInfo()
        {
            ILibrary mockLibrary = GetLibrary<MockLibrary>();

            PropertyInfo property = mockLibrary.GetProperty("TestProperty");

            Assert.IsTrue(property != null);
        }

        [TestMethod]
        public void LibraryGetPropertyValue()
        {
            ILibrary mockLibrary = GetLibrary<MockLibrary>();

            object result = mockLibrary.Get("TestProperty");
            Assert.IsInstanceOfType(result, typeof(bool));
            Assert.IsTrue((bool)result);
        }

        [TestMethod]
        public void LibraryGetPropertyValueWithCast()
        {
            ILibrary mockLibrary = GetLibrary<MockLibrary>();

            bool result = mockLibrary.Get<bool>("TestProperty");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void LibrarySetPropertyValue()
        {
            ILibrary mockLibrary = GetLibrary<MockLibrary>();

            bool result = mockLibrary.Set("TestProperty", false);
            Assert.IsFalse(result);

            result = mockLibrary.Get<bool>("TestProperty");
            Assert.IsFalse(result);
        }
    }
}
