extern alias References;

using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using References::Newtonsoft.Json;
using References::Newtonsoft.Json.Linq;
using uMod.Common.Web;
using uMod.IO;
using uMod.Libraries;
using uMod.Mock;
using uMod.Plugins;
using uMod.Web;

namespace uMod.Tests
{
    [TestClass]
    [Application("WebClient")]
    [Plugin(typeof(MockPlugin))]
    public class PluginWebTest : BaseTest
    {
        private Client Web => new Client(GetPlugin<MockPlugin>());

        [TestMethod]
        public void GetLegacyRequestCallback()
        {
            MockPlugin plugin = GetPlugin<MockPlugin>();
            WebRequests webrequest = CurrentScope.Module.Libraries.Get<WebRequests>();
            int responseCount = 0;
            webrequest.Enqueue("http://httpbin.org/get?test=hello", null, delegate (WebResponse response)
            {
                responseCount++;
            }, plugin);

            webrequest.Enqueue("http://httpbin.org/get?test=hello", null, delegate (int code, string response)
            {
                responseCount++;
            }, plugin);

            Assert.Await.IsTrue(() => responseCount == 2, 100, 50);
        }

        [TestMethod]
        public void GetRequestCallback()
        {
            bool responseTriggered = false;
            JObject responseData = null;
            Web.Get("http://httpbin.org/get?test=hello")
                .Done(delegate (WebResponse e)
                {
                    string responseString = e.ReadAsString();
                    responseData = JObject.Parse(responseString);
                    responseTriggered = true;
                });

            Assert.Await.IsTrue(() => responseTriggered);
            Assert.AreEqual(responseData["args"]["test"], "hello");
        }

        [TestMethod]
        public void PostUploadJsonCallback()
        {
            bool responseTriggered = false;
            DataFileTest.MockData mockData = new DataFileTest.MockData()
            {
                Test = true
            };

            JsonFile<DataFileTest.MockData> inputFile = new JsonFile<DataFileTest.MockData>(DataFileTest.JsonDataFile, mockData);
            inputFile.Formatting = Formatting.None;
            JObject responseData = null;
            Web.Post("http://httpbin.org/post", null, null, new Dictionary<string, IMultipartFile>
                {
                    { "file", inputFile.ToAttachment()}
                })
                .Done(delegate (WebResponse e)
                {
                    string responseString = e.ReadAsString();
                    responseData = JObject.Parse(responseString);
                    responseTriggered = true;
                });

            Assert.Await.IsTrue(() => responseTriggered);
            Assert.AreEqual(responseData["files"]["file"], inputFile.ToString());
        }

        [TestMethod]
        public void GetMultipleRequestCallback()
        {
            int count = 0;
            JObject responseData = null;
            Web.Get("http://httpbin.org/get?test=hello")
                .Done(delegate (WebResponse e)
                {
                    string responseString = e.ReadAsString();
                    responseData = JObject.Parse(responseString);
                    count++;
                });

            Assert.Await.IsTrue(() => count == 1, 100, 200);
            Assert.AreEqual(responseData["args"]["test"], "hello");

            Web.Get("http://httpbin.org/get?test=hello")
                .Done(delegate (WebResponse e)
                {
                    string responseString = e.ReadAsString();
                    responseData = JObject.Parse(responseString);
                    count++;
                });

            Assert.Await.IsTrue(() => count == 2, 100, 200);
            Assert.AreEqual(responseData["args"]["test"], "hello");
        }

        [TestMethod]
        public void GetRequestCallbackWithoutContext()
        {
            JObject responseData = null;
            bool responseTriggered = false;
            Client web = new Client();
            web.Get("http://httpbin.org/get?test=hello")
                .Done(delegate (WebResponse e)
                {
                    string responseString = e.ReadAsString();
                    responseData = JObject.Parse(responseString);
                    responseTriggered = true;
                });

            Assert.Await.IsTrue(() => responseTriggered);
            Assert.AreEqual(responseData["args"]["test"], "hello");
        }

        [TestMethod]
        public void GetRequestFailed()
        {
            bool responseTriggered = false;
            bool failTriggered = false;
            Web.Get("not_a_url")
                .Done(delegate (WebResponse e)
                {
                    responseTriggered = true;
                }, delegate (Exception ex)
                {
                    failTriggered = true;
                });

            Assert.Await.IsTrue(() => failTriggered);
            Assert.IsFalse(responseTriggered);
        }
    }
}
