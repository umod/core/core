﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Mock;
using uMod.Plugins;

namespace uMod.Tests
{
    [TestClass]
    public class HookDeprecationTest : BaseTest
    {
        [TestMethod]
        [Plugin(typeof(MockDeprecationPlugin))]
        public void DeprecatedHookWithSameNameAndDifferentParameters()
        {
            Plugin plugin = GetPlugin<MockDeprecationPlugin>();

            object[] oldArgs = { "hello" };

            Interface.uMod.CallDeprecatedHook("OnOldHook", "OnOldHook(string, bool)", new DateTime(9999, 1, 1), oldArgs);

            Assert.Await.IsLogged("obsolete hook 'OnOldHook'");
            ClearLog();

            object[] newArgs = { true, "hello" };

            Interface.uMod.CallHook("OnOldHook", newArgs);

            Assert.Await.IsNotLogged("obsolete hook 'OnOldHook'");
        }
    }
}
