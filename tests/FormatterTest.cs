﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Text;

namespace uMod.Tests
{
    [TestClass]
    public class FormatterTest : BaseTest
    {
        [DataTestMethod]
        [DataRow("[#555555]text[/#]")]
        [DataRow("[+9]text[/+]")]
        [DataRow("[b]text[/b]")]
        [DataRow("[i]text[/i]")]
        public void ToPlaintextFormat(string input)
        {
            string actual = Formatter.ToPlaintext(input);

            Assert.AreEqual("text", actual);
        }

        [DataTestMethod]
        [DataRow("[#555555]text[/#]", "<color=#555555ff>text</color>")]
        [DataRow("[+9]text[/+]", "<size=9>text</size>")]
        [DataRow("[b]text[/b]", "<b>text</b>")]
        [DataRow("[i]text[/i]", "<i>text</i>")]
        public void ToUnityFormat(string input, string expected)
        {
            string actual = Formatter.ToUnity(input);

            Assert.AreEqual(expected, actual);
        }

        [DataTestMethod]
        [DataRow("[#555555]text[/#]", "[555555]text[e7e7e7]")]
        public void ToRoKAnd7DTDFormat(string input, string expected)
        {
            string actual = Formatter.ToRoKAnd7DTD(input);

            Assert.AreEqual(expected, actual);
        }

        [DataTestMethod]
        [DataRow("[#555555]text[/#]", "[c/555555:text]")]
        public void ToTerrariaFormat(string input, string expected)
        {
            string actual = Formatter.ToTerraria(input);

            Assert.AreEqual(expected, actual);
        }

        private class StringFormat
        {
            [Placeholder("greeting")]
            public string GreetingImpl = "hello";

            [Placeholder("location")]
            public string LocationImpl = "world";

            [PlaceholderIgnore]
            public string SomethingIgnored = "ignored";
        }

        private class FormatContainer
        {
            [Placeholder("container")]
            public StringFormat GreetingFormat = new StringFormat();
        }

        [TestMethod]
        public void FormatContainerObjectArgs()
        {
            string format = "{container.greeting} {container.location}";

            string actual = format.Interpolate(new FormatContainer());

            Assert.AreEqual("hello world", actual);
        }

        [TestMethod]
        public void FormatObjectArg()
        {
            string format = "{greeting} {location}";

            string actual = format.Interpolate(new StringFormat());

            Assert.AreEqual("hello world", actual);
        }

        [TestMethod]
        public void FormatSingleArgs()
        {
            string format = "{greeting} {location}";

            string actual = format.Interpolate("greeting", "hello");
            actual = actual.Interpolate("location", "world");

            Assert.AreEqual("hello world", actual);
        }

        [TestMethod]
        public void FormatDictionaryArgObject()
        {
            string format = "{greeting} {location}";

            string actual = format.Interpolate(new Dictionary<string, object>()
            {
                ["greeting"] = "hello",
                ["location"] = "world"
            });

            Assert.AreEqual("hello world", actual);
        }

        [TestMethod]
        public void FormatValueTuple()
        {
            string format = "{greeting} {location}";

            string actual = format.Interpolate(("greeting", "hello"), ("location", "world"));

            Assert.AreEqual("hello world", actual);
        }

        [TestMethod]
        public void FormatDictionaryArgString()
        {
            string format = "{greeting} {location}";

            string actual = format.Interpolate(new Dictionary<string, string>()
            {
                ["greeting"] = "hello",
                ["location"] = "world"
            });

            Assert.AreEqual("hello world", actual);
        }

        [TestMethod]
        public void FormatDictionaryCaseMismatchArg()
        {
            string format = "{greeting} {location}";

            string actual = format.Interpolate(new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
            {
                ["Greeting"] = "hello",
                ["Location"] = "world"
            });

            Assert.AreEqual("hello world", actual);
        }

        [TestMethod]
        public void FormatPluralize()
        {
            string format = "banana|bananas";

            string actual = format.Choice(2);

            Assert.AreEqual("bananas", actual);

            actual = format.Choice(1);

            Assert.AreEqual("banana", actual);

            actual = format.Choice(10);

            Assert.AreEqual("bananas", actual);
        }

        [TestMethod]
        public void FormatDictionaryPluralize()
        {
            string format = "banana is {color} at {date|hh}|bananas are {color} at {date|hh}";

            string actual = format.Interpolate(new Dictionary<string, object>
            {
                ["color"] = "yellow",
                ["date"] = DateTime.MinValue
            }, 2);

            Assert.AreEqual("bananas are yellow at 12", actual);

            actual = format.Interpolate(new Dictionary<string, object>
            {
                ["color"] = "yellow",
                ["date"] = DateTime.MinValue
            }, 1);

            Assert.AreEqual("banana is yellow at 12", actual);
        }

        [TestMethod]
        public void FormatDictionaryExplicitChoicePluralize()
        {
            string format = "no bananas|[1]one banana|[1,*]many bananas";

            string actual = format.Choice(0);
            Assert.AreEqual("no bananas", actual);

            actual = format.Choice(1);
            Assert.AreEqual("one banana", actual);

            format = "no bananas|one banana|[1,*]many bananas";

            actual = format.Choice(6);
            Assert.AreEqual("many bananas", actual);

            format = "[0]no bananas|[1]one banana|[1,*]many bananas";
            actual = format.Choice(0);
            Assert.AreEqual("no bananas", actual);

            actual = format.Choice(1);
            Assert.AreEqual("one banana", actual);

            actual = format.Choice(6);
            Assert.AreEqual("many bananas", actual);
        }

        [TestMethod]
        public void FormatDictionaryDateTimeArg()
        {
            string format = "{date|hh:ss tt}";

            string actual = format.Interpolate(new Dictionary<string, object>()
            {
                ["date"] = DateTime.MinValue,
            });

            Assert.AreEqual("12:00 AM", actual);
        }
    }
}
