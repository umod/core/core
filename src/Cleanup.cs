﻿using System;
using System.Collections.Generic;
using System.IO;
using uMod.Common;

namespace uMod
{
    public static class Cleanup
    {
        internal static HashSet<string> Files = new HashSet<string>();

        public static void Add(string file) => Files.Add(file);

        internal static void Run(ILogger logger)
        {
            if (Files != null)
            {
                foreach (string file in Files)
                {
                    try
                    {
                        if (File.Exists(file))
                        {
                            logger.Debug($"Cleanup file: {file}");
                            File.Delete(file);
                        }
                    }
                    catch (Exception)
                    {
                        logger.Warning($"Failed to cleanup file: {file}");
                    }
                }

                Files = null;
            }
        }
    }
}
