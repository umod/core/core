﻿namespace uMod.Apps
{
    internal interface IApp
    {
        IAppInfo Info { get; }

        bool Initialize();

        void Shutdown();
    }
}
