extern alias References;

using System.Collections.Generic;
#if DEBUG
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
#endif
using References::Newtonsoft.Json;
using uMod.Common;
using uMod.Common.WebSockets;
using uMod.IO;
using uMod.Pooling;
using uMod.WebSockets;

namespace uMod.Apps
{
    internal class WebSockets : BaseApp
    {
        internal static DynamicPool MessagePool = Pools.Default<WebSocketMessage>();

        /// <summary>
        /// ObjectStreamClient
        /// </summary>
        private ObjectStreamClient<WebSocketMessage> _client;

        /// <summary>
        /// Queue of pending messages
        /// </summary>
        private readonly Queue<WebSocketMessage> _messageQueue;

        /// <summary>
        /// Dictionary of connections keyed by id
        /// </summary>
        private readonly Dictionary<string, SocketConnection> _connections = new Dictionary<string, SocketConnection>();

        /// <summary>
        /// Last message id
        /// </summary>
        private int _lastMessageId = 1;

        /// <summary>
        /// Server module
        /// </summary>
        private Module _module;

        /// <summary>
        /// Create new WebSockets application
        /// </summary>
        /// <param name="info"></param>
        /// <param name="logger"></param>
        public WebSockets(Module module, IAppInfo info, ILogger logger) : base(info, logger)
        {
            _module = module;
            _messageQueue = new Queue<WebSocketMessage>();
            AutoShutdownEnabled = false;
        }

        /// <summary>
        /// Initialize web socket server
        /// </summary>
        /// <returns></returns>
        public override bool Initialize()
        {
            string[] args = {
                $"--logPath=\"{PathUtils.Escape(_module.LogDirectory)}\""
            };

            if (!StartProcess(args))
            {
                return false;
            }

            _client = new ObjectStreamClient<WebSocketMessage>(Process.StandardOutput.BaseStream, Process.StandardInput.BaseStream, null, null, MessagePool);

            _client.Message += OnMessage;
            _client.Error += OnError;
            _client.Start();

            return true;
        }

        /// <summary>
        /// Enqueue message instructing web socket server to send packet to specified connection
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="data"></param>
        public void EnqueuePacket(SocketConnection connection, string data)
        {
            EnqueuePacket(connection, data as object);
        }

        /// <summary>
        /// Enqueue packet
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="data"></param>
        public void EnqueuePacket(SocketConnection connection, object data)
        {
            EnqueuePacketMessage(connection, data, WebSocketMessageType.SocketPacket);
        }

        /// <summary>
        /// Enqueue multiple packets
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="data"></param>
        public void EnqueueAggregatePacket(SocketConnection connection, IEnumerable<object> data)
        {
            EnqueuePacketMessage(connection, data, WebSocketMessageType.AggregatePacket);
        }

        /// <summary>
        /// Enqueue multiple packets
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="data"></param>
        public void EnqueueAggregatePacket(SocketConnection connection, IEnumerable<Packet> data)
        {
            EnqueuePacketMessage(connection, data, WebSocketMessageType.AggregatePacket);
        }

        /// <summary>
        /// Enqueue multiple packets
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="data"></param>
        public void EnqueueAggregatePacket(SocketConnection connection, Packet[] data)
        {
            EnqueuePacketMessage(connection, data, WebSocketMessageType.AggregatePacket);
        }

        /// <summary>
        /// Enqueue multiple packets
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="data"></param>
        public void EnqueueAggregatePacket(SocketConnection connection, IEnumerable<string> data)
        {
            EnqueuePacketMessage(connection, data, WebSocketMessageType.AggregatePacket);
        }

        /// <summary>
        /// Enqueue message instructing web socket server to send packet to specified connection
        /// </summary>
        /// <param name="data"></param>
        private void EnqueuePacketMessage(SocketConnection connection, object data, WebSocketMessageType messageType = WebSocketMessageType.SocketPacket)
        {
            int id = _lastMessageId++;
            WebSocketMessage message = MessagePool.Get<WebSocketMessage>();
            message.Id = id;
            message.Data = data;
            message.ExtraData = connection.ConnectionName;
            message.ClientId = connection.Id;
            message.Type = messageType;

            EnqueueMessage(message);
        }

        /// <summary>
        /// Enqueue message instructing web socket server to send packet to specified connection
        /// </summary>
        /// <param name="data"></param>
        public void EnqueueBroadcast(string serverName, object data)
        {
            EnqueueBroadcastMessage(serverName, data, WebSocketMessageType.SocketPacket);
        }

        /// <summary>
        /// Enqueue message instructing web socket server to send packet to specified connection
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="data"></param>
        private void EnqueueBroadcastMessage(string serverName, object data, WebSocketMessageType messageType = WebSocketMessageType.SocketPacket)
        {
            int id = _lastMessageId++;
            WebSocketMessage message = MessagePool.Get<WebSocketMessage>();
            message.Id = id;
            message.Data = data;
            message.ExtraData = serverName;
            message.Type = messageType;

            EnqueueMessage(message);
        }

        /// <summary>
        /// Enqueue multiple messages
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="data"></param>
        public void EnqueueAggregateBroadcast(string serverName, IEnumerable<object> data)
        {
            EnqueueBroadcastMessage(serverName, data, WebSocketMessageType.AggregatePacket);
        }

        /// <summary>
        /// Enqueue multiple packets
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="data"></param>
        public void EnqueueAggregateBroadcast(string serverName, IEnumerable<Packet> data)
        {
            EnqueueBroadcastMessage(serverName, data, WebSocketMessageType.AggregatePacket);
        }

        /// <summary>
        /// Enqueue multiple packets
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="data"></param>
        public void EnqueueAggregateBroadcast(string serverName, Packet[] data)
        {
            EnqueueBroadcastMessage(serverName, data, WebSocketMessageType.AggregatePacket);
        }

        /// <summary>
        /// Enqueue multiple messages
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="data"></param>
        public void EnqueueAggregateBroadcast(string serverName, IEnumerable<string> data)
        {
            EnqueueBroadcastMessage(serverName, data, WebSocketMessageType.AggregatePacket);
        }

        /// <summary>
        /// Enqueue message instructing web socket server to close specified socket
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="code"></param>
        public void EnqueueSocketClose(SocketConnection connection, int code = 1000)
        {
            int id = _lastMessageId++;
            WebSocketMessage message = MessagePool.Get<WebSocketMessage>();
            message.Id = id;
            message.Data = connection.ConnectionName;
            message.ClientId = connection.Id;
            message.ExtraData = code;
            message.Type = WebSocketMessageType.SocketClose;

            EnqueueMessage(message);
        }

        /// <summary>
        /// Enqueue message instructing web socket server to close specified server
        /// </summary>
        /// <param name="name"></param>
        public void EnqueueServerClose(string name)
        {
            int id = _lastMessageId++;
            WebSocketMessage message = MessagePool.Get<WebSocketMessage>();
            message.Id = id;
            message.Data = name;
            message.ExtraData = null;
            message.ClientId = null;
            message.Type = WebSocketMessageType.ServerClose;

            EnqueueMessage(message);
        }

        /// <summary>
        /// Enqueue message instructing web socket server to create a new server with the specified connection info
        /// </summary>
        /// <param name="connection"></param>
        public void EnqueueServerStart(ConnectionInfo connection)
        {
#if !DEBUG
            if (connection.Name == "rcon" && string.IsNullOrEmpty(connection.Path))
            {
                Logger.Warning("Could not start \"rcon\" websocket server, no password specified");
                return;
            }
#endif

            int id = _lastMessageId++;
            WebSocketMessage message = MessagePool.Get<WebSocketMessage>();
            message.Id = id;
            message.Data = connection;
            message.ExtraData = null;
            message.ClientId = null;
            message.Type = WebSocketMessageType.ServerStart;

            EnqueueMessage(message);
        }

        /// <summary>
        /// Push message into object stream
        /// </summary>
        /// <param name="message"></param>
        private void EnqueueMessage(WebSocketMessage message)
        {
            /*
#if DEBUG
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(Path.Combine(_module.AppDirectory, $"socket-{message.Id}.dat"), FileMode.Append, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, message);
            stream.Close();
#endif
            */

            Interface.uMod.Dispatcher.Dispatch(delegate ()
            {
                if (!CheckProcess())
                {
                    _messageQueue.Enqueue(message);
                    return true;
                }

                if (Ready)
                {
                    _client.PushMessage(message);
                }
                else
                {
                    _messageQueue.Enqueue(message);
                }

                return true;
            }, ref scopeLock);
        }

        /// <summary>
        /// ObjectStreamClient message handler
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="message"></param>
        private void OnMessage(ObjectStreamConnection<WebSocketMessage, WebSocketMessage> connection, WebSocketMessage message)
        {
            if (message == null)
            {
                Fail(_module.Strings.Apps.Disconnected);
                return;
            }

            if (_lastMessageId > 999)
            {
                _lastMessageId = 1;
            }

            SocketConnection socket;
            string serverName;

            switch (message.Type)
            {
                case WebSocketMessageType.SocketPacket:
                    if (message.ExtraData is string && _connections.TryGetValue(message.ClientId, out socket) && message.Data is Packet packet)
                    {
                        serverName = message.ExtraData.ToString();
                        _module.CallHook($"OnSocketMessageReceived", serverName, socket, packet);
                    }

                    break;

                case WebSocketMessageType.SocketStart:
                    if (message.Data is string)
                    {
                        socket = JsonConvert.DeserializeObject<SocketConnection>(message.Data.ToString());
                        _connections.Add(socket.Id.ToString(), socket);
                        _module.CallHook("OnSocketConnected", socket);
                    }
                    break;

                case WebSocketMessageType.SocketClose:
                    if (_connections.TryGetValue(message.ClientId, out socket))
                    {
                        _module.CallHook("OnSocketDisconnected", socket);
                        _connections.Remove(message.ClientId);
                    }
                    break;

                case WebSocketMessageType.SocketStatus:
                    if (_connections.TryGetValue(message.ClientId, out socket))
                    {
                        WebSocketConnectionState newState = (WebSocketConnectionState)message.Data;
                        if (newState == WebSocketConnectionState.Open)
                        {
                            _module.CallHook("OnSocketConnected", socket);
                        }
                        else if (newState == WebSocketConnectionState.Closed)
                        {
                            _module.CallHook("OnSocketDisconnected", socket);
                            _connections.Remove(message.ClientId);
                        }
                    }
                    break;

                case WebSocketMessageType.ServerStatus:
                    ConnectionState state = (ConnectionState)message.Data;
                    string stateServerName = message.ExtraData.ToString();
                    _module.CallHook("OnSocketServerStateChanged", stateServerName, state);
                    if (message.ExtraData2 is string responseMessage)
                    {
                        _module.LogInfo(responseMessage);
                    }
                    break;

                case WebSocketMessageType.Error:
                    Logger.Error(message.Data.ToString());
                    break;

                case WebSocketMessageType.Ready:
                    connection.PushMessage(message);
                    if (!Ready)
                    {
                        Activate();
                        while (_messageQueue.Count > 0)
                        {
                            WebSocketMessage webMessage = _messageQueue.Dequeue();

                            try
                            {
                                connection.PushMessage(webMessage);
                            }
                            finally
                            {
                                MessagePool.Free(webMessage);
                            }
                        }
                    }
                    break;
            }

            CheckIdleTimer();
        }

        /// <summary>
        /// Invoked when web socket server fails
        /// </summary>
        /// <param name="reason"></param>
        internal override void OnFailed(string reason)
        {
            reason = reason.Interpolate(("version", Version), ("app", GetType().Name));
            Logger.Error($"WebSocket server failed: {reason}");
        }

        /// <summary>
        /// Shutdown web server application
        /// </summary>
        public override void Shutdown()
        {
            EndProcess();
            if (_client == null)
            {
                return;
            }

            _client.Message -= OnMessage;
            _client.Error -= OnError;
            _client.PushMessage(new WebSocketMessage { Type = WebSocketMessageType.Exit });
            _client.Stop();
            Ready = false;
            _client = null;
        }
    }
}
