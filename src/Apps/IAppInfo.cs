﻿namespace uMod.Apps
{
    public interface IAppInfo
    {
        string Name { get; }
        string FileName { get; }
        string DistributablePath { get; }
        string ApplicationPath { get; }
    }
}
