using System.Collections.Generic;
using System.Linq;
#if DEBUG
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
#endif
using System.Text;
using uMod.Common;
using uMod.Common.Compiler;
using uMod.IO;
using uMod.Pooling;
using uMod.Plugins.Compiler;

namespace uMod.Apps
{
    internal class Compiler : BaseApp
    {
        internal static DynamicPool CompilationPool = Pools.Default<Compilation>();
        internal static DynamicPool MessagePool = Pools.Default<CompilationMessage>();
        /// <summary>
        /// The compiler stream client
        /// </summary>
        private ObjectStreamClient<CompilationMessage> _client;

        /// <summary>
        /// List of active compile requests
        /// </summary>
        private readonly List<CompilerRequest> _activeCompileRequests = new List<CompilerRequest>();

        /// <summary>
        /// List of finished compile requests
        /// </summary>
        private readonly Queue<CompilerRequest> _finishedRequests = new Queue<CompilerRequest>();

        /// <summary>
        /// Queue of messages to be sent to the compiler
        /// </summary>
        private readonly Queue<CompilationMessage> _messageQueue;

        /// <summary>
        /// Last message id
        /// </summary>
        private volatile int _lastId;

        /// <summary>
        /// Request lock
        /// </summary>
        private readonly object _requestLock = new object();

        /// <summary>
        /// List of blacklist namespaces
        /// </summary>
        private static IEnumerable<string> BlacklistedNamespaces => new[]
        {
            "Harmony",
            "Microsoft.CSharp.RuntimeBinder",
            "Mono.CSharp",
            "Mono.Cecil",
            "ServerFileSystem",
            "System.Activator",
            "System.AppDomain",
            "System.AppDomainManager",
            "System.Diagnostics",
            "System.Dynamic",
            "System.Environment",
            "System.IO",
            "System.Linq.Expressions",
            "System.Net",
            "System.Reflection",
            "System.Runtime.CompilerServices",
            "System.Runtime.InteropServices",
            "System.Runtime.Remoting",
            "System.Security",
            "System.Threading",
            "System.Timers",
            "System.Xml",
            "UnityEngine.WWW"
        };

        /// <summary>
        /// List of whitelisted namespaces
        /// </summary>
        private static IEnumerable<string> WhitelistedNamespaces => new[]
        {
            "ProtoBuf",
            "System.Collections",
            "System.Diagnostics.Stopwatch",
            "System.IO.BinaryReader",
            "System.IO.BinaryWriter",
            "System.IO.MemoryStream",
            "System.IO.Stream",
            "System.Net.Dns",
            "System.Net.Dns.GetHostEntry",
            "System.Net.IPAddress",
            "System.Net.IPEndPoint",
            "System.Net.NetworkInformation",
            "System.Net.Sockets.SocketFlags",
            "System.Security.Cryptography",
            "System.Text",
            "System.Threading.Interlocked",
            "System.Threading.Monitor",
            "System.Runtime.CompilerServices.RuntimeHelpers",
            "System.Runtime.CompilerServices.DefaultInterpolatedStringHandler",
            "System.Environment.NewLine"
        };

        /// <summary>
        /// Cached array of whitelisted namespaces
        /// </summary>
        private string[] _whitelistedNamespaces = null;

        /// <summary>
        /// Cached array of blacklisted namespaces
        /// </summary>
        private string[] _blacklistedNamespaces = null;

        /// <summary>
        /// Create a new instance of the Compiler class
        /// </summary>
        /// <param name="logger"></param>
        public Compiler(IAppInfo appInfo, ILogger logger) : base(appInfo, logger)
        {
            _messageQueue = new Queue<CompilationMessage>();
        }

        /// <summary>
        /// Compile the specified compiler request
        /// </summary>
        /// <param name="request"></param>
        internal void Compile(CompilerRequest request)
        {
            int id = _lastId++;

            request.OnQueued(id);

            _activeCompileRequests.Add(request);

            EnqueueCompilation(request);
        }

        /// <summary>
        /// Initialize compiler application
        /// </summary>
        /// <returns></returns>
        public override bool Initialize()
        {
#if !DEBUG
            string[] args = {
                $"--log_dir=\"{PathUtils.Escape(Interface.uMod.LogDirectory)}\"",
                $"--reference=\"{PathUtils.Escape(Interface.uMod.ExtensionDirectory)}\""
            }; // TODO: Handle exception if log path is not accessible?
#else
            string[] args = {
                $"--log_dir=\"{PathUtils.Escape(Interface.uMod.LogDirectory)}\""
            };
#endif

            if (!StartProcess(args))
            {
                return false;
            }

            _client = new ObjectStreamClient<CompilationMessage>(Process.StandardOutput.BaseStream, Process.StandardInput.BaseStream, null, null, MessagePool);
            _client.Message += OnMessage;
            _client.Error += OnError;
            _client.Start();
            return true;
        }

        /// <summary>
        /// Shutdown compiler application
        /// </summary>
        public override void Shutdown()
        {
            EndProcess();

            if (_client == null)
            {
                return;
            }

            _client.Message -= OnMessage;
            _client.Error -= OnError;
            _client.PushMessage(new CompilationMessage { Type = CompilationMessageType.Exit });
            _client.Stop();
            _client = null;
        }

        /// <summary>
        /// Gets array of whitelisted namespaces
        /// </summary>
        /// <returns></returns>
        private string[] GetWhitelistedNamespaces()
        {
            if (_whitelistedNamespaces != null)
            {
                return _whitelistedNamespaces;
            }

            List<string> whitelistedNamespaces = new List<string>(WhitelistedNamespaces);

            foreach (IExtension extension in Interface.uMod.Extensions.All)
            {
                if (extension.WhitelistedNamespaces == null)
                {
                    continue;
                }

                foreach (string whitelistedNamespace in extension.WhitelistedNamespaces)
                {
                    if (!whitelistedNamespaces.Contains(whitelistedNamespace))
                    {
                        whitelistedNamespaces.Add(whitelistedNamespace);
                    }
                }
            }

            return _whitelistedNamespaces = whitelistedNamespaces.ToArray();
        }

        /// <summary>
        /// Gets array of blacklisted namespaces
        /// </summary>
        /// <returns></returns>
        private string[] GetBlacklistedNamespaces()
        {
            if (_blacklistedNamespaces != null)
            {
                return _blacklistedNamespaces;
            }

            List<string> blacklistedNamespaces = new List<string>(BlacklistedNamespaces);

            foreach (IExtension extension in Interface.uMod.Extensions.All)
            {
                if (extension.BlacklistedNamespaces == null)
                {
                    continue;
                }
                foreach (string blacklistedNamespace in extension.BlacklistedNamespaces)
                {
                    if (!blacklistedNamespaces.Contains(blacklistedNamespace))
                    {
                        blacklistedNamespaces.Add(blacklistedNamespace);
                    }
                }
            }

            return _blacklistedNamespaces = blacklistedNamespaces.ToArray();
        }

        /// <summary>
        /// Delete cached whitelist and blacklist, rebuilt on next compilation
        /// </summary>
        internal void RefreshSandbox()
        {
            _whitelistedNamespaces = null;
            _blacklistedNamespaces = null;
        }

        /// <summary>
        /// Enqueue compilation to be sent to the compiler
        /// </summary>
        /// <param name="request"></param>
        private void EnqueueCompilation(CompilerRequest request)
        {
            if (request.Plugins.Count < 1)
            {
                Logger.Debug("EnqueueCompilation called for an empty compilation"); //TODO: Localization
                return;
            }



            request.OnSentToCompiler();

            //Logger.Debug($"Compiling : {string.Join(",", request.Plugins.Select(x => x.Name).ToArray())}");

            //Pass .cs files
            List<CompilationFile> scriptFiles = request.Plugins.Select(plugin => new CompilationFile($"{plugin.FileName}.cs", plugin.ScriptBytes, plugin.Directory, Encoding.UTF8)).ToList();

            //Pass referenced DLLs
            List<Common.Compiler.AssemblyReference> dllFiles = Pools.GetList<Common.Compiler.AssemblyReference>();

            Compilation data = CompilationPool.Get<Compilation>();

            try
            {
                Common.Compiler.AssemblyReference[] pluginReferenceAssemblies = request.Plugins
                    .SelectMany(x => x.ReferencedAssemblies.Values)
                    .GroupBy(x => x.FullPath).Select(x => x.FirstOrDefault()).Distinct()
                    .Select(dll => dll.Reference)
                    .OrderBy(x => x.Name).ToArray();

                if (pluginReferenceAssemblies.Length > 0)
                {
                    dllFiles.AddRange(pluginReferenceAssemblies);
                }

                //Interface.uMod.LogDebug("Compiling files: {0}", sourceFiles.Select(f => f.Name).ToSentence());
                data.AssemblyName = request.DLLName;
                data.SourceFiles = scriptFiles.ToArray();
                data.AssemblyReferences = dllFiles.ToArray();
                data.Sandbox = Interface.uMod.ForceSandbox || Interface.uMod.Plugins.Configuration.Security.Sandbox;
                data.WhitelistedNamespaces = GetWhitelistedNamespaces();
                data.BlacklistedNamespaces = GetBlacklistedNamespaces();
            }
            finally
            {
                Pools.FreeList(ref dllFiles);
            }

            CompilationMessage message = MessagePool.Get<CompilationMessage>();
            message.Id = request.RequestId;
            message.Data = data;
            message.Type = CompilationMessageType.Compile;

            EnqueueMessage(message);
        }

        private void EnqueueMessage(CompilationMessage message)
        {
#if DEBUG
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(Path.Combine(Interface.uMod.AppDirectory, $"compiler-{message.Id}.dat"), FileMode.Append, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, message);
            stream.Close();
#endif

            Interface.uMod.Dispatcher.Dispatch(delegate ()
            {
                if (!CheckProcess())
                {
                    _messageQueue.Enqueue(message);
                    return true;
                }

                if (Ready)
                {
                    try
                    {
                        _client.PushMessage(message);
                    }
                    finally
                    {
                        /*
                        CompilationPool.Free(data);
                        */
                        MessagePool.Free(message);
                    }
                }
                else
                {
                    _messageQueue.Enqueue(message);
                }
                return true;
            });
        }

        /// <summary>
        /// Process a message received from the compiler
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="message"></param>
        private void OnMessage(ObjectStreamConnection<CompilationMessage, CompilationMessage> connection, CompilationMessage message)
        {
            if (message == null)
            {
                Fail(Interface.uMod.Strings.Apps.Disconnected);
                return;
            }

            switch (message.Type)
            {
                case CompilationMessageType.Assembly:
                case CompilationMessageType.Error:
                    CompilerRequest request = _activeCompileRequests.FirstOrDefault(x => x.RequestId == message.Id);

                    if (request == null)
                    {
                        Logger.Warning(Interface.uMod.Strings.Compiler.CompiledUnknownAssembly);
                        return;
                    }

                    request.OnResponseFromCompiler(message);

                    _activeCompileRequests.Remove(request);

                    lock (_requestLock)
                    {
                        _finishedRequests.Enqueue(request);
                    }

                    break;

                case CompilationMessageType.Ready:
                    connection.PushMessage(message);
                    if (!Ready)
                    {
                        Activate();
                        while (_messageQueue.Count > 0)
                        {
                            CompilationMessage compilationMessage = _messageQueue.Dequeue();
                            try
                            {
                                connection.PushMessage(compilationMessage);
                            }
                            finally
                            {
                                /*
                                if (compilationMessage.Data is Compilation data)
                                {
                                    CompilationPool.Free(data);
                                }
                                */
                                MessagePool.Free(compilationMessage);

                            }
                        }
                    }
                    break;
            }

            CheckIdleTimer();
        }

        /// <summary>
        /// Attempt to dequeue finished requests
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool TryDequeueRequest(out CompilerRequest request)
        {
            lock (_requestLock)
            {
                if (_finishedRequests.Count == 0)
                {
                    request = null;
                    return false;
                }
                request = _finishedRequests.Dequeue();
            }

            return true;
        }

        /// <summary>
        /// Handles compilation failures in active compile requests
        /// </summary>
        /// <param name="reason"></param>
        internal override void OnFailed(string reason)
        {
            reason = reason.Interpolate(("version", Version), ("app", GetType().Name));
            foreach (CompilerRequest request in _activeCompileRequests)
            {
                foreach (CompilePluginRequest plugin in request.Plugins)
                {
                    plugin.EncounteredError(reason);
                }

                request.OnCompilerFailed(reason);
            }
        }
    }
}
