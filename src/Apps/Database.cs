extern alias References;

using System;
using System.Collections.Generic;

#if DEBUG
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
#endif

using uMod.Collections;
using uMod.Common;
using uMod.Common.Database;
using uMod.Database;
using uMod.Exceptions;
using uMod.IO;
using uMod.Pooling;
using Query = uMod.Common.Database.Query;

namespace uMod.Apps
{
    internal class Database : BaseApp
    {
        internal static DynamicPool MessagePool = Pools.Default<DatabaseMessage>();

        /// <summary>
        /// ObjectStreamClient
        /// </summary>
        private ObjectStreamClient<DatabaseMessage> _client;

        /// <summary>
        /// List of queries being executed
        /// </summary>
        private readonly Hash<int, object> _callbacks;

        /// <summary>
        /// Queue of pending messages
        /// </summary>
        private readonly Queue<DatabaseMessage> _messageQueue;

        /// <summary>
        /// Last message id
        /// </summary>
        private int _lastMessageId = 1;

        /// <summary>
        /// Number of activity checks where connections are inactive
        /// </summary>
        private int _inactiveCount = 0;

        /// <summary>
        /// Create new Database client
        /// </summary>
        /// <param name="info"></param>
        /// <param name="logger"></param>
        public Database(IAppInfo info, ILogger logger) : base(info, logger)
        {
            _callbacks = new Hash<int, object>();
            _messageQueue = new Queue<DatabaseMessage>();
        }

        /// <summary>
        /// Initialize database client
        /// </summary>
        /// <returns></returns>
        public override bool Initialize()
        {
            string[] args = {
                $"--logPath=\"{PathUtils.Escape(Interface.uMod.LogDirectory)}\"",
                $"--dataPath=\"{PathUtils.Escape(Interface.uMod.DataDirectory)}\""
            };

            // Copy uMod.Common and uMod.Promise from core to database
            /*
            string directory = Path.GetDirectoryName(Info.ApplicationPath);
            string promiseName = Path.GetFileName(typeof(ISettlebalePromise).Assembly.Location);
            string destPromisePath = Path.Combine(directory, promiseName);
            string commonName = Path.GetFileName(typeof(IPlugin).Assembly.Location);
            string destCommonPath = Path.Combine(directory, commonName);

            if (File.Exists(destPromisePath))
            {
                File.Delete(destPromisePath);
            }

            if (File.Exists(destCommonPath))
            {
                File.Delete(destCommonPath);
            }

            File.Copy(typeof(ISettlebalePromise).Assembly.Location, Path.Combine(directory, promiseName));
            File.Copy(typeof(IPlugin).Assembly.Location, Path.Combine(directory, commonName));*/

            if (!StartProcess(args))
            {
                return false;
            }

            _client = new ObjectStreamClient<DatabaseMessage>(Process.StandardOutput.BaseStream, Process.StandardInput.BaseStream, null, null, MessagePool);
            _client.Message += OnMessage;
            _client.Error += OnError;
            _client.Start();

            return true;
        }

        /// <summary>
        /// Checks if any connections are open and, if not, close application after 2 tries
        /// </summary>
        protected override void AutoShutdown()
        {
            EnqueueActiveCheck(delegate (int connectionCount)
            {
                if (connectionCount == 0)
                {
                    if (_inactiveCount == 0)
                    {
                        _inactiveCount++;
                    }
                    else
                    {
                        _inactiveCount = 0;
                        Interface.uMod.NextTick(() =>
                        {
                            base.AutoShutdown();
                        });
                    }
                }
                else
                {
                    _inactiveCount = 0;
                }
            });
        }

        /// <summary>
        /// Enqueue database query
        /// </summary>
        /// <param name="data"></param>
        /// <param name="callback"></param>
        internal void EnqueueQuery(Query data, ISettleablePromise<object> callback)
        {
            int id = _lastMessageId++;
            _callbacks.Add(id, callback);
            DatabaseMessage message = MessagePool.Get<DatabaseMessage>();
            message.Id = id;
            message.Data = data;
            message.ExtraData = null;
            message.Type = DatabaseMessageType.Query;
            EnqueueMessage(message);
        }

        /// <summary>
        /// Enqueue database query
        /// </summary>
        /// <param name="data"></param>
        /// <param name="callback"></param>
        internal void EnqueueQuery(Query data, Action<bool, string> callback)
        {
            int id = _lastMessageId++;
            _callbacks.Add(id, callback);
            DatabaseMessage message = MessagePool.Get<DatabaseMessage>();
            message.Id = id;
            message.Data = data;
            message.ExtraData = null;
            message.Type = DatabaseMessageType.Query;
            EnqueueMessage(message);
        }

        /// <summary>
        /// Enqueue database connect message
        /// </summary>
        /// <param name="connectionInfo"></param>
        /// <param name="callback"></param>
        internal void EnqueueConnect(ConnectionInfo connectionInfo, Action<ConnectionState, string> callback)
        {
            int id = _lastMessageId++;
            _callbacks.Add(id, callback);
            DatabaseMessage message = MessagePool.Get<DatabaseMessage>();
            message.Id = id;
            message.Data = connectionInfo;
            message.ExtraData = null;
            message.Type = DatabaseMessageType.Connect;
            EnqueueMessage(message);
        }

        /// <summary>
        /// Enqueue database disconnect message
        /// </summary>
        /// <param name="connectionInfo"></param>
        /// <param name="callback"></param>
        internal void EnqueueDisconnect(ConnectionInfo connectionInfo, bool forever, Action<ConnectionState, string> callback = null)
        {
            int id = _lastMessageId++;
            if (callback != null)
            {
                _callbacks.Add(id, callback);
            }

            DatabaseMessage message = MessagePool.Get<DatabaseMessage>();
            message.Id = id;
            message.Data = connectionInfo.Name;
            message.ExtraData = forever;
            message.Type = DatabaseMessageType.Disconnect;
            EnqueueMessage(message);
        }

        /// <summary>
        /// Enqueue database status check
        /// </summary>
        /// <param name="connectionInfo"></param>
        /// <param name="callback"></param>
        internal void EnqueueStatusCheck(ConnectionInfo connectionInfo, Action<ConnectionState> callback)
        {
            int id = _lastMessageId++;
            _callbacks.Add(id, callback);
            DatabaseMessage message = MessagePool.Get<DatabaseMessage>();
            message.Id = id;
            message.Data = connectionInfo.Name;
            message.ExtraData = null;
            message.Type = DatabaseMessageType.Status;
            EnqueueMessage(message);
        }

        /// <summary>
        /// Enqueue database activity check
        /// </summary>
        /// <param name="callback"></param>
        internal void EnqueueActiveCheck(Action<int> callback)
        {
            int id = _lastMessageId++;
            _callbacks.Add(id, callback);
            DatabaseMessage message = MessagePool.Get<DatabaseMessage>();
            message.Id = id;
            message.Type = DatabaseMessageType.ConnectionCount;
            EnqueueMessage(message);
        }

        /// <summary>
        /// Enqueue database transaction commit message
        /// </summary>
        /// <param name="transactionName"></param>
        /// <param name="connectionInfo"></param>
        /// <param name="callback"></param>
        internal void EnqueueCommit(string transactionName, ConnectionInfo connectionInfo, Action<bool, string> callback)
        {
            int id = _lastMessageId++;
            _callbacks.Add(id, callback);
            DatabaseMessage message = MessagePool.Get<DatabaseMessage>();
            message.Id = id;
            message.Data = transactionName;
            message.ExtraData = connectionInfo.Name;
            message.Type = DatabaseMessageType.Commit;
            EnqueueMessage(message);
        }

        /// <summary>
        /// Enqueue database client message
        /// </summary>
        /// <param name="message"></param>
        private void EnqueueMessage(DatabaseMessage message)
        {
#if DEBUG
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(Path.Combine(Interface.uMod.AppDirectory, $"database-{message.Id}.dat"), FileMode.Append, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, message);
            stream.Close();
#endif

            Interface.uMod.Dispatcher.Dispatch(delegate ()
            {
                if (!CheckProcess())
                {
                    _messageQueue.Enqueue(message);
                    return true;
                }

                if (Ready)
                {
                    _client.PushMessage(message);
                }
                else
                {
                    _messageQueue.Enqueue(message);
                }

                return true;
            }, ref scopeLock);
        }

        /// <summary>
        /// ObjectStreamClient message handler
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="message"></param>
        private void OnMessage(ObjectStreamConnection<DatabaseMessage, DatabaseMessage> connection, DatabaseMessage message)
        {
            if (message == null)
            {
                Fail(Interface.uMod.Strings.Apps.Disconnected);
                return;
            }

            if (_lastMessageId > 999999)
            {
                _lastMessageId = 1;
            }

            object callback = _callbacks[message.Id];
            if (callback == null)
            {
                if (message.Type != DatabaseMessageType.Ready && message.Type != DatabaseMessageType.Disconnect)
                {
                    Logger.Warning(Interface.uMod.Strings.Database.UnknownQuery.Interpolate(("type", message.Type)));
                }
            }
            else
            {
                _callbacks.Remove(message.Id);
            }

            switch (message.Type)
            {
                case DatabaseMessageType.Commit:
                    if (callback is Action<bool, string> commitAction)
                    {
                        commitAction.Invoke(true, message.Data?.ToString());
                    }
                    break;

                case DatabaseMessageType.Result:
                    if (callback is ISettleablePromise<object> resultCallback)
                    {
                        resultCallback.ReportResolved(message.Data);
                    }
                    else if (callback is Action<bool, string> resultActionCallback)
                    {
                        resultActionCallback.Invoke(true, string.Empty);
                    }
                    break;

                case DatabaseMessageType.Status:
                    if (callback is Action<ConnectionState> statusAction)
                    {
                        statusAction.Invoke(message.Data is string ? ConnectionState.Closed : (ConnectionState)message.Data);
                    }
                    break;

                case DatabaseMessageType.ConnectionCount:
                    if (callback is Action<int> countAction)
                    {
                        countAction.Invoke((int)message.Data);
                    }
                    break;

                case DatabaseMessageType.Connect:
                case DatabaseMessageType.Disconnect:
                    if (callback is Action<ConnectionState, string> connectionAction)
                    {
                        connectionAction.Invoke((ConnectionState)message.ExtraData, message.Data?.ToString());
                    }
                    break;

                case DatabaseMessageType.Error:
                    if (callback is ISettleablePromise<object> errorCallback)
                    {
                        errorCallback.ReportRejected(new DatabaseException(message.Data.ToString()));
                    }
                    else if (callback is Action<ConnectionState, string> errorAction)
                    {
                        errorAction.Invoke((ConnectionState)message.ExtraData, message.Data?.ToString());
                    }
                    else if (callback is Action<ConnectionState> statusErrorAction)
                    {
                        statusErrorAction.Invoke(message.Data is string ? ConnectionState.Closed : (ConnectionState)message.Data);
                    }
                    else if (callback is Action<string> messageAction)
                    {
                        messageAction.Invoke(message.Data?.ToString());
                    }
                    else if (callback is Action<bool, string> transactionErrorAction)
                    {
                        transactionErrorAction.Invoke(false, message.Data?.ToString());
                    }
                    else
                    {
                        Interface.uMod.LogError($"Unhandled database error: {message.Data}");
                    }

                    break;

                case DatabaseMessageType.Rollback:
                    if (callback is Action<bool, string> transactionRollbackAction)
                    {
                        transactionRollbackAction.Invoke(false, message.Data?.ToString());
                    }

                    break;

                case DatabaseMessageType.Ready:
                    connection.PushMessage(message);
                    if (!Ready)
                    {
                        Activate();
                        while (_messageQueue.Count > 0)
                        {
                            DatabaseMessage databaseMessage = _messageQueue.Dequeue();

                            connection.PushMessage(databaseMessage);
                        }
                    }
                    break;
            }

            CheckIdleTimer();
        }

        /// <summary>
        /// Invoked when database client fails
        /// </summary>
        /// <param name="reason"></param>
        internal override void OnFailed(string reason)
        {
            reason = reason.Interpolate(("version", Version), ("app", GetType().Name));
            DatabaseException exception = new DatabaseException(reason);
            foreach (object callback in _callbacks.Values)
            {
                if (callback is ISettleablePromise<object> queryCallback)
                {
                    queryCallback.ReportRejected(exception);
                }
            }

            Logger.Error($"Database failure: {reason}");

            _callbacks.Clear();
        }

        /// <summary>
        /// Shutdown database client application
        /// </summary>
        public override void Shutdown()
        {
            EndProcess();
            if (_client == null)
            {
                return;
            }

            _client.Message -= OnMessage;
            _client.Error -= OnError;
            _client.PushMessage(new DatabaseMessage { Type = DatabaseMessageType.Exit });
            _client.Stop();
            _client = null;
        }
    }
}
