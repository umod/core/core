﻿using System.IO;

namespace uMod.Apps
{
    /// <summary>
    /// Represents an standalone application
    /// </summary>
    public abstract class AppInfo : IAppInfo
    {
        /// <summary>
        /// Gets the application name
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the application file
        /// </summary>
        public abstract string FileName { get; }

        /// <summary>
        /// Gets the directory of the distributable
        /// </summary>
        public abstract string DistributablePath { get; }

        /// <summary>
        /// Full application path
        /// </summary>
        private string _applicationPath = null;

        /// <summary>
        /// Gets the full application path
        /// </summary>
        public string ApplicationPath
        {
            get
            {
                if (string.IsNullOrEmpty(_applicationPath))
                {
                    _applicationPath = Path.Combine(Interface.uMod.AppDirectory, DistributablePath);
                }

                return _applicationPath;
            }
        }

        /// <summary>
        /// Create new AppInfo
        /// </summary>
        protected AppInfo()
        {
            string name = GetType().Name;
            if (name.EndsWith("Info"))
            {
                name = name.Substring(0, name.Length - 4);
            }

            Name = name;
        }
    }
}
