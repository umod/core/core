using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using uMod.Common;

namespace uMod.Apps
{
    public sealed class CompilerInfo : AppInfo
    {
        private string _distributablePath;

        public override string FileName { get; } = "uMod.Compiler.dll";

        public override string DistributablePath
        {
            get
            {
                if (string.IsNullOrEmpty(_distributablePath))
                {
                    _distributablePath = Path.Combine("compiler", FileName);
                }

                return _distributablePath;
            }
        }
    }

    public sealed class DatabaseInfo : AppInfo
    {
        private string _distributablePath;

        public override string FileName { get; } = "uMod.Database.dll";

        public override string DistributablePath
        {
            get
            {
                if (string.IsNullOrEmpty(_distributablePath))
                {
                    _distributablePath = Path.Combine("database", FileName);
                }

                return _distributablePath;
            }
        }
    }

    public sealed class WebClientInfo : AppInfo
    {
        private string _distributablePath;

        public override string FileName { get; } = "uMod.WebClient.dll";

        public override string DistributablePath
        {
            get
            {
                if (string.IsNullOrEmpty(_distributablePath))
                {
                    _distributablePath = Path.Combine("webclient", FileName);
                }

                return _distributablePath;
            }
        }
    }

    public sealed class WebSocketsInfo : AppInfo
    {
        private string _distributablePath;

        public override string FileName { get; } = "uMod.WebSockets.dll";

        public override string DistributablePath
        {
            get
            {
                if (string.IsNullOrEmpty(_distributablePath))
                {
                    _distributablePath = Path.Combine("websockets", FileName);
                }

                return _distributablePath;
            }
        }
    }

    public class AppManager
    {
        private readonly ILogger _logger;
        private readonly Dictionary<string, IAppInfo> _appInfo = new Dictionary<string, IAppInfo>();
        internal bool AgentDisabled { get; }

        /// <summary>
        /// Determine Agent path
        /// </summary>
        public string AgentPath
        {
            get
            {
                if (!Interface.uMod.CommandLine.HasVariable("umod.agent"))
                {
                    return "umod";
                }

                Interface.uMod.CommandLine.GetArgument("umod.agent", out _, out string commandLinePath);
                if (!string.IsNullOrEmpty(commandLinePath))
                {
                    return File.Exists(commandLinePath) ? commandLinePath : "umod";
                }

                return "umod";
            }
        }

        /// <summary>
        /// Get an AppInfo from the manager
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IAppInfo this[string key]
        {
            get => _appInfo?[key];
            set
            {
                if (_appInfo != null)
                {
                    _appInfo[key] = value;
                }
            }
        }

        /// <summary>
        /// Create AppManager
        /// </summary>
        public AppManager(ILogger logger, bool agentDisabled = false, IInitializationInfo info = null)
        {
            AgentDisabled = agentDisabled;
            _logger = logger;
            if (info?.Applications == null)
            {
                _appInfo.Add("Compiler", new CompilerInfo());
                _appInfo.Add("Database", new DatabaseInfo());
                _appInfo.Add("WebClient", new WebClientInfo());
                _appInfo.Add("WebSockets", new WebSocketsInfo());
            }
            else
            {
                foreach (string appName in info.Applications)
                {
                    switch (appName)
                    {
                        case "Compiler":
                            _appInfo.Add("Compiler", new CompilerInfo());
                            break;

                        case "Database":
                            _appInfo.Add("Database", new DatabaseInfo());
                            break;

                        case "WebClient":
                            _appInfo.Add("WebClient", new WebClientInfo());
                            break;

                        case "WebSockets":
                            _appInfo.Add("WebSockets", new WebSocketsInfo());
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Determine if standalone apps are installed
        /// </summary>
        public bool AppsInstalled
        {
            get
            {
                //TODO: temporarily allow WebSockets server to be totally optional
                return _appInfo.Where(x => x.Key != "WebSockets").All(kvp => File.Exists(kvp.Value.ApplicationPath));
            }
        }

        /// <summary>
        /// Determine if specified standalone app is installed
        /// </summary>
        /// <param name="appName"></param>
        /// <returns></returns>
        public bool IsInstalled(string appName)
        {
            if (_appInfo.TryGetValue(appName, out IAppInfo app))
            {
                return File.Exists(app.ApplicationPath);
            }

            return false;
        }

        /// <summary>
        /// Install standalone applications
        /// </summary>
        /// <returns></returns>
        public bool Install()
        {
#if MASTER_BRANCH
            string startArguments = "update -A --filter=apps";
#else
            string startArguments = "update -A --filter=apps --prerelease";
#endif
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                FileName = AgentPath,
                Arguments = startArguments,
                WorkingDirectory = Interface.uMod.RootDirectory,
                CreateNoWindow = true,
                UseShellExecute = false
            };

            try
            {
                Process process = Process.Start(startInfo);
                _logger.Info(Interface.uMod.Strings.Apps.Installing);
                process?.WaitForExit();
                return process?.ExitCode == 0 && AppsInstalled;
            }
            catch (Win32Exception win32Exception)
            {
                if (win32Exception.NativeErrorCode == 2)
                {
                    Interface.uMod.LogError(Interface.uMod.Strings.Apps.InstallFailure.Interpolate("reason", "because the uMod Agent is not installed"));
                }
                else
                {
                    Interface.uMod.LogException(Interface.uMod.Strings.Apps.InstallFailure.Interpolate("reason", "due to exception"), win32Exception);
                }

                return false;
            }
            catch (Exception ex)
            {
                Interface.uMod.LogException(Interface.uMod.Strings.Apps.InstallFailure.Interpolate("reason", "due to exception"), ex);
                return false;
            }
        }

        /// <summary>
        /// Determine if installation is valid
        /// </summary>
        public void ValidateInstall()
        {
            if (AppsInstalled)
            {
                return;
            }

            if (AgentDisabled || !Install())
            {
                throw new Exception(Interface.uMod.Strings.Apps.InstallFailure.Interpolate("reason", ", ensure the uMod Agent is installed"));
            }
        }
    }
}
