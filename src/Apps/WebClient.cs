using System.Collections.Generic;
using uMod.Collections;
using uMod.Common;
using uMod.Common.Web;
using uMod.Exceptions;
using uMod.IO;
using uMod.Pooling;

namespace uMod.Apps
{
    internal class WebClient : BaseApp
    {
        internal static DynamicPool MessagePool = Pools.Default<WebMessage>();

        /// <summary>
        /// ObjectStreamClient
        /// </summary>
        private ObjectStreamClient<WebMessage> _client;

        /// <summary>
        /// List of requests being executed
        /// </summary>
        private readonly Hash<int, ISettleablePromise<WebResponse>> _callbacks;

        /// <summary>
        /// Queue of pending messages
        /// </summary>
        private readonly Queue<WebMessage> _messageQueue;

        /// <summary>
        /// Last message id
        /// </summary>
        private int _lastMessageId = 1;

        /// <summary>
        /// Create new WebClient
        /// </summary>
        /// <param name="info"></param>
        /// <param name="logger"></param>
        public WebClient(IAppInfo info, ILogger logger) : base(info, logger)
        {
            _callbacks = new Hash<int, ISettleablePromise<WebResponse>>();
            _messageQueue = new Queue<WebMessage>();
        }

        /// <summary>
        /// Initialize web client
        /// </summary>
        /// <returns></returns>
        public override bool Initialize()
        {
            string[] args = {
                $"--logPath=\"{PathUtils.Escape(Interface.uMod.LogDirectory)}\""
            };

            if (!StartProcess(args))
            {
                return false;
            }

            _client = new ObjectStreamClient<WebMessage>(Process.StandardOutput.BaseStream, Process.StandardInput.BaseStream, null, null, MessagePool);
            _client.Message += OnMessage;
            _client.Error += OnError;
            _client.Start();

            return true;
        }

        /// <summary>
        /// Enqueue web request
        /// </summary>
        /// <param name="data"></param>
        /// <param name="request"></param>
        public void EnqueueRequest(WebRequest data, ISettleablePromise<WebResponse> request)
        {
            int id = _lastMessageId++;
            _callbacks.Add(id, request);
            WebMessage message = MessagePool.Get<WebMessage>();
            message.Id = id;
            message.Data = data;
            message.ExtraData = null;
            message.Type = WebMessageType.Request;

            EnqueueMessage(message);
        }

        /// <summary>
        /// Enqueue web message
        /// </summary>
        /// <param name="webMessage"></param>
        private void EnqueueMessage(WebMessage webMessage)
        {
            Interface.uMod.Dispatcher.Dispatch(delegate ()
            {
                if (!CheckProcess())
                {
                    _messageQueue.Enqueue(webMessage);
                    return true;
                }

                if (Ready)
                {
                    _client.PushMessage(webMessage);
                }
                else
                {
                    _messageQueue.Enqueue(webMessage);
                }
                return true;
            });
        }

        /// <summary>
        /// ObjectStreamClient message handler
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="message"></param>
        private void OnMessage(ObjectStreamConnection<WebMessage, WebMessage> connection, WebMessage message)
        {
            if (message == null)
            {
                Fail(Interface.uMod.Strings.Apps.Disconnected);
                return;
            }

            if (_lastMessageId > 999999)
            {
                _lastMessageId = 1;
            }

            ISettleablePromise<WebResponse> webRequestCallback;
            bool pending = true;
            switch (message.Type)
            {
                case WebMessageType.Response:
                    webRequestCallback = _callbacks[message.Id];
                    if (webRequestCallback == null)
                    {
                        Logger.Warning(Interface.uMod.Strings.WebClient.InvalidResponse);
                        return;
                    }

                    webRequestCallback.FinallyPeek(() => pending = false);
                    if (pending)
                    {
                        webRequestCallback.ReportResolved(message.Data as WebResponse);
                    }

                    if (webRequestCallback.GetType() == typeof(Web.Request))
                    {
                        Pools.Requests.Free(webRequestCallback);
                    }
                    _callbacks.Remove(message.Id);
                    break;

                case WebMessageType.Error:
                    webRequestCallback = _callbacks[message.Id];
                    if (webRequestCallback == null)
                    {
                        Logger.Warning(Interface.uMod.Strings.WebClient.InvalidError);
                        return;
                    }

                    webRequestCallback.FinallyPeek(() => pending = false);
                    if (pending)
                    {
                        webRequestCallback.ReportRejected(new WebException(message.Data.ToString()));
                    }

                    if (webRequestCallback.GetType() == typeof(Web.Request))
                    {
                        Pools.Requests.Free(webRequestCallback);
                    }
                    _callbacks.Remove(message.Id);
                    break;

                case WebMessageType.Ready:
                    connection.PushMessage(message);
                    if (!Ready)
                    {
                        Activate();
                        while (_messageQueue.Count > 0)
                        {
                            WebMessage webMessage = _messageQueue.Dequeue();

                            connection.PushMessage(webMessage);
                        }
                    }
                    break;
            }

            CheckIdleTimer();
        }

        /// <summary>
        /// Invoked when web client fails
        /// </summary>
        /// <param name="reason"></param>
        internal override void OnFailed(string reason)
        {
            reason = reason.Interpolate(("version", Version), ("app", GetType().Name));
            WebException exception = new WebException(reason);
            foreach (Promise<WebResponse> promise in _callbacks.Values)
            {
                promise.ReportRejected(exception);
            }

            _callbacks.Clear();
        }

        /// <summary>
        /// Shutdown web client application
        /// </summary>
        public override void Shutdown()
        {
            EndProcess();
            if (_client == null)
            {
                return;
            }

            _client.Message -= OnMessage;
            _client.Error -= OnError;
            _client.PushMessage(new WebMessage { Type = WebMessageType.Exit });
            _client.Stop();
            _client = null;
        }
    }
}
