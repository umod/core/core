﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using uMod.Common;
using uMod.Logging;

namespace uMod.Apps
{
    /// <summary>
    /// Represents the base of a standalone companion application
    /// </summary>
    internal abstract class BaseApp : IApp
    {
        /// <summary>
        /// Application process
        /// </summary>
        protected Process Process;

        /// <summary>
        /// Determines if process is ready
        /// </summary>
        protected volatile bool Ready;

        /// <summary>
        /// Gets the logging name of application
        /// </summary>
        protected string LogName => GetType().Name.ToLower();

        /// <summary>
        /// Gets the application metadata
        /// </summary>
        public IAppInfo Info { get; }

        /// <summary>
        /// Gets or sets the application version
        /// </summary>
        public string Version { get; protected set; }

        /// <summary>
        /// Timer used to track process idle
        /// </summary>
        private Libraries.Timer.TimerInstance _idleTimer;

        private Libraries.Timer _timerLib;

        protected object scopeLock = new object();

        /// <summary>
        /// Gets the timer library
        /// </summary>
        protected Libraries.Timer timer
        {
            get
            {
                return _timerLib = _timerLib ?? Interface.uMod.Libraries.Get<Libraries.Timer>();
            }
        }

        /// <summary>
        /// Application logger
        /// </summary>
        protected readonly ILogger Logger;

        /// <summary>
        /// Determines if autoshutdown is enabled
        /// </summary>
        protected bool AutoShutdownEnabled = true;

        /// <summary>
        /// Gets the relative path of the application
        /// </summary>
        protected string RelativePath =>
            Info?.ApplicationPath?.Replace(AppDomain.CurrentDomain.BaseDirectory + Path.DirectorySeparatorChar,
                string.Empty);

        /// <summary>
        /// Create a new instance of an application
        /// </summary>
        /// <param name="appInfo"></param>
        internal BaseApp(IAppInfo appInfo, ILogger logger)
        {
            Logger = logger;
            Info = appInfo;
            if (Info != null)
            {
                Version = File.Exists(Info.ApplicationPath)
                    ? FileVersionInfo.GetVersionInfo(Info.ApplicationPath).FileVersion
                    : "unknown";
            }
            else
            {
                Version = "unknown";
            }
        }

        /// <summary>
        /// Sets logging version
        /// </summary>
        protected void InitializeRemoteLogger()
        {
            SentryLogger.SetTag($"{LogName} version", Version);
        }

        /// <summary>
        /// Checks that process is operatiing
        /// </summary>
        /// <returns></returns>
        protected bool CheckProcess()
        {
            DestroyIdleTimer();

            if (Info?.ApplicationPath == null)
            {
                return false;
            }

            try
            {
                if (Process != null && Process.Handle != IntPtr.Zero && !Process.HasExited)
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }


            InitializeRemoteLogger();
            PurgeLogs();
            Shutdown();

            try
            {
                if (Initialize())
                {
                    return true;
                }
            }
            catch (Exception)
            {
                // Temporarily queue message until object stream re-established
                return false;
            }

            OnFailed(Interface.uMod.Strings.Apps.StartFailure);

            return false;
        }

        /// <summary>
        /// Destroy idle timer
        /// </summary>
        protected void DestroyIdleTimer()
        {
            _idleTimer?.Destroy();
        }

        /// <summary>
        /// Creates an idle timer
        /// </summary>
        protected void CheckIdleTimer()
        {
            if (AutoShutdownEnabled)
            {
                if (_idleTimer == null)
                {
                    _idleTimer = timer?.Once(60, AutoShutdown);
                    return;
                }

                if (_idleTimer.Destroyed)
                {
                    Interface.uMod.NextTick(() =>
                    {
                        if (AutoShutdownEnabled)
                        {
                            _idleTimer = timer?.Once(60, AutoShutdown);
                        }
                    });
                }
                else
                {
                    _idleTimer.SetDelay(60);
                }
            }
            else
            {
                _idleTimer?.Destroy();
            }
        }

        /// <summary>
        /// Starts application process
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        protected bool StartProcess(params string[] args)
        {
            string argString = string.Empty;
            if (args != null)
            {
                argString = string.Join(" ", args);
            }
            try
            {
                Process = new Process
                {
                    StartInfo =
                    {
                        FileName = "dotnet",
                        Arguments =  $"{RelativePath} {argString}",
                        WorkingDirectory = Interface.uMod.RootDirectory,
                        CreateNoWindow = true,
                        UseShellExecute = false,
                        RedirectStandardInput = true,
                        RedirectStandardOutput = true,
                    },
                    EnableRaisingEvents = true,
                };
                Process.Exited += OnProcessExited;
                Process.Start();
            }
            catch (Exception ex)
            {
                Process?.Dispose();
                Process = null;
                Logger.Report(Interface.uMod.Strings.Apps.StartException.Interpolate(("version", Version), ("app", GetType().Name)), ex);
                if (ex.GetBaseException() != ex)
                {
                    Logger.Report("BaseException: ", ex.GetBaseException());
                }
                if (ex is Win32Exception win32)
                {
                    Logger.Error(Interface.uMod.Strings.Exceptions.Win32.Interpolate(win32));
                }

                return false;
            }

            return Process != null;
        }

        /// <summary>
        /// Ends application process
        /// </summary>
        protected void EndProcess()
        {
            Ready = false;
            Process endedProcess = Process;
            if (endedProcess != null)
            {
                endedProcess.Exited -= OnProcessExited;
                ThreadPool.QueueUserWorkItem(_ =>
                {
#if !DEBUG
                    Thread.Sleep(5000);
#endif
                    // Calling Close can block up to 60 seconds on certain machines
                    try
                    {
                        if (endedProcess?.HasExited == false)
                        {
                            endedProcess.Close();
                        }
                    }
                    catch (Exception)
                    {
                        // Ignore
                    }
                });
            }

            Process = null;
        }

        /// <summary>
        /// Activates application
        /// </summary>
        protected virtual void Activate()
        {
            Ready = true;
        }

        /// <summary>
        /// Purges stale log files
        /// </summary>
        protected void PurgeLogs()
        {
            try
            {
                IEnumerable<string> filePaths = Directory.GetFiles(Interface.uMod.LogDirectory, "*.log").Where(f => Path.GetFileName(f).StartsWith($"{LogName}_"));
                foreach (string filePath in filePaths)
                {
                    File.Delete(filePath);
                }
            }
            catch (Exception)
            {
                // Ignore
            }
        }

        /// <summary>
        /// Initializes standalone application and listener
        /// </summary>
        /// <returns></returns>
        public abstract bool Initialize();

        /// <summary>
        /// Automatic shutdown procedure
        /// </summary>
        protected virtual void AutoShutdown()
        {
            if (Ready)
            {
                Logger.Debug(Interface.uMod.Strings.Apps.ClosingInactivity.Interpolate("app", GetType().Name));
                Shutdown();
            }
        }

        /// <summary>
        /// Shutdown standalone application and listener
        /// </summary>
        public abstract void Shutdown();

        /// <summary>
        /// ObjectStreamClient error handler
        /// </summary>
        /// <param name="exception"></param>
        protected void OnError(Exception exception)
        {
            Logger.Report(Interface.uMod.Strings.Apps.Error.Interpolate("app", GetType().Name), exception);
        }

        /// <summary>
        /// Per-application failure method
        /// </summary>
        /// <param name="reason"></param>
        internal abstract void OnFailed(string reason);

        /// <summary>
        /// Captured by exiting process
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnProcessExited(object sender, System.EventArgs e)
        {
            Fail(Interface.uMod.Strings.Apps.ClosedUnexpected);
        }

        /// <summary>
        /// Signals failure and initiates shutdown next tick
        /// </summary>
        /// <param name="message"></param>
        protected void Fail(string message)
        {
            Interface.uMod.NextTick(() =>
            {
                OnFailed(message);
                Shutdown();
            });
        }
    }
}
