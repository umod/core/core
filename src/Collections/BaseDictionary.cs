extern alias References;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using References::Newtonsoft.Json;
using References::Newtonsoft.Json.Linq;

namespace uMod.Collections
{
    /// <summary>
    /// Generic dictionary to use as a basis for other dictionary implementations
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class BaseDictionary<TKey, TValue> : IDictionary<TKey, TValue>, IEnumerable<TValue>
    {
        /// <summary>
        /// Local store of dictionary data
        /// </summary>
        public Dictionary<TKey, TValue> Data { get; internal set; }

        /// <summary>
        /// Create a new dictionary object
        /// </summary>
        /// <param name="data"></param>
        public BaseDictionary(IDictionary<TKey, TValue> data)
        {
            if (data == null)
            {
                data = new Dictionary<TKey, TValue>();
            }

            Data = data.ToDictionary(x => x.Key, x => x.Value);
        }

        /// <summary>
        /// Destructor that clears dictionary data
        /// </summary>
        ~BaseDictionary()
        {
            Data?.Clear();
            Data = null;
        }

        /// <summary>
        /// Get stored dictionary data
        /// </summary>
        /// <returns></returns>
        public Dictionary<TKey, TValue> ToDictionary()
        {
            return Data;
        }

        /// <summary>
        /// Add the specified value to the dictionary
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Add(TKey key, TValue value)
        {
            Data?.Add(key, value);
        }

        /// <summary>
        /// Determine if the specified key is contained within dictionary
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual bool ContainsKey(TKey key)
        {
            return Data?.ContainsKey(key) ?? false;
        }

        /// <summary>
        /// Get a collection of the keys in the dictionary
        /// </summary>
        public ICollection<TKey> Keys => Data?.Keys ?? throw new InvalidOperationException();

        /// <summary>
        /// Remove an item from the dictionary
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual bool Remove(TKey key)
        {
            return Data?.Remove(key) ?? false;
        }

        /// <summary>
        /// Try to get a value from the dictionary using the specified key
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetValue(TKey key, out TValue value)
        {
            value = default;
            return Data?.TryGetValue(key, out value) ?? false;
        }

        /// <summary>
        /// Get a collection of the values in the dictionary
        /// </summary>
        public ICollection<TValue> Values => Data.Values;

        /// <summary>
        /// Get a value from the dictionary
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TValue this[TKey key]
        {
            get => Data == null ? default : Data[key];
            set
            {
                if (Data != null)
                {
                    Data[key] = value;
                }
            }
        }

        #region ICollection<KeyValuePair<int,T>> Members

        /// <summary>
        /// Add a KeyValuePair to the dictionary
        /// </summary>
        /// <param name="item"></param>
        public void Add(KeyValuePair<TKey, TValue> item)
        {
            Data?.Add(item.Key, item.Value);
        }

        /// <summary>
        /// Clear all data stored in the dictionary
        /// </summary>
        public virtual void Clear()
        {
            Data?.Clear();
        }

        /// <summary>
        /// Determine if dictionary contains the specified KeyValuePair
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return ((Data?.ContainsKey(item.Key) ?? false) && (bool)Data?.ContainsValue(item.Value));
        }

        /// <summary>
        /// Copy this dictionary to the specified dictionary
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="index"></param>
        public void CopyTo(KeyValuePair<TKey, TValue>[] dictionary, int index)
        {
            Copy(this, dictionary, index);
        }

        /// <summary>
        /// Get the number of items stored in the dictionary
        /// </summary>
        public int Count => Data?.Count ?? 0;

        /// <summary>
        /// Determine if the dictionary is read-only
        /// </summary>
        public virtual bool IsReadOnly => false;

        /// <summary>
        /// Remove the specified KeyValuePair from the dictionary
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            IEnumerable<KeyValuePair<TKey, TValue>> found = Data.Where(x => x.Value.Equals(item.Value));
            if (found.Any())
            {
                Data = Data.Except(found).ToDictionary(x => x.Key, x => x.Value);
                return true;
            }

            return false;
        }

        #endregion ICollection<KeyValuePair<int,T>> Members

        #region IEnumerable<KeyValuePair<string,string>> Members

        /// <summary>
        /// Get a generic enumerator for the dictionary
        /// </summary>
        /// <returns></returns>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return Data.GetEnumerator();
        }

        #endregion IEnumerable<KeyValuePair<string,string>> Members

        #region IEnumerable Members

        /// <summary>
        /// Get a basic enumerator for the dictionary
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return Data.GetEnumerator();
        }

        /// <summary>
        /// Get a value enumerator for the dictionary
        /// </summary>
        /// <returns></returns>
        IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator()
        {
            return Values.GetEnumerator();
        }

        #endregion IEnumerable Members

        #region JSON handling

        /// <summary>
        /// Convert dictionary to a JSON string using specified formatting and converters
        /// </summary>
        /// <param name="formatting"></param>
        /// <param name="converters"></param>
        /// <returns></returns>
        public string ToJson(Formatting formatting, params JsonConverter[] converters)
        {
            return ToJObject().ToString(formatting, converters);
        }

        /// <summary>
        /// Convert dictionary to a JSON string using specified converters
        /// </summary>
        /// <param name="converters"></param>
        /// <returns></returns>
        public string ToJson(params JsonConverter[] converters)
        {
            return ToJObject().ToString(Formatting.None, converters);
        }

        /// <summary>
        /// Convert dictionary to a Newtonsoft.JObject
        /// </summary>
        /// <returns></returns>
        public JObject ToJObject()
        {
            return JObject.FromObject(Data);
        }

        /// <summary>
        /// Convert dictionary to a string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return ToJObject().ToString(Formatting.None);
        }

        #endregion JSON handling

        /// <summary>
        /// Copy the specified collection to the dictionary with integer keys
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="array"></param>
        /// <param name="arrayIndex"></param>
        private void Copy<T>(ICollection<T> source, T[] array, int arrayIndex)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (arrayIndex < 0 || arrayIndex > array.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(arrayIndex));
            }

            if ((array.Length - arrayIndex) < source.Count)
            {
                throw new ArgumentException("Destination array is not large enough. Check array.Length and arrayIndex.");
            }

            foreach (T item in source)
            {
                array[arrayIndex++] = item;
            }
        }
    }
}
