﻿using System.Collections.Generic;

namespace uMod.Collections
{
    /// <summary>
    /// A wrapper for a dictionary reference
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class ByRefDictionary<TKey, TValue> : BaseDictionary<TKey, TValue>
    {
        /// <summary>
        /// Create a new ByRef dictionary object using specified reference dictionary
        /// </summary>
        /// <param name="data"></param>
        public ByRefDictionary(ref Dictionary<TKey, TValue> data) : base(data) { }
    }
}
