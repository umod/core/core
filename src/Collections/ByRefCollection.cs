﻿using System.Collections;
using System.Collections.Generic;

namespace uMod.Collections
{
    /// <summary>
    /// A wrapper for a collection reference
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    public class ByRefCollection<TValue> : ICollection<TValue>
    {
        protected List<TValue> Collection;

        /// <summary>
        /// Create a new ByRef collection object (without reference)
        /// </summary>
        public ByRefCollection() { }

        /// <summary>
        /// Create a new ByRef collection object with specified reference
        /// </summary>
        /// <param name="collection"></param>
        public ByRefCollection(ref ICollection<TValue> collection)
        {
            Collection = collection as List<TValue>;
        }

        /// <summary>
        /// Get number of items in collection
        /// </summary>
        public int Count => Collection?.Count ?? 0;

        /// <summary>
        /// Determine if collection is read-only
        /// </summary>
        public virtual bool IsReadOnly => false;

        /// <summary>
        /// Add the specified item to the collection
        /// </summary>
        /// <param name="item"></param>
        public void Add(TValue item)
        {
            Collection?.Add(item);
        }

        /// <summary>
        /// Clear all items from the collection
        /// </summary>
        public void Clear()
        {
            Collection?.Clear();
        }

        /// <summary>
        /// Determine if the collection contains the specified item
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(TValue item)
        {
            return Collection?.Contains(item) ?? false;
        }

        /// <summary>
        /// Copy the specified array to the collection
        /// </summary>
        /// <param name="array"></param>
        /// <param name="arrayIndex"></param>
        public void CopyTo(TValue[] array, int arrayIndex)
        {
            Collection.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Get a strongly-typed enumerator for the collection
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerator<TValue> GetEnumerator()
        {
            return Collection.GetEnumerator();
        }

        /// <summary>
        /// Remove the specified item from the collection
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Remove(TValue item)
        {
            return Collection.Remove(item);
        }

        /// <summary>
        /// Get a basic enumerator for the collection
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return Collection.GetEnumerator();
        }
    }
}
