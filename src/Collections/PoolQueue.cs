﻿namespace uMod.Collections
{
    public class PoolQueue
    {
        protected readonly object Default;
        protected readonly ushort Capacity;
        protected readonly object[] Nodes;
        protected ushort Current;

        /// <summary>
        /// Create a new PoolQueue
        /// </summary>
        /// <param name="capacity"></param>
        public PoolQueue(ushort capacity)
        {
            Capacity = capacity;
            Nodes = new object[capacity];
            Current = 0;
            Default = default;
        }

        /// <summary>
        /// Enqueue an object into PoolQueue
        /// </summary>
        /// <param name="value"></param>
        protected void Enqueue(object value)
        {
            if (Current <= Nodes.Length)
            {
                Nodes[Current] = value;
                Current += 1;
            }

            if (Current >= Capacity)
            {
                Current = 0;
            }
        }

        /// <summary>
        /// Dequeue an object from PoolQueue
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        protected bool Dequeue(out object result)
        {
            if (Current <= 0)
            {
                Current = 0;
                result = Default;
                return false;
            }

            Current -= 1;
            result = Nodes[Current];
            return true;
        }
    }
}
