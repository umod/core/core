﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace uMod.Collections
{
    /// <summary>
    /// A dictionary which returns null for non-existent keys and removes keys when setting an index to null.
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class Hash<TKey, TValue> : IDictionary<TKey, TValue>
    {
        private readonly IDictionary<TKey, TValue> _dictionary;

        /// <summary>
        /// Create a new hash dictionary object
        /// </summary>
        public Hash()
        {
            _dictionary = new Dictionary<TKey, TValue>();
        }

        /// <summary>
        /// Get a value from the hash dictionary
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TValue this[TKey key]
        {
            get
            {
                if (TryGetValue(key, out TValue value))
                {
                    return value;
                }

                if (typeof(TValue).IsValueType)
                {
                    return (TValue)Activator.CreateInstance(typeof(TValue));
                }

                return default;
            }

            set
            {
                if (value == null)
                {
                    _dictionary.Remove(key);
                }
                else
                {
                    _dictionary[key] = value;
                }
            }
        }

        public ICollection<TKey> Keys => _dictionary.Keys;
        public ICollection<TValue> Values => _dictionary.Values;
        public int Count => _dictionary.Count;
        public bool IsReadOnly => _dictionary.IsReadOnly;

        /// <summary>
        /// Get an enumerator for the hash dictionary
        /// </summary>
        /// <returns></returns>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => _dictionary.GetEnumerator();

        /// <summary>
        /// Determine if the hash dictionary contains the specified key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ContainsKey(TKey key) => _dictionary.ContainsKey(key);

        /// <summary>
        /// Determine if the hash dictionary contains the specified KeyValuePair
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(KeyValuePair<TKey, TValue> item) => _dictionary.Contains(item);

        /// <summary>
        /// Copy the specified array into the hash dictionary
        /// </summary>
        /// <param name="array"></param>
        /// <param name="index"></param>
        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int index) => _dictionary.CopyTo(array, index);

        /// <summary>
        /// Try to get a value from the hash dictionary
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetValue(TKey key, out TValue value) => _dictionary.TryGetValue(key, out value);

        /// <summary>
        /// Add the specified value to the hash dictionary
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Add(TKey key, TValue value) => _dictionary.Add(key, value);

        /// <summary>
        /// Add the specified KeyValuePair to the hash dictionary
        /// </summary>
        /// <param name="item"></param>
        public void Add(KeyValuePair<TKey, TValue> item) => _dictionary.Add(item);

        /// <summary>
        /// Remove the specified key from the hash dictionary
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Remove(TKey key) => _dictionary.Remove(key);

        /// <summary>
        /// Remove the specified KeyValuePair from the hash dictionary
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Remove(KeyValuePair<TKey, TValue> item) => _dictionary.Remove(item);

        /// <summary>
        /// Clear all values from the hash dictionary
        /// </summary>
        public void Clear() => _dictionary.Clear();

        /// <summary>
        /// Get a basic enumerator for the hash dictionary
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
