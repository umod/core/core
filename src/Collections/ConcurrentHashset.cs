﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace uMod.Collections
{
    /// <summary>
    /// A partially thread-safe HashSet (iterating is not thread-safe)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ConcurrentHashSet<T> : ICollection<T>
    {
        private readonly HashSet<T> _collection;
        private readonly object _syncRoot = new object();

        /// <summary>
        /// Create a new concurrent hashset object
        /// </summary>
        public ConcurrentHashSet()
        {
            _collection = new HashSet<T>();
        }

        /// <summary>
        /// Create a new concurrent hashset object with the specified collection values
        /// </summary>
        /// <param name="values"></param>
        public ConcurrentHashSet(IEnumerable<T> values)
        {
            _collection = new HashSet<T>(values);
        }

        /// <summary>
        /// Create a new concurrent hashset object with the specified array values
        /// </summary>
        /// <param name="values"></param>
        public ConcurrentHashSet(T[] values)
        {
            _collection = new HashSet<T>(values);
        }

        public bool IsReadOnly => false;

        /// <summary>
        /// Get the number of elements stored in the hashset
        /// </summary>
        public int Count { get { lock (_syncRoot) { return _collection.Count; } } }

        /// <summary>
        /// Determine if the hashset contains the specified value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Contains(T value)
        {
            lock (_syncRoot)
            {
                return _collection.Contains(value);
            }
        }

        /// <summary>
        /// Add the specified value to the hashset
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Add(T value)
        {
            lock (_syncRoot)
            {
                return _collection.Add(value);
            }
        }

        /// <summary>
        /// Remove the specified value from the hashset
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Remove(T value)
        {
            lock (_syncRoot)
            {
                return _collection.Remove(value);
            }
        }

        /// <summary>
        /// Clear all values from the hashset
        /// </summary>
        public void Clear()
        {
            lock (_syncRoot)
            {
                _collection.Clear();
            }
        }

        /// <summary>
        /// Copy the specified array into the hashset
        /// </summary>
        /// <param name="array"></param>
        /// <param name="index"></param>
        public void CopyTo(T[] array, int index)
        {
            lock (_syncRoot)
            {
                _collection.CopyTo(array, index);
            }
        }

        /// <summary>
        /// Get a strongly-typed enumerator for the hashset (not thread safe)
        /// </summary>
        /// <returns></returns>
        public IEnumerator<T> GetEnumerator() => _collection.GetEnumerator();

        /// <summary>
        /// Perform a callback on all elements of the hashset
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public bool Any(Func<T, bool> callback)
        {
            lock (_syncRoot)
            {
                return _collection.Any(callback);
            }
        }

        /// <summary>
        /// Get the array of items stored in the hashset
        /// </summary>
        /// <returns></returns>
        public T[] ToArray()
        {
            lock (_syncRoot)
            {
                return _collection.ToArray();
            }
        }

        /// <summary>
        /// Try to dequeue an element from the beginning of the hashset
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryDequeue(out T value)
        {
            lock (_syncRoot)
            {
                value = _collection.ElementAtOrDefault(0);
                if (value != null)
                {
                    _collection.Remove(value);
                }

                return value != null;
            }
        }

        /// <summary>
        /// Get a basic enumerator from the hashset
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        /// <summary>
        /// Add the specified value to the hashset
        /// </summary>
        /// <param name="value"></param>
        void ICollection<T>.Add(T value) => Add(value);
    }
}
