﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using uMod.Common;

namespace uMod.Plugins
{
    /// <summary>
    /// Wrapper for dynamically managed plugin fields
    /// </summary>
    internal class PluginFieldInfo
    {
        public IPlugin Plugin;
        public FieldInfo Field;
        public Type FieldType;
        public Type PlayerType;
        public Type[] GenericArguments;
        public Dictionary<string, MethodInfo> Methods = new Dictionary<string, MethodInfo>();
        public string Directory;
        public object Value => Field.GetValue(Plugin);
        private Type ValueType
        {
            get
            {
                if (GenericArguments.Length > 1)
                {
                    return GenericArguments[1];
                }

                return null;
            }
        }

        private MethodInfo OnConnectMethod;
        private MethodInfo OnDisconnectMethod;

        /// <summary>
        /// Create a new instance of the PluginFieldInfo class
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="field"></param>
        /// <param name="directory"></param>
        public PluginFieldInfo(IPlugin plugin, FieldInfo field, string directory = null)
        {
            Plugin = plugin;
            Field = field;
            FieldType = field.FieldType;
            GenericArguments = FieldType.GetGenericArguments();
            Directory = directory;

            if (ValueType != null)
            {
                OnConnectMethod = ValueType.GetMethod("OnConnect");
                OnDisconnectMethod = ValueType.GetMethod("OnDisconnect");
            }
        }

        /// <summary>
        /// Invokes the OnConnect method if it exists
        /// </summary>
        /// <param name="onlinePlayer"></param>
        public void OnConnect(object onlinePlayer)
        {
            OnConnectMethod?.Invoke(onlinePlayer, null);
        }

        /// <summary>
        /// Invokes the OnDisconnect method if it exists
        /// </summary>
        /// <param name="onlinePlayer"></param>
        public void OnDisconnect(object onlinePlayer)
        {
            OnDisconnectMethod?.Invoke(onlinePlayer, null);
        }

        /// <summary>
        /// Determine if type constructor is valid given the supplied argument types
        /// </summary>
        /// <param name="argumentTypes"></param>
        /// <returns></returns>
        public bool HasValidConstructor(params Type[] argumentTypes)
        {
            Type type = GenericArguments[1];
            return type.GetConstructor(new Type[0]) != null || type.GetConstructor(argumentTypes) != null;
        }

        /// <summary>
        /// Finds a method and caches it
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="argumentTypes"></param>
        /// <returns></returns>
        public bool FindMethod(string methodName, params Type[] argumentTypes)
        {
            MethodInfo method = FieldType.GetMethod(methodName, argumentTypes);
            if (method != null)
            {
                Methods[methodName] = method;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Invokes a cached method by name
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public object Call(string methodName, params object[] args)
        {
            if (!Methods.TryGetValue(methodName, out MethodInfo method))
            {
                method = FieldType.GetMethod(methodName, BindingFlags.Instance | BindingFlags.Public);
                Methods[methodName] = method;
            }
            if (method == null)
            {
                throw new MissingMethodException(FieldType.Name, methodName);
            }

            return method.Invoke(Value, args);
        }

        /// <summary>
        /// Gets the player record from the generic OnlinePlayer type
        /// </summary>
        /// <param name="onlinePlayer"></param>
        /// <returns></returns>
        public IPlayer GetPlayer(object onlinePlayer)
        {
            FieldInfo fieldInfo = onlinePlayer.GetType().GetField("Player", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            if (fieldInfo != null)
            {
                return fieldInfo.GetValue(onlinePlayer) as IPlayer;
            }

            PropertyInfo propertyInfo = onlinePlayer.GetType().GetProperty("Player", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            if (propertyInfo != null)
            {
#if NET35 || NET40
                if (propertyInfo.GetGetMethod() == null)
#else
                if (propertyInfo.GetMethod == null)
#endif
                {
                    if (propertyInfo.DeclaringType?.GetField($"<{propertyInfo.Name}>k__BackingField", BindingFlags.NonPublic | BindingFlags.Instance) is FieldInfo backingField)
                    {
                        return backingField.GetValue(onlinePlayer) as IPlayer;
                    }
                    else
                    {
                        return propertyInfo.GetValue(onlinePlayer, null) as IPlayer;
                    }
                }
                else
                {
                    return propertyInfo.GetValue(onlinePlayer, null) as IPlayer;
                }
            }

            return null;
        }

        /// <summary>
        /// Sets the player on the generic OnlinePlayer type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="player"></param>
        /// <param name="onlinePlayer"></param>
        public void SetPlayer(Type type, IPlayer player, object onlinePlayer)
        {
            FieldInfo fieldInfo = type.GetField("Player", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            if (fieldInfo != null)
            {
                fieldInfo.SetValue(onlinePlayer, player);
                return;
            }

            PropertyInfo propertyInfo = type.GetProperty("Player", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            if (propertyInfo != null)
            {
#if NET35 || NET40
                if (propertyInfo.GetSetMethod() == null)
#else
                if (propertyInfo.SetMethod == null)
#endif
                {
                    if (propertyInfo.DeclaringType?.GetField($"<{propertyInfo.Name}>k__BackingField", BindingFlags.NonPublic | BindingFlags.Instance) is FieldInfo backingField)
                    {
                        backingField.SetValue(onlinePlayer, player);
                    }
                    else
                    {
                        propertyInfo.SetValue(onlinePlayer, player, null);
                    }
                }
                else
                {
                    propertyInfo.SetValue(onlinePlayer, player, null);
                }
            }
        }

        /// <summary>
        /// Gets the file path for per-player storage of generic OnlinePlayer types
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public string GetFilePath(IPlayer player)
        {
            string extension = Path.GetExtension(Directory);

            if (!string.IsNullOrEmpty(extension) || Directory.Contains('{'))
            {
                string pattern = Directory.Interpolate(player);
                return Path.Combine(Interface.uMod.DataDirectory, pattern);
            }
            return Path.Combine(Interface.uMod.DataDirectory, Path.Combine(Directory, player.Id));
        }
    }
}
