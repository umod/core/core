﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using uMod.Common;

namespace uMod.Plugins.Compiler
{
    /// <summary>
    /// Represents a successfully compiled and loaded plugin
    /// </summary>
    internal class CompiledPlugin
    {
        /// <summary>
        /// The last successfully compiled assembly
        /// </summary>
        internal Assembly LastGoodAssembly;

        /// <summary>
        /// The last successfuly loaded plugin instance
        /// </summary>
        internal IPlugin LastGoodPlugin;

        /// <summary>
        /// The last successfully compiled script
        /// </summary>
        internal byte[] LastGoodScript;

        /// <summary>
        /// The name of the plugin
        /// </summary>
        internal string PluginName;

        /// <summary>
        /// The directory of the plugin
        /// </summary>
        internal string Directory;

        /// <summary>
        /// The filename of the plugin
        /// </summary>
        internal string FileName;

        /// <summary>
        /// The full path of the plugin
        /// </summary>
        internal string FullPath;

        /// <summary>
        /// References associated with the plugin
        /// </summary>
        internal List<PluginReference> PluginReferences;

        /// <summary>
        /// Check if compiled plugin still exists
        /// </summary>
        internal bool IsAvailable
        {
            get
            {
                return File.Exists(FullPath);
            }
        }

        /// <summary>
        /// Determines if plugin is successfully loaded into the server
        /// </summary>
        internal bool IsLoaded { get { return LastGoodPlugin != null && LastGoodPlugin.IsLoaded; } }

        /// <summary>
        /// Create a new instance of the CompiledPlugin class
        /// </summary>
        /// <param name="plugin"></param>
        internal CompiledPlugin(CompilePluginRequest plugin)
        {
            LastGoodPlugin = plugin.Plugin;
            LastGoodScript = plugin.ScriptBytes;
            LastGoodAssembly = plugin.CompiledAssembly;
            FileName = plugin.FileName;
            FullPath = plugin.FullPath;
            Directory = plugin.Directory;
            PluginName = plugin.PluginName;
            PluginReferences = plugin.PluginReferences;
        }

        /// <summary>
        /// Converts CompiledPlugin to string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return PluginName;
        }
    }
}
