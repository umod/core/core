﻿using System.IO;

namespace uMod.Plugins.Compiler
{
    /// <summary>
    /// Represents an assembly reference
    /// </summary>
    internal class AssemblyReference
    {
        /// <summary>
        /// Gets the assembly name
        /// </summary>
        internal string Name { get; private set; }

        /// <summary>
        /// Gets the assembly directory location
        /// </summary>
        internal string Directory { get; private set; }

        private string _extension = ".dll";

        /// <summary>
        /// Gets the assembly filename
        /// </summary>
        internal string FileName { get { return Name + _extension; } }

        /// <summary>
        /// Gets the assembly full path
        /// </summary>
        internal string FullPath { get; private set; }

        /// <summary>
        /// Gets the last error
        /// </summary>
        internal string Error { get; private set; }

        /// <summary>
        /// Determines if reference is valid
        /// </summary>
        internal bool IsValid { get; private set; }

        /// <summary>
        /// Determines if reference is cached
        /// </summary>
        internal bool IsCached { get; set; }

        /// <summary>
        /// Serializable model sent to application
        /// </summary>
        internal Common.Compiler.AssemblyReference Reference;

        /// <summary>
        /// Create a new instance of the AssemblyReference class
        /// </summary>
        /// <param name="fullPath"></param>
        internal AssemblyReference(string fullPath)
        {
            fullPath = GetReferencePath(fullPath);
            Name = Path.GetFileNameWithoutExtension(fullPath);
            Directory = Path.GetDirectoryName(fullPath);
            FullPath = fullPath;
            Reference = new Common.Compiler.AssemblyReference(Directory, FileName);
        }

        /// <summary>
        /// Gets full reference path
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        internal string GetReferencePath(string fullPath)
        {
            _extension = Path.GetExtension(fullPath) ?? string.Empty;
            if (!_extension.EndsWith("dll") && !_extension.EndsWith("exe"))
            {
                string testPath = fullPath + ".dll";
                if (File.Exists(testPath))
                {
                    _extension = ".dll";
                    fullPath = testPath;
                }
                else
                {
                    testPath = fullPath + ".exe";
                    if (File.Exists(testPath))
                    {
                        _extension = ".exe";
                        fullPath = testPath;
                    }
                }
            }

            return fullPath;
        }

        /// <summary>
        /// Ensure reference assembly can be loaded and was loaded at least once
        /// </summary>
        /// <param name="pluginName"></param>
        internal bool Validate(string pluginName)
        {
            #region Look for DLL on disk

            if (!File.Exists(FullPath))
            {
                Error = Interface.uMod.Strings.Compiler.ReferenceAssemblyNotFound.Interpolate(("assembly", FullPath), ("plugin", pluginName));
                return IsValid = false;
            }

            #endregion Look for DLL on disk

            return IsValid = true;
        }

        /// <summary>
        /// Determine if reference is included by default
        /// </summary>
        /// <param name="dllName"></param>
        /// <returns></returns>
        private bool IsReferenceIncluded(string dllName)
        {
            return dllName.StartsWith("uMod.") || dllName.StartsWith("Newtonsoft.Json") || dllName.StartsWith("protobuf-net") || dllName.StartsWith("Rust.");
        }
    }
}
