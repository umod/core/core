extern alias References;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using References::Newtonsoft.Json;
using Tommy;
using uMod.Common;
using uMod.Pooling;
using uMod.Utilities;

namespace uMod.Plugins.Compiler
{
    internal class Client
    {
        /// <summary>
        /// List of references specific to the core testing environment
        /// </summary>
        private static List<AssemblyReference> _testReferences;

        /// <summary>
        /// Used to sort compile order duration 
        /// </summary>
        private int _compileOrderIndex = 1;

        /// <summary>
        /// List of most recent compilation requests keyed by plugin name.
        /// </summary>
        internal Dictionary<string, CompilePluginRequest> RecentRequests = new Dictionary<string, CompilePluginRequest>();

        /// <summary>
        /// Pauses compilation, ensuring plugins in a sequence are always completely loaded
        /// </summary>
        internal bool Paused = true;

        /// <summary>
        /// Pushed from the main thread and taken off by the compile thread
        /// </summary>
        private readonly List<CompileAction> _pendingActions = new List<CompileAction>();

        /// <summary>
        /// Thread safe because it is only touched by the compile thread
        /// </summary>
        private readonly CompilationStack _workQueue = new CompilationStack();

        /// <summary>
        /// Taken off the work queue then loaded on the main thread into the game
        /// </summary>
        private readonly CompilationStack _completedQueue = new CompilationStack();

        /// <summary>
        /// Any compile data for compiled plugins so we can reference them later on when compiling dependancies
        /// </summary>
        private readonly CompiledPluginList _compiledPlugins = new CompiledPluginList();

        /// <summary>
        /// The standalone compiler application 
        /// </summary>
        private readonly Apps.Compiler _compiler;

        /// <summary>
        /// In-memory reference cache (managed references)
        /// </summary>
        private readonly AssemblyCache _assemblyCache;

        /// <summary>
        /// The compilable plugin loader
        /// </summary>
        private readonly CompilablePluginLoader _loader;

        /// <summary>
        /// The compiler logger
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// Determines if compiler thread is running
        /// </summary>
        public bool IsInitialized { get; private set; }

        private readonly Func<string, bool> _directoryFilter;

        /// <summary>
        /// Create a new instance of the Compiler.Client class
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="loader"></param>
        internal Client(PluginProvider provider, CompilablePluginLoader loader)
        {
            _assemblyCache = new AssemblyCache();

            _logger = Interface.uMod.RootLogger;
            _loader = loader;

            _compiler = provider.Application;
            _directoryFilter = FileUtility.GetPluginDirectoryFilter();
        }

        /// <summary>
        /// Initialize the compiler client
        /// </summary>
        internal void Initialize()
        {
            if (IsInitialized)
            {
                return;
            }

            SetupExtensionNames();

            Interface.uMod.Libraries.Get<Libraries.Timer>().Repeat(0.1f, -1, ProcessMainThread);

            ThreadPool.QueueUserWorkItem(ProcessSeperateThread);

            IsInitialized = true;

            _logger.Debug("Compiler initialized");
        }

        /// <summary>
        /// Gets a pending compilation request by the specified plugin name
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        internal CompilePluginRequest GetPendingPlugin(string pluginName)
        {
            lock (_workQueue)
            {
                return _workQueue.Find(pluginName);
            }
        }

        /// <summary>
        /// Check if the specified plugin names are pending compilations
        /// </summary>
        /// <param name="pluginNames"></param>
        /// <returns></returns>
        internal bool IsPending(params string[] pluginNames)
        {
            return _IsPending(pluginNames);
        }

        /// <summary>
        /// Check if the specified plugin names are pending compilations
        /// </summary>
        /// <param name="pluginNames"></param>
        /// <returns></returns>
        internal bool IsPending(IEnumerable<string> pluginNames)
        {
            return _IsPending(pluginNames);
        }

        /// <summary>
        /// Check if the specified plugin names are pending compilations
        /// </summary>
        /// <param name="pluginNames"></param>
        /// <returns></returns>
        private bool _IsPending(IEnumerable<string> pluginNames)
        {
            lock (_workQueue)
            {
                foreach (string pluginName in pluginNames)
                {
                    if (RecentRequests.TryGetValue(pluginName, out CompilePluginRequest cpr))
                    {
                        if (!cpr.IsFinished)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Check if the specified plugin names are pending compilations
        /// </summary>
        /// <param name="pluginNames"></param>
        /// <returns></returns>
        internal bool AnyPending(params string[] pluginNames)
        {
            return _AnyPending(pluginNames);
        }

        /// <summary>
        /// Check if the specified plugin names are pending compilations
        /// </summary>
        /// <param name="pluginNames"></param>
        /// <returns></returns>
        internal bool AnyPending(IEnumerable<string> pluginNames)
        {
            return _AnyPending(pluginNames);
        }

        /// <summary>
        /// Check if the specified plugin names are pending compilations
        /// </summary>
        /// <param name="pluginNames"></param>
        /// <returns></returns>
        private bool _AnyPending(IEnumerable<string> pluginNames)
        {
            lock (_workQueue)
            {
                foreach (string pluginName in pluginNames)
                {
                    if (!RecentRequests.TryGetValue(pluginName, out CompilePluginRequest cpr))
                    {
                        continue;
                    }

                    if (cpr.IsFinished)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Gets a successfully compiled plugin by the specified plugin name
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        internal CompiledPlugin GetCompiledPlugin(string pluginName)
        {
            lock (_compiledPlugins)
            {
                return _compiledPlugins.Find(pluginName);
            }
        }

        /// <summary>
        /// Gets a successfully compiled plugin by the specified plugin name
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        internal bool TryGetCompiledPlugin(string pluginName, out CompiledPlugin compiledPlugin)
        {
            lock (_compiledPlugins)
            {
                compiledPlugin = _compiledPlugins.Find(pluginName);
            }

            return compiledPlugin != null;
        }

        /// <summary>
        /// Gets a list of all successfully compiled plugins
        /// </summary>
        /// <returns></returns>
        internal CompiledPlugin[] GetAllCompiledPlugins()
        {
            lock (_compiledPlugins)
            {
                return _compiledPlugins.All.ToArray();
            }
        }

        #region Extension Names

        private string[] _extensionNames;
        private string _includePath;
        internal string _gameExtensionName;
        internal string _gameExtensionBranch;

        /// <summary>
        /// Loads extension details
        /// </summary>
        private void SetupExtensionNames()
        {
            _includePath = Path.Combine(Interface.uMod.PluginDirectory, "include");
            _extensionNames = Interface.uMod.Extensions.All.Select(ext => ext.Name).ToArray();
            IExtension gameExtension = Interface.uMod.Extensions.All.SingleOrDefault(ext => ext.IsGameExtension);
            _gameExtensionName = gameExtension?.Name.Replace("uMod.", string.Empty).Replace(".", "_").ToUpper();
            _gameExtensionBranch = gameExtension?.Branch?.ToUpper();
        }

        #endregion Extension Names

        #region Recompilation

        /// <summary>
        /// Recompile the specified compilation request at the specified stage
        /// </summary>
        /// <param name="request"></param>
        /// <param name="stage"></param>
        internal void Recompile(CompilePluginRequest request, PluginCompileStage stage = PluginCompileStage.WaitingOnDependencies)
        {
            request.CompiledAssembly = null;
            request.CurrentStep = stage;
            request.Errors.Clear();
            lock (_workQueue)
            {
                _workQueue.Add(request);
            }
        }

        /// <summary>
        /// Recompile the specified compilation requests
        /// </summary>
        /// <param name="requests"></param>
        internal void Recompile(params CompilePluginRequest[] requests)
        {
            RecompileAll(PluginCompileStage.WaitingOnDependencies, requests);
        }

        /// <summary>
        /// Recompile the specified requests from the specified stage onward
        /// </summary>
        /// <param name="stage"></param>
        /// <param name="requests"></param>
        internal void RecompileAll(PluginCompileStage stage = PluginCompileStage.WaitingOnDependencies, CompilePluginRequest[] requests = null)
        {
            bool all = (requests == null);
            if (all)
            {
                requests = _workQueue.All.ToArray();
            }

#if DEBUG
            _logger.Debug($"Marking for recompilation {string.Join(", ", requests.Select(x => x.Name).ToArray())}");
#endif
            lock (_workQueue)
            {
                if (all)
                {
                    _workQueue.Clear();
                }

                foreach (CompilePluginRequest request in requests)
                {
                    request.CurrentStep = stage;
                    request.CompiledAssembly = null;
                    request.Errors.Clear();
#if DEBUG
                    request.TimeoutTime = DateTime.UtcNow.AddMinutes(5);
#else
                    request.TimeoutTime = DateTime.UtcNow.AddSeconds(60);
#endif

                    if (!all)
                    {
                        _workQueue.Add(request);
                    }
                }

                if (all)
                {
                    _workQueue.AddRange(requests);
                }
            }
        }

        #endregion Recompilation

        #region Compile

        internal void Compile(PluginReference pluginReference)
        {
            Compile(pluginReference.PluginName);
        }

        internal void Compile(string pluginName)
        {
            OnScriptAdded(Interface.uMod.PluginDirectory, pluginName);
        }

        #endregion Compile

        #region File watcher events

        /// <summary>
        /// Notified when a script at the specified path is added
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="fileName"></param>
        internal void OnScriptAdded(string directory, string fileName)
        {
            string pluginName = NormalizePluginName(fileName);

            if (_loader.LoadingPlugins.Contains(pluginName))
            {
#if DEBUG
                _logger.Debug("Pre-mature recompilation due to script add during compilation process");
#endif
                RecompileAll(PluginCompileStage.AwaitingUnload);
                return;
            }

            _loader.LoadingPlugins.Add(pluginName);

            lock (_pendingActions)
            {
                _pendingActions.Add(new CompileAction()
                {
                    Action = CompileActionType.Load,
                    Directory = directory,
                    FileName = fileName,
                    PluginName = pluginName,
                });
            }
        }

        /// <summary>
        /// Notified when a script at the specified path is removed
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="fileName"></param>
        internal void OnScriptRemove(string directory, string fileName)
        {
            // Remove cached CompilePluginRequests when file is removed
            if (RecentRequests.ContainsKey(fileName))
            {
                RecentRequests.Remove(fileName);
            }

            lock (_workQueue)
            {
                if (_workQueue.Count == 0)
                {
                    return;
                }
            }

            string pluginName = NormalizePluginName(fileName);

            lock (_workQueue)
            {
                if (_workQueue.Contains(pluginName))
                {
                    _workQueue.Remove(pluginName);
                }
            }
#if DEBUG
            _logger.Debug("Recompiling due to removed script");
#endif
            RecompileAll(PluginCompileStage.PreParseScript);
        }

        /// <summary>
        /// Notified when a script with the specified path is modified
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="fileName"></param>
        internal void OnScriptModified(string directory, string fileName)
        {
            string pluginName = NormalizePluginName(fileName);

            if (_loader.LoadingPlugins.Contains(pluginName))
            {
#if DEBUG
                _logger.Debug("Pre-mature recompilation due to script change during compilation process");
#endif
                RecompileAll(PluginCompileStage.PreParseScript);
                return;
            }

            _loader.LoadingPlugins.Add(pluginName);

            lock (_pendingActions)
            {
                _pendingActions.Add(new CompileAction()
                {
                    Action = CompileActionType.Load,
                    Directory = directory,
                    FileName = fileName,
                    PluginName = pluginName,
                });
            }
        }

        #endregion File watcher events

        #region Main Thread

        /// <summary>
        /// Handles main thread
        /// </summary>
        internal void ProcessMainThread()
        {
            ProcessFinishedPlugins();
        }

        #region Plugin handling

        /// <summary>
        /// Process plugin finishes in the compilation stack
        /// </summary>
        private void ProcessFinishedPlugins()
        {
            CompilePluginRequest[] finishedArray = null;

            lock (_completedQueue)
            {
                if (_completedQueue.Count > 0)
                {
                    finishedArray = _completedQueue.All.ToArray();
                }
            }

            if (finishedArray == null)
            {
                return;
            }

            //Now its safe to work on the main thread and load the plugins

            foreach (CompilePluginRequest plugin in finishedArray)
            {
                if (!FinishPlugin(plugin))
                {
                    continue;
                }

                lock (_completedQueue)
                {
                    //Remove the plugin when we finished processing it
                    _completedQueue.Remove(plugin.PluginName);
                }

                _loader.LoadingPlugins.Remove(plugin.PluginName);
            }
        }

        /// <summary>
        /// Finish the compilation process for the specified plugin compilation request
        /// </summary>
        /// <param name="plugin"></param>
        private bool FinishPlugin(CompilePluginRequest plugin)
        {
            if (plugin.HasFailed)
            {
                //What do we do when a plugin failed to compile

                if (plugin.RollbackRetries == 0)
                {
                    _logger.Error(Interface.uMod.Strings.Compiler.CompileErrors.Interpolate(
                        ("plugin", plugin.PluginName),
                        ("errors", string.Join(Environment.NewLine, plugin.Errors.ToArray()))));
                }

                return TryRollback(plugin);
            }

            // This calls unload then l.oad, etc
            bool loadingPluginWorked = plugin.LoadRetries > 0 ? plugin.Plugin.IsLoaded : TryLoadPlugin(plugin.Plugin);

            if (loadingPluginWorked == false)
            {
                if (_loader.PluginFailureReasons.TryGetValue(plugin.Name, out PluginFailureReason failureReason))
                {
                    switch (failureReason)
                    {
                        case PluginFailureReason.ConfigError:
                        case PluginFailureReason.LocalizationError:
                            return true;
                        case PluginFailureReason.Unknown:
                            bool rollbackResult = TryRollback(plugin);
                            if (!rollbackResult)
                            {
                                plugin.LoadRetries++;
                            }

                            return rollbackResult;
                    }
                }

                if (plugin.LoadRetries > 50)
                {
                    bool rollbackResult = TryRollback(plugin);
                    if (!rollbackResult)
                    {
                        plugin.LoadRetries++;
                    }

                    return rollbackResult;
                }

                plugin.LoadRetries++;
                return false;
            }

            OnPluginFinished(plugin, true);

            return true;
        }

        /// <summary>
        /// Tries to rollback the specified compilation request to the last previously loaded plugin
        /// </summary>
        /// <param name="plugin"></param>
        private bool TryRollback(CompilePluginRequest plugin)
        {
            plugin.LoadRetries = 0;
            CompiledPlugin oldPlugin = _compiledPlugins.Find(plugin.PluginName);

            if (oldPlugin == null)
            {
                //No plugin was loaded before so just print fail message
                _logger.Info(Interface.uMod.Strings.Compiler.RollbackFailureMissing.Interpolate("script", plugin.FileName));
                OnPluginRollback(plugin, null, false);
                return true;
            }
            else
            {
                // This calls unload then load, etc
                bool loadingPluginWorked = oldPlugin.IsLoaded;

                if (plugin.RollbackRetries == 0)
                {
                    _logger.Info(Interface.uMod.Strings.Compiler.Rollback.Interpolate("script", plugin.FileName));

                    loadingPluginWorked =
                        oldPlugin.LastGoodPlugin != null && TryLoadPlugin(oldPlugin.LastGoodPlugin);
                }

                if (loadingPluginWorked)
                {
                    //The end result was calling Unload() then Load()

                    OnPluginRollback(plugin, oldPlugin, true);
                    return true;
                }

                // Give plugin time to reload
                if (plugin.RollbackRetries > 50)
                {
                    _logger.Error(Interface.uMod.Strings.Compiler.RollbackFailureLoad.Interpolate("plugin", plugin.FileName));

                    //We have failed to call Load(), call Unload() it to be safe
                    //if (oldPlugin.IsLoaded && plugin.CanBeUnloaded)
                    if (oldPlugin.LastGoodPlugin != null && plugin.CanBeUnloaded)
                    {
                        _loader.PluginUnloaded(oldPlugin.LastGoodPlugin);
                    }

                    return true;
                }

                plugin.RollbackRetries++;
                return false;
            }
        }

        /// <summary>
        /// Tries to load the specified plugin
        /// </summary>
        /// <param name="pluginClass"></param>
        /// <returns></returns>
        private bool TryLoadPlugin(IPlugin pluginClass)
        {
            if (pluginClass == null)
            {
                throw new ArgumentException("Cannot load null plugin", nameof(pluginClass));
            }

            _loader.Load(pluginClass);

            return pluginClass.IsLoaded;
        }

        /// <summary>
        /// Notified when a plugin compilation request finishes
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="loaded"></param>
        private void OnPluginFinished(CompilePluginRequest plugin, bool loaded)
        {
            lock (_compiledPlugins)
            {
                _compiledPlugins.Remove(plugin.PluginName);
            }

            if (!loaded)
            {
                return;
            }

            lock (_compiledPlugins)
            {
                _compiledPlugins.Add(new CompiledPlugin(plugin));
            }

            OnPluginLoaded(plugin);
        }

        private void OnPluginLoaded(CompilePluginRequest plugin)
        {
            plugin.LoadRetries = 0;

            if (_loader.LoadedPlugins.ContainsKey(plugin.PluginName))
            {
                _loader.LoadedPlugins[plugin.PluginName] = plugin.Plugin;
            }
            else
            {
                _loader.LoadedPlugins.Add(plugin.PluginName, plugin.Plugin);
            }
        }

        private void OnPluginRollback(CompiledPlugin plugin)
        {
            if (_loader.LoadedPlugins.ContainsKey(plugin.PluginName))
            {
                _loader.LoadedPlugins[plugin.PluginName] = plugin.LastGoodPlugin;
            }
            else
            {
                _loader.LoadedPlugins.Add(plugin.PluginName, plugin.LastGoodPlugin);
            }
        }

        private void OnPluginRollback(CompilePluginRequest plugin, CompiledPlugin oldPlugin, bool loaded)
        {
            if (oldPlugin != null)
            {
                lock (plugin.ReferenceLock)
                {
                    plugin.PluginReferences = oldPlugin.PluginReferences;
                }
            }

            plugin.RollbackRetries = 0;

            plugin?.OnRollback?.Invoke();

            OnPluginLoaded(plugin);
        }

        #endregion Plugin handling

        /// <summary>
        /// Add pending compilation requests to compilation stack
        /// </summary>
        private void DoAdds()
        {
            List<CompileAction> temp = Pools.GetList<CompileAction>();

            lock (_pendingActions)
            {
                if (_pendingActions.Count > 0)
                {
                    temp.AddRange(_pendingActions);
                    _pendingActions.Clear();
                }
            }

            if (temp == null || temp.Count == 0)
            {
                return;
            }

            List<CompilePluginRequest> pluginRequests = Pools.GetList<CompilePluginRequest>();

            try
            {
                foreach (CompileAction action in temp)
                {
                    if (action.Action == CompileActionType.Load)
                    {
                        CompilePluginRequest request =
                            new CompilePluginRequest(_logger, action.Directory, action.FileName, action.PluginName);

                        string key = action.PluginName;

                        if (RecentRequests.ContainsKey(key))
                        {
                            RecentRequests[key] = request;
                        }
                        else
                        {
                            RecentRequests.Add(key, request);
                        }

                        pluginRequests.Add(request);
                    }
                }

                if (pluginRequests.Count > 0)
                {
                    lock (_workQueue)
                    {
#if DEBUG
                        _logger.Debug($"Adding {pluginRequests.Count} plugins to work queue: {string.Join(", ", pluginRequests.Select(x => x.Name).ToArray())}");
#endif
                        _workQueue.AddRange(pluginRequests);
                    }
                }
            }
            finally
            {
                Pools.FreeList(ref pluginRequests);
                Pools.FreeList(ref temp);
            }
        }

        #endregion Main Thread

        #region Compile Thread

        /// <summary>
        /// Determines if separate thread should wait or skip
        /// </summary>
        /// <returns></returns>
        private bool CheckWaitAndSkip()
        {
            if (Paused)
            {
                Thread.Sleep(25);
                return true;
            }
            bool sleep = false;
            lock (_workQueue)
            {
                DoAdds();

                if (_workQueue.Count == 0)
                {
                    sleep = true;
                }
            }

            if (sleep)
            {
                Thread.Sleep(250);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Handle separate thread processing
        /// </summary>
        /// <param name="context"></param>
        private void ProcessSeperateThread(object context)
        {
            while (true)
            {
                if (Interface.uMod._isShuttingDown)
                {
                    return;
                }

                if (CheckWaitAndSkip())
                {
                    continue;
                }

                CompilePluginRequest[] workQueueAll = _workQueue.All.ToArray();

                //Everything below operates on the work queue solely

                ProcessScriptsBeforeCompile(workQueueAll);

                ValidateDependencies(workQueueAll);

                //Order of send / recieve isn't that important, but may as well put recv first
                ReceiveCompileRequests();

                TimeOutPurge(workQueueAll);

                SortPluginOrder(workQueueAll);

                SendCompileRequests(workQueueAll);

                ProcessScriptsAfterCompile(workQueueAll);

                CreatePluginInstances(workQueueAll);

                DoRemoves(workQueueAll);

                // 100ms delays between loops will be fine but can increase later if it's important to shave 100ms off total compile time (2000ms usually)
                if (Interface.uMod._isShuttingDown)
                {
                    return;
                }
                Thread.Sleep(100);
            }
        }

        /// <summary>
        /// Preprocess scripts
        /// </summary>
        /// <param name="workQueueAll"></param>
        private void ProcessScriptsBeforeCompile(CompilePluginRequest[] workQueueAll)
        {
            foreach (CompilePluginRequest plugin in workQueueAll)
            {
                if (plugin.CurrentStep == PluginCompileStage.Nothing)
                {
                    plugin.CurrentStep = PluginCompileStage.AwaitingUnload;
                }

                if (plugin.CurrentStep == PluginCompileStage.AwaitingUnload)
                {
                    plugin.IsScriptLoaded = false;
                    bool any = false;
                    string message = string.Empty;
                    CompiledPlugin compiledPlugin = _compiledPlugins.Find(plugin.PluginName);
                    if ((compiledPlugin?.IsLoaded ?? false) && plugin.CanBeUnloaded && !plugin.DependentCompilation)
                    {
                        foreach (CompiledPlugin refPlugin in GetReverseReferences(plugin.PluginName))
                        {
                            if (refPlugin != null && refPlugin.IsLoaded)
                            {
                                plugin.OnRollback.Add(delegate
                                {
                                    if (!refPlugin.IsLoaded)
                                    {
                                        TryLoadPlugin(refPlugin.LastGoodPlugin);
                                        OnPluginRollback(refPlugin);
                                    }
                                });
                            }
                        }

                        _loader.PluginUnloaded(compiledPlugin.LastGoodPlugin);
                        any = true;
                        message += $" {compiledPlugin.PluginName}";
                    }

                    lock (plugin.ReferenceLock)
                    {
                        foreach (PluginReference refPlugin in plugin.PluginReferences)
                        {
                            if (!_workQueue.Contains(refPlugin.PluginName))
                            {
                                continue;
                            }

                            CompiledPlugin refPluginImpl = _compiledPlugins.Find(plugin.PluginName);
                            if (refPluginImpl == null || !refPluginImpl.IsLoaded)
                            {
                                continue;
                            }

                            message += $" {refPluginImpl.PluginName}";
                            any = true;
                        }
                    }

                    if (!any)
                    {
                        plugin.NextStep();
                        plugin.UnloadAttempts = 0;
                    }
                    else
                    {
                        plugin.UnloadAttempts++;

                        if (plugin.UnloadAttempts > 5)
                        {
                            plugin.EncounteredError(
                                Interface.uMod.Strings.PluginLoader.PluginLoadFailureMessage.Interpolate(
                                    ("plugin", plugin.PluginName), ("message", $"Not unloaded {message}"))); // TODO: Localization
                        }
                    }
                }

                if (!plugin.Load(_gameExtensionName, _gameExtensionBranch, true))
                {
                    continue;
                }

                plugin.NextStep();
            }
        }

        /// <summary>
        /// Validate compilation stack dependencies
        /// </summary>
        /// <param name="workQueueAll"></param>
        private void ValidateDependencies(CompilePluginRequest[] workQueueAll)
        {
            // We make sure all dependancies are queued up or already loaded on the server
            foreach (CompilePluginRequest plugin in workQueueAll)
            {
                if (plugin.CurrentStep == PluginCompileStage.AddDefaultReferences)
                {
                    AddDefaultReferences(plugin);

                    // Do not think it can fail adding the built in references
                    plugin.NextStep();
                }

                if (plugin.CurrentStep == PluginCompileStage.ValidateAssemblyReferences)
                {
                    if (ValidateAssemblyReferences(plugin))
                    {
                        plugin.NextStep();
                    }
                }

                if (plugin.CurrentStep == PluginCompileStage.ValidatePluginReferences)
                {
                    if (ValidatePluginReferences(plugin))
                    {
                        plugin.NextStep();
                    }
                }
            }
        }

        /// <summary>
        /// Sort compilation order
        /// </summary>
        /// <param name="workQueueAll"></param>
        private void SortPluginOrder(CompilePluginRequest[] workQueueAll)
        {
            // If a plugin becomes ready to compile, we need to check again if any other plugins are now ready, and so on
            bool recheckLoop = true;

            int loops = 0;

            while (recheckLoop)
            {
                recheckLoop = false;

                foreach (CompilePluginRequest plugin in workQueueAll)
                {
                    if (plugin.CurrentStep != PluginCompileStage.WaitingOnDependencies &&
                        plugin.CurrentStep != PluginCompileStage.SortLoadOrder)
                    {
                        continue;
                    }

                    lock (plugin.ReferenceLock)
                    {
                        if (plugin.CurrentStep == PluginCompileStage.WaitingOnDependencies &&
                            plugin.PluginReferences.Any(x => x.IsReady(_workQueue, _compiledPlugins) == false))
                        {
                            continue;
                        }
                    }

                    lock (plugin.ReferenceLock)
                    {
                        foreach (PluginReference pluginReference in plugin.PluginReferences)
                        {
                            CompilePluginRequest pendingWorkItem = _workQueue.Find(pluginReference.PluginName);
                            if (pendingWorkItem == null ||
                                (pendingWorkItem.CurrentStep != PluginCompileStage.WaitingOnDependencies &&
                                 pendingWorkItem.CurrentStep != PluginCompileStage.SortLoadOrder &&
                                 pendingWorkItem.CurrentStep != PluginCompileStage.EnsureUnloaded))
                            {
                                continue;
                            }

                            if (pendingWorkItem.CurrentStep == PluginCompileStage.SortLoadOrder ||
                                pendingWorkItem.CurrentStep == PluginCompileStage.EnsureUnloaded)
                            {
                                if (pluginReference.Required)
                                {
                                    pendingWorkItem.LoadOrder = plugin.LoadOrder - 1;
                                }
                                else
                                {
                                    pendingWorkItem.LoadOrder = _compileOrderIndex++;
                                }
                            }
                            else
                            {
                                pendingWorkItem.CompileOrder = _compileOrderIndex++;
                            }

                            pendingWorkItem.NextStep();
                        }
                    }

#if DEBUG
                    _logger.Debug($"Pushing request: {plugin.Name}");
#endif

                    if (plugin.CurrentStep == PluginCompileStage.SortLoadOrder)
                    {
                        if (plugin.LoadOrder == 0)
                        {
                            plugin.LoadOrder = _compileOrderIndex++;
                        }
                    }
                    else
                    {
                        plugin.CompileOrder = _compileOrderIndex++;
                    }
                    plugin.NextStep();

                    recheckLoop = true;
                }

                if (loops++ <= 300)
                {
                    continue;
                }

                break;
            }

            _compileOrderIndex = 1;
        }

        private bool TryGetPluginPath(string pluginName, out string path)
        {
            List<string> dirs = new List<string>
            {
                string.Empty
            };

            if (!pluginName.EndsWith(".cs"))
            {
                pluginName = $"{pluginName}.cs";
            }

            if (Interface.uMod.Plugins?.Configuration?.Watchers?.PluginDirectories != null)
            {
                if (Interface.uMod.Plugins.Configuration.Watchers.PluginDirectories.Any(x => x.Trim() == "*"))
                {
                    string[] allDirectories = Directory.GetDirectories(Interface.uMod.PluginDirectory, "*", SearchOption.AllDirectories);
                    foreach (string anyDir in allDirectories)
                    {
                        dirs.Add(anyDir.Replace(Interface.uMod.PluginDirectory, string.Empty).Trim('/').Trim('\\'));
                    }
                }
                else
                {
                    dirs.AddRange(Interface.uMod.Plugins.Configuration.Watchers.PluginDirectories);
                }
            }

            foreach (string dir in dirs)
            {
                string dirPath = string.IsNullOrEmpty(dir) ?
                    Interface.uMod.PluginDirectory :
                    Path.Combine(Interface.uMod.PluginDirectory, dir);

                string potentialPath = Path.Combine(dirPath, pluginName);
                if (!File.Exists(potentialPath))
                {
                    continue;
                }

                if (!_directoryFilter(potentialPath))
                {
                    continue;
                }

                path = potentialPath;
                return true;
            }

            path = string.Empty;
            return false;
        }

        /// <summary>
        /// Validate request plugin references
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns></returns>
        private bool ValidatePluginReferences(CompilePluginRequest plugin)
        {
            //Here we validate plugins referencing other plugins

            bool recompile = false;

            lock (plugin.ReferenceLock)
            {
                if (plugin.PluginReferences.Count == 0)
                {
                    return true;
                }

                foreach (PluginReference reference in plugin.PluginReferences)
                {
                    if (reference.Valid)
                    {
                        continue;
                    }

                    CompiledPlugin loadedPlugin = _compiledPlugins.Find(reference.PluginName);

                    // Plugin is already compiled and loaded, therefore is valid
                    if (loadedPlugin != null && loadedPlugin.IsLoaded && loadedPlugin.IsAvailable)
                    {
                        reference.Valid = true;
                        continue;
                    }

                    CompilePluginRequest loadingPlugin = _workQueue.Find(reference.PluginName);

                    // Plugin has been queued to be compiled, so it also is valid
                    if (loadingPlugin != null)
                    {
                        reference.Valid = true;
                        continue;
                    }

                    // Now we need to look for it on disk
                    if (!TryGetPluginPath(reference.PluginName, out string path))
                    {
                        TypePromiseManager.AddPromise(new[] { $"uMod.Plugins.{reference.PluginName}", $"Oxide.Plugins.{reference.PluginName}" },
                            plugin.Name + _loader.FileExtension);
                        plugin.EncounteredError(Interface.uMod.Strings.Compiler.RequirementsMissingLog
                            .Interpolate(
                                ("plugin", plugin.Name),
                                ("requirements", reference.PluginName),
                                ("dependency", "plugin")
                            ));

                        continue;
                    }

                    CompilePluginRequest cpr = new CompilePluginRequest(_logger, Path.GetDirectoryName(path),
                        reference.PluginName, reference.PluginName);
                    _workQueue.Add(cpr);

                    recompile = true;
                }

                if (!recompile)
                {
                    lock (plugin.ReferenceLock)
                    {
                        return plugin.PluginReferences.All(x => x.Valid);
                    }
                }

                RecompileAll();
                return false;
            }
        }

        /// <summary>
        /// Validate request assembly references
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns></returns>
        private bool ValidateAssemblyReferences(CompilePluginRequest plugin)
        {
            // We also look for cached dll references in here

            #region Turn DLLs referenced by magic strings to DLLReference

            foreach (string referenceName in plugin.ReferencedAssemblyNames)
            {
                // We keep any previously loaded DLL references cached: they cant be hot reloaded anyways
                AssemblyReference reference = _assemblyCache.Find(referenceName) ??
                    new AssemblyReference(Path.Combine(Interface.uMod.ExtensionDirectory, referenceName));

                if (plugin.ReferencedAssemblies.ContainsKey(reference.Name))
                {
                    plugin.ReferencedAssemblies[reference.Name] = reference;
                }
                else
                {
                    plugin.ReferencedAssemblies.Add(reference.Name, reference);
                }
            }

            #endregion Turn DLLs referenced by magic strings to DLLReference

            #region Validate all DLLReference

            foreach (KeyValuePair<string, AssemblyReference> reference in plugin.ReferencedAssemblies)
            {
                if (reference.Value.IsValid)
                {
                    continue;
                }

                if (!reference.Value.Validate(plugin.Name))
                {
                    plugin.EncounteredError(Interface.uMod.Strings.Compiler.ValidateReferenceFailed.Interpolate(("file", reference.Value.FileName), ("message", reference.Value.Error)));
                    return false;
                }

                if (reference.Value.IsCached == false)
                {
                    _assemblyCache.Add(reference.Value);
                }
            }

            #endregion Validate all DLLReference

            return true;
        }

        private IEnumerable<AssemblyReference> CreateTestReferences(params string[] paths)
        {
            List<AssemblyReference> references = new List<AssemblyReference>();

            string runtimePath = System.Runtime.InteropServices.RuntimeEnvironment.GetRuntimeDirectory();

            foreach (string path in paths)
            {
                string newPath;
                if (Path.IsPathRooted(path))
                {
                    newPath = path;
                }
                else
                {
                    newPath = Path.Combine(runtimePath, path);
                }

                if (File.Exists(newPath))
                {
                    references.Add(new AssemblyReference(newPath));
                }
            }

            return references;
        }

        /// <summary>
        /// Add default request references
        /// </summary>
        /// <param name="plugin"></param>
        private void AddDefaultReferences(CompilePluginRequest plugin)
        {
            if (Interface.uMod.IsTesting)
            {
                #region Add references when running tests

                if (_testReferences == null)
                {
                    _testReferences = new List<AssemblyReference>();
                }

                if (_testReferences.Count == 0)
                {
                    _testReferences.AddRange(CreateTestReferences(new[]
                    {
                        typeof(object).Assembly.Location,
                        "netstandard.dll",
                        "mscorlib.dll",
                        "System.Collections.dll",
                        "System.ComponentModel.Primitives.dll",
                        "System.Collections.Immutable.dll",
                        //"System.Linq.dll",
                        "System.Runtime.dll",
                        "System.Data.Common.dll",
                        typeof(Queue<>).Assembly.Location,
                        typeof(Enumerable).Assembly.Location,
                        typeof(JsonConverter).Assembly.Location,
                        typeof(Promise).Assembly.Location,
                        typeof(IContext).Assembly.Location,
                        typeof(Plugin).Assembly.Location,
                        typeof(TomlTable).Assembly.Location
                    }));

                    string testAssemblyPath = Path.Combine(Interface.uMod.ExtensionDirectory, "uMod.Tests.dll");

                    if (File.Exists(testAssemblyPath))
                    {
                        _testReferences.Add(new AssemblyReference(testAssemblyPath));
                    }

                    testAssemblyPath = Path.Combine(Interface.uMod.ExtensionDirectory, "uMod.TestServer.dll");
                    if (File.Exists(testAssemblyPath))
                    {
                        _testReferences.Add(new AssemblyReference(testAssemblyPath));
                    }

                    //uMod.Promise
                    _testReferences.Add(new AssemblyReference(typeof(Promise).Assembly.Location));

                    //uMod.Common
                    _testReferences.Add(new AssemblyReference(typeof(IContext).Assembly.Location));

                    //uMod.Core
                    _testReferences.Add(new AssemblyReference(typeof(Plugin).Assembly.Location));
                }

                foreach (AssemblyReference testReference in _testReferences)
                {
                    if (!plugin.ReferencedAssemblies.ContainsKey(testReference.Name))
                    {
                        plugin.ReferencedAssemblies.Add(testReference.Name, testReference);
                    }
                }

                #endregion Add references when running tests
            }
            else
            {
                #region Ensure core library is sent

                if (!_assemblyCache.TryFind(typeof(object).Assembly.Location, out AssemblyReference coreReference))
                {
                    _assemblyCache.Add(coreReference = new AssemblyReference(typeof(object).Assembly.Location));
                }

                // Add mscorlib as a reference
                if (!plugin.ReferencedAssemblies.ContainsKey(coreReference.Name))
                {
                    plugin.ReferencedAssemblies.Add(coreReference.Name, coreReference);
                }

                #endregion Ensure core library is sent
            }

            #region Load default references

            CacheAssemblyReferences(CompilablePluginLoader.PluginReferences);

            HashSet<string> extensionReferences = new HashSet<string>();
            foreach (IExtension extension in Interface.uMod.Extensions.All)
            {
                string dllRefName;

                if (extension.DefaultReferences != null && extension.DefaultReferences.Length > 0)
                {
                    foreach (string defaultReference in extension.DefaultReferences)
                    {
                        GetReferenceFileName(defaultReference, out _, out dllRefName);

                        if (!extensionReferences.Contains(dllRefName))
                        {
                            extensionReferences.Add(dllRefName);
                        }
                    }
                }

                GetReferenceFileName(extension.Filename, out _, out dllRefName);

                extensionReferences.Add(dllRefName);
            }

            if (extensionReferences.Count > 0)
            {
                CacheAssemblyReferences(extensionReferences);
            }

            foreach (string assemblyName in _assemblyCache.GetNames())
            {
                if (!_assemblyCache.TryFind(assemblyName, out AssemblyReference assemblyReference))
                {
                    continue;
                }

                if (!plugin.ReferencedAssemblies.ContainsKey(assemblyName))
                {
                    plugin.ReferencedAssemblies.Add(assemblyName, assemblyReference);
                }
            }

            #endregion Load default references
        }

        /// <summary>
        /// Gets file name from potentially imprecise dll definition
        /// </summary>
        /// <param name="referenceName"></param>
        /// <param name="fileExtension"></param>
        /// <param name="dllRefName"></param>
        /// <returns></returns>
        internal static string GetReferenceFileName(string referenceName, out string fileExtension, out string dllRefName)
        {
            fileExtension = Path.GetExtension(referenceName);
            dllRefName = referenceName;
            if (fileExtension == ".dll")
            {
                dllRefName = Path.GetFileNameWithoutExtension(referenceName);
            }

            return $"{dllRefName}.dll";
        }

        private void CacheAssemblyReferences(IEnumerable<string> pluginReferences)
        {
            foreach (string filename in pluginReferences)
            {
                string dllFileName = GetReferenceFileName(filename, out string fileExtension, out string dllRefName);
                string dllFullPath = Path.Combine(Interface.uMod.ExtensionDirectory, dllFileName);

                if (!string.IsNullOrEmpty(dllRefName) &&
                    !_assemblyCache.Contains(dllRefName) &&
                    File.Exists(dllFullPath))
                {
                    _assemblyCache.Add(new AssemblyReference(dllFullPath));
                }
                else
                {
                    string exeRefName = filename;
                    if (fileExtension == ".exe")
                    {
                        exeRefName = Path.GetFileNameWithoutExtension(filename);
                    }
                    string exeFileName = $"{exeRefName}.exe";
                    string exeFullPath = Path.Combine(Interface.uMod.ExtensionDirectory, exeFileName);
                    if (!string.IsNullOrEmpty(exeRefName) &&
                        !_assemblyCache.Contains(exeRefName) &&
                        File.Exists(exeFullPath))
                    {
                        _assemblyCache.Add(new AssemblyReference(exeFullPath));
                    }
                }
            }
        }

        /// <summary>
        /// Purge timed out plugins
        /// </summary>
        /// <param name="workQueueAll"></param>
        private void TimeOutPurge(CompilePluginRequest[] workQueueAll)
        {
            foreach (CompilePluginRequest plugin in workQueueAll)
            {
                if (plugin.TimeoutTime == DateTime.MinValue)
                {
                    continue;
                }

                if (DateTime.UtcNow > plugin.TimeoutTime)
                {
                    plugin.OnCompilationTimeout();
                }
            }
        }

        /// <summary>
        /// Get reverse plugin references
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        private List<CompiledPlugin> GetReverseReferences(string pluginName)
        {
            List<CompiledPlugin> compiledPlugins = new List<CompiledPlugin>(); // TODO: pool
            foreach (CompiledPlugin compiledPlugin in _compiledPlugins.All)
            {
                if (pluginName == compiledPlugin.PluginName)
                {
                    foreach (PluginReference pluginReference in compiledPlugin.PluginReferences)
                    {
                        CompiledPlugin compiledPluginReference = _compiledPlugins.Find(pluginReference.PluginName);
                        if (compiledPluginReference != null && !compiledPlugins.Contains(compiledPluginReference))
                        {
                            compiledPlugins.Add(compiledPluginReference);

                            List<CompiledPlugin> referenceReferences = GetReverseReferences(compiledPluginReference.PluginName);
                            if (referenceReferences.Count > 0)
                            {
                                compiledPlugins.AddRange(referenceReferences);
                            }
                        }
                    }
                }
                else
                {
                    if (compiledPlugin.PluginReferences.Any(x => x.IsExplicit &&
                                                                 x.PluginName.Equals(pluginName, StringComparison.InvariantCulture)))
                    {
                        compiledPlugins.Add(compiledPlugin);
                    }
                }
            }

            return compiledPlugins;
        }

        /// <summary>
        /// Send compilation requests to the compiler
        /// </summary>
        /// <param name="workQueueAll"></param>
        private void SendCompileRequests(CompilePluginRequest[] workQueueAll)
        {
            //First we get a list of every plugin that has a dependency that is already loaded

            CompilePluginRequest[] validRequests = workQueueAll.Where(x => x.CurrentStep == PluginCompileStage.ReadyToCompile).ToArray();

            if (validRequests.Length == 0)
            {
                return;
            }

            if (validRequests.Length > 1)
            {
                validRequests = validRequests.OrderBy(x => x.CompileOrder).ToArray();
            }

#if DEBUG
            _logger.Debug($"Compiling {string.Join(", ", validRequests.Select(x => x.Name).ToArray())}");
#endif
            CompilerRequest request = new CompilerRequest(this, _loader, _logger, validRequests);

            Dictionary<CompilePluginRequest, List<CompiledPlugin>> pluginReferenceAssemblies = new Dictionary<CompilePluginRequest, List<CompiledPlugin>>();

            foreach (CompilePluginRequest plugin in validRequests)
            {
                List<CompiledPlugin> pluginReferences = GetReverseReferences(plugin.PluginName);

                if (plugin.PluginReferences.Count > 0)
                {
                    lock (plugin.ReferenceLock)
                    {
                        foreach (PluginReference pluginRef in plugin.PluginReferences)
                        {
                            if (!_compiledPlugins.Exists(pluginRef.PluginName))
                            {
                                continue;
                            }

                            CompiledPlugin compiledPluginReference = _compiledPlugins.Find(pluginRef.PluginName);
                            if (compiledPluginReference.IsAvailable)
                            {
                                pluginReferences.Add(compiledPluginReference);
                            }
                        }
                    }
                }

                if (pluginReferences.Count > 0)
                {
                    if (pluginReferenceAssemblies.ContainsKey(plugin))
                    {
                        pluginReferenceAssemblies[plugin].AddRange(pluginReferences);
                    }
                    else
                    {
                        pluginReferenceAssemblies.Add(plugin, pluginReferences);
                    }
                }

                plugin.CurrentStep = PluginCompileStage.CompilerProcessing;
                plugin.OnCompilationStarted();
            }

            // Add per-transaction plugin requirement (without reloading plugin, only reloading if primary compilation succeeds)
            bool recompile = false;
            if (pluginReferenceAssemblies.Count > 0)
            {
                foreach (KeyValuePair<CompilePluginRequest, List<CompiledPlugin>> kvp in pluginReferenceAssemblies)
                {
                    foreach (CompiledPlugin pluginReferenceAssembly in kvp.Value)
                    {
                        if (request.Plugins.All(x => x.PluginName != pluginReferenceAssembly.PluginName) && pluginReferenceAssembly.IsAvailable)
                        {
                            CompilePluginRequest cpr = new CompilePluginRequest(_logger, pluginReferenceAssembly.Directory,
                                pluginReferenceAssembly.FileName, pluginReferenceAssembly.PluginName)
                            {
                                DependentCompilation = true,
                                FinishWhen = () => kvp.Key.CurrentStep >= PluginCompileStage.CompileSucceeded &&
                                                   kvp.Key.CurrentStep <= PluginCompileStage.Finished
                            };
                            cpr.SkipWhen = delegate ()
                            {
                                // Skip if dependency is not a child dependency, parent is finished, not skipped, and not dependent compilation
                                bool isChildDependency =
                                    cpr.PluginReferences.Any(x => x.PluginName == kvp.Key.PluginName);
                                return !isChildDependency && kvp.Key.IsFinished && !kvp.Key.IsSkipped && !kvp.Key.DependentCompilation;
                            };

                            cpr.Load(_gameExtensionName, _gameExtensionBranch);

                            request.Plugins.Add(cpr);
                            recompile = true;
                        }
                    }
                }
            }

            if (recompile)
            {
                Recompile(request.Plugins.ToArray());
            }
            else
            {
                _compiler.Compile(request);
            }
        }

        private void ReceiveCompileRequests()
        {
            while (_compiler.TryDequeueRequest(out CompilerRequest request))
            {
                if (request.Recompiling)
                {
                    continue;
                }

                if (request.ErrorMessages.Count > 0 || request.Plugins.Any(x => x.CurrentStep != PluginCompileStage.CompilerProcessing))
                {
                    ProcessFailedRequest(request);
                }

                ProcessSuccessfulRequest(request);
            }
        }

        private void ProcessFailedRequest(CompilerRequest request)
        {
            List<CompilePluginRequest> recompilePlugins = new List<CompilePluginRequest>();
            foreach (CompilePluginRequest plugin in request.Plugins)
            {
                if (request.ErrorMessages.ContainsKey(plugin.Name))
                {
                    plugin.CurrentStep = PluginCompileStage.CompileFailed;
                    plugin.OnCompilationFailed();

                    lock (plugin.ReferenceLock)
                    {
                        foreach (PluginReference pluginRef in plugin.PluginReferences)
                        {
                            // If this reference is in the compilation queue because of this plugin...
                            // It was unloaded, so we should recompile it without this plugin to reload it
                            CompiledPlugin compiledPlugin = _compiledPlugins.Find(pluginRef.PluginName);
                            CompilePluginRequest compileRequest =
                                request.Plugins.FirstOrDefault(x => x.PluginName == pluginRef.PluginName);
                            if (compileRequest != null &&
                                compiledPlugin?.LastGoodPlugin != null)
                            {
                                recompilePlugins.Add(compileRequest);
                            }
                        }
                    }
                }
            }

            if (recompilePlugins.Count > 0)
            {
#if DEBUG
                _logger.Debug("Recompiling because of failed request");
#endif
                RecompileAll(PluginCompileStage.AwaitingUnload, recompilePlugins.ToArray());
            }
        }

        private void ProcessSuccessfulRequest(CompilerRequest request)
        {
            if (request.DLLBytes == null)
            {
                string[] names = request.Plugins.OrderBy(x => x.CompileOrder).Select(x => x.Name).ToArray();
                if (request.ErrorMessages.Count == 0)
                {
                    _logger.Error(Interface.uMod.Strings.Compiler.AssemblyMissing.Interpolate(("names", string.Join(",", names))));
                }

                ProcessFailedRequest(request);
                return;
            }

            request.LoadedAssembly = Assembly.Load(request.DLLBytes);

            // After loading assembly, determine if types sought by promises have been fulfilled
            // If so, start over
            if (OnAssemblyResolved(request))
            {
                return;
            }

            //AssemblyRegister.Loaded(request.LoadedAssembly, request.DLLBytes);

            foreach (CompilePluginRequest plugin in request.Plugins)
            {
                if (plugin.CurrentStep == PluginCompileStage.CompilerProcessing)
                {
                    plugin.OnCompileRequestSuccess(request);
                }
            }
        }

        private void ProcessRecompileRequest(CompilerRequest request)
        {
            foreach (CompilePluginRequest plugin in request.Plugins)
            {
                plugin.OnCompileRequestRecompile(request);
            }
        }

        private bool OnAssemblyResolved(CompilerRequest request)
        {
            if (request.ResolvePromises(out List<string> promiseRemoval))
            {
                ProcessRecompileRequest(request);
                foreach (string promiseName in promiseRemoval)
                {
                    request.Plugins.Add(RecentRequests[promiseName]);
                }

#if DEBUG
                _logger.Debug("Recompiling due to explicit promise found");
#endif
                Recompile(request.Plugins.ToArray());

                request.OnRecompile();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Preprocess scripts
        /// </summary>
        /// <param name="workQueueAll"></param>
        private void ProcessScriptsAfterCompile(CompilePluginRequest[] workQueueAll)
        {
            foreach (CompilePluginRequest plugin in workQueueAll)
            {
                if (plugin.SkipWhen != null && plugin.SkipWhen())
                {
                    plugin.Skip();
                    continue;
                }

                if (plugin.FinishWhen != null && !plugin.FinishWhen())
                {
                    continue;
                }

                if (plugin.CurrentStep == PluginCompileStage.EnsureUnloaded)
                {
                    plugin.IsScriptLoaded = false;
                    bool any = false;
                    string message = string.Empty;
                    CompiledPlugin compiledPlugin = _compiledPlugins.Find(plugin.PluginName);
                    if ((compiledPlugin?.IsLoaded ?? false) && plugin.CanBeUnloaded && plugin.DependentCompilation)
                    {
                        _loader.PluginUnloaded(compiledPlugin.LastGoodPlugin);
                        any = true;
                        message += $" {compiledPlugin.PluginName}";
                    }

                    /*
                    foreach (PluginReference refPlugin in plugin.ReferencedPlugins)
                    {
                        if (_workQueue.Contains(refPlugin.PluginName))
                        {
                            CompiledPlugin refPluginImpl = _compiledPlugins.Find(plugin.PluginName);
                            //IPlugin refPluginImpl = Utility.Plugins.Find(refPlugin.PluginName);
                            if (refPluginImpl != null && refPluginImpl.IsLoaded)
                            {
                                message += $" {refPluginImpl.PluginName}";
                                any = true;
                            }
                        }
                    }
                    */

                    if (!any)
                    {
                        plugin.NextStep();
                        plugin.UnloadAttempts = 0;
                    }
                    else
                    {
                        plugin.UnloadAttempts++;

                        if (plugin.UnloadAttempts > 5)
                        {
                            plugin.EncounteredError(
                                Interface.uMod.Strings.PluginLoader.PluginLoadFailureMessage.Interpolate(
                                    ("plugin", plugin.PluginName), ("message", $"Not unloaded {message}"))); // TODO: Localization
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Create instance of a plugin class
        /// </summary>
        /// <param name="workQueueAll"></param>
        private void CreatePluginInstances(CompilePluginRequest[] workQueueAll)
        {
            List<CompilePluginRequest> delayedRequests = new List<CompilePluginRequest>();
            bool anyReferenceFails = false;
            foreach (CompilePluginRequest plugin in workQueueAll.OrderBy(x => x.LoadOrder))
            {
                //First we load the compiled bytes into memory as an assembly
                if (plugin.CurrentStep == PluginCompileStage.FindPluginType)
                {
                    // This isn't even named right, this finds the plugin type
                    if (plugin.FindPluginType())
                    {
                        plugin.NextStep();
                    }
                }

                // Check if requires attributes are available
                if (plugin.CurrentStep == PluginCompileStage.ValidateRequires)
                {
                    if (plugin.ValidateRequires(_workQueue.Requests.Keys))
                    {
                        plugin.ReferenceRetries = 0;
                        plugin.NextStep();
                    }
                }

                // Check if plugin is already unloaded and skip
                /*
                if (plugin.CurrentStep == PluginCompileStage.EnsureUnloaded)
                {
                    CompiledPlugin compiledPlugin = _compiledPlugins.Find(plugin.PluginName);
                    if ((compiledPlugin?.IsLoaded ?? false) == false)
                    {
                        plugin.NextStep();
                    }
                }
                */

                //Then we create the plugin
                if (plugin.CurrentStep == PluginCompileStage.CreatePluginInstance)
                {
                    if (plugin.IsLoaded)
                    {
                        continue;
                    }

                    lock (plugin.ReferenceLock)
                    {
                        foreach (PluginReference pluginReference in plugin.PluginReferences)
                        {
                            // Fail-safe to prevent type mismatch
                            CompiledPlugin loadedPlugin = _compiledPlugins.Find(pluginReference.PluginName);
                            if (_workQueue.Contains(pluginReference.PluginName) && (loadedPlugin?.IsLoaded ?? false))
                            {
#if DEBUG
                                _logger.Debug("Recompiling to prevent type mismatch");
#endif
                                RecompileAll(PluginCompileStage.AwaitingUnload);
                                return;
                            }
                        }

                        // Assuming any plugin types may be resolved by queued assemblies, delay Assembly.Load until base assemblies are loaded
                        if (plugin.PluginReferences.Any(ValidateReferenceInstances))
                        {
                            anyReferenceFails = true;
#if DEBUG
                            _logger.Debug($"Delaying plugin creation {plugin.PluginName}");
#endif
                            delayedRequests.Add(plugin);
                        }
                        else
                        {
                            plugin.CreatePlugin(_loader);
                            plugin.NextStep();
                        }
                    }
                }
            }

            int retries = 0;

            if (!anyReferenceFails)
            {
                // Load referenced assemblies
                List<CompilePluginRequest> newDelayedPlugins = new List<CompilePluginRequest>();
                while (delayedRequests.Count > 0)
                {
                    foreach (CompilePluginRequest plugin in delayedRequests)
                    {
                        if (plugin.CurrentStep == PluginCompileStage.CreatePluginInstance)
                        {
                            if (plugin.PluginReferences.Any(ValidateReferenceInstances))
                            {
#if DEBUG
                                _logger.Debug($"Delaying plugin creation1 {plugin.PluginName}");
#endif
                                newDelayedPlugins.Add(plugin);
                                retries++;
                            }
                            else
                            {
                                plugin.CreatePlugin(_loader);
                                plugin.NextStep();
                            }
                        }
                    }

                    if (retries > 6)
                    {
                        foreach (CompilePluginRequest plugin in delayedRequests)
                        {
                            plugin.EncounteredError("Expected type not resolved.  Does dependency depth exceed 6?");
                        }

                        break;
                    }

                    delayedRequests = newDelayedPlugins;
                    Thread.Sleep(10);
                }
            }
        }

        /// <summary>
        /// Ensure instance references are properly resolved
        /// </summary>
        /// <param name="pluginReference"></param>
        /// <returns></returns>
        private bool ValidateReferenceInstances(PluginReference pluginReference)
        {
            if (pluginReference.Required)
            {
                CompilePluginRequest cpr = _workQueue.Find(pluginReference.PluginName);

                if (cpr == null)
                {
                    return false;
                }

                if (cpr.CurrentStep < PluginCompileStage.CreatePluginInstance)
                {
                    return true;
                }

                if (cpr.CurrentStep == PluginCompileStage.CreatePluginInstance)
                {
                    return false;
                }

                if (!_compiledPlugins.Exists(cpr.PluginName))
                {
                    return true;
                }

                if (!cpr.IsLoaded)
                {
                    return true;
                }

                CompiledPlugin compiledPlugin = _compiledPlugins.Find(cpr.PluginName);

                if (!compiledPlugin.IsAvailable)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Remove queued compilations from queue if they have failed or finished
        /// </summary>
        /// <param name="workQueueAll"></param>
        private void DoRemoves(CompilePluginRequest[] workQueueAll)
        {
            bool didRemove = false;
            //Ran on second thread so safe to loop through, only have to lock when accessing a main thread loop
            if (workQueueAll.Length == 0)
            {
                return;
            }

            foreach (CompilePluginRequest plugin in workQueueAll)
            {
                if (plugin.HasFailed)
                {
                    CancelPendingDependencies(plugin);
                }

                if (plugin.IsFinished)
                {
                    didRemove = true;
                    lock (_workQueue)
                    {
                        //Done working on it
                        _workQueue.Remove(plugin.PluginName);
                    }

                    if (!plugin.IsSkipped)
                    {
                        lock (_completedQueue)
                        {
                            _completedQueue.Add(plugin);
                        }
                    }
                }
            }

            if (!didRemove)
            {
                return;
            }

            //Prevents us from running a linq query every time
            lock (_workQueue)
            {
                foreach (string name in _workQueue.Requests.Where(x => x.Value.IsFinished).Select(x => x.Key))
                {
                    _workQueue.Remove(name);
                }
            }
        }

        private void CancelPendingDependencies(CompilePluginRequest plugin)
        {
            HashSet<string> cancelled = new HashSet<string> { plugin.Name };
            Queue<CompilePluginRequest> cancelQueue = new Queue<CompilePluginRequest>();

            cancelQueue.Enqueue(plugin);

            while (cancelQueue.Count > 0)
            {
                CompilePluginRequest cancelling = cancelQueue.Dequeue();

                if (cancelling == null)
                {
                    break;
                }

                // Here is where we actually cancel it
                lock (_workQueue)
                {
                    _workQueue.Remove(cancelling.PluginName);
                }

                lock (cancelling.ReferenceLock)
                {
                    foreach (PluginReference dependency in cancelling.PluginReferences)
                    {
                        // Only cancel pending load orders for plugins that are loaded
                        if (dependency.Optional || TypePromiseManager.TypePromises.ContainsKey(dependency.PluginName))
                        {
                            continue;
                        }

                        CompilePluginRequest dependencyPlugin = _workQueue.Find(dependency.PluginName);

                        if (cancelled.Add(dependency.PluginName))
                        {
                            cancelling.EncounteredError(Interface.uMod.Strings.Compiler.CanceledPending.Interpolate(
                                ("dependency", cancelling.PluginName), ("plugin", dependency.PluginName)));
                            cancelQueue.Enqueue(dependencyPlugin);
                        }
                    }
                }
            }
        }

        #endregion Compile Thread

        /// <summary>
        /// Normalize plugin name
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string NormalizePluginName(string fileName)
        {
            return Path.GetFileNameWithoutExtension(fileName)?.Replace("_", "");
        }
    }
}
