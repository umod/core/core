﻿using System.Collections.Generic;
using System.IO;

namespace uMod.Plugins.Compiler
{
    internal class AssemblyCache
    {
        /// <summary>
        /// Cached assembly references
        /// </summary>
        private readonly Dictionary<string, AssemblyReference> _cachedDlls = new Dictionary<string, AssemblyReference>();

        /// <summary>
        /// Checks if assembly is already cached
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal bool Contains(string name)
        {
            name = Path.GetFileName(name);
            return _cachedDlls.ContainsKey(name);
        }

        /// <summary>
        /// G
        /// </summary>
        /// <returns></returns>
        internal IEnumerable<string> GetNames() => _cachedDlls?.Keys;

        /// <summary>
        /// Gets assembly reference by path
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal AssemblyReference Find(string name)
        {
            name = Path.GetFileName(name);
            _cachedDlls.TryGetValue(name, out AssemblyReference dll);
            return dll;
        }

        /// <summary>
        /// Gets assembly reference by path
        /// </summary>
        /// <param name="name"></param>
        /// <param name="dll"></param>
        /// <returns></returns>
        internal bool TryFind(string name, out AssemblyReference dll)
        {
            return _cachedDlls.TryGetValue(name, out dll);
        }

        /// <summary>
        /// Adds assembly reference
        /// </summary>
        /// <param name="reference"></param>
        /// <returns></returns>
        internal bool Add(AssemblyReference reference)
        {
            if (_cachedDlls.ContainsKey(reference.Name))
            {
                return false;
            }

            reference.IsCached = true;

            _cachedDlls[reference.FileName] = reference;

            return true;
        }
    }
}
