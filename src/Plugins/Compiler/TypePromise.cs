﻿using System.Collections.Generic;
using uMod.Common;

namespace uMod.Plugins.Compiler
{
    /// <summary>
    /// Manages type promises
    /// </summary>
    internal static class TypePromiseManager
    {
        public static Dictionary<string, ITypePromise> TypePromises = new Dictionary<string, ITypePromise>();

        /// <summary>
        /// Add a type promise
        /// </summary>
        /// <param name="typeName"></param>
        /// <param name="sourcePlugin"></param>
        public static void AddPromise(string[] typeNames, string sourcePlugin)
        {
            if (!TypePromises.ContainsKey(sourcePlugin))
            {
                TypePromises.Add(sourcePlugin, new TypePromise()
                {
                    TypeNames = typeNames,
                    SourcePlugin = sourcePlugin
                });
            }
        }

        /// <summary>
        /// Remove a type promise
        /// </summary>
        /// <param name="sourcePlugin"></param>
        public static void RemovePromise(string sourcePlugin)
        {
            TypePromises.Remove(sourcePlugin);
        }
    }

    /// <summary>
    /// Represents a type promise
    /// </summary>
    internal interface ITypePromise
    {
        string[] TypeNames { get; }
        string SourcePlugin { get; }

        bool IsResolved(IPlugin plugin);
    }

    /// <summary>
    /// Implements a basic type promise
    /// </summary>
    internal class TypePromise : ITypePromise
    {
        public string[] TypeNames { get; set; }
        public string SourcePlugin { get; set; }

        public bool IsResolved(IPlugin plugin)
        {
            foreach (string typeName in TypeNames)
            {
                if (Interface.uMod.Application.Resolved(typeName))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
