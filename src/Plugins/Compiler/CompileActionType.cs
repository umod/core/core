﻿namespace uMod.Plugins.Compiler
{
    /// <summary>
    /// Represents possible compilation actions
    /// </summary>
    public enum CompileActionType
    {
        Load = 0,
        Unload = 1,
        Reload = 2
    }
}
