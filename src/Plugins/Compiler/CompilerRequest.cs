using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using uMod.Common;
using uMod.Common.Compiler;

namespace uMod.Plugins.Compiler
{
    /// <summary>
    /// Represents a request to the compiler
    /// </summary>
    internal class CompilerRequest
    {
        /// <summary>
        /// Gets the list of plugin requests within the compiler request
        /// </summary>
        internal List<CompilePluginRequest> Plugins { get; } = new List<CompilePluginRequest>();

        /// <summary>
        /// Gets the id number of the request
        /// </summary>
        internal int RequestId { get; private set; }

        /// <summary>
        /// Gets the time the request was made
        /// </summary>
        internal DateTime StartedAt { get; private set; }

        /// <summary>
        /// Gets the time the requested ended
        /// </summary>
        internal DateTime EndedAt { get; private set; }

        /// <summary>
        /// Gets the name of the dll associated with the request
        /// </summary>
        internal string DLLName { get; private set; }

        /// <summary>
        /// Gets the array of bytes associated with the request
        /// </summary>
        internal byte[] DLLBytes { get; private set; }

        /// <summary>
        /// Gets the assembly loaded by the request
        /// </summary>
        internal Assembly LoadedAssembly { get; set; }

        /// <summary>
        /// The logger used by the request
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// The compiler client that created the request
        /// </summary>
        private readonly Client _compiler;

        /// <summary>
        /// Determines whether or not the request has completed
        /// </summary>
        internal bool Completed = false;

        /// <summary>
        /// Determines whether or not the request is marked for recompilation
        /// </summary>
        internal bool Recompiling { get; private set; }

        /// <summary>
        /// Gets the number of times the request has recompiled
        /// </summary>
        internal int RecompileAttempts;

        /// <summary>
        /// Gets the list of warnings created by the request
        /// </summary>
        internal Dictionary<string, List<string>> WarningMessages { get; set; } = new Dictionary<string, List<string>>();

        /// <summary>
        /// Gets the list of errors created by the request
        /// </summary>
        internal Dictionary<string, List<string>> ErrorMessages { get; set; } = new Dictionary<string, List<string>>();

        /// <summary>
        /// The compilable plugin loader
        /// </summary>
        private readonly CompilablePluginLoader _loader;

        /// <summary>
        /// The regular expression to parse error messages
        /// </summary>
        private static readonly Regex FileErrorRegex = new Regex(@"([\w\.]+): \((\d+)\,(\d+)\+?\): (error \w+: .*)");

        /// <summary>
        /// The regular expression to parse warning messages
        /// </summary>
        private static readonly Regex FileWarningRegex = new Regex(@"([\w\.]+): \((\d+)\,(\d+)\+?\): (warning \w+: .*)");

        /// <summary>
        /// The regular expression to parse compiler type promises
        /// </summary>
        private static readonly Regex CompilationPromiseRegex = new Regex(@"uMod.Promise\(([\w\.]+),([\w\.]+)\).*");

        /// <summary>
        /// List of types that are promised
        /// </summary>
        private List<string> awaitingPromisesList = new List<string>();

        /// <summary>
        /// Creates a new instance of the CompilerRequest class
        /// </summary>
        /// <param name="compiler"></param>
        /// <param name="loader"></param>
        /// <param name="logger"></param>
        /// <param name="pluginsToCompile"></param>
        internal CompilerRequest(Client compiler, CompilablePluginLoader loader, ILogger logger, IEnumerable<CompilePluginRequest> pluginsToCompile)
        {
            _compiler = compiler;
            _loader = loader;
            _logger = logger;
            Plugins.AddRange(pluginsToCompile);
        }

        /// <summary>
        /// Notifies request when the compiler queues this request and with what id
        /// </summary>
        /// <param name="requestId"></param>
        internal void OnQueued(int requestId)
        {
            DLLName = (Plugins.Count < 2 ? Plugins.First().Name : "plugins_") + Math.Round(Interface.uMod.Now * 10000000f) + ".dll";
            RequestId = requestId;
        }

        /// <summary>
        /// Notified when the request is sent to the compiler
        /// </summary>
        internal void OnSentToCompiler()
        {
            RecompileAttempts++;
            ErrorMessages.Clear();
            WarningMessages.Clear();
            Recompiling = false;
            StartedAt = DateTime.UtcNow;
        }

        /// <summary>
        /// Notified when the request is recompiled
        /// </summary>
        internal void OnRecompile()
        {
            LoadedAssembly = null;
            Recompiling = true;
            ErrorMessages.Clear();
            WarningMessages.Clear();
        }

        /// <summary>
        /// Attempts to resolve existing promises using the loaded assembly
        /// </summary>
        /// <param name="promiseRemoval"></param>
        /// <returns></returns>
        internal bool ResolvePromises(out List<string> promiseRemoval)
        {
            promiseRemoval = new List<string>();
            foreach (KeyValuePair<string, ITypePromise> kvp in TypePromiseManager.TypePromises)
            {
                foreach (string typeName in kvp.Value.TypeNames)
                {
                    Type type = LoadedAssembly.GetType(typeName, false, true);
                    if (Plugins.Any(x => $"{x.Name}.cs" == kvp.Key || x.Name == kvp.Key))
                    {
                        TypePromiseManager.TypePromises.Remove(kvp.Key);
                        continue;
                    }
                    if (type != null)
                    {
                        promiseRemoval.Add(kvp.Key.EndsWith(_loader.FileExtension) ? kvp.Key.Basename(_loader.FileExtension) : kvp.Key);
                    }
                }
            }

            return promiseRemoval.Count > 0;
        }

        internal void AddWarning(string message)
        {
            AddWarning(string.Empty, message);
        }

        internal void AddWarning(string key, string message)
        {
            if (!WarningMessages.TryGetValue(key, out List<string> warnings))
            {
                WarningMessages.Add(key, warnings = new List<string>());
            }

            warnings.Add(message);
        }

        internal void AddError(string message)
        {
            AddError(string.Empty, message);
        }

        internal void AddError(string key, string message)
        {
            if (!ErrorMessages.TryGetValue(key, out List<string> errors))
            {
                ErrorMessages.Add(key, errors = new List<string>());
            }

            errors.Add(message);
        }

        /// <summary>
        /// Notified when the compiler responds to the request
        /// </summary>
        /// <param name="message"></param>
        internal void OnResponseFromCompiler(CompilationMessage message)
        {
            EndedAt = DateTime.UtcNow;

            string stdOutput = (string)message.ExtraData;
            string warningOutput = (string)message.ExtraData2;

            DLLBytes = message.Data as byte[];

            if (stdOutput != null)
            {
                FilterCompilerOutput(stdOutput);
            }

            if (warningOutput != null)
            {
                FilterCompilerWarnings(warningOutput);
            }

            if (LoadedAssembly == null && message.Type == CompilationMessageType.Error)
            {
                AddError(!string.IsNullOrEmpty(stdOutput) ? stdOutput : Interface.uMod.Strings.Compiler.ErrorMissingInfo);
            }
        }

        #region Filtering Compiler Output

        /// <summary>
        /// Filters the specified compiler output
        /// </summary>
        /// <param name="compilerOutput"></param>
        private void FilterCompilerOutput(string compilerOutput)
        {
            awaitingPromisesList.Clear();
            string[] lines = compilerOutput.Split(Environment.NewLine.ToCharArray());
            bool anyPromise = false;
            foreach (string line in lines)
            {
                if (string.IsNullOrEmpty(line)) continue;

                if (LookForError(line))
                {
                    continue;
                }

                if (HandlePromises(line))
                {
                    anyPromise = true;
                    continue;
                }

                LookForWarning(line);
            }

            if (anyPromise)
            {
                LoadedAssembly = null;

#if DEBUG
                Interface.uMod.LogDebug("Recompiling due to compiler promise");
#endif

                _compiler.RecompileAll(PluginCompileStage.AwaitingUnload, Plugins.ToArray());
                OnRecompile();
            }
        }

        /// <summary>
        /// Filters the specified compiler warning output
        /// </summary>
        /// <param name="warnings"></param>
        private void FilterCompilerWarnings(string warnings)
        {
            foreach (string line in warnings.Split(Environment.NewLine.ToCharArray()))
            {
                LookForWarning(line);
            }
        }

        /// <summary>
        /// Handle promises in output
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        private bool HandlePromises(string line)
        {
            Match promiseMatch = CompilationPromiseRegex.Match(line.Trim());

            if (promiseMatch.Groups.Count == 3)
            {
                string originalFullTypeName = promiseMatch.Groups[1].Value;
                List<string> typeFullNames = new List<string>
                {
                    originalFullTypeName
                };

                if (originalFullTypeName.Contains("uMod.Plugins."))
                {
                    typeFullNames.Add(originalFullTypeName.Replace("uMod.Plugins.", "Oxide.Plugins."));
                }
                string pluginFile = promiseMatch.Groups[2].Value;

                string scriptName = pluginFile.Substring(0, pluginFile.Length - 3);
                CompilePluginRequest compilablePlugin = Plugins.SingleOrDefault(pl => pl.FileName == scriptName);

                if (compilablePlugin != null && !compilablePlugin.DependentCompilation)
                {
                    foreach (string typeFullName in typeFullNames)
                    {
                        // If type already loaded, do not load type promise
                        // Instead, recompile with promised type immediately
                        if (Interface.uMod.Application.Resolved(typeFullName) &&
                            Interface.uMod.Application.TryGetType(typeFullName, out Type pluginType) &&
                            _compiler.TryGetCompiledPlugin(pluginType.Name, out CompiledPlugin compiledPluginReference) &&
                            compiledPluginReference.IsLoaded && compiledPluginReference.IsAvailable)
                        {
                            if (compilablePlugin.TryAddPluginReference(compiledPluginReference.PluginName, PluginReferenceType.ExplicitRequires))
                            {
                                return true; // Recompile immediately
                            }

                            return false; // Do not recompile
                        }
                    }

                    string awaitingTypeMessage =
                        Interface.uMod.Strings.Compiler.AwaitingType.Interpolate(("name", pluginFile),
                            ("type", string.Join(", ", typeFullNames.ToArray())));

                    // Do not show awaiting promise error twice in the same compilation if it's the same type
                    if (!awaitingPromisesList.Contains(originalFullTypeName))
                    {
                        compilablePlugin.EncounteredError(awaitingTypeMessage);
                        awaitingPromisesList.Add(originalFullTypeName);
                    }

                    foreach (string typeFullName in typeFullNames)
                    {
                        string dependentPlugin = typeFullName.Replace("uMod.Plugins.", string.Empty);

                        if (Plugins.Any(x => x.PluginName == dependentPlugin))
                        {
                            // If we didn't already know about this reference, recompile immediately
                            if (!compilablePlugin.TryAddPluginReference(dependentPlugin,
                                PluginReferenceType.ExplicitRequires))
                            {
                                return true;
                            }
                        }

                        if (File.Exists(Path.Combine(Interface.uMod.PluginDirectory, $"{dependentPlugin}.cs")))
                        {
                            if (!compilablePlugin.TryAddPluginReference(dependentPlugin,
                                PluginReferenceType.ExplicitRequires))
                            {
                                CompiledPlugin compiledPlugin = _compiler.GetCompiledPlugin(dependentPlugin);
                                if (compiledPlugin?.IsLoaded ?? false)
                                {
                                    return true;
                                }

                                CompilePluginRequest cpr = new CompilePluginRequest(_logger, Interface.uMod.PluginDirectory,
                                    dependentPlugin, dependentPlugin);

                                cpr.Load(_compiler._gameExtensionName, _compiler._gameExtensionBranch);

                                Plugins.Add(cpr);
                                return true;
                            }
                        }
                    }

                    AddError(Path.GetFileNameWithoutExtension(pluginFile), awaitingTypeMessage);
                    TypePromiseManager.AddPromise(typeFullNames.ToArray(), pluginFile);
                }
            }

            return false;
        }

        /// <summary>
        /// We need to look through error messages and apply them to the plugin they reference which will cancel the CompileRequest
        /// </summary>
        /// <param name="line"></param>
        private bool LookForError(string line)
        {
            Match errorMatch = FileErrorRegex.Match(line.Trim());
            if (errorMatch.Groups.Count == 5)
            {
                string sourceFile = errorMatch.Groups[1].Value;
                string lineNumberString = errorMatch.Groups[2].Value;
                string lineColumnString = errorMatch.Groups[3].Value;
                string errorMessage = errorMatch.Groups[4].Value;

                if (sourceFile.Trim() == string.Empty)
                {
                    return false;
                }

                string fileName = sourceFile.Basename();
                string scriptName = fileName.Substring(0, fileName.Length - 3);
                CompilePluginRequest targetPlugin = Plugins.SingleOrDefault(pl => pl.FileName == scriptName);

                if (targetPlugin == null)
                {
                    AddError(Path.GetFileNameWithoutExtension(sourceFile), Interface.uMod.Strings.Compiler.InvalidScriptError.Interpolate("line", line));
                    return true;
                }

                if (int.TryParse(lineNumberString, out int lineNumber))
                {
                    lineNumber -= targetPlugin.LinesAddedToScript;
                    lineNumberString = lineNumber.ToString();
                }

                string newMessage = $"{sourceFile}: ({lineNumberString}, {lineColumnString}): {errorMessage}";

                targetPlugin.EncounteredError(newMessage);
                AddError(Path.GetFileNameWithoutExtension(sourceFile), newMessage);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Find warnings and apply them to the plugin they reference
        /// </summary>
        /// <param name="line"></param>
        private void LookForWarning(string line)
        {
            Match warningMatch = FileWarningRegex.Match(line.Trim());
            if (warningMatch.Groups.Count == 5)
            {
                string sourceFile = warningMatch.Groups[1].Value;
                string lineNumberString = warningMatch.Groups[2].Value;
                string lineColumnString = warningMatch.Groups[3].Value;
                string warningMessage = warningMatch.Groups[4].Value;

                if (sourceFile.Trim() == string.Empty)
                {
                    return;
                }

                string fileName = sourceFile.Basename();
                string scriptName = fileName.Substring(0, fileName.Length - 3);
                CompilePluginRequest targetPlugin = Plugins.SingleOrDefault(pl => pl.FileName == scriptName);

                if (targetPlugin == null)
                {
                    AddError(Path.GetFileNameWithoutExtension(sourceFile), Interface.uMod.Strings.Compiler.InvalidScriptWarning.Interpolate("line", line));
                    return;
                }

                if (int.TryParse(lineNumberString, out int lineNumber))
                {
                    lineNumber -= targetPlugin.LinesAddedToScript;
                    lineNumberString = lineNumber.ToString();
                }

                string newMessage =
                    $"{sourceFile}: ({lineNumberString}, {lineColumnString}): {warningMessage}";

                AddWarning(Path.GetFileNameWithoutExtension(sourceFile), newMessage);
                targetPlugin.EncounteredWarning(Environment.NewLine + newMessage.Trim());
            }
        }

        /// <summary>
        /// Notified when the request failed
        /// </summary>
        /// <param name="reason"></param>
        internal void OnCompilerFailed(string reason)
        {
            foreach (CompilePluginRequest plugin in Plugins)
            {
                plugin.EncounteredError("Compiler failed: " + reason);
            }
        }

        #endregion Filtering Compiler Output
    }
}
