﻿using System.Collections.Generic;

namespace uMod.Plugins.Compiler
{
    internal class CompiledPluginList
    {
        private readonly List<CompiledPlugin> _list = new List<CompiledPlugin>();
        private readonly Dictionary<string, CompiledPlugin> _dict = new Dictionary<string, CompiledPlugin>();

        /// <summary>
        /// Gets an enumerable list of all compiled plugins
        /// </summary>
        internal IEnumerable<CompiledPlugin> All { get { return _list; } }

        /// <summary>
        /// Determines if a plugin is compiled with the specified name
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        internal bool Exists(string pluginName)
        {
            return _dict.ContainsKey(pluginName);
        }

        /// <summary>
        /// Tries to get a compiled plugin reference using the specified name
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        internal CompiledPlugin Find(string pluginName)
        {
            _dict.TryGetValue(pluginName, out CompiledPlugin plugin);

            return plugin;
        }

        /// <summary>
        /// Adds a compiled plugin reference
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns></returns>
        internal bool Add(CompiledPlugin plugin)
        {
            _list.Add(plugin);
            _dict.Add(plugin.PluginName, plugin);

            return true;
        }

        /// <summary>
        /// Removes a compile plugin reference with the specified name
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        internal bool Remove(string pluginName)
        {
            return _dict.Remove(pluginName) && _list.RemoveAll(x => x.PluginName == pluginName) > 0;
        }

        /// <summary>
        /// Clear all compiled plugin references
        /// </summary>
        internal void Clear()
        {
            _list.Clear();
            _dict.Clear();
        }
    }
}
