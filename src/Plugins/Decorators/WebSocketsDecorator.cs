﻿extern alias References;

using System;
using System.Linq;
using References::Newtonsoft.Json;
using uMod.Common.WebSockets;
using uMod.Configuration;
using uMod.Logging;
using uMod.WebSockets;
using static uMod.WebSockets.RconAdminPlugin;

namespace uMod.Plugins.Decorators
{
    public class WebSocketsDecorator : HookDecorator
    {
        string ServerName { get; }
        public WebSocketsDecorator(Plugin plugin, string name) : base(plugin)
        {
            ServerName = name;
        }

        /// <summary>
        /// Called when the socket server associated with the connection name receives a packet
        /// </summary>
        [Hook("OnSocketMessageReceived")]
        private void OnSocketMessageReceived(string serverName, SocketConnection connection, Packet packet)
        {
            if (!serverName.Equals(ServerName, StringComparison.InvariantCultureIgnoreCase))
            {
                return;
            }
            object packetImpl = null;
            if (packet?.Type == null)
            {
                return;
            }

            if (packet.Type.Equals("Command", StringComparison.InvariantCultureIgnoreCase))
            {
                packetImpl = new ServerCommand()
                {
                    Command = packet.Message
                };
            }
            else if (packet.Type.Equals("Commands", StringComparison.InvariantCultureIgnoreCase)) // Run multiple commands to minimize overhead
            {
                packetImpl = new ServerCommands()
                {
                    Commands = packet.Message.Split('\n')
                };
            }
            else if (packet.Type.Equals("Chat", StringComparison.InvariantCultureIgnoreCase)) // Simplest chat protocol for backwards compatibility
            {
                packetImpl = new PlayerChat()
                {
                    Message = packet.Message
                };
            }
            else if (!string.IsNullOrEmpty(packet.Type) && Plugin.Meta.Mediator.TryGetTypes($"Packet.{packet.Type}", out var types))
            {
                Type type = types.First();
                bool packetDeserialized = false;
                try
                {
                    if (!string.IsNullOrEmpty(packet.Message))
                    {
                        packetImpl = JsonConvert.DeserializeObject(packet.Message, type);
                        packetDeserialized = true;
                    }
                }
                catch (Exception)
                {
                }
                finally
                {
                    if (!packetDeserialized)
                    {
                        packetImpl = Interface.uMod.Application.Make<object>(type);
                    }
                }
            }

            if (packetImpl != null) // Mediator found something to deserialize to
            {
                if (packetImpl is IReceivablePacket socketPacket)
                {
                    socketPacket.OnReceived(connection);
                }
                else
                {
                    Plugin.CallHook("OnSocketMessage", connection, packetImpl, packet);
                }
            }
            else // No packet class, just send raw data let them figure it out
            {
                Plugin.CallHook("OnSocketMessage", connection, packet);
            }
        }

        private Action OnServerStopped = null;

        [Hook("OnSocketServerStateChanged")]
        private void OnSocketServerStateChanged(string serverName, ConnectionState state)
        {
            if (!serverName.Equals(ServerName, StringComparison.InvariantCultureIgnoreCase))
            {
                return;
            }

            switch (state)
            {
                case ConnectionState.Closed:
                    Plugin.CallHook("OnSocketServerStopped");
                    OnServerStopped?.Invoke();
                    break;
                case ConnectionState.Open:
                    Plugin.CallHook("OnSocketServerStarted");
                    if (Interface.uMod.Web.Configuration.Servers.TryGetValue(serverName, out IWebServer webServer) &&
                       webServer is IWebSocketServer webSocketServer &&
                       !string.IsNullOrEmpty(webSocketServer.Logger) &&
                       Interface.uMod.Logging.Channels.TryGetValue(webSocketServer.Logger, out ILogChannel logChannel) &&
                       logChannel?.Logger is StackLogger stackLogger)
                    {
                        WebSocketPipe pipe = new WebSocketPipe(serverName);
                        stackLogger.AddLogger(pipe);
                        OnServerStopped = delegate ()
                        {
                            stackLogger.RemoveLogger(pipe);
                        };
                    }
                    break;
            }
        }

        public override void Unsubscribe()
        {
            OnServerStopped?.Invoke();
            base.Unsubscribe();
        }
    }
}
