﻿namespace uMod.Plugins.Decorators
{
    internal class DependencyDecorator : HookDecorator
    {
        public DependencyDecorator(Plugin plugin) : base(plugin)
        {
        }

        [Hook("OnPluginLoaded")]
        private void OnPluginLoaded(Plugin plugin)
        {
            if (Plugin.Meta.PluginReferenceFields.TryGetValue(plugin.Name, out PluginMeta.PluginReference pluginReference))
            {
                pluginReference.Loaded(plugin);
            }
        }

        [Hook("OnPluginUnloaded")]
        private void OnPluginUnloaded(Plugin plugin)
        {
            if (Plugin.Meta.PluginReferenceFields.TryGetValue(plugin.Name, out PluginMeta.PluginReference pluginReference))
            {
                pluginReference.Unloaded();
            }
        }
    }
}
