﻿using uMod.Libraries;
using uMod.Logging;

namespace uMod.Plugins.Decorators
{
    public class ServerDecorator : HookDecorator
    {
        private readonly Universal _universal;
        private bool _serverInitialized;
        private bool _loggingInitialized;

        public ServerDecorator(Plugin plugin, Universal universal) : base(plugin)
        {
            _universal = universal;
        }

        /// <summary>
        /// Called when the plugin is initializing
        /// </summary>
        [Hook("Init")]
        private void Init()
        {
            // Configure remote error logging
            SentryLogger.SetTag("game", _universal.Game.ToLowerInvariant());
            SentryLogger.SetTag("game version", _universal.Server.Version);
        }

        [Hook("InitLogging")]
        private void InitLogging()
        {
            if (!_loggingInitialized)
            {
                Interface.uMod.OnLoggingInitialized.Invoke();
                _loggingInitialized = true;
            }
        }

        /// <summary>
        /// Called when the server is first initialized
        /// </summary>
        [Hook("IOnServerInitialized")]
        private void IOnServerInitialized()
        {
            if (!_serverInitialized)
            {
                // Let plugins know server startup is complete
                Interface.CallHook("OnServerInitialized", _serverInitialized = true);

                Plugin.logger.Info(Interface.uMod.Strings.Server.VersionLog.Interpolate(
                    ("version", Module.Version),
                    ("game", _universal.Game),
                    ("serverVersion", _universal.Server.Version)
                ));

                if (!Interface.uMod.IsTesting && (Interface.uMod?.Telemetry?.Analytics?.Analytics ?? true))
                {
                    Analytics.Collect();
                }
            }
        }

        [After("OnPluginLoaded")]
        private void OnPluginLoadedAfter(Plugin plugin)
        {
            if (_serverInitialized)
            {
                // Call OnServerInitialized for hotloaded plugins
                plugin.CallHook("OnServerInitialized", false);
            }
        }

        /// <summary>
        /// Called when the server is saved
        /// </summary>
        [Hook("OnServerSave")]
        private void OnServerSave()
        {
            // Trigger save process
            Interface.uMod.OnSave();
        }

        /// <summary>
        /// Called when the server is shutting down
        /// </summary>
        [Hook("IOnServerShutdown")]
        private void IOnServerShutdown()
        {
            // Let plugins know about server shutdown
            Interface.CallHook("OnServerShutdown");

            // Trigger shutdown process
            Interface.uMod.OnShutdown();
        }
    }
}
