﻿using System;
using System.Collections.Generic;
using uMod.Common;

namespace uMod.Plugins.Decorators
{
    internal class OnlinePlayersDecorator : HookDecorator
    {
        private object reconnectLock = new object();
        private readonly Dictionary<string, Timer> _reconnected = new Dictionary<string, Timer>();
        public OnlinePlayersDecorator(Plugin plugin) : base(plugin)
        {
        }

        [Hook("OnPlayerConnected")]
        private void OnPlayerConnected(IPlayer player)
        {
            lock (reconnectLock)
            {
                if (_reconnected.TryGetValue(player.Id, out Timer timer))
                {
                    if (!timer.Destroyed)
                    {
                        timer.Destroy();
                    }

                    _reconnected.Remove(player.Id);
                }

                _reconnected.Add(player.Id, Plugin.Timer.Once(5, delegate ()
                {
                    _reconnected.Remove(player.Id);
                }));
            }

            AddOnlinePlayer(player);
        }

        [Hook("OnPlayerDisconnected")]
        private void OnPlayerDisconnected(IPlayer player)
        {
            lock (reconnectLock)
            {
                if (_reconnected.TryGetValue(player.Id, out Timer timer))
                {
                    if (!timer.Destroyed)
                    {
                        timer.Destroy();
                    }

                    _reconnected.Remove(player.Id);
                }
            }
            // Delay removing player until OnPlayerDisconnected has fired in plugin
            Interface.uMod.NextTick(() =>
            {
                RemoveOnlinePlayer(player);
            });
        }

        private void RemoveOnlinePlayer(IPlayer player)
        {
            foreach (PluginFieldInfo pluginField in Plugin.Meta.OnlinePlayerFields)
            {
                object val = pluginField.Call("get_Item", player);
                if (!string.IsNullOrEmpty(pluginField.Directory))
                {
                    Plugin._files.WriteObject(pluginField.GetFilePath(player), val).Done(delegate ()
                    {
                        lock (reconnectLock)
                        {
                            if (_reconnected.ContainsKey(player.Id)) // Player reconnected (very quickly)
                            {
                                return;
                            }

                            if (pluginField.PlayerType == typeof(IPlayer))
                            {
                                pluginField.Call("Remove", player);
                            }
                            else if (player.Object != null)
                            {
                                pluginField.Call("Remove", player.Object);
                            }
                        }

                        try
                        {
                            pluginField.OnDisconnect(val);
                        }
                        catch (Exception ex)
                        {
                            Plugin.logger.Report(ex);
                        }
                    });
                }
                else
                {
                    lock (reconnectLock)
                    {
                        if (_reconnected.ContainsKey(player.Id)) // Player reconnected (very quickly)
                        {
                            return;
                        }

                        if (pluginField.PlayerType == typeof(IPlayer))
                        {
                            pluginField.Call("Remove", player);
                        }
                        else if (player.Object != null)
                        {
                            pluginField.Call("Remove", player.Object);
                        }
                    }

                    try
                    {
                        pluginField.OnDisconnect(val);
                    }
                    catch (Exception ex)
                    {
                        Plugin.logger.Report(ex);
                    }
                }
            }
        }

        /// <summary>
        /// Adds the online player to the collection
        /// </summary>
        /// <param name="player"></param>
        private void AddOnlinePlayer(IPlayer player)
        {
            foreach (PluginFieldInfo pluginField in Plugin.Meta.OnlinePlayerFields)
            {
                Type type = pluginField.GenericArguments[1];
                string path;

                if (!string.IsNullOrEmpty(pluginField.Directory) && Plugin._files.Exists(path = pluginField.GetFilePath(player)))
                {
                    Plugin._files.ReadObject(type, path).Done(delegate (object onlinePlayer)
                    {
                        pluginField.SetPlayer(type, player, onlinePlayer);
                        pluginField.Call("Add", player, onlinePlayer);
                        try
                        {
                            pluginField.OnConnect(onlinePlayer);
                        }
                        catch (Exception ex)
                        {
                            Plugin.logger.Report(ex);
                        }
                    });
                }
                else
                {
                    object onlinePlayer = type.GetConstructor(new[] { pluginField.PlayerType }) == null ? Activator.CreateInstance(type) : Activator.CreateInstance(type, (object)player);
                    pluginField.SetPlayer(type, player, onlinePlayer);
                    pluginField.Call("Add", player, onlinePlayer);

                    try
                    {
                        pluginField.OnConnect(onlinePlayer);
                    }
                    catch (Exception ex)
                    {
                        Plugin.logger.Report(ex);
                    }
                }
            }
        }
    }
}
