using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using uMod.Common;

namespace uMod.Plugins
{
    /// <summary>
    /// Represents a hook decorator
    /// </summary>
    public class HookDecorator : IHookDecorator
    {
        /// <summary>
        /// Plugin associated with hook decorator
        /// </summary>
        public Plugin Plugin { get; }

        /// <summary>
        /// Name of hook decorator
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Determine if decorator is currently registerd
        /// </summary>
        public bool IsRegistered { get; private set; }

        /// <summary>
        /// Create a new instance of the HookDecorator class
        /// </summary>
        /// <param name="plugin"></param>
        public HookDecorator(Plugin plugin)
        {
            Name = Utility.ReflectedName(GetType().Name);
            Plugin = plugin;
        }

        /// <summary>
        /// List of hooks added manually (not with Hook attribute)
        /// </summary>
        private readonly Dictionary<string, MethodInfo> _manualHooks = new Dictionary<string, MethodInfo>();

        /// <summary>
        /// Add a hook with the specified name and method
        /// </summary>
        /// <param name="hook"></param>
        /// <param name="methodName"></param>
        protected void AddHook(string hook, string methodName)
        {
            MethodInfo method = GetType().GetMethod(methodName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            try
            {
                Plugin.Dispatcher.AddHookMethod(hook, method, method.GetCustomAttribute<HookAttribute>()?.ParameterMatching ?? ParameterMatching.Extended, this, method.GetCustomAttributes<Policy>().Cast<IPolicy>(), method.GetCustomAttributes<DeferAttribute>());
            }
            catch (Exception ex)
            {
                Plugin.logger.Report($"Unable to decorate hook {GetType().Name}.{hook}", ex);
            }

            _manualHooks.Add(hook, method);
        }

        /// <summary>
        /// Remove a hook with the specified name
        /// </summary>
        /// <param name="hook"></param>
        protected void RemoveHook(string hook)
        {
            if (_manualHooks.TryGetValue(hook, out MethodInfo method))
            {
                try
                {
                    Plugin.Dispatcher.RemoveHookMethod(hook, method);
                }
                catch (Exception ex)
                {
                    Plugin.logger.Report($"Unable to remove decorated hook {GetType().Name}.{hook}", ex);
                }

                _manualHooks.Remove(hook);
            }
        }

        /// <summary>
        /// Register decorated hooks to plugin
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> Register()
        {
            if (IsRegistered)
            {
                return Enumerable.Empty<string>();
            }

            List<string> hooks = new List<string>();
            foreach (MethodInfo method in GetType().GetMethodsWithAttribute<HookAttribute>(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                HookAttribute hookmethod = method.GetCustomAttribute<HookAttribute>();
                HookName hookName;
                lock (HookName.Cache.HookNameLock)
                {
                    hookName = HookName.Cache.Get(hookmethod.FullName);
                }

                AsyncAttribute asyncAttribute = method.GetCustomAttribute<AsyncAttribute>();
                if (string.IsNullOrEmpty(hookmethod.Name))
                {
                    hookmethod.Name = Utility.ReflectedName(method.Name);
                }

                if (string.IsNullOrEmpty(hookmethod.Name))
                {
                    continue;
                }

                try
                {
                    if (asyncAttribute != null)
                    {
                        Plugin.Dispatcher.AddHookAsync(hookmethod.FullName, method, hookmethod.ParameterMatching, this, method.GetCustomAttributes<Policy>().Cast<IPolicy>(), method.GetCustomAttributes<DeferAttribute>());
                    }
                    else
                    {
                        Plugin.Dispatcher.AddHookMethod(hookmethod.FullName, method, hookmethod.ParameterMatching, this, method.GetCustomAttributes<Policy>().Cast<IPolicy>(), method.GetCustomAttributes<DeferAttribute>());
                    }
                }
                catch (Exception ex)
                {
                    Plugin.logger.Report($"Unable to decorate hook {GetType().Name}.{hookmethod.Name}", ex);
                }

                hooks.Add(hookName.name);
            }

            foreach (var kvp in _manualHooks)
            {
                hooks.Add(kvp.Key);
            }

            Plugin.AttributeResolver.ResolveCommands(this);

            Plugin.Dispatcher.SaveHookDeferrals();

            IsRegistered = true;

            return hooks;
        }

        /// <summary>
        /// Unregister decorated hooks from plugin
        /// </summary>
        public void Unregister()
        {
            if (!IsRegistered)
            {
                return;
            }

            Type type = GetType();
            foreach (MethodInfo method in type.GetMethodsWithAttribute<HookAttribute>(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                HookAttribute hookmethod = method.GetCustomAttribute<HookAttribute>();
                if (string.IsNullOrEmpty(hookmethod.Name))
                {
                    hookmethod.Name = Utility.ReflectedName(method.Name);
                }

                if (string.IsNullOrEmpty(hookmethod.Name))
                {
                    continue;
                }

                try
                {
                    Plugin.Dispatcher.RemoveHookMethod(hookmethod.FullName, method);
                }
                catch (Exception ex)
                {
                    Plugin.logger.Report($"Unable to remove decorated hook {type.Name}.{hookmethod.Name}", ex);
                }
            }

            foreach (var kvp in _manualHooks)
            {
                try
                {
                    Plugin.Dispatcher.RemoveHookMethod(kvp.Key, kvp.Value);
                }
                catch (Exception ex)
                {
                    Plugin.logger.Report($"Unable to remove decorated hook {type.Name}.{kvp.Key}", ex);
                }
            }

            foreach (MethodInfo method in GetType().GetMethodsWithAttribute<CommandAttribute>(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                IEnumerable<CommandAttribute> commandAttributes = method.GetCustomAttributes<CommandAttribute>();
                foreach (CommandAttribute commandAttribute in commandAttributes)
                {
                    foreach (string command in commandAttribute.Commands)
                    {
                        Plugin.Commands.Remove(command);
                    }
                }
            }

            _manualHooks.Clear();

            IsRegistered = false;
        }

        /// <summary>
        /// Subscribe decorated hook methods
        /// </summary>
        public virtual void Subscribe()
        {
            IEnumerable<string> hookKeys = Register();
            foreach (string hookKey in hookKeys)
            {
                Plugin._manager.SubscribeToHook(hookKey, Plugin);
            }
        }

        /// <summary>
        /// Unsubscribe decorated hook methods
        /// </summary>
        public virtual void Unsubscribe()
        {
            Unregister();
        }
    }
}
