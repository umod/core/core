﻿using System;
using System.Collections;
using uMod.Common;

namespace uMod.Plugins
{
    /// <summary>
    /// Represents a hook event
    /// </summary>
    public class HookEvent : EventArgs<HookContext>
    {
        /// <summary>
        /// Gets whether or not the hook has already authorized against gates
        /// </summary>
        internal bool AuthorizedAsCommand;

        /// <summary>
        /// Create a new hook event
        /// </summary>
        public HookEvent()
        {
            context = new HookContext();
        }

        /// <summary>
        /// Create a new hook event
        /// </summary>
        public HookEvent(IEnumerable signatureContext = null)
        {
            context = new HookContext(signatureContext);
        }

        /// <summary>
        /// Try to inject signature context variables
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public bool TryInject(Type type, out object @object)
        {
            return context.TryInject(type, out @object);
        }

        /// <summary>
        /// Dispose of event
        /// </summary>
        public override void Dispose()
        {
            state = EventState.None;
            stateReason = string.Empty;
            AuthorizedAsCommand = false;
            context.Dispose();
        }

        /// <summary>
        /// Bind event to explicit context
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        internal new HookEvent Bind(HookContext context)
        {
            this.context = context;
            return this;
        }
    }
}
