﻿using System.Collections.Generic;
using uMod.Common;

namespace uMod.Plugins.Watchers
{
    /// <summary>
    /// A class that watches for changes in the source of an arbitrary set of plugins
    /// </summary>
    public abstract class ChangeWatcher : IChangeWatcher
    {
        /// <summary>
        /// Directory that this change watcher is watching
        /// </summary>
        public string Directory { get; protected set; }

        /// <summary>
        /// List of files being watched
        /// </summary>
        protected ICollection<string> WatchedFiles;

        /// <summary>
        /// Called when new plugin has been added
        /// </summary>
        public IEvent<string> OnAdded { get; } = new Event<string>();

        /// <summary>
        /// Called when the source of the plugin has changed
        /// </summary>
        public IEvent<string> OnChanged { get; } = new Event<string>();

        /// <summary>
        /// Called when new plugin has been removed
        /// </summary>
        public IEvent<string> OnRemoved { get; } = new Event<string>();

        /// <summary>
        /// Fires the OnAdded event
        /// </summary>
        /// <param name="name"></param>
        protected void FireAdded(string name) => OnAdded?.Invoke(name);

        /// <summary>
        /// Fires the OnChanged event
        /// </summary>
        /// <param name="name"></param>
        protected void FireChanged(string name) => OnChanged?.Invoke(name);

        /// <summary>
        /// Fires the OnRemoved event
        /// </summary>
        /// <param name="name"></param>
        protected void FireRemoved(string name) => OnRemoved?.Invoke(name);

        /// <summary>
        /// Start watching mapped files
        /// </summary>
        public abstract void StartWatcher();

        /// <summary>
        /// Stop watching mapped files
        /// </summary>
        public abstract void StopWatcher();

        /// <summary>
        /// Adds the specified file to this watcher
        /// </summary>
        /// <param name="name"></param>
        public void AddMapping(string name) => WatchedFiles.Add(name);

        /// <summary>
        /// Adds multiple specified files to this watcher
        /// </summary>
        /// <param name="names"></param>
        public void AddMappings(params string[] names)
        {
            foreach (string name in names)
            {
                WatchedFiles.Add(name);
            }
        }

        /// <summary>
        /// Removes the specified files from this watcher
        /// </summary>
        /// <param name="name"></param>
        public void RemoveMapping(string name) => WatchedFiles.Remove(name);

        /// <summary>
        /// Removes multiple specified files from this watcher
        /// </summary>
        /// <param name="names"></param>
        public void RemoveMappings(params string[] names)
        {
            foreach (string name in names)
            {
                WatchedFiles.Remove(name);
            }
        }
    }
}
