using System;
using System.Collections.Generic;
using System.IO;
#if !NETSTANDARD
using System.Security.Permissions;
#endif
using uMod.IO;
using FileSystemWatcher = uMod.IO.FileSystemWatcher;

namespace uMod.Plugins.Watchers
{
    /// <summary>
    /// Represents a filesystem watcher for a specific directory and set of files
    /// </summary>
    public abstract class AbstractWatcher : ChangeWatcher
    {
        /// <summary>
        /// Represents a filesystem change before being processed
        /// </summary>
        protected class QueuedChange : IDisposable
        {
            internal FileChangeType Type;
            internal Libraries.Timer.TimerInstance Timer;

            public void Dispose()
            {
                Timer?.Destroy();
                Timer = null;
            }
        }

        /// <summary>
        /// The file system watcher
        /// </summary>
        protected FileSystemWatcher Watcher;

        /// <summary>
        /// Changes are buffered briefly to avoid duplicate events
        /// </summary>
        protected Dictionary<string, QueuedChange> ChangeQueue;

        /// <summary>
        /// Gets the timer library
        /// </summary>
        protected Libraries.Timer Timers;

        private readonly string _filter;
        private int _watcherPathLen;
        private readonly SearchOption _searchOption;
        protected Func<string, bool> DirectoryFilter;

        /// <summary>
        /// Initializes a new instance of the AbstractWatcher class
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="filter"></param>
        protected AbstractWatcher(string directory, string filter, SearchOption searchOption, Func<string, bool> directoryFilter = null)
        {
            WatchedFiles = new HashSet<string>();
            ChangeQueue = new Dictionary<string, QueuedChange>();
            Timers = Interface.uMod.Libraries.Get<Libraries.Timer>();
            Directory = directory;
            _filter = filter;
            _searchOption = searchOption;
            DirectoryFilter = directoryFilter;
        }

        /// <summary>
        /// Starts the file system watcher
        /// </summary>
#if !NETSTANDARD && !NETCOREAPP3_1 && !NET5_0 && !NET6_0
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
#endif
        public override void StartWatcher()
        {
            if (Watcher != null)
            {
                StopWatcher();
            }
            // Create and start the watcher
            Watcher = new FileSystemWatcher(Interface.uMod.RootLogger, Directory, watcher_Changed, _searchOption, _filter, DirectoryFilter);
            _watcherPathLen = Watcher.Path.Length;
        }

        /// <summary>
        /// Stops the file system watcher
        /// </summary>
        public override void StopWatcher()
        {
            ChangeQueue.Clear();
            Watcher?.Close();
        }

        /// <summary>
        /// Called when the watcher has registered a file system change
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fileChangeType"></param>
        protected virtual void watcher_Changed(string path, FileChangeType fileChangeType)
        {
            int length = path.Length - _watcherPathLen - (Path.GetExtension(path)?.Length ?? 0) - 1;
            string subPath = path.Substring(_watcherPathLen + 1, length);

            if (!ChangeQueue.TryGetValue(subPath, out QueuedChange change))
            {
                change = new QueuedChange
                {
                    Type = fileChangeType
                };
                ChangeQueue[subPath] = change;
            }
            else
            {
                change.Type = fileChangeType;
            }

            switch (fileChangeType)
            {
                case FileChangeType.Changed:
                    if (change.Type != FileChangeType.Created)
                    {
                        change.Type = FileChangeType.Changed;
                    }

                    break;

                case FileChangeType.Created:
                    if (change.Type == FileChangeType.Deleted)
                    {
                        change.Type = FileChangeType.Changed;
                    }
                    else
                    {
                        change.Type = FileChangeType.Created;
                    }

                    break;

                case FileChangeType.Deleted:
                    if (change.Type == FileChangeType.Created)
                    {
                        ChangeQueue.Remove(subPath);
                        return;
                    }

                    change.Type = FileChangeType.Deleted;
                    break;
            }

            Interface.uMod.NextTick(delegate ()
            {
                if (change.Timer != null)
                {
                    if (change.Timer.Destroyed)
                    {
                        change.Timer.Reset(0.2f);
                    }
                    else
                    {
                        change.Timer.SetDelay(0.2f);
                    }
                }
                else
                {
                    change.Timer = Timers.Once(.2f, () =>
                    {
                        ChangeQueue.Remove(subPath);
                        change.Dispose();
                        /*
                        if (Regex.Match(subPath, @"include\\", RegexOptions.IgnoreCase).Success)
                        {
                            if (change.Type == FileChangeType.Created || change.Type == FileChangeType.Changed)
                            {
                                FireChanged(subPath);
                            }
    
                            return;
                        }
                        */

                        switch (change.Type)
                        {
                            case FileChangeType.Changed:
                                if (WatchedFiles.Contains(subPath))
                                {
                                    FireChanged(subPath);
                                }
                                else
                                {
                                    FireAdded(subPath);
                                }

                                break;

                            case FileChangeType.Created:
                                FireAdded(subPath);
                                break;

                            case FileChangeType.Deleted:
                                if (WatchedFiles.Contains(subPath))
                                {
                                    FireRemoved(subPath);
                                }

                                break;
                        }
                    });
                }
            });
        }
    }
}
