﻿using System.Collections.Generic;

namespace uMod.Plugins
{
    internal class HookCache
    {
        private const string NullKey = "null";

        public Dictionary<string, HookCache> Cache = new Dictionary<string, HookCache>();

        public HookMethod[] Methods;

        public void Clear()
        {
            Cache.Clear();
        }

        public void Clear(string hookName)
        {
            if (Cache.TryGetValue(hookName, out HookCache nextCache))
            {
                nextCache.Clear();
                Cache.Remove(hookName);
            }
        }

        public HookMethod[] GetHookMethod(string hookName, object[] args, out HookCache cache)
        {
            if (!Cache.TryGetValue(hookName, out HookCache nextCache))
            {
                nextCache = new HookCache();
                Cache.Add(hookName, nextCache);
            }

            return nextCache.GetHookMethod(args, 0, out cache);
        }

        public HookMethod[] GetHookMethod(object[] args, int index, out HookCache cache) // TODO: Fix error with some same-named hooks
        {
            if (args == null || index >= args.Length)
            {
                cache = this;
                return Methods;
            }

            HookCache nextCache = null;
            if (args[index] == null)
            {
                if (!Cache.TryGetValue(NullKey, out nextCache))
                {
                    nextCache = new HookCache();
                    Cache.Add(NullKey, nextCache);
                }
            }
            else
            {
                string typeName = args[index].GetType().FullName;
                if (!Cache.TryGetValue(typeName, out nextCache))
                {
                    nextCache = new HookCache();
                    Cache.Add(typeName, nextCache);
                }
            }

            return nextCache.GetHookMethod(args, index + 1, out cache);
        }

        public HookMethod[] SetupMethods(HookMethod[] methods)
        {
            return Methods = methods;
        }
    }
}
