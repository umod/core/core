using System;
using System.Collections.Generic;
using System.Linq;
using uMod.Auth;
using uMod.Command;
using uMod.Common;
using uMod.Common.Command;
using uMod.Common.Plugins;
using uMod.Libraries;
using uMod.Pooling;

namespace uMod.Plugins
{
    public class CommandInfo : PolicyValidator, ICommandInfo
    {
        /// <summary>
        /// Command definition
        /// </summary>
        public ICommandDefinition Definition { get; }

        /// <summary>
        /// Command callback
        /// </summary>
        public CommandCallback Callback { get; }

        /// <summary>
        /// Command help strings
        /// </summary>
        public IDictionary<string, string> Help { get; }

        /// <summary>
        /// Command description strings
        /// </summary>
        public IDictionary<string, string> Descriptions { get; }

        /// <summary>
        /// Create a new instance of the CommandInfo class
        /// </summary>
        /// <param name="commandDefinition"></param>
        /// <param name="callback"></param>
        public CommandInfo(ICommandDefinition commandDefinition, CommandCallback callback)
        {
            Definition = commandDefinition;
            Callback = callback;
        }

        /// <summary>
        /// Create a new instance of the CommandInfo class
        /// </summary>
        /// <param name="commandDefinition"></param>
        /// <param name="callback"></param>
        /// <param name="permission"></param>
        /// <param name="descriptions"></param>
        /// <param name="help"></param>
        public CommandInfo(ICommandDefinition commandDefinition, CommandCallback callback, string permission = null, IDictionary<string, string> descriptions = null, IDictionary<string, string> help = null) : base(permission)
        {
            Definition = commandDefinition;
            Callback = callback;
            Descriptions = descriptions;
            Help = help;
        }

        /// <summary>
        /// Create a new instance of the CommandInfo class
        /// </summary>
        /// <param name="commandDefinition"></param>
        /// <param name="callback"></param>
        /// <param name="permissions"></param>
        /// <param name="descriptions"></param>
        /// <param name="help"></param>
        public CommandInfo(ICommandDefinition commandDefinition, CommandCallback callback, string[] permissions = null, IDictionary<string, string> descriptions = null, IDictionary<string, string> help = null) : base(permissions)
        {
            Definition = commandDefinition;
            Callback = callback;
            Descriptions = descriptions;
            Help = help;
        }

        /// <summary>
        /// Create a new instance of the CommandInfo class
        /// </summary>
        /// <param name="commandDefinition"></param>
        /// <param name="callback"></param>
        /// <param name="gates"></param>
        /// <param name="descriptions"></param>
        /// <param name="help"></param>
        public CommandInfo(ICommandDefinition commandDefinition, CommandCallback callback, IEnumerable<IPolicy> gates = null, IDictionary<string, string> descriptions = null, IDictionary<string, string> help = null) : base(gates)
        {
            Definition = commandDefinition;
            Callback = callback;
            Descriptions = descriptions;
            Help = help;
        }

        /// <summary>
        /// Gets command help for the specified language (default: en)
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public string GetHelp(string lang = null)
        {
            if (string.IsNullOrEmpty(lang))
            {
                lang = Lang.DefaultLang;
            }

            if (Help.TryGetValue(lang, out string result))
            {
                return result;
            }

            return Interface.uMod.Strings.Command.CommandHelpDefault;
        }

        /// <summary>
        /// Gets command description for the specified language (default: en)
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public string GetDescription(string lang = null)
        {
            if (string.IsNullOrEmpty(lang))
            {
                lang = Lang.DefaultLang;
            }

            if (Descriptions.TryGetValue(lang, out string result))
            {
                return result;
            }

            return Interface.uMod.Strings.Command.CommandDescriptionDefault;
        }
    }

    /// <summary>
    /// Plugin command handler
    /// </summary>
    public class PluginCommands : IPluginCommandHandler
    {
        /// <summary>
        /// Determine if command handler is already registered
        /// </summary>
        private bool _registered;

        /// <summary>
        /// Command plugin
        /// </summary>
        private readonly Plugin _plugin;

        /// <summary>
        /// Permission library
        /// </summary>
        protected Permission Permission = Interface.uMod.Libraries.Get<Permission>();

        /// <summary>
        /// Universal library
        /// </summary>
        protected Universal Universal = Interface.uMod.Libraries.Get<Universal>();

        /// <summary>
        /// Dictionary of registered commands
        /// </summary>
        private readonly IDictionary<string, ICommandInfo> _commandInfos;

        /// <summary>
        /// Gets command logger
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// The module
        /// </summary>
        private readonly Module _module;

        /// <summary>
        /// Create a new plugin command handler
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="logger"></param>
        internal PluginCommands(Plugin plugin, ILogger logger, Module module)
        {
            _logger = logger;
            _module = module;
            _plugin = plugin;
            _commandInfos = new Dictionary<string, ICommandInfo>();
            plugin.OnRemovedFromManager?.Add(plugin_OnRemovedFromManager);
        }

        /// <summary>
        /// Creates a new command callback using specified function
        /// </summary>
        /// <param name="definition"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public CommandCallback CreateCallback(ICommandDefinition definition,
            Func<IPlayer, IArgs, CommandState> callback)
        {
            return delegate (IPlayer caller, string command, string fullCommand, object[] context, ICommandInfo cmdInfo)
            {
                Args args = Pools.CommandArgs.Get<Args>().ResetArgs(definition, caller, fullCommand);

                try
                {
                    return callback(caller, args);
                }
                finally
                {
                    Pools.CommandArgs.Free(args);
                }
            };
        }

        /// <summary>
        /// Creates a new command callback using the specified hook name
        /// </summary>
        /// <param name="definition"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public CommandCallback CreateCallback(ICommandDefinition definition, string callback)
        {
            HookName hookName = HookName.Cache.Get(callback);

            return delegate (IPlayer caller, string command, string fullCommand, object[] context, ICommandInfo cmdInfo)
            {
                HookEvent hookEvent = Pools.HookEvents.Get();
                Args args = Pools.CommandArgs.Get<Args>().ResetArgs(definition, caller, fullCommand, context);

                try
                {
                    hookEvent.context.
                        AddContext(caller).
                        AddContext(typeof(IPlayer), caller).
                        AddContext(command).
                        AddContext(args).
                        AddContext(typeof(IArgs), args).
                        AddContext(args.GetStringValues()).
                        AddContext(typeof(ICommandInfo), cmdInfo);

                    if (context != null)
                    {
                        for (int i = 0; i < context.Length; i++)
                        {
                            hookEvent.context.AddContext(context[i]);
                        }
                    }

                    hookEvent.AuthorizedAsCommand = true; // Gates already authorized in UniversalCommandCallback

                    object result = _plugin.Dispatcher.Dispatch(hookName, null, hookEvent);
                    if (result is CommandState commandState)
                    {
                        return commandState;
                    }
                }
                finally
                {
                    Pools.CommandArgs.Free(args);
                    Pools.HookEvents.Free(hookEvent);
                }

                if (hookEvent.Canceled)
                {
                    return CommandState.Canceled;
                }

                return CommandState.Completed;
            };
        }

        /// <summary>
        /// Adds a new command
        /// </summary>
        /// <param name="command"></param>
        public void Add(ICommandInfo command)
        {
            foreach (string cmdName in command.Definition.Names)
            {
                if (_commandInfos.ContainsKey(cmdName.ToLowerInvariant()))
                {
                    _logger.Warning(_module.Strings.Command.AliasAlreadyExists.Interpolate("command", cmdName));
                    continue;
                }

                _commandInfos.Add(cmdName, command);
            }

            if (command.Gates != null)
            {
                string prefix = _plugin.Name.ToLower();
                string fullPermission = string.Empty;
                foreach (IPolicy gate in command.Gates)
                {
                    switch (gate)
                    {
                        case ISinglePolicy singleGate:
                            {
                                if (singleGate.GateType == null && !string.IsNullOrEmpty(singleGate.PolicyName))
                                {
                                    fullPermission = singleGate.PolicyName.ToLower();
                                    if (!fullPermission.StartsWith(prefix))
                                    {
                                        fullPermission = $"{prefix}.{fullPermission}";
                                    }

                                    if (!Permission.PermissionExists(fullPermission))
                                    {
                                        Permission.RegisterPermission(fullPermission, _plugin);
                                    }
                                }

                                break;
                            }
                        case IAggregatePolicy aggregateGate:
                            {
                                if (aggregateGate.GateType == null)
                                {
                                    foreach (string perm in aggregateGate.PolicyNames)
                                    {
                                        fullPermission = perm.ToLower();
                                        if (!fullPermission.StartsWith(prefix))
                                        {
                                            fullPermission = $"{prefix}.{fullPermission}";
                                        }
                                        if (!Permission.PermissionExists(fullPermission))
                                        {
                                            Permission.RegisterPermission(fullPermission, _plugin);
                                        }
                                    }
                                }

                                break;
                            }
                    }
                }
            }

            if (!_registered)
            {
                return;
            }

            foreach (string cmdName in command.Definition.Names)
            {
                Universal.RegisterCommand(cmdName, _plugin, UniversalCommandCallback);
            }
        }

        /// <summary>
        /// Removes a command
        /// </summary>
        /// <param name="command"></param>
        public void Remove(ICommandInfo command)
        {
            foreach (string cmdName in command.Definition.Names)
            {
                if (_commandInfos.ContainsKey(cmdName))
                {
                    _commandInfos.Remove(cmdName);
                }
            }
        }

        /// <summary>
        /// Removes a command
        /// </summary>
        /// <param name="command"></param>
        public void Remove(string command)
        {
            if (_commandInfos.TryGetValue(command, out ICommandInfo commandInfo))
            {
                Remove(commandInfo);
            }
        }

        /// <summary>
        /// Gets a command
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        internal ICommandInfo GetCommand(string command)
        {
            if (_commandInfos.TryGetValue(command, out ICommandInfo commandInfo))
            {
                return commandInfo;
            }

            return null;
        }

        /// <summary>
        /// Gets command syntax help
        /// </summary>
        /// <param name="command"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public string GetHelp(string command, string lang = null)
        {
            return _commandInfos.TryGetValue(command, out ICommandInfo pluginCommand) ? pluginCommand.GetHelp(lang) : null;
        }

        /// <summary>
        /// Gets command description
        /// </summary>
        /// <param name="command"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public string GetDescription(string command, string lang = null)
        {
            return _commandInfos.TryGetValue(command, out ICommandInfo pluginCommand) ? pluginCommand.GetDescription(lang) : null;
        }

        /// <summary>
        /// Register commands with universal provider
        /// </summary>
        internal void Register()
        {
            foreach (KeyValuePair<string, ICommandInfo> pair in _commandInfos)
            {
                Universal.RegisterCommand(pair.Key, _plugin, UniversalCommandCallback); // TODO: pair.Value.Callback?
            }

            _registered = true;
        }

        /// <summary>
        /// Unregister commands from universal provider
        /// </summary>
        internal void Unregister()
        {
            foreach (KeyValuePair<string, ICommandInfo> pair in _commandInfos)
            {
                Universal.UnregisterCommand(pair.Key, _plugin);
            }

            _registered = false;
        }

        /// <summary>
        /// Called when the plugin was unloaded
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="manager"></param>
        internal void plugin_OnRemovedFromManager(IPlugin plugin, IPluginManager manager)
        {
            foreach (ICommandInfo commandInfo in _commandInfos.Values.ToArray())
            {
                Remove(commandInfo);
            }

            _commandInfos.Clear();
        }

        /// <summary>
        /// Command callback provider
        /// </summary>
        /// <param name="caller"></param>
        /// <param name="cmd"></param>
        /// <param name="fullCommand"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private CommandState UniversalCommandCallback(IPlayer caller, string cmd, string fullCommand, object[] context = null, ICommandInfo cmdInfo = null)
        {
            if (cmdInfo == null && !_commandInfos.TryGetValue(cmd, out cmdInfo))
            {
                if (_module.Plugins.Configuration.Commands.UnknownCommands)
                {
                    caller.Message(_module.GetStrings(caller).Command.UnknownCommand
                        .Interpolate("command", cmd));
                }

                return CommandState.Unrecognized;
            }

            if (caller == null)
            {
                _logger.Warning(_module.Strings.Command.NullCaller);
                return CommandState.Unrecognized;
            }

            if (cmdInfo.Gates != null)
            {
                if (!caller.IsServer && !caller.IsAdmin && !cmdInfo.Authorize(_plugin.gate, _plugin, caller))
                {
                    if (_module.Plugins.Configuration.Commands.DeniedCommands)
                    {
                        caller.Message(_module.GetStrings(caller).Command.PermissionDenied
                            .Interpolate("command", cmd));
                    }

                    return CommandState.Completed;
                }
            }

            /*
            if (cmdInfo.Permissions != null)
            {
                foreach (string perm in cmdInfo.Permissions)
                {
                    if (!caller.HasPermission(perm) && !caller.IsServer && (!caller.IsAdmin || !_plugin.IsCorePlugin))
                    {
                        if (_module.Plugins.Configuration.Commands.DeniedCommands)
                        {
                            caller.Message(_module.GetStrings(caller).Command.PermissionDenied
                                .Interpolate("command", cmd));
                        }

                        return CommandState.Completed;
                    }
                }
            }
            */

            cmdInfo.Callback(caller, cmd, fullCommand, context, cmdInfo);

            return CommandState.Completed;
        }
    }
}
