﻿using System;
using System.Collections.Generic;
using System.IO;
using uMod.Common;
using uMod.IO;

namespace uMod.Plugins
{
    /// <summary>
    /// Handles the interaction of plugin files
    /// </summary>
    public class PluginFiles : IFileSystemDataAdapter, IDynamicDataSystem<string>
    {
        /// <summary>
        /// The plugin
        /// </summary>
        private readonly Plugin _plugin;

        /// <summary>
        /// Dictionary of data files mediated for the plugin
        /// </summary>
        private readonly Dictionary<Type, IDataFile<object>> _dataFiles = new Dictionary<Type, IDataFile<object>>();

        /// <summary>
        /// Dictionary of converted data files mediated for the plugin
        /// </summary>
        private readonly Dictionary<IDataFile<object>, Dictionary<Type, object>> _convertedDataFiles = new Dictionary<IDataFile<object>, Dictionary<Type, object>>();

        /// <summary>
        /// Gets the data file system for configuration files
        /// </summary>
        public DynamicFileSystem Configuration { get; }

        /// <summary>
        /// Gets the data file system for localization files
        /// </summary>
        public DynamicFileSystem Localization { get; }

        /// <summary>
        /// Gets the data file system for data files
        /// </summary>
        public DynamicFileSystem Data { get; }

        /// <summary>
        /// Create a new instance of the PluginFiles class
        /// </summary>
        /// <param name="plugin"></param>
        internal PluginFiles(Plugin plugin)
        {
            _plugin = plugin;
            Data = new DynamicFileSystem(plugin.Name);
            Configuration = new DynamicFileSystem(null, DataCategory.Configuration);
            Localization = new DynamicFileSystem(null, DataCategory.Localization);
            _plugin.OnRemovedFromManager.Add(plugin_OnRemovedFromManager);
        }

        /// <summary>
        /// Determine if local data file store has of type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal bool HasFileOfType(Type type)
        {
            return _dataFiles.ContainsKey(type);
        }

        /// <summary>
        /// Adds file to local data file store
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataFile"></param>
        internal void AddFile<T>(IDataFile<object> dataFile)
        {
            AddFile(typeof(T), dataFile);
        }

        /// <summary>
        /// Adds file to local data file store
        /// </summary>
        /// <param name="type"></param>
        /// <param name="dataFile"></param>
        internal void AddFile(Type type, IDataFile<object> dataFile)
        {
            if (_dataFiles.ContainsKey(type))
            {
                _dataFiles[type] = dataFile;
            }
            else
            {
                _dataFiles.Add(type, dataFile);
            }
        }

        /// <summary>
        /// Gets data file for the specified plugin type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public IDataFile<object> GetDataFile(Type type)
        {
            return _dataFiles.TryGetValue(type, out IDataFile<object> dataFile) ? dataFile : null;
        }

        /// <summary>
        /// Gets data file of the specified plugin type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IDataFile<T> GetDataFile<T>() where T : class
        {
            IDataFile<object> dataFile = GetDataFile(typeof(T));

            // Ensure we do not convert it again if it's already converted
            if (_convertedDataFiles.TryGetValue(dataFile, out Dictionary<Type, object> convertedDataFiles) &&
                convertedDataFiles.TryGetValue(typeof(T), out object convertedDataFile))
            {
                return convertedDataFile as IDataFile<T>;
            }

            if (!_convertedDataFiles.TryGetValue(dataFile, out convertedDataFiles))
            {
                _convertedDataFiles.Add(dataFile, convertedDataFiles = new Dictionary<Type, object>());
            }

            // Create a new IDataFile of the given generic type
            IDataFile<T> newDataFile = dataFile.As<T>();
            if (!convertedDataFiles.ContainsKey(typeof(T)))
            {
                convertedDataFiles.Add(typeof(T), newDataFile);
            }
            return newDataFile;
        }

        /// <summary>
        /// Gets data file for the specified type and specified path
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public IDataFile<object> GetDataFile(Type type, string path)
        {
            return FileSystem.GetDataFile(type, path);
        }

        /// <summary>
        /// Gets data file for the specified type and specified path
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public IDataFile<object> GetDataFile(Type type, string path, object defaultValue = null)
        {
            IDataFile<object> dataFile = FileSystem.GetDataFile(type, path);
            if (dataFile != null && dataFile.Object == null)
            {
                dataFile.Object = defaultValue;
            }

            return dataFile;
        }

        /// <summary>
        /// Gets data file of the generic type for the specified path
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public IDataFile<TValue> GetDataFile<TValue>(string path) where TValue : class
        {
            return FileSystem.GetDataFile<TValue>(Path.Combine(_plugin.Name, path));
        }

        /// <summary>
        /// Gets data file of the generic type for the specified path
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public IDataFile<TValue> GetDataFile<TValue>(string path, TValue defaultValue = null) where TValue : class
        {
            IDataFile<TValue> dataFile = FileSystem.GetDataFile<TValue>(Path.Combine(_plugin.Name, path));
            if (dataFile != null && dataFile.Object == null)
            {
                dataFile.Object = defaultValue;
            }

            return dataFile;
        }

        /// <summary>
        /// Gets data file for the specified type and specified path
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public IDataFile<object> GetDataFile(Type type, string path, bool create = false)
        {
            IDataFile<object> dataFile = FileSystem.GetDataFile(type, Path.Combine(_plugin.Name, path));
            if (dataFile != null && dataFile.Object == null && create)
            {
                dataFile.Object = Interface.uMod.Application.Make<object>(type);
                dataFile.OnCreateObject?.Invoke(dataFile);
            }

            return dataFile;
        }

        /// <summary>
        /// Gets data file of the generic type for the specified path
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public IDataFile<TValue> GetDataFile<TValue>(string path, bool create = false) where TValue : class
        {
            IDataFile<TValue> dataFile = FileSystem.GetDataFile<TValue>(Path.Combine(_plugin.Name, path));
            if (dataFile != null && dataFile.Object == null && create)
            {
                dataFile.Object = Interface.uMod.Application.Make<TValue>();
                dataFile.OnCreateObject?.Invoke(dataFile);
            }

            return dataFile;
        }

        /// <summary>
        /// Gets data file for the specified type and specified path
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public IDataFile<object> GetDataFile(Type type, string path, Func<object> defaultValue = null)
        {
            IDataFile<object> dataFile = FileSystem.GetDataFile(type, Path.Combine(_plugin.Name, path));
            if (dataFile != null && dataFile.Object == null && defaultValue != null)
            {
                dataFile.Object = defaultValue();
                dataFile.OnCreateObject?.Invoke(dataFile);
            }

            return dataFile;
        }

        /// <summary>
        /// Gets data file of the generic type for the specified path
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public IDataFile<TValue> GetDataFile<TValue>(string path, Func<TValue> defaultValue = null) where TValue : class
        {
            IDataFile<TValue> dataFile = FileSystem.GetDataFile<TValue>(Path.Combine(_plugin.Name, path));
            if (dataFile != null && dataFile.Object == null && defaultValue != null)
            {
                dataFile.Object = defaultValue();
                dataFile.OnCreateObject?.Invoke(dataFile);
            }

            return dataFile;
        }

        /// <summary>
        /// Gets data file for the specified type and specified path
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public IDataFile<object> GetDataFile(Type type, string path, IFactory factory = null)
        {
            IDataFile<object> dataFile = FileSystem.GetDataFile(type, Path.Combine(_plugin.Name, path));
            if (dataFile != null && dataFile.Object == null && factory != null)
            {
                dataFile.Object = factory.Make<object>(type);
                dataFile.OnCreateObject?.Invoke(dataFile);
            }

            return dataFile;
        }

        /// <summary>
        /// Gets data file of the generic type for the specified path
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public IDataFile<TValue> GetDataFile<TValue>(string path, IFactory factory = null) where TValue : class
        {
            IDataFile<TValue> dataFile = FileSystem.GetDataFile<TValue>(Path.Combine(_plugin.Name, path));
            if (dataFile != null && dataFile.Object == null && factory != null)
            {
                dataFile.Object = factory.Make<TValue>();
                dataFile.OnCreateObject?.Invoke(dataFile);
            }

            return dataFile;
        }

        /// <summary>
        /// Read object from specified path
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public IPromise<TValue> ReadObject<TValue>(string path, bool create = false) where TValue : class
        {
            return FileSystem.ReadObject<TValue>(Path.Combine(_plugin.Name, path), create);
        }

        /// <summary>
        /// Read object from specified path
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public IPromise<object> ReadObject(Type type, string path, bool create = false)
        {
            return FileSystem.ReadObject(type, Path.Combine(_plugin.Name, path), create);
        }

        /// <summary>
        /// Read objects of type from specified paths
        /// </summary>
        /// <param name="type"></param>
        /// <param name="paths"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public IPromise<IEnumerable<object>> ReadObjects(Type type, IEnumerable<string> paths, bool create = false)
        {
            return FileSystem.ReadObjects(type, paths, create, _plugin.Name);
        }

        /// <summary>
        /// Read objects of type from specified paths
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="paths"></param>
        /// <param name="create"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public IPromise<IEnumerable<TValue>> ReadObjects<TValue>(IEnumerable<string> paths, bool create = false, string prefix = null) where TValue : class
        {
            if (!string.IsNullOrEmpty(prefix))
            {
                prefix = Path.Combine(_plugin.Name, prefix);
            }
            else
            {
                prefix = _plugin.Name;
            }
            return FileSystem.ReadObjects<TValue>(paths, create, prefix);
        }

        /// <summary>
        /// Read objects of type from specified paths
        /// </summary>
        /// <param name="type"></param>
        /// <param name="paths"></param>
        /// <param name="create"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public IPromise<IEnumerable<object>> ReadObjects(Type type, IEnumerable<string> paths, bool create = false, string prefix = null)
        {
            if (!string.IsNullOrEmpty(prefix))
            {
                prefix = Path.Combine(_plugin.Name, prefix);
            }
            else
            {
                prefix = _plugin.Name;
            }
            return FileSystem.ReadObjects(type, paths, create, prefix);
        }

        /// <summary>
        /// Read objects of types from specified paths
        /// </summary>
        /// <param name="objects"></param>
        /// <param name="create"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public IPromise<IEnumerable<object>> ReadObjects(IDictionary<Type, string> objects, bool create = false, string prefix = null)
        {
            if (!string.IsNullOrEmpty(prefix))
            {
                prefix = Path.Combine(_plugin.Name, prefix);
            }
            else
            {
                prefix = _plugin.Name;
            }
            return FileSystem.ReadObjects(objects, create, prefix);
        }

        /// <summary>
        /// Write object of generic type to the specified path
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public IPromise WriteObject<TValue>(string path, TValue @object) where TValue : class
        {
            return FileSystem.WriteObject(path, @object);
        }

        /// <summary>
        /// Write object to the specified path
        /// </summary>
        /// <param name="path"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public IPromise WriteObject(string path, object @object)
        {
            return FileSystem.WriteObject(path, @object);
        }

        /// <summary>
        /// Write objects of type to specified paths
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="objects"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public IPromise WriteObjects<TValue>(IDictionary<string, TValue> objects, string prefix = null) where TValue : class
        {
            if (!string.IsNullOrEmpty(prefix))
            {
                prefix = Path.Combine(_plugin.Name, prefix);
            }
            else
            {
                prefix = _plugin.Name;
            }
            return FileSystem.WriteObjects(objects, prefix);
        }

        /// <summary>
        /// Write objects of type to specified paths
        /// </summary>
        /// <param name="objects"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public IPromise WriteObjects(IDictionary<string, object> objects, string prefix = null)
        {
            if (!string.IsNullOrEmpty(prefix))
            {
                prefix = Path.Combine(_plugin.Name, prefix);
            }
            else
            {
                prefix = _plugin.Name;
            }
            return FileSystem.WriteObjects(objects, prefix);
        }

        /// <summary>
        /// Read objects of generic type from specified paths
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="paths"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public IPromise<IEnumerable<TValue>> ReadObjects<TValue>(IEnumerable<string> paths, bool create = false) where TValue : class
        {
            return FileSystem.ReadObjects<TValue>(paths, create, _plugin.Name);
        }

        /// <summary>
        /// Read objects of specified types and paths
        /// </summary>
        /// <param name="objects"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public IPromise<IEnumerable<object>> ReadObjects(IDictionary<Type, string> objects, bool create = false)
        {
            return FileSystem.ReadObjects(objects, create, _plugin.Name);
        }

        /// <summary>
        /// Write objects of the generic type to the specified paths
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="objects"></param>
        /// <returns></returns>
        public IPromise WriteObjects<TValue>(IDictionary<string, TValue> objects) where TValue : class
        {
            return FileSystem.WriteObjects(objects, _plugin.Name);
        }

        /// <summary>
        /// Write objects to the specified paths
        /// </summary>
        /// <param name="objects"></param>
        /// <returns></returns>
        public IPromise WriteObjects(IDictionary<string, object> objects)
        {
            return FileSystem.WriteObjects(objects, _plugin.Name);
        }

        /// <summary>
        /// Check if file exists
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool Exists(string path = null)
        {
            return Data.Exists(path);
        }

        /// <summary>
        /// Delete file(s) using specified path or path pattern
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool Delete(string path)
        {
            return Data.Delete(path);
        }

        /// <summary>
        /// Delete file(s) using specified path or path pattern
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool Delete(Type type, string path)
        {
            return Data.Delete(type, path);
        }

        /// <summary>
        /// Delete file(s) using specified path or path pattern
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool Delete<T>(string path) where T : class
        {
            return Data.Delete<T>(path);
        }

        /// <summary>
        /// Gets file(s) using specified path or path pattern
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public IEnumerable<string> GetFiles(string path = null)
        {
            return Data.GetFiles(path);
        }

        /// <summary>
        /// Gets file(s) using specified path or path pattern
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public IEnumerable<string> GetFiles(Type type, string path = null)
        {
            return Data.GetFiles(type, path);
        }

        /// <summary>
        /// Gets file(s) using specified path or path pattern
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public IEnumerable<string> GetFiles<T>(string path = null) where T : class
        {
            return Data.GetFiles<T>(path);
        }

        /// <summary>
        /// Clear out unused references
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="manager"></param>
        internal void plugin_OnRemovedFromManager(IPlugin sender, IPluginManager manager)
        {
            /*
            foreach (KeyValuePair<Type, IDataFile<object>> dataFile in _dataFiles)
            {
                if (dataFile.Key?.Namespace?.StartsWith("uMod.Plugins") ?? false)
                {
                    Configuration.RemoveDataFileSystem(dataFile.Key);
                    Data.RemoveDataFileSystem(dataFile.Key);
                    Localization.RemoveDataFileSystem(dataFile.Key);
                }

                if (_convertedDataFiles.TryGetValue(dataFile.Value,
                    out Dictionary<Type, object> convertedDictionary))
                {
                    foreach (KeyValuePair<Type, object> convertedDataFile in convertedDictionary)
                    {
                        if (convertedDataFile.Key?.Namespace?.StartsWith("uMod.Plugins") ?? false)
                        {
                            Configuration.RemoveDataFileSystem(convertedDataFile.Key);
                            Data.RemoveDataFileSystem(convertedDataFile.Key);
                            Localization.RemoveDataFileSystem(convertedDataFile.Key);
                        }
                    }
                }
            }*/
            Data.Dispose();
            Configuration.Dispose();
            Localization.Dispose();
            _dataFiles.Clear();
            _convertedDataFiles.Clear();
        }
    }
}
