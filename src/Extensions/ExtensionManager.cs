using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using uMod.Common;
using uMod.Common.Serialization;
using uMod.Plugins;
using uMod.Plugins.Compiler;

namespace uMod.Extensions
{
    public class ExtensionManagerEvent : Event<IExtension, IExtensionManager>, IContextManagerEvent<IExtension, IExtensionManager> { }

    /// <summary>
    /// Responsible for managing all uMod extensions
    /// </summary>
    public sealed class ExtensionManager : IExtensionManager
    {
        private readonly IDictionary<string, Assembly> _extensionAssemblies;

        // All loaded extensions
        private readonly IDictionary<string, IExtension> _extensions;

        // The search patterns for extensions
        private const string ExtSearchPattern = "uMod.*.dll";

        // All registered plugin loaders
        private readonly IList<IPluginLoader> _pluginLoaders;

        // All registered libraries
        private readonly IDictionary<string, ILibrary> _libraries;

        // All registered watchers
        private readonly IList<IChangeWatcher> _changeWatchers;

        /// <summary>
        /// Gets the logger to which this extension manager writes
        /// </summary>
        public ILogger Logger { get; }

        /// <summary>
        /// Called when an extension has been added
        /// </summary>
        public IEvent<IExtension> OnContextAdded { get; } = new Event<IExtension>();

        /// <summary>
        /// Called when an extension has been removed
        /// </summary>
        public IEvent<IExtension> OnContextRemoved { get; } = new Event<IExtension>();

        /// <summary>
        /// Initializes a new instance of the ExtensionManager class
        /// </summary>
        public ExtensionManager(ILogger logger)
        {
            // Initialize
            Logger = logger;
            _extensions = new Dictionary<string, IExtension>();
            _extensionAssemblies = new Dictionary<string, Assembly>();
            _pluginLoaders = new List<IPluginLoader>();
            _libraries = new Dictionary<string, ILibrary>();
            _changeWatchers = new List<IChangeWatcher>();
        }

        #region Registering

        /// <summary>
        /// Registers the specified plugin loader
        /// </summary>
        /// <param name="loader"></param>
        public void RegisterPluginLoader(IPluginLoader loader) => _pluginLoaders.Add(loader);

        /// <summary>
        /// Unregisters the specified plugin loader
        /// </summary>
        /// <param name="loader"></param>
        public bool UnregisterPluginLoader(IPluginLoader loader) => _pluginLoaders.Remove(loader);

        /// <summary>
        /// Gets all plugin loaders
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IPluginLoader> GetPluginLoaders() => _pluginLoaders;

        /// <summary>
        /// Registers the specified library
        /// </summary>
        /// <param name="name"></param>
        /// <param name="library"></param>
        public void RegisterLibrary(string name, ILibrary library)
        {
            if (_libraries.ContainsKey(name))
            {
                Interface.uMod.LogError(Interface.uMod.Strings.Extension.DuplicateExtension);
            }
            else
            {
                _libraries[name] = library;
            }
        }

        /// <summary>
        /// Gets all library names
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetLibraryNames() => _libraries.Keys;

        /// <summary>
        /// Gets all library names
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ILibrary> GetLibraries() => _libraries.Values;

        /// <summary>
        /// Gets the library by the specified name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ILibrary GetLibrary(string name)
        {
            return !_libraries.TryGetValue(name, out ILibrary lib) ? null : lib;
        }

        /// <summary>
        /// Registers the specified change watcher
        /// </summary>
        /// <param name="watcher"></param>
        public void RegisterChangeWatcher(IChangeWatcher watcher) => _changeWatchers.Add(watcher);

        /// <summary>
        /// Gets all plugin change watchers
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IChangeWatcher> GetChangeWatchers() => _changeWatchers;

        /// <summary>
        /// Removes the specified change watcher
        /// </summary>
        /// <param name="watcher"></param>
        public void RemoveChangeWatcher(IChangeWatcher watcher) => _changeWatchers.Remove(watcher);

        #endregion Registering

        /// <summary>
        /// Loads the extension at the specified filename
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="forced"></param>
        public bool LoadExtension(string filename, bool forced = false)
        {
            return LoadExtension(filename, false, forced);
        }

        private bool LoadExtension(string filename, bool coreOnly, bool forced = false)
        {
            string name = Utility.GetFileNameWithoutExtension(filename);
            string pdbFileName = filename.Replace(".dll", "") + ".pdb";

            try
            {
                if (!_extensionAssemblies.TryGetValue(filename, out Assembly assembly))
                {
                    // Read the assembly from file
                    byte[] data = File.ReadAllBytes(filename);

                    if (File.Exists(pdbFileName))
                    {
                        // Read debug information from file
                        byte[] pdbData = File.ReadAllBytes(pdbFileName);

                        // Load the assembly with debug data
                        assembly = Assembly.Load(data, pdbData);
                    }
                    else
                    {
                        // Load the assembly
                        assembly = Assembly.Load(data);
                    }

                    _extensionAssemblies.Add(filename, assembly);
                    Interface.uMod.Types.AddAssembly(assembly);
                    AssemblyRegister.Loaded(assembly, data);
                }

                // Search for a type that derives Extension
                Type extensionType = null;
                foreach (Type type in assembly.GetExportedTypes())
                {
                    if (!typeof(IExtension).IsAssignableFrom(type))
                    {
                        continue;
                    }

                    extensionType = type;
                    break;
                }

                if (extensionType == null)
                {
                    Logger.Write(LogLevel.Error, $"Failed to load extension {name} (Specified assembly does not implement an Extension class)"); // TODO: Locqalization
                    return false;
                }

                GameExtension gameExtensionAttribute = extensionType.GetCustomAttribute<GameExtension>();
                EngineExtension engineExtensionAttribute = extensionType.GetCustomAttribute<EngineExtension>();
                if (!coreOnly && (gameExtensionAttribute != null || engineExtensionAttribute != null))
                {
                    return false;
                }

                if (coreOnly && gameExtensionAttribute == null && engineExtensionAttribute == null)
                {
                    return false;
                }

                if (coreOnly && engineExtensionAttribute != null)
                {
                    Interface.uMod.GameEngine = engineExtensionAttribute.Engine;
                }

                // Check if the extension is already loaded
                if (_extensions.Any(x => x.Value.Filename == filename))
                {
                    Logger.Write(LogLevel.Error, Interface.uMod.Strings.Extension.LoadFailed.Interpolate(("name", name), ("message", "extension already loaded"))); //TODO: Localization
                    return false;
                }

                // Create and register the extension
                if (Activator.CreateInstance(extensionType) is IExtension extension)
                {
                    if (extension.Resolve(name, filename))
                    {
                        return LoadExtension(extension, forced);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Report(Interface.uMod.Strings.Extension.LoadFailed.Interpolate(("name", name)), ex);
            }

            return false;
        }

        /// <summary>
        /// Loads the specified extension
        /// </summary>
        /// <param name="extension"></param>
        /// <param name="forced"></param>
        public bool LoadExtension(IExtension extension, bool forced = false)
        {
            if (extension == null)
            {
                return false;
            }

            if (!forced)
            {
                if (extension.IsCoreExtension || extension.IsGameExtension)
                {
                    Logger.Write(LogLevel.Error, Interface.uMod.Strings.Extension.LoadFailed.Interpolate(("name", extension.Name), ("message", "cannot hotload Core or Game extensions"))); //TODO: Localization
                    return false;
                }

                if (!extension.SupportsReloading)
                {
                    Logger.Write(LogLevel.Error, Interface.uMod.Strings.Extension.LoadFailed.Interpolate(("name", extension.Name), ("message", "this extension does not support reloading"))); //TODO: Localization
                    return false;
                }
            }

            return AddContext(extension);
        }

        /// <summary>
        /// Add extension context to manager
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        public bool AddContext(IExtension extension)
        {
            try
            {
                extension.Load();
                _extensions.Add(extension.Name, extension);
                extension.HandleAddedToManager(this);
                OnContextAdded?.Invoke(extension);
                Interface.uMod.Application.Bind(extension);

                if (Interface.uMod.Plugins is PluginProvider provider)
                {
                    provider.Application?.RefreshSandbox();
                }

                // Log extension loaded
                Logger.Write(LogLevel.Info, Interface.uMod.Strings.Extension.Loaded.Interpolate(("name", extension.Name), ("version", extension.Version), ("author", extension.Author)));
                return true;
            }
            catch (Exception ex)
            {
                Logger.Report(Interface.uMod.Strings.Extension.LoadFailed.Interpolate(("name", extension.Name)), ex);
            }

            return false;
        }

        /// <summary>
        /// Unloads the extension at the specified filename
        /// </summary>
        /// <param name="filename"></param>
        public bool UnloadExtension(string filename)
        {
            string name = Utility.GetFileNameWithoutExtension(filename);

            // Find the extension
            IExtension extension = _extensions.Select(x => x.Value).SingleOrDefault(x => x.Filename == filename);

            if (extension == null)
            {
                Logger.Write(LogLevel.Error, Interface.uMod.Strings.Extension.UnloadFailedMessage.Interpolate(("name", name),
                    ("message", "extension not loaded"))); // TODO: Localization
                return false;
            }

            // Unload the extension
            return UnloadExtension(extension);
        }

        /// <summary>
        /// Unloads the specified extension
        /// </summary>
        /// <param name="extension"></param>
        public bool UnloadExtension(IExtension extension)
        {
            if (extension == null)
            {
                return false;
            }

            // Check if it is a Core or Game extension
            if (extension.IsCoreExtension || extension.IsGameExtension)
            {
                Logger.Write(LogLevel.Error, Interface.uMod.Strings.Extension.UnloadFailedMessage.Interpolate(
                    ("name", extension.Name),
                    ("message", "cannot unload Core or Game extensions"))); // TODO: Localization

                return false;
            }

            // Check if the extension supports reloading
            if (!extension.SupportsReloading)
            {
                Logger.Write(LogLevel.Error, Interface.uMod.Strings.Extension.UnloadFailedMessage.Interpolate(
                    ("name", extension.Name),
                    ("message", "this extension does not support reloading"))); // TODO: Localization

                return false;
            }

            return RemoveContext(extension);
        }

        /// <summary>
        /// Remove extension context from manager
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        public bool RemoveContext(IExtension extension)
        {
            // Unload it
            try
            {
                extension.Unload();
                _extensions.Remove(extension.Name);
                Interface.uMod.Application.Unbind(extension);
                extension.HandleRemovedFromManager(this);
                OnContextRemoved?.Invoke(extension);

                // Log extension unloaded
                Logger.Write(LogLevel.Info, Interface.uMod.Strings.Extension.Unloaded.Interpolate(("name", extension.Name), ("version", extension.Version), ("author", extension.Author)));
                return true;
            }
            catch (Exception ex)
            {
                Logger.Report(Interface.uMod.Strings.Extension.UnloadFailedMessage.Interpolate(("name", extension.Name)), ex);
            }

            return false;
        }

        /// <summary>
        /// Reloads the extension at the specified filename
        /// </summary>
        /// <param name="filename"></param>
        public bool ReloadExtension(string filename)
        {
            string name = Utility.GetFileNameWithoutExtension(filename);

            // Find the extension
            IExtension extension = _extensions.Select(x => x.Value).SingleOrDefault(x => Utility.GetFileNameWithoutExtension(x.Filename) == name);

            // If the extension is not already loaded, load it
            if (extension == null)
            {
                LoadExtension(filename);
                return false;
            }

            return ReloadExtension(extension);
        }

        /// <summary>
        /// Reloads the specified extension
        /// </summary>
        /// <param name="extension"></param>
        public bool ReloadExtension(IExtension extension)
        {
            if (extension == null)
            {
                return false;
            }

            // Check if it is a Core or Game extension
            if (extension.IsCoreExtension || extension.IsGameExtension)
            {
                Logger.Write(LogLevel.Error,
                    Interface.uMod.Strings.Extension.UnloadFailedMessage.Interpolate(("name", extension.Name), ("message", "cannot unload Core or Game extensions"))); //TODO: Localization
                return false;
            }

            // Check if the extension supports reloading
            if (!extension.SupportsReloading)
            {
                Logger.Write(LogLevel.Error,
                    Interface.uMod.Strings.Extension.ReloadFailedMessage.Interpolate(("name", extension.Name), ("message", "this extension does not support reloading"))); //TODO: Localization
                return false;
            }

            if (!UnloadExtension(extension))
            {
                return false;
            }

            return LoadExtension(extension);
        }

        /// <summary>
        /// Removes all plugins from context manager
        /// </summary>
        public void RemoveAll()
        {
            foreach (KeyValuePair<string, IExtension> kvp in _extensions)
            {
                RemoveContext(kvp.Value);
            }
        }

        /// <summary>
        /// Loads all extensions in the specified directory
        /// </summary>
        /// <param name="directory"></param>
        [Obsolete("Use LoadFrom instead")]
        public void LoadAllExtensions(string directory)
        {
            LoadFrom(directory);
        }

        /// <summary>
        /// Loads all extensions in the specified directory
        /// </summary>
        /// <param name="directory"></param>
        public void LoadFrom(DirectoryInfo directory)
        {
            LoadFrom(directory.FullName);
        }

        private readonly List<string> _excludeFiles = new List<string>()
        {
            "uMod.Core.dll",
            "uMod.Common.dll",
            "uMod.References.dll",
            "uMod.Tests.dll",
            "uMod.Testing.dll",
            "uMod.TestServer.dll",
            "uMod.Mock.dll",
            "uMod.Tommy.dll",
            "uMod.Promise.dll"
        };

        /// <summary>
        /// Loads all extensions in the specified directory
        /// </summary>
        /// <param name="directory"></param>
        public void LoadFrom(string directory)
        {
            LoadFrom(directory, false);
        }

        /// <summary>
        /// Loads all extensions in the specified directory
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="coreOnly"></param>
        public void LoadFrom(string directory, bool coreOnly)
        {
            string[] foundExtensions = Directory.GetFiles(directory, ExtSearchPattern);
            foreach (string extPath in foundExtensions.Where(e => !_excludeFiles.Any(e.EndsWith)))
            {
                LoadExtension(Path.Combine(directory, extPath), coreOnly, true);
            }

            foreach (KeyValuePair<string, IExtension> kvp in _extensions.ToArray())
            {
                try
                {
                    kvp.Value.OnModLoad();
                }
                catch (Exception ex)
                {
                    _extensions.Remove(kvp.Key);
                    Logger.Report(Interface.uMod.Strings.Extension.ModLoadFailed.Interpolate(("name", kvp.Key), ("version", kvp.Value.Version)), ex);
                }
            }
        }

        /// <summary>
        /// Gets all currently loaded extensions
        /// </summary>
        /// <returns></returns>
        [Obsolete("Use GetExtensions instead")]
        public IEnumerable<IExtension> GetAllExtensions() => _extensions.Values;

        /// <summary>
        /// Gets all loaded extensions
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IExtension> GetExtensions() => _extensions.Values;

        /// <summary>
        /// Returns if an extension by the given name is present
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [Obsolete("Use HasExtension instead")]
        public bool IsExtensionPresent(string name) => _extensions.Any(e => e.Value.Name == name);

        /// <summary>
        /// Gets the extension with the specified name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IExtension GetExtension(string name)
        {
            _extensions.TryGetValue(name, out IExtension extension);
            return extension;
        }

        /// <summary>
        /// Gets the extension with the specified name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public T GetExtension<T>(string name) where T : class, IExtension
        {
            return GetExtension(name) as T;
        }

        /// <summary>
        /// Try to get an extension with the specified name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public bool TryGetExtension(string name, out IExtension extension)
        {
            return _extensions.TryGetValue(name, out extension);
        }

        /// <summary>
        /// Try to get an extension with the specified name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public bool TryGetExtension<T>(string name, out T extension) where T : class, IExtension
        {
            if (_extensions.TryGetValue(name, out IExtension extensionImpl))
            {
                extension = extensionImpl as T;
                return true;
            }

            extension = null;
            return false;
        }

        /// <summary>
        /// Determine if an extension with the specified name is loaded
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool HasExtension(string name)
        {
            return _extensions.ContainsKey(name);
        }
    }
}
