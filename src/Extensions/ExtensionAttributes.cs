﻿using System;
using uMod.Common;

namespace uMod.Extensions
{
    [AttributeUsage(AttributeTargets.Class)]
    public class EngineExtension : Attribute
    {
        public GameEngine Engine { get; }

        public EngineExtension(GameEngine engine)
        {
            Engine = engine;
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class GameExtension : Attribute
    {
    }
}
