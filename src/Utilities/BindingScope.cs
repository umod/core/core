using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using uMod.Common;

namespace uMod.Utilities
{
    internal class BindingCache
    {
        private BindingManager _bindingManager;
        internal readonly IDictionary<Type, IDictionary<Type, List<BindingScope>>> Cache = new Dictionary<Type, IDictionary<Type, List<BindingScope>>>();
        internal readonly IDictionary<Type, Type> _contextCache = new Dictionary<Type, Type>();

        public BindingCache(BindingManager manager)
        {
            _bindingManager = manager;
        }

        public bool Remove(Type boundType)
        {
            /*
            if (_contextCache.TryGetValue(boundType, out Type context) && _bindingManager.BindingScopes.TryGetValue(context, out List<BindingScope> bindingScopes))
            {
                var removed = bindingScopes.Where(x => x.NeedsType == boundType);
                foreach (var remove in removed)
                {
                    bindingScopes.Remove(remove);
                }
            }
            */
            _contextCache.Remove(boundType);
            return Cache.Remove(boundType);
        }

        public void Add(Type boundType, IDictionary<Type, List<BindingScope>> bindingCache, Type context)
        {
            if (!Cache.ContainsKey(boundType))
            {
                Cache.Add(boundType, bindingCache);
                _contextCache.Add(boundType, context);
            }
        }
    }
    /// <summary>
    /// BindingManager class
    /// Manages binding scopes
    /// </summary>
    internal class BindingManager
    {
        public IContextContainer Provider { get; }
        internal readonly List<BindingScope> GlobalScope = new List<BindingScope>();
        internal readonly Dictionary<Type, List<BindingScope>> BindingScopes = new Dictionary<Type, List<BindingScope>>();
        internal readonly BindingCache Cache;
        internal readonly object BindingLock = new object();
        private ICallback<Type, object> _cacheCallback;

        /// <summary>
        /// Create a binding manager object for the specified context
        /// </summary>
        /// <param name="provider"></param>
        public BindingManager(IContextContainer provider)
        {
            Cache = new BindingCache(this);
            Provider = provider;
            _cacheCallback = Provider.Removed.Add(delegate (Type boundType, object src)
            {
                BindingScopes.Remove(boundType);

                lock (BindingLock)
                {
                    Cache.Remove(boundType);
                }
            });
        }

        /// <summary>
        /// Create provider binding scope
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public BindingScope When<T>()
        {
            return When(typeof(T));
        }

        /// <summary>
        /// Create provider binding scope
        /// </summary>
        /// <param name="types"></param>
        /// <returns></returns>
        public BindingScope When(params Type[] types)
        {
            return When(new BindingScope(types));
        }

        /// <summary>
        /// Add binding scope to binding manager
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        public BindingScope When(BindingScope scope)
        {
            if (scope.Context == null)
            {
                GlobalScope.Add(scope);
                return scope;
            }

            foreach (Type type in scope.Context)
            {
                if (!BindingScopes.TryGetValue(type, out List<BindingScope> scopeList))
                {
                    scopeList = new List<BindingScope>
                    {
                        scope
                    };

                    lock (BindingLock)
                    {
                        BindingScopes.Add(type, scopeList);
                    }
                }
                else
                {
                    lock (BindingLock)
                    {
                        scopeList.Add(scope);
                    }
                }
            }

            return scope;
        }
    }

    /// <summary>
    /// BindingScope class
    /// </summary>
    public class BindingScope : IBindingScope
    {
        /// <summary>
        /// List of types where this binding scope applies
        /// </summary>
        public IList<Type> Context { get; internal set; }

        /// <summary>
        /// Gets the type that depends on the context
        /// </summary>
        public Type NeedsType { get; private set; }

        internal object boundObject;

        /// <summary>
        /// Gets the object that binding scope should return
        /// </summary>
        public object BoundObject { get => boundObject; private set => boundObject = value; }

        /// <summary>
        /// Gets a callback to determine if binding scope applies to context
        /// </summary>
        public Func<object, object> BindCallbackWithParameter { get; private set; }

        /// <summary>
        /// Gets a callback to determine if binding scope applies to context
        /// </summary>
        public Func<object> BindCallback { get; private set; }

        /// <summary>
        /// Gets a method to determine if binding scope applies to context
        /// </summary>
        public MethodInfo BindMethod { get; private set; }

        /// <summary>
        /// Gets the target of the bound method
        /// </summary>
        public object BindMethodTarget { get; private set; }

        /// <summary>
        /// Gets an array of parameters used by the bind method
        /// </summary>
        public Type[] BindMethodParameters { get; private set; }

        /// <summary>
        /// Check if bound method is anonymous
        /// </summary>
        public bool IsAnonymousMethod
        {
            get
            {
                if (BindCallback != null)
                {
                    return true;
                }

                if (BindMethod != null && BindMethod.DeclaringType != null && BindMethod.DeclaringType.BaseType == typeof(object) && BindMethod.DeclaringType.IsSealed && !BindMethod.DeclaringType.IsPublic)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Create a binding scope object with the specified context types
        /// </summary>
        /// <param name="context"></param>
        public BindingScope(params Type[] context)
        {
            Context = context?.Length > 0 ? new List<Type>(context) : null;
        }

        /// <summary>
        /// Assign the type that depends on the context
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public IBindingScope Needs(Type type)
        {
            NeedsType = type;
            return this;
        }

        /// <summary>
        /// Bind the object that binding scope should return
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public IBindingScope Bind(object obj)
        {
            BoundObject = obj;
            return this;
        }

        /// <summary>
        /// Assign the specified callback to determine if object applies to context
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public IBindingScope Bind(Func<object, object> callback)
        {
            BindCallbackWithParameter = callback;
            return this;
        }

        /// <summary>
        /// Assign the specified callback to determine if object applies to context
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public IBindingScope Bind(Func<object> callback)
        {
            BindCallback = callback;
            return this;
        }

        /// <summary>
        /// Assign the specified callback to determine if object applies to context
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public IBindingScope Bind<T, T1>(Func<T, T1> callback)
        {
            BindMethodTarget = callback.Target;
            Bind(callback.Method);
            return this;
        }

        /// <summary>
        /// Assign the specified callback to determine if object applies to context
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public IBindingScope Bind<T>(Func<T> callback)
        {
            BindMethodTarget = callback.Target;
            Bind(callback.Method);
            return this;
        }

        /// <summary>
        /// Assign the specified method to determine if object applies to context
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public IBindingScope Bind(MethodInfo method)
        {
            BindMethod = method;
            BindMethodParameters = BindMethod.GetParameters().Select(x => x.ParameterType).ToArray();
            return this;
        }
    }

    /// <summary>
    /// BindingScope class (with implied generic type)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BindingScope<T> : BindingScope, IBindingScope<T>
    {
        /// <summary>
        /// Create a new instance of the BindingScope class
        /// </summary>
        public BindingScope() : base(typeof(T))
        {
        }
    }
}
