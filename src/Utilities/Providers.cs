﻿using System;
using System.Collections.Generic;
using System.Linq;
using uMod.Common;

namespace uMod.Utilities
{
    /// <summary>
    /// BaseContainer class
    /// Base container implementation underlying other core containers
    /// </summary>
    public abstract class BaseContainer : IContextContainer
    {
        /// <summary>
        /// Container application
        /// </summary>
        protected Application Application;

        /// <summary>
        /// Container converters
        /// </summary>
        protected Dictionary<Type, Dictionary<Type, Func<object, object>>> Converters = new Dictionary<Type, Dictionary<Type, Func<object, object>>>();

        /// <summary>
        /// Gets the Added event
        /// </summary>
        public IEvent<Type, object> Added { get; } = new Event<Type, object>();

        /// <summary>
        /// Gets the Removed event
        /// </summary>
        public IEvent<Type, object> Removed { get; } = new Event<Type, object>();

        /// <summary>
        /// Gets the AddedType event
        /// </summary>
        public IEvent<Type> AddedType { get; } = new Event<Type>();

        /// <summary>
        /// Gets the RemovedType event
        /// </summary>
        public IEvent<Type> RemovedType { get; } = new Event<Type>();

        /// <summary>
        /// Gets the Added event callback
        /// </summary>
        protected ICallback<Type, object> AddedCallback;

        /// <summary>
        /// Gets the Removed event callback
        /// </summary>
        protected ICallback<Type, object> RemovedCallback;

        /// <summary>
        /// Gets the AddedType event callback
        /// </summary>
        protected ICallback<Type> AddedTypeCallback;

        /// <summary>
        /// Gets the RemovedType event callback
        /// </summary>
        protected ICallback<Type> RemovedTypeCallback;

        /// <summary>
        /// Determines if container is disposed
        /// </summary>
        protected bool DisposedValue;

        /// <summary>
        /// Unregister container from context
        /// </summary>
        public virtual void Unregister()
        {
            if (AddedCallback != null)
            {
                Event.Remove(ref AddedCallback);
            }

            if (RemovedCallback != null)
            {
                Event.Remove(ref RemovedCallback);
            }

            if (AddedTypeCallback != null)
            {
                Event.Remove(ref AddedTypeCallback);
            }

            if (RemovedTypeCallback != null)
            {
                Event.Remove(ref RemovedTypeCallback);
            }
        }

        /// <summary>
        /// Create a new container
        /// </summary>
        /// <param name="application"></param>
        protected BaseContainer(Application application)
        {
            Application = application;

            AddedTypeCallback = AddedType.Add(delegate (Type type)
            {
                application.AddedType?.Invoke(type);
                if (this is IObjectContainer && !application.Context.Mapping.ReverseContainers.ContainsKey(type))
                {
                    application.Context.Mapping.ReverseContainers.Add(type, this as IObjectContainer);
                }

                if (type.IsGenericType)
                {
                    if (!application.Context.Mapping.ReverseGenericTypes.ContainsKey(type.FullName))
                    {
                        application.Context.Mapping.ReverseGenericTypes.Add(type.FullName, type);
                    }
                }
                else
                {
                    if (!application.Context.Mapping.ReverseTypes.ContainsKey(Utility.ReflectedName(type.FullName)))
                    {
                        application.Context.Mapping.ReverseTypes.Add(Utility.ReflectedName(type.FullName), type);
                    }
                }
            });

            RemovedTypeCallback = RemovedType.Add(delegate (Type type)
            {
                application.RemovedType?.Invoke(type);
                if (this is IObjectContainer)
                {
                    application.Context.Mapping.ReverseContainers.Remove(type);
                }

                if (type.IsGenericType)
                {
                    application.Context.Mapping.ReverseGenericTypes.Remove(type.FullName);

                    if (application.Context.Mapping.ReverseAliases.TryGetValue(type, out List<string> aliases))
                    {
                        foreach (string alias in aliases)
                        {
                            application.Context.Mapping.ReverseGenericTypes.Remove(alias);
                        }

                        application.Context.Mapping.ReverseAliases.Remove(type);
                    }
                }
                else
                {
                    application.Context.Mapping.ReverseTypes.Remove(Utility.ReflectedName(type.FullName));

                    if (application.Context.Mapping.ReverseAliases.TryGetValue(type, out List<string> aliases))
                    {
                        foreach (string alias in aliases)
                        {
                            application.Context.Mapping.ReverseTypes.Remove(alias);
                        }

                        application.Context.Mapping.ReverseAliases.Remove(type);
                    }
                }

                foreach (KeyValuePair<Type, Dictionary<Type, Func<object, object>>> kvp in Converters)
                {
                    if (kvp.Value.ContainsKey(type))
                    {
                        kvp.Value.Remove(type);
                    }
                }

                if (Converters.TryGetValue(type, out Dictionary<Type, Func<object, object>> _))
                {
                    Converters.Remove(type);
                }
            });

            AddedCallback = Added.Add(delegate (Type type, object @object)
            {
                application.Added?.Invoke(type, @object);
            });

            RemovedCallback = Removed.Add(delegate (Type type, object @object)
            {
                application.Removed?.Invoke(type, @object);
            });
        }

        /// <summary>
        /// Add converter to container
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="converterFunc"></param>
        public void AddConverter(Type from, Type to, Func<object, object> converterFunc = null)
        {
            if (!Converters.TryGetValue(to, out Dictionary<Type, Func<object, object>> convertFrom))
            {
                convertFrom = new Dictionary<Type, Func<object, object>>()
                {
                    { from, converterFunc }
                };

                Converters.Add(to, convertFrom);
            }
            else if (!convertFrom.ContainsKey(from))
            {
                convertFrom.Add(from, converterFunc);
            }
        }

        /// <summary>
        /// Determine if container converts from one type to the other type
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public virtual bool Converts(Type from, Type to)
        {
            if (Converters.TryGetValue(from, out Dictionary<Type, Func<object, object>> convertsFrom))
            {
                return convertsFrom.Any(x => x.Key == to || x.Key.IsAssignableFrom(to));
            }

            return false;
        }

        /// <summary>
        /// Dispose of container
        /// </summary>
        public abstract void Dispose();

        #region Binding Abstraction

        /// <summary>
        /// Bind specified enumerable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objects"></param>
        /// <returns></returns>
        public abstract IEnumerable<T> Bind<T>(IEnumerable<T> objects);

        /// <summary>
        /// Bind specified object for the explicit type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public virtual object Bind(Type type, object @object)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Bind the specified object
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        public virtual object Bind(object @object)
        {
            return Bind(@object.GetType(), @object);
        }

        /// <summary>
        /// Bind the specified object of a generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="object"></param>
        /// <returns></returns>
        public virtual T Bind<T>(T @object)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Determine if type is resolved
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public abstract bool Resolved(Type type);

        /// <summary>
        /// Determine if type alias is resolved
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public abstract bool Resolved(string typeName);

        /// <summary>
        /// Determine if object is resolved
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        public abstract bool Resolved(object @object);

        /// <summary>
        /// Determine if object is resolved
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public abstract bool Resolved(Type type, object @object);

        /// <summary>
        /// Determine if generic object is resolved
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public abstract bool Resolved<T>();

        /// <summary>
        /// Unbind type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public abstract bool Unbind(Type type);

        /// <summary>
        /// Unbind object of specified type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public abstract bool Unbind(Type type, object @object);

        /// <summary>
        /// Unbind object
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        public abstract bool Unbind(object @object);

        /// <summary>
        /// Unbind generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public abstract bool Unbind<T>();

        /// <summary>
        /// Unbind object of generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="object"></param>
        /// <returns></returns>
        public abstract bool Unbind<T>(T @object);

        /// <summary>
        /// Unbind enumerable objects
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objects"></param>
        /// <returns></returns>
        public abstract bool Unbind<T>(IEnumerable<T> objects);

        #endregion Binding Abstraction
    }

    /// <summary>
    /// Container of singleton objects
    /// </summary>
    public sealed class SingletonContainer : BaseContainer, ISingletonContainer
    {
        internal readonly Dictionary<Type, object> Instances = new Dictionary<Type, object>();

        /// <summary>
        /// Create a new instance of the SingletonContainer class
        /// </summary>
        /// <param name="application"></param>
        public SingletonContainer(Application application) : base(application)
        {
        }

        /// <summary>
        /// Bind specified object to container as singleton
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public override object Bind(Type type, object @object)
        {
            if (!Instances.ContainsKey(type))
            {
                Instances.Add(type, @object);
                Added?.Invoke(type, @object);
                AddedType?.Invoke(type);
            }

            return @object;
        }

        /// <summary>
        /// Bind specified object to container as singleton
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="object"></param>
        /// <returns></returns>
        public override T Bind<T>(T @object)
        {
            Type type = typeof(T);
            if (!Instances.ContainsKey(type))
            {
                Instances.Add(type, @object);
                Added?.Invoke(type, @object);
                AddedType?.Invoke(type);
            }

            return @object;
        }

        #region IDisposable Support

        /// <summary>
        /// Dispose of container
        /// </summary>
        /// <param name="disposing"></param>
        private void Dispose(bool disposing)
        {
            if (!DisposedValue)
            {
                if (disposing)
                {
                    foreach (KeyValuePair<Type, object> kvp in Instances)
                    {
                        Unbind(kvp.Key, kvp.Value);
                    }
                }

                DisposedValue = true;
            }
        }

        /// <summary>
        /// Dispose of container
        /// </summary>
        public override void Dispose()
        {
            Dispose(true);
        }

        #endregion IDisposable Support

        /// <summary>
        /// Determine if specified type is resolved within container
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool Resolved(Type type)
        {
            return Instances.ContainsKey(type);
        }

        /// <summary>
        /// Determine if specified type name or alias is resolved within container
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public override bool Resolved(string typeName)
        {
            return Application.Context.Mapping.ReverseTypes.ContainsKey(typeName);
        }

        /// <summary>
        /// Determine if specified object type is resolved within container
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        public override bool Resolved(object @object)
        {
            return Instances.ContainsKey(@object.GetType());
        }

        /// <summary>
        /// Determine if generic type is resolved within container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public override bool Resolved<T>()
        {
            return Instances.ContainsKey(typeof(T));
        }

        /// <summary>
        /// Determine if specified type is resolved within container
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public override bool Resolved(Type type, object @object)
        {
            return Instances.ContainsKey(type);
        }

        /// <summary>
        /// Unbind specified object from container
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public override bool Unbind(Type type, object @object)
        {
            Removed?.Invoke(type, @object);
            RemovedType?.Invoke(type);
            return Instances.Remove(type);
        }

        /// <summary>
        /// Unbind all objects of specified type from container
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool Unbind(Type type)
        {
            RemovedType?.Invoke(type);
            return Instances.Remove(type);
        }

        /// <summary>
        /// Unbind specified object from container
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        public override bool Unbind(object @object)
        {
            return Unbind(@object.GetType(), @object);
        }

        /// <summary>
        /// Unbind all objects of generic type from container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public override bool Unbind<T>()
        {
            return Unbind(typeof(T));
        }

        /// <summary>
        /// Unbind specified object from container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="object"></param>
        /// <returns></returns>
        public override bool Unbind<T>(T @object)
        {
            return Unbind(typeof(T), @object);
        }

        /// <summary>
        /// Unbind multiple objects from container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objects"></param>
        /// <returns></returns>
        public override IEnumerable<T> Bind<T>(IEnumerable<T> objects)
        {
            foreach (T @object in objects)
            {
                Bind(@object);
            }

            return objects;
        }

        /// <summary>
        /// Unbind multiple objects from container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objects"></param>
        /// <returns></returns>
        public override bool Unbind<T>(IEnumerable<T> objects)
        {
            foreach (T @object in objects)
            {
                Unbind(@object);
            }

            return true;
        }

        /// <summary>
        /// Try to get singleton object of specified type from container
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public bool TryGetObject(Type type, out object @object)
        {
            return Instances.TryGetValue(type, out @object);
        }

        /// <summary>
        /// Try to get singleton object of specified type from container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public bool TryGetObject<T>(Type type, out T @object)
        {
            if (Instances.TryGetValue(type, out object objectValue))
            {
                @object = (T)objectValue;
                return true;
            }

            @object = default;
            return false;
        }

        /// <summary>
        /// Try to get object of generic type from container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="object"></param>
        /// <returns></returns>
        public bool TryGetObject<T>(out T @object) where T : class
        {
            if (Instances.TryGetValue(typeof(T), out object objectValue))
            {
                @object = objectValue as T;
                return true;
            }

            @object = default;
            return false;
        }
    }

    /// <summary>
    /// Container of arbitrary objects
    /// </summary>
    public sealed class ObjectContainer : BaseContainer, IObjectContainer
    {
        private readonly Dictionary<Type, HashSet<object>> _instances = new Dictionary<Type, HashSet<object>>();

        /// <summary>
        /// Create a new instance of the ObjectContainer class
        /// </summary>
        /// <param name="application"></param>
        public ObjectContainer(Application application) : base(application)
        {
            RemovedTypeCallback = RemovedType.Add(delegate (Type type)
            {
                _instances.Remove(type);
            });
        }

        /// <summary>
        /// Bind specified object to container
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public override object Bind(Type type, object @object)
        {
            if (!_instances.TryGetValue(type, out HashSet<object> instances))
            {
                _instances.Add(type, instances = new HashSet<object>());
                AddedType?.Invoke(type);
            }

            if (!instances.Contains(@object))
            {
                Added?.Invoke(type, @object);
                instances.Add(@object);
            }

            return @object;
        }

        /// <summary>
        /// Bind specified object to container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="object"></param>
        /// <returns></returns>
        public override T Bind<T>(T @object)
        {
            Type type = typeof(T);
            if (!_instances.TryGetValue(type, out HashSet<object> instances))
            {
                _instances.Add(type, instances = new HashSet<object>());
                AddedType?.Invoke(type);
            }

            if (!instances.Contains(@object))
            {
                Added?.Invoke(type, @object);
                instances.Add(@object);
            }

            return @object;
        }

        #region IDisposable Support

        /// <summary>
        /// Dispose of container
        /// </summary>
        /// <param name="disposing"></param>
        private void Dispose(bool disposing)
        {
            if (!DisposedValue)
            {
                if (disposing)
                {
                    foreach (KeyValuePair<Type, HashSet<object>> kvp in _instances)
                    {
                        Unbind(kvp.Key, kvp.Value);
                    }
                }

                DisposedValue = true;
            }
        }

        /// <summary>
        /// Dispose of container
        /// </summary>
        public override void Dispose()
        {
            Dispose(true);
        }

        #endregion IDisposable Support

        /// <summary>
        /// Determine if specified type is resolved within container
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool Resolved(Type type)
        {
            return _instances.ContainsKey(type);
        }

        /// <summary>
        /// Determine if specified type name or alias is resolved within container
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public override bool Resolved(string typeName)
        {
            return Application.Context.Mapping.ReverseGenericTypes.ContainsKey(typeName) || Application.Context.Mapping.ReverseTypes.ContainsKey(typeName);
        }

        /// <summary>
        /// Determine if specified object is resolved within container
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        public override bool Resolved(object @object)
        {
            return Resolved(@object.GetType(), @object);
        }

        /// <summary>
        /// Determine if generic type is resolved within container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public override bool Resolved<T>()
        {
            return Resolved(typeof(T));
        }

        /// <summary>
        /// Determine if specified object is resolved within container
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public override bool Resolved(Type type, object @object)
        {
            if (!_instances.TryGetValue(type, out HashSet<object> instances))
            {
                return false;
            }

            return instances.Contains(@object);
        }

        /// <summary>
        /// Unbind all objects of specified type from container
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool Unbind(Type type)
        {
            if (_instances.TryGetValue(type, out HashSet<object> instances))
            {
                foreach (object @object in instances.ToArray())
                {
                    Unbind(@object);
                }

                RemovedType?.Invoke(type);

                return true;
            }

            return false;
        }

        /// <summary>
        /// Unbind specified object from container
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public override bool Unbind(Type type, object @object)
        {
            if (_instances.TryGetValue(type, out HashSet<object> instances))
            {
                if (instances.Remove(@object))
                {
                    Removed?.Invoke(type, @object);

                    if (instances.Count == 0)
                    {
                        RemovedType?.Invoke(type);
                    }
                    return true;
                }

                if (instances.Count == 0)
                {
                    RemovedType?.Invoke(type);
                }

                return false;
            }

            return false;
        }

        /// <summary>
        /// Unbind specified object from container
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        public override bool Unbind(object @object)
        {
            return Unbind(@object.GetType(), @object);
        }

        /// <summary>
        /// Unbind all objects of generic type from container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public override bool Unbind<T>()
        {
            return Unbind(typeof(T));
        }

        /// <summary>
        /// Unbind specified object from container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="object"></param>
        /// <returns></returns>
        public override bool Unbind<T>(T @object)
        {
            return Unbind(@object.GetType(), @object);
        }

        /// <summary>
        /// Bind multiple objects of generic type to container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objects"></param>
        /// <returns></returns>
        public override IEnumerable<T> Bind<T>(IEnumerable<T> objects)
        {
            Type type = typeof(T);
            if (!_instances.TryGetValue(type, out HashSet<object> instances))
            {
                _instances.Add(type, instances = new HashSet<object>());
                RemovedType?.Invoke(type);
            }

            foreach (T @object in objects)
            {
                instances.Add(@object);
            }

            return objects;
        }

        /// <summary>
        /// Unbind multiple objects of generic type to container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objects"></param>
        /// <returns></returns>
        public override bool Unbind<T>(IEnumerable<T> objects)
        {
            Type type = typeof(T);
            if (_instances.TryGetValue(type, out HashSet<object> instances))
            {
                foreach (T @object in objects)
                {
                    if (instances.Remove(@object))
                    {
                        Removed?.Invoke(type, @object);
                    }
                }

                if (instances.Count == 0)
                {
                    RemovedType?.Invoke(type);
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Try to get all objects of specified type from container
        /// </summary>
        /// <param name="type"></param>
        /// <param name="objects"></param>
        /// <returns></returns>
        public bool TryGetObjects(Type type, out IEnumerable<object> objects)
        {
            if (_instances.TryGetValue(type, out HashSet<object> objectValue))
            {
                objects = objectValue;
                return objects != null;
            }

            objects = null;
            return false;
        }

        /// <summary>
        /// Try to get all objects of specified type from container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <param name="objects"></param>
        /// <returns></returns>
        public bool TryGetObjects<T>(Type type, out IEnumerable<T> objects)
        {
            if (_instances.TryGetValue(type, out HashSet<object> objectValue))
            {
                objects = objectValue as IEnumerable<T>;
                return objects != null;
            }

            objects = null;
            return false;
        }
    }
}
