﻿using System;
using System.Reflection;
using uMod.Common;

namespace uMod.Utilities
{
    /// <summary>
    /// Represents a base GameTypes abstraction
    /// </summary>
    public class GameTypes : IGameTypes
    {
        /// <summary>
        /// Gets the game-specific player type
        /// </summary>
        public Type Player { get; set; }

        private readonly IServer _server;
        private bool _playerFieldFound;
        private FieldInfo _playerField;

        /// <summary>
        /// Create a new GameTypes object
        /// </summary>
        /// <param name="server"></param>
        public GameTypes(IServer server)
        {
            _server = server;
        }

        /// <summary>
        /// Get an IPlayer from a game-specific game player object of unknown type
        /// </summary>
        /// <param name="gamePlayer"></param>
        /// <returns></returns>
        public IPlayer GetPlayer(object gamePlayer)
        {
            if (!_playerFieldFound)
            {
                _playerField = Player.GetField("IPlayer", BindingFlags.Instance | BindingFlags.Public);
                _playerFieldFound = true;
            }

            if (_playerField != null)
            {
                return _playerField.GetValue(gamePlayer) as IPlayer;
            }

            return _server?.PlayerManager.FindPlayerByObj(gamePlayer);
        }
    }
}
