﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace uMod.Utilities
{
    internal static class AssemblyResolver
    {
        internal static Dictionary<string, Assembly> RedirectedAssemblies = new Dictionary<string, Assembly>();
        /// <summary>
        /// Resolves deserialized assembly as a different assembly
        /// </summary>
        /// <param name="fromAssemblyShotName"></param>
        /// <param name="replacementAssemblyShortName"></param>
        internal static void RedirectAssembly(string fromAssemblyShotName, string replacementAssemblyShortName)
        {
            if (RedirectedAssemblies.ContainsKey(fromAssemblyShotName))
            {
                return;
            }

#if DEBUG
            Interface.uMod.LogDebug($"Adding custom resolver redirect rule (from:{fromAssemblyShotName}, to:{replacementAssemblyShortName})");
#endif

            Assembly Handler(object sender, ResolveEventArgs args)
            {
                // Use latest strong name & version when trying to load SDK assemblies
                AssemblyName requestedAssembly = new AssemblyName(args.Name);
#if DEBUG
                Interface.uMod.LogDebug($"RedirectAssembly >  requesting:{requestedAssembly} replacement (from:{fromAssemblyShotName}, to:{replacementAssemblyShortName})");
#endif
                if (requestedAssembly.Name == fromAssemblyShotName)
                {
                    try
                    {
#if DEBUG
                        Interface.uMod.LogDebug($"Redirecting Assembly {fromAssemblyShotName} to: {replacementAssemblyShortName}");
#endif
                        return RedirectedAssemblies[fromAssemblyShotName];
                    }
                    catch (Exception ex)
                    {
#if DEBUG
                        Interface.uMod.LogException($"Unable to find replacement Assembly {fromAssemblyShotName} to: {replacementAssemblyShortName}", ex);
#endif
                        return null;
                    }
                }

#if DEBUG
                Interface.uMod.LogDebug($"Unable to find {requestedAssembly}, trying to provide replacment from:{fromAssemblyShotName}, to:{replacementAssemblyShortName}");
#endif

                return null;
            }

            try
            {
                Assembly replacementAssembly = Assembly.Load(replacementAssemblyShortName);
                RedirectedAssemblies.Add(fromAssemblyShotName, replacementAssembly);
                AppDomain.CurrentDomain.AssemblyResolve += Handler;
            }
            catch (Exception ex)
            {
#if DEBUG
                Interface.uMod.LogException($"Failed to redirect assembly \"{fromAssemblyShotName}\" to \"{replacementAssemblyShortName}\"", ex);
#endif
            }
        }
    }
}
