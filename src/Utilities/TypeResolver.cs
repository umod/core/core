﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using uMod.Common;

namespace uMod.Utilities
{
    internal class TypeResolver
    {
        private readonly List<Assembly> _candidateAssemblies = new List<Assembly>();
        private readonly ILogger _logger;

        public TypeResolver(ILogger logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Resolves candidate assemblies to search for candidate types
        /// </summary>
        /// <returns></returns>
        internal IEnumerable<Assembly> ResolveCandidateAssemblies()
        {
            if (_candidateAssemblies.Count > 0)
            {
                return _candidateAssemblies;
            }

#if !NET35
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies().Where(p => !p.IsDynamic))
#else
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
#endif
            {
                AddCandidateAssembly(assembly);
            }

            return _candidateAssemblies;
        }

        /// <summary>
        /// Add candidate assembly
        /// </summary>
        /// <param name="assembly"></param>
        internal void AddCandidateAssembly(Assembly assembly)
        {
            AssemblyName assemblyName = assembly.GetName();

            if (!assemblyName.Name.StartsWith("uMod") && !assemblyName.Name.StartsWith("Oxide"))
            {
                return;
            }

            _candidateAssemblies.Add(assembly);
            _candidateSet.Clear();
        }

        private readonly List<Type> _candidateSet = new List<Type>();

        /// <summary>
        /// Resolves types from interfaces from dynamically loaded assemblies
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        internal IEnumerable<Type> ResolveCandidateTypes<T>()
        {
            //List<Type> candidateSet = new List<Type>();

            if (_candidateSet.Count == 0)
            {
                foreach (Assembly assembly in ResolveCandidateAssemblies())
                {
                    Type[] assemblyTypes = null;
                    try
                    {
                        assemblyTypes = assembly.GetTypes();
                    }
                    catch (ReflectionTypeLoadException rtlEx)
                    {
                        assemblyTypes = rtlEx.Types;
                    }
                    catch (TypeLoadException tlEx)
                    {
                        _logger.Warning(Interface.uMod.Strings.Exceptions.TypeLoadFailed.Interpolate(
                            ("type", tlEx.TypeName), ("assembly", assembly.FullName), ("message", tlEx)));
                    }

                    if (assemblyTypes != null)
                    {
                        _candidateSet.AddRange(assemblyTypes);
                    }
                }
            }

            return _candidateSet.Where(t => t != null && t.IsClass && !t.IsAbstract && t.ImplementsInterface(typeof(T)));
        }
    }
}
