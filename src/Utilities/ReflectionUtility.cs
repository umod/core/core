using System;
using System.Linq;
using System.Reflection;
using uMod.Pooling;

namespace uMod.Utilities
{
#if DEBUG
    public class ReflectionUtility
#else
    internal class ReflectionUtility
#endif
    {
        private const BindingFlags DefaultBindingFlags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;
        private const BindingFlags BackingFieldBindingFlags = BindingFlags.NonPublic | BindingFlags.Instance;

        /// <summary>
        /// Get value of field, property, or method from specified object with cast
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="path"></param>
        /// <param name="bindingAttr"></param>
        /// <returns></returns>
        public static T GetValue<T>(object source, string path, BindingFlags bindingAttr = DefaultBindingFlags)
        {
            object val = GetValue(source, path, bindingAttr);
            if (typeof(T) == typeof(string))
            {
                val = val.ToString();
            }
            return (T)val;
        }

        /// <summary>
        /// Invokes a method on the specified object with the specified arguments
        /// </summary>
        /// <param name="source"></param>
        /// <param name="path"></param>
        /// <param name="bindingAttr"></param>
        /// <returns></returns>
        public static object Invoke(object source, string path, object[] args = null, BindingFlags bindingAttr = DefaultBindingFlags)
        {
            MethodInfo methodInfo = source?.GetType().GetMethod(path, bindingAttr);
            return methodInfo?.Invoke(source, args);
        }

        /// <summary>
        /// Invokes a method on the specified object with the specified arguments
        /// </summary>
        /// <param name="source"></param>
        /// <param name="path"></param>
        /// <param name="arg"></param>
        /// <param name="bindingAttr"></param>
        /// <returns></returns>
        public static object Invoke(object source, string path, object arg = null, BindingFlags bindingAttr = DefaultBindingFlags)
        {
            MethodInfo methodInfo = source?.GetType().GetMethod(path, bindingAttr);
            object[] args = ArrayPool.Get(1);
            args[0] = arg;

            try
            {
                return methodInfo?.Invoke(source, args);
            }
            finally
            {
                ArrayPool.Free(args);
            }
        }

        /// <summary>
        /// Get value of field, property, or method from specified object
        /// </summary>
        /// <param name="source"></param>
        /// <param name="path"></param>
        /// <param name="bindingAttr"></param>
        /// <returns></returns>
        public static object GetValue(object source, string path, BindingFlags bindingAttr = DefaultBindingFlags)
        {
            if (source == null)
            {
                return null;
            }

            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentException("Field, property, or method name cannot be empty", nameof(path)); //TODO: Localization
            }

            Type type = source.GetType();
            MemberInfo memberInfo;
            string fieldName = path;
            if (path.IndexOf('.') <= -1)
            {
                memberInfo = GetMember(type, fieldName, bindingAttr);

                if (memberInfo is PropertyInfo propertyInfo)
                {
                    return propertyInfo.GetValue(source, null);
                }
                if (memberInfo is FieldInfo fieldInfo)
                {
                    return fieldInfo.GetValue(source);
                }
                if (memberInfo is MethodInfo methodInfo)
                {
                    return methodInfo.Invoke(source, null);
                }
            }
            else
            {
                string[] parts = path.Split('.');
                string childPath = string.Join(".", parts.Skip(1).ToArray());
                object childValue = null;
                fieldName = parts[0];

                memberInfo = GetMember(type, fieldName, bindingAttr);

                if (memberInfo is PropertyInfo propertyInfo)
                {
                    childValue = propertyInfo.GetValue(source, null);
                }
                if (memberInfo is FieldInfo fieldInfo)
                {
                    childValue = fieldInfo.GetValue(source);
                }
                if (memberInfo is MethodInfo methodInfo)
                {
                    return methodInfo.Invoke(source, null);
                }

                if (childValue != null)
                {
                    return GetValue(childValue, childPath, bindingAttr);
                }
            }

            return null;
        }

        /// <summary>
        /// Find member using specified search path on the specified object
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <param name="bindingAttr"></param>
        /// <returns></returns>
        public static MemberInfo FindMember(Type type, string path, BindingFlags bindingAttr = DefaultBindingFlags)
        {
            if (type == null)
            {
                return null;
            }

            string fieldName = path;
            if (path.IndexOf('.') <= -1)
            {
                return GetMember(type, fieldName, bindingAttr);
            }

            string[] parts = path.Split('.');
            fieldName = parts[0];

            MemberInfo member = GetMember(type, fieldName, bindingAttr);

            return member != null ? FindMember(GetMemberType(member), string.Join(".", parts.Skip(1).ToArray()), bindingAttr) : null;
        }

        /// <summary>
        /// Gets member runtime type
        /// </summary>
        /// <param name="memberInfo"></param>
        /// <returns></returns>
        public static Type GetMemberType(MemberInfo memberInfo)
        {
            if (memberInfo is FieldInfo fieldInfo)
            {
                return fieldInfo.FieldType;
            }

            if (memberInfo is PropertyInfo propertyInfo)
            {
                return propertyInfo.PropertyType;
            }

            if (memberInfo is MethodInfo methodInfo)
            {
                return methodInfo.ReturnType;
            }

            return null;
        }

        /// <summary>
        /// Gets member info with the specified name in the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="memberName"></param>
        /// <param name="bindingAttr"></param>
        /// <returns></returns>
        public static MemberInfo GetMember(Type type, string memberName, BindingFlags bindingAttr = DefaultBindingFlags)
        {
            FieldInfo fieldInfo = type.GetField(memberName, bindingAttr);

            if (fieldInfo != null)
            {
                return fieldInfo;
            }

            MethodInfo methodInfo = type.GetMethod(memberName, bindingAttr);
            if (methodInfo != null)
            {
                return methodInfo;
            }

            PropertyInfo propertyInfo = type.GetProperty(memberName, bindingAttr);
            if (propertyInfo == null)
            {
                return null;
            }
            FieldInfo backingFieldInfo = propertyInfo.DeclaringType?.GetField($"<{propertyInfo.Name}>k__BackingField",
                BackingFieldBindingFlags);

            if (backingFieldInfo != null)
            {
                return backingFieldInfo;
            }

            return propertyInfo;
        }
    }
}
