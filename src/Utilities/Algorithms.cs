﻿using System.Security.Cryptography;

namespace uMod.Utilities
{
    internal static class Algorithms
    {
        public static readonly HashAlgorithm MD5 = System.Security.Cryptography.MD5.Create();
        public static readonly HashAlgorithm SHA1 = System.Security.Cryptography.SHA1.Create();
        public static readonly HashAlgorithm SHA256 = System.Security.Cryptography.SHA256.Create();
        public static readonly HashAlgorithm SHA512 = System.Security.Cryptography.SHA512.Create();
    }
}
