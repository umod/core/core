﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using uMod.Common;

namespace uMod.Utilities
{
    public static class FileUtility
    {
        internal static readonly string[] IgnoredDirectories = { ".git", "bin", "obj", "packages", ".idea" };

        internal static void CopyDirectory(ILogger logger, string sourceDirName, string destDirName, bool copySubDirs = true)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            if (!dir.Exists)
            {
                return;
            }

            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            foreach (FileInfo file in dir.GetFiles())
            {
                string destFileName = Path.Combine(destDirName, file.Name);
                try
                {
                    file.CopyTo(destFileName, true);
                }
                catch (Exception)
                {
                    logger?.Error(Interface.uMod.Strings.Exceptions.FileCopyFailed.Interpolate(("file", file.Name)));
                }
            }

            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string destFileName = Path.Combine(destDirName, subdir.Name);
                    CopyDirectory(logger, subdir.FullName, destFileName);
                }
            }
        }

        /// <summary>
        /// Filters directories using whitelist and blacklist in plugins configuration
        /// </summary>
        /// <returns></returns>
        internal static Func<string, bool> GetPluginDirectoryFilter()
        {
            return delegate(string directory)
            {
                if (File.Exists(directory))
                {
                    directory = Path.GetDirectoryName(directory);
                }

                string dirName = directory.Split(Path.DirectorySeparatorChar).Last();
                string relativeDirPath = directory.Replace(Interface.uMod.PluginDirectory, string.Empty).Trim('/').Trim('\\');
                if (IgnoredDirectories.Contains(dirName, StringComparer.InvariantCultureIgnoreCase))
                {
                    return false;
                }

                if (Interface.uMod?.Plugins?.Configuration?.Watchers?.ExcludeDirectories != null)
                {
                    foreach (string excluded in Interface.uMod.Plugins.Configuration.Watchers.ExcludeDirectories)
                    {
                        if (Regex.IsMatch(relativeDirPath, excluded.NormalizePath().WildcardToRegEx()))
                        {
                            return false;
                        }
                    }
                }

                if (Interface.uMod?.Plugins?.Configuration?.Watchers?.PluginDirectories != null)
                {
                    foreach (string pluginDir in Interface.uMod.Plugins.Configuration.Watchers.PluginDirectories)
                    {
                        if (Regex.IsMatch(relativeDirPath, pluginDir.NormalizePath().WildcardToRegEx()))
                        {
                            return true;
                        }
                    }
                }

                return string.IsNullOrEmpty(relativeDirPath);
            };
        }
    }
}
