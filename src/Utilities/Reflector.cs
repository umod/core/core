using System;
using System.Linq.Expressions;
using System.Reflection;

namespace uMod.Utilities
{
    public class Reflector<TObject>
    {
#if NET35
        private Action<TObject, TValue> GetMutator<TValue>(string name)
        {
            MethodInfo xPropSetter = typeof(TObject)
                .GetProperty(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                .GetSetMethod();
            ParameterExpression newValPar = Expression.Parameter(typeof(TValue), "val");
            ParameterExpression objectPar = Expression.Parameter(typeof(TObject), "source");
            MethodCallExpression callExpr = Expression.Call(objectPar, xPropSetter, newValPar);
            Action<TObject, TValue> setterAction = (Action<TObject, TValue>)
                Expression.Lambda(callExpr, objectPar, newValPar).Compile();

            return setterAction;
        }
#else
        public Action<TObject, TValue> GetMutator<TValue>(string name)
        {
            ParameterExpression paramExpression = Expression.Parameter(typeof(TObject), "source");

            ParameterExpression paramExpression2 = Expression.Parameter(typeof(TValue), name);

            MemberExpression propertyGetterExpression = Expression.Property(paramExpression, name);

            Action<TObject, TValue> result = Expression.Lambda<Action<TObject, TValue>>
            (
                Expression.Assign(propertyGetterExpression, paramExpression2), paramExpression, paramExpression2
            ).Compile();

            return result;
        }
#endif

        public Func<TObject, TResult> GetAccessor<TResult>(string name)
        {
            ParameterExpression param =
                Expression.Parameter(typeof(TObject), "source");

            return (Func<TObject, TResult>)Expression.Lambda(typeof(Func<TObject, TResult>), Expression.PropertyOrField(param, name), param).Compile();
        }

        public TResult GetValue<TResult>(TObject source, string name)
        {
            Func<TObject, TResult> accessor = GetAccessor<TResult>(name);
            return accessor.Invoke(source);
        }

        public TValue SetValue<TValue>(TObject source, string name, TValue value)
        {
            Action<TObject, TValue> mutator = GetMutator<TValue>(name);
            mutator.Invoke(source, value);
            return value;
        }
    }
}
