﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using uMod.Common;

namespace uMod.Libraries
{
    /// <summary>
    /// Indicates that the specified function is a library function with a name
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class LibraryFunction : Attribute
    {
        /// <summary>
        /// Gets the name for the library function
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Creates a library function using the methods name
        /// </summary>
        public LibraryFunction()
        {
        }

        /// <summary>
        /// Creates a library function using the given name
        /// </summary>
        /// <param name="name"></param>
        public LibraryFunction(string name)
        {
            Name = name;
        }
    }

    /// <summary>
    /// Indicates that the specified function is a library property with a name
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class LibraryProperty : Attribute
    {
        /// <summary>
        /// Gets the name for the library property
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Creates a library property using the methods name
        /// </summary>
        public LibraryProperty()
        {
        }

        /// <summary>
        /// Creates a library property using the given name
        /// </summary>
        /// <param name="name"></param>
        public LibraryProperty(string name)
        {
            Name = name;
        }
    }

    /// <summary>
    /// Represents a library containing a set of functions for script languages to use
    /// </summary>
    public abstract class Library : ILibrary, ISingleton
    {
        public static implicit operator bool(Library library) => library != null;

        public static bool operator !(Library library) => !library;

        // Functions stored in this library
        private readonly IDictionary<string, MethodInfo> _functions;

        // Properties stored in this library
        private readonly IDictionary<string, PropertyInfo> _properties;

        /// <summary>
        /// Stores the last exception
        /// </summary>
        public Exception LastException { get; protected set; }

        protected IApplication Application;

        /// <summary>
        /// Initializes a new instance of the Library class
        /// </summary>
        protected Library(IApplication application)
        {
            Application = application;
            _functions = new Dictionary<string, MethodInfo>();
            _properties = new Dictionary<string, PropertyInfo>();

            Type type = GetType();

            foreach (MethodInfo method in type.GetMethods())
            {
                LibraryFunction attribute;
                try
                {
                    attribute = method.GetCustomAttributes(typeof(LibraryFunction), true).SingleOrDefault() as LibraryFunction;
                    if (attribute == null)
                    {
                        continue;
                    }
                }
                catch (TypeLoadException)
                {
                    continue; // Ignore rare exceptions caused by type information being loaded for all methods
                }
                string name = attribute.Name ?? method.Name;
                if (_functions.ContainsKey(name))
                {
                    Interface.uMod.LogError($"{type.FullName} library tried to register an already registered function: {name}");
                }
                else
                {
                    _functions[name] = method;
                }
            }

            foreach (PropertyInfo property in type.GetProperties())
            {
                LibraryProperty attribute;
                try
                {
                    attribute = property.GetCustomAttributes(typeof(LibraryProperty), true).SingleOrDefault() as LibraryProperty;
                    if (attribute == null)
                    {
                        continue;
                    }
                }
                catch (TypeLoadException)
                {
                    continue; // Ignore rare exceptions caused by type information being loaded for all properties
                }
                string name = attribute.Name ?? property.Name;
                if (_properties.ContainsKey(name))
                {
                    Interface.uMod.LogError($"{type.FullName} library tried to register an already registered property: {name}");
                }
                else
                {
                    _properties[name] = property;
                }
            }

            Application.Bind(this);
        }

        public virtual void Initialize()
        {
        }

        /// <summary>
        /// Called to perform any library-specific clean up
        /// </summary>
        public virtual void Shutdown()
        {
            Application.Unbind(this);
        }

        /// <summary>
        /// Gets all function names in this library
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetFunctionNames() => _functions.Keys;

        /// <summary>
        /// Gets all property names in this library
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetPropertyNames() => _properties.Keys;

        /// <summary>
        /// Gets a function by the specified name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public MethodInfo GetFunction(string name)
        {
            return _functions.TryGetValue(name, out MethodInfo info) ? info : null;
        }

        /// <summary>
        /// Gets a property by the specified name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public PropertyInfo GetProperty(string name)
        {
            return _properties.TryGetValue(name, out PropertyInfo info) ? info : null;
        }

        /// <summary>
        /// Gets library property
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public object Get(string name)
        {
            if (_properties.TryGetValue(name, out PropertyInfo info))
            {
                return info.GetValue(this, null);
            }

            return null;
        }

        /// <summary>
        /// Gets library property
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public T Get<T>(string name)
        {
            if (_properties.TryGetValue(name, out PropertyInfo info))
            {
                return (T)info.GetValue(this, null);
            }

            return default;
        }

        /// <summary>
        /// Sets library property
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public T Set<T>(string name, T value)
        {
            if (_properties.TryGetValue(name, out PropertyInfo propertyInfo))
            {
#if NET35 || NET40
                if (propertyInfo.GetSetMethod() == null)
#else
                if (propertyInfo.SetMethod == null)
#endif
                {
                    if (propertyInfo.DeclaringType?.GetField($"<{propertyInfo.Name}>k__BackingField", BindingFlags.NonPublic | BindingFlags.Instance) is FieldInfo backingField)
                    {
                        backingField.SetValue(this, value);
                    }
                    else
                    {
                        return value;
                    }
                }
                else
                {
                    propertyInfo.SetValue(this, value, null);
                }

                return value;
            }

            return value;
        }

        /// <summary>
        /// Invoke library method
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public object Invoke(string name, params object[] parameters)
        {
            if (_functions.TryGetValue(name, out MethodInfo info))
            {
                return info.Invoke(this, parameters);
            }

            return null;
        }

        /// <summary>
        /// Invoke library method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public T Invoke<T>(string name, params object[] parameters)
        {
            if (_functions.TryGetValue(name, out MethodInfo info))
            {
                return (T)info.Invoke(this, parameters);
            }

            return default;
        }
    }
}
