extern alias References;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using References::Newtonsoft.Json;
using uMod.Auth;
using uMod.Common;
using uMod.Configuration.Toml;
using uMod.Pooling;

namespace uMod.Libraries
{
    public class Lang : Library
    {
        #region Initialization

        private IAuthManager _authManager;

        internal const string DefaultLang = "en";
        private readonly Dictionary<string, Dictionary<string, string>> _langFiles;
        private readonly Dictionary<IPlugin, ICallback<IPlugin, IPluginManager>> _pluginRemovedFromManager;
        private readonly ILogger _logger;
        private readonly string _langDirectory;

        /// <summary>
        /// Initializes a new instance of the Lang class
        /// </summary>
        public Lang(IApplication application, ILogger logger) : base(application)
        {
            _logger = logger;
            _langFiles = new Dictionary<string, Dictionary<string, string>>();
            _pluginRemovedFromManager = new Dictionary<IPlugin, ICallback<IPlugin, IPluginManager>>();
            _langDirectory = Interface.uMod.LangDirectory;
        }

        /// <summary>
        /// Set auth provider
        /// </summary>
        /// <param name="manager"></param>
        internal void SetProvider(IAuthManager manager)
        {
            _authManager = manager;
        }

        #endregion Initialization

        #region Library Functions

        /// <summary>
        /// Registers a language set for a plugin
        /// </summary>
        /// <param name="messages"></param>
        /// <param name="plugin"></param>
        /// <param name="lang"></param>
        [LibraryFunction("RegisterMessages")]
        public void RegisterMessages(Dictionary<string, string> messages, IPlugin plugin = null, string lang = DefaultLang)
        {
            RegisterJson(messages, plugin, lang);
        }

        [LibraryFunction("RegisterJsonMessages")]
        public void RegisterJson(Dictionary<string, string> messages, IPlugin plugin = null, string lang = DefaultLang)
        {
            if (messages == null || string.IsNullOrEmpty(lang))
            {
                return;
            }

            string name = plugin?.Name ?? "umod";


            string file = $"{lang}{Path.DirectorySeparatorChar}{name}.json";
            RegisterMessages(name, file, messages, plugin, lang);
        }

        [LibraryFunction("RegisterTomlMessages")]
        public void RegisterToml(Dictionary<string, string> messages, IPlugin plugin = null, string lang = DefaultLang)
        {
            if (messages == null || string.IsNullOrEmpty(lang))
            {
                return;
            }

            string name = plugin?.Name ?? "umod";


            string file = $"{lang}{Path.DirectorySeparatorChar}{name}.toml";
            RegisterMessages(name, file, messages, plugin, lang);
        }

        private void RegisterMessages(string name, string file, Dictionary<string, string> messages, IPlugin plugin = null,
            string lang = DefaultLang)
        {
            bool changed;
            Dictionary<string, string> existingMessages = GetMessageFile(file, name, lang);
            if (existingMessages == null)
            {
                _langFiles.Remove(file);
                AddLangFile(file, messages, plugin);
                changed = true;
            }
            else
            {
                changed = MergeMessages(existingMessages, messages);
                _langFiles.Remove(file);
                AddLangFile(file, messages, plugin);
                messages = existingMessages;
            }

            if (changed)
            {
                if (!Directory.Exists(Path.Combine(_langDirectory, lang)))
                {
                    Directory.CreateDirectory(Path.Combine(_langDirectory, lang));
                }

                FileInfo fileInfo = new FileInfo(Path.Combine(_langDirectory, file));

                string content;
                if (Path.GetExtension(file) == ".json")
                {
                    content = JsonConvert.SerializeObject(messages, Formatting.Indented);
                }
                else
                {
                    content = TomlConvert.SerializeObject(messages);
                }

                using (FileStream stream = fileInfo.Open(FileMode.OpenOrCreate, FileAccess.Write, FileShare.Delete | FileShare.ReadWrite))
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    stream.SetLength(0);
                    writer.Write(content);
                }
            }
        }

        /// <summary>
        /// Gets the language for the player, fall back to the default server language if no language is set
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        [LibraryFunction("GetLanguage")]
        public string GetLanguage(string playerId)
        {
            return _authManager.GetLanguage(playerId);
        }

        /// <summary>
        /// Gets all available languages or only those for the specified plugin
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="suffix"></param>
        /// <returns></returns>
        [LibraryFunction("GetLanguages")]
        public string[] GetLanguages(IPlugin plugin = null, string suffix = null)
        {
            List<string> languages = Pools.GetList<string>();
            try
            {
                foreach (string directory in Directory.GetDirectories(_langDirectory))
                {
                    if (Directory.GetFiles(directory).Length != 0)
                    {
                        if (plugin == null)
                        {
                            languages.Add(directory.Substring(_langDirectory.Length + 1));
                        }
                        else
                        {
                            string jsonFileName = !string.IsNullOrEmpty(suffix) ? $"{plugin.Name}_{suffix}.json" : $"{plugin.Name}.json";
                            string tomlFileName = !string.IsNullOrEmpty(suffix) ? $"{plugin.Name}_{suffix}.toml" : $"{plugin.Name}.toml";
                            if (File.Exists(Path.Combine(directory, jsonFileName)) || File.Exists(Path.Combine(directory, tomlFileName)))
                            {
                                languages.Add(directory.Substring(_langDirectory.Length + 1));
                            }
                        }
                    }
                }

                return languages.ToArray();
            }
            finally
            {
                Pools.FreeList(ref languages);
            }
        }

        /// <summary>
        /// Gets all language files for the specified plugin
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="suffix"></param>
        /// <returns></returns>
        [LibraryFunction("GetLanguageFiles")]
        public string[] GetLanguageFiles(IPlugin plugin, string suffix = null)
        {
            List<string> languages = Pools.GetList<string>();
            try
            {
                foreach (string directory in Directory.GetDirectories(_langDirectory))
                {
                    if (Directory.GetFiles(directory).Length != 0)
                    {
                        string jsonFileName = !string.IsNullOrEmpty(suffix) ? $"{plugin.Name}_{suffix}.json" : $"{plugin.Name}.json";
                        string jsonFullPath = Path.Combine(directory, jsonFileName);

                        if (File.Exists(jsonFullPath))
                        {
                            languages.Add(jsonFullPath.Substring(_langDirectory.Length + 1));
                        }
                        else
                        {
                            string tomlFileName = !string.IsNullOrEmpty(suffix) ? $"{plugin.Name}_{suffix}.toml" : $"{plugin.Name}.toml";
                            string tomlFullPath = Path.Combine(directory, tomlFileName);

                            if (File.Exists(tomlFullPath))
                            {
                                languages.Add(tomlFullPath.Substring(_langDirectory.Length + 1));
                            }
                        }
                    }
                }

                return languages.ToArray();
            }
            finally
            {
                Pools.FreeList(ref languages);
            }
        }

        /// <summary>
        /// Checks if translations exist for the specified language
        /// </summary>
        /// <param name="language"></param>
        /// <param name="plugin"></param>
        /// <param name="suffix"></param>
        /// <returns></returns>
        [LibraryFunction("HasLanguage")]
        public bool HasLanguage(string language, IPlugin plugin = null, string suffix = null)
        {
            string directory = Path.Combine(_langDirectory, language);
            if (Directory.Exists(directory) && Directory.GetFiles(directory).Length != 0)
            {
                if (plugin == null)
                {
                    return true;
                }

                string jsonFileName = !string.IsNullOrEmpty(suffix) ? $"{plugin.Name}_{suffix}.json" : $"{plugin.Name}.json";
                string tomlFileName = !string.IsNullOrEmpty(suffix) ? $"{plugin.Name}_{suffix}.toml" : $"{plugin.Name}.toml";
                if (File.Exists(Path.Combine(directory, jsonFileName)) || File.Exists(Path.Combine(directory, tomlFileName)))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets a message using the specified key for the specified plugin and player
        /// </summary>
        /// <param name="key"></param>
        /// <param name="plugin"></param>
        /// <param name="playerId"></param>
        /// <returns></returns>
        [LibraryFunction("GetMessage")]
        public string GetMessage(string key, IPlugin plugin = null, string playerId = null)
        {
            return GetMessageKey(key, plugin, GetLanguage(playerId));
        }

        /// <summary>
        /// Gets a message using the specified key for the specified plugin and language
        /// </summary>
        /// <param name="key"></param>
        /// <param name="plugin"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [LibraryFunction("GetLangMessage")]
        public string GetLangMessage(string key, IPlugin plugin = null, string lang = null)
        {
            return GetMessageKey(key, plugin, lang);
        }

        /// <summary>
        /// Gets all messages for a plugin in a language
        /// </summary>
        /// <returns></returns>
        [LibraryFunction("GetMessages")]
        public Dictionary<string, string> GetMessages(string lang, IPlugin plugin)
        {
            if (!string.IsNullOrEmpty(lang) && plugin != null)
            {
                string name = plugin?.Name ?? "umod";
                string tomlFilePath = $"{lang}{Path.DirectorySeparatorChar}{name}.toml";
                string jsonFilePath = $"{lang}{Path.DirectorySeparatorChar}{name}.json";
                if (!_langFiles.TryGetValue(jsonFilePath, out Dictionary<string, string> langFile) &&
                    !_langFiles.TryGetValue(tomlFilePath, out langFile))
                {
                    langFile = GetMessageFile(jsonFilePath, plugin.Name, lang);
                    if (langFile != null)
                    {
                        AddLangFile(jsonFilePath, langFile, plugin);
                    }
                }

                return langFile.ToDictionary(k => k.Key, v => v.Value);
            }

            return new Dictionary<string, string>();
        }

        /// <summary>
        /// Gets the default language for the server
        /// </summary>
        /// <returns></returns>
        [LibraryFunction("GetServerLanguage")]
        public string GetServerLanguage()
        {
            if (_authManager == null)
            {
                return CultureInfo.CurrentCulture.TwoLetterISOLanguageName == "iv"
                    ? DefaultLang
                    : CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
            }

            return _authManager.GetServerLanguage();
        }

        /// <summary>
        /// Sets the language preference for the player
        /// </summary>
        /// <param name="lang"></param>
        /// <param name="playerId"></param>
        [LibraryFunction("SetLanguage")]
        public void SetLanguage(string lang, string playerId)
        {
            _authManager.SetLanguage(playerId, lang);
        }

        /// <summary>
        /// Sets the default language for the server
        /// </summary>
        /// <param name="lang"></param>
        [LibraryFunction("SetServerLanguage")]
        public void SetServerLanguage(string lang)
        {
            _authManager.SetServerLanguage(lang);
        }

        #endregion Library Functions

        #region Lang Handling

        /// <summary>
        /// Caches a filename and attaches the plugin remove callback
        /// </summary>
        /// <param name="file"></param>
        /// <param name="langFile"></param>
        /// <param name="plugin"></param>
        private void AddLangFile(string file, Dictionary<string, string> langFile, IPlugin plugin)
        {
            if (_langFiles.ContainsKey(file))
            {
                _langFiles[file] = langFile;
            }
            else
            {
                _langFiles.Add(file, langFile);
            }

            if (plugin != null && !_pluginRemovedFromManager.ContainsKey(plugin))
            {
                _pluginRemovedFromManager[plugin] = plugin.OnRemovedFromManager.Add(plugin_OnRemovedFromManager);
            }
        }

        /// <summary>
        /// Loads a specific language file for a plugin
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        private Dictionary<string, string> GetMessageFile(string file, string plugin, string lang = DefaultLang)
        {
            if (!string.IsNullOrEmpty(plugin))
            {
                foreach (char invalidChar in Path.GetInvalidFileNameChars())
                {
                    lang = lang.Replace(invalidChar, '_');
                }
                string filename = Path.Combine(_langDirectory, file);
                if (Path.GetExtension(file) == ".json")
                {
                    return File.Exists(filename)
                        ? JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText(filename))
                        : null;
                }

                return File.Exists(filename) ? TomlConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText(filename)) : null;
            }

            return new Dictionary<string, string>();
        }

        /// <summary>
        /// Loads a specific key from the requested language file for a plugin
        /// </summary>
        /// <param name="key"></param>
        /// <param name="plugin"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        private string GetMessageKey(string key, IPlugin plugin = null, string lang = DefaultLang)
        {
            string name = plugin?.Name ?? "umod";

            string jsonFilePath = $"{lang}{Path.DirectorySeparatorChar}{name}.json";
            if (_langFiles.TryGetValue(jsonFilePath, out _))
            {
                return GetMessageKey(name, jsonFilePath, key, plugin, lang);
            }
            string tomlFilePath = $"{lang}{Path.DirectorySeparatorChar}{name}.toml";
            return GetMessageKey(name, tomlFilePath, key, plugin, lang);
        }

        private string GetMessageKey(string name, string file, string key, IPlugin plugin = null, string lang = DefaultLang)
        {
            if (!_langFiles.TryGetValue(file, out Dictionary<string, string> langFile))
            {
                langFile = GetMessageFile(file, name, lang) ?? GetMessageFile(file, name, _authManager.GetServerLanguage()) ?? GetMessageFile(file, name);
                if (langFile == null || langFile.Count == 0)
                {
                    if (plugin != null)
                    {
                        _logger.Warning(Interface.uMod.Strings.Plugin.MessagesNotFound.Interpolate("plugin", name));
                    }

                    return key;
                }

                Dictionary<string, string> defaultLangFile = GetMessageFile(file, name);
                if (defaultLangFile != null && MergeMessages(langFile, defaultLangFile) && File.Exists(Path.Combine(_langDirectory, file)))
                {
                    FileInfo fileInfo = new FileInfo(Path.Combine(_langDirectory, file));
                    string content;
                    if (Path.GetExtension(file) == ".json")
                    {
                        content = JsonConvert.SerializeObject(langFile, Formatting.Indented);
                    }
                    else
                    {
                        content = TomlConvert.SerializeObject(langFile);
                    }

                    using (FileStream stream = fileInfo.Open(FileMode.OpenOrCreate, FileAccess.Write, FileShare.Delete | FileShare.ReadWrite))
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        stream.SetLength(0);
                        writer.Write(content);
                    }
                }
                AddLangFile(file, langFile, plugin);
            }

            return langFile.TryGetValue(key, out string message) ? message : Interface.uMod.Strings.Plugin.MessageNotFound.Interpolate("key", key);
        }

        /// <summary>
        /// Update an existing language file by adding new keys and removing old keys
        /// </summary>
        /// <param name="existingMessages"></param>
        /// <param name="messages"></param>
        /// <returns></returns>
        private bool MergeMessages(Dictionary<string, string> existingMessages, Dictionary<string, string> messages)
        {
            bool changed = false;
            foreach (KeyValuePair<string, string> message in messages)
            {
                if (!existingMessages.ContainsKey(message.Key))
                {
                    existingMessages.Add(message.Key, message.Value);
                    changed = true;
                }
            }
            if (existingMessages.Count > 0)
            {
                foreach (string message in existingMessages.Keys.ToArray())
                {
                    if (!messages.ContainsKey(message))
                    {
                        existingMessages.Remove(message);
                        changed = true;
                    }
                }
            }
            return changed;
        }

        /// <summary>
        /// Saves all data to the "umod.lang" file
        /// </summary>
        internal IPromise SaveData() => _authManager.Save(AuthData.Users);

        /// <summary>
        /// Called when the plugin was unloaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="manager"></param>
        private void plugin_OnRemovedFromManager(IPlugin sender, IPluginManager manager)
        {
            if (_pluginRemovedFromManager.TryGetValue(sender, out ICallback<IPlugin, IPluginManager> callback))
            {
                callback.Remove();
                _pluginRemovedFromManager.Remove(sender);
            }
            string[] langs = GetLanguages(sender);
            foreach (string lang in langs)
            {
                _langFiles.Remove($"{lang}{Path.DirectorySeparatorChar}{sender.Name}.json");
                _langFiles.Remove($"{lang}{Path.DirectorySeparatorChar}{sender.Name}.toml");
            }
        }

        #endregion Lang Handling
    }
}
