﻿using System.Collections.Generic;
using uMod.Common;

namespace uMod.Libraries
{
    public class LibraryProvider : ILibraryProvider
    {
        private readonly IExtensionProvider _extensionProvider;

        public IEnumerable<ILibrary> All => _extensionProvider.Manager.GetLibraries();

        public ILibrary this[string name] => Get(name);

        public LibraryProvider(IExtensionProvider extensionProvider)
        {
            _extensionProvider = extensionProvider;
        }

        /// <summary>
        /// Register a library using the specified name and library
        /// </summary>
        /// <param name="name"></param>
        /// <param name="library"></param>
        public void Register(string name, ILibrary library)
        {
            _extensionProvider.Manager.RegisterLibrary(name, library);
        }

        /// <summary>
        /// Gets the library by the specified name or generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public T Get<T>(string name = null) where T : class, ILibrary
        {
            return _extensionProvider.Manager.GetLibrary(name ?? typeof(T).Name) as T;
        }

        /// <summary>
        /// Gets the library by the specified name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ILibrary Get(string name)
        {
            return _extensionProvider.Manager.GetLibrary(name);
        }
    }
}
