using System;
using System.Collections.Generic;
using System.Net;
using uMod.Common;
using uMod.Common.Web;
using uMod.Libraries;

namespace uMod.Web
{
    public class Request : IWebRequest, IDisposable
    {
        /// <summary>
        /// Callback invoked when request is cancelled
        /// </summary>
        private Action _canceledAction;

        /// <summary>
        /// Default user agent
        /// </summary>
        private static string UserAgent =>
            $"uMod/{Module.Version} ({Environment.OSVersion}; {Environment.OSVersion.Platform}; https://umod.org)";

        /// <summary>
        /// Stores default request headers
        /// </summary>
        private static IDictionary<string, string> _defaultHeaders;

        /// <summary>
        /// Gets default request headers
        /// </summary>
        private static IDictionary<string, string> DefaultHeaders
        {
            get
            {
                if (_defaultHeaders == null)
                {
                    return _defaultHeaders = new Dictionary<string, string>()
                    {
                        ["User-Agent"] = UserAgent
                    };
                }

                return _defaultHeaders;
            }
        }

        /// <summary>
        /// Maximum request duration
        /// </summary>
        public float Timeout { get; internal set; } = Interface.uMod?.Web?.Configuration?.DefaultTimeout ?? 30f;

        /// <summary>
        /// Request HTTP method (GET, POST, etc)
        /// </summary>
        public WebRequestMethod Method { get; internal set; } = WebRequestMethod.GET;

        /// <summary>
        /// Request Url
        /// </summary>
        public string Url { get; internal set; }

        /// <summary>
        /// Request Body
        /// </summary>
        public string Body { get; internal set; }

        /// <summary>
        /// Request cookies
        /// </summary>
        public IDictionary<string, string> Cookies { get; internal set; }

        /// <summary>
        /// Request headers
        /// </summary>
        public IDictionary<string, string> Headers { get; internal set; }

        /// <summary>
        /// Request form input
        /// </summary>
        public IDictionary<string, FormSection> Form { get; internal set; }

        /// <summary>
        /// Request file uploads
        /// </summary>
        public IDictionary<string, IMultipartFile> Files { get; internal set; }

        /// <summary>
        /// Request Context
        /// </summary>
        public IContext Context;

        /// <summary>
        /// Web client
        /// </summary>
        private Apps.WebClient _client;

        /// <summary>
        /// Callback associated with unloading the owner plugin
        /// </summary>
        private ICallback<IPlugin, IPluginManager> _removedFromManager;

        /// <summary>
        /// The response of this request.
        /// </summary>
        private IPromise<Common.Web.WebResponse> _response;

        /// <summary>
        /// Create new Request
        /// </summary>
        /// <param name="client"></param>
        /// <param name="context"></param>
        internal Request(Apps.WebClient client, IContext context)
        {
            _client = client;
            Context = context;
            if (_removedFromManager == null && context is IPlugin plugin)
            {
                _removedFromManager = plugin.OnRemovedFromManager.Add(owner_OnRemovedFromManager);
            }
        }

        /// <summary>
        /// Create new Request without context
        /// </summary>
        public Request()
        {
        }

        /// <summary>
        /// Reset request after pooling
        /// </summary>
        /// <param name="client"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        internal Request Reset(Apps.WebClient client, IContext context)
        {
            _client = client;
            Context = context;
            if (context is IPlugin plugin)
            {
                _removedFromManager = plugin.OnRemovedFromManager.Add(owner_OnRemovedFromManager);
            }
            return this;
        }

        /// <summary>
        /// Get intermediate WebRequestData for web client
        /// </summary>
        /// <returns></returns>
        internal Common.Web.WebRequest GetData()
        {
            if (Headers == null)
            {
                Headers = DefaultHeaders;
            }
            else
            {
                if (!Headers.ContainsKey("User-Agent"))
                {
                    Headers.Add("User-Agent", UserAgent);
                }
                else
                {
                    Headers["User-Agent"] = UserAgent;
                }
            }

            return new Common.Web.WebRequest
            {
                Method = Method,
                Url = Url,
                Timeout = (int)Math.Round((Timeout.Equals(0f) ? (Interface.uMod?.Web?.Configuration?.DefaultTimeout ?? 30f) : Timeout)),
                Decompression = WebRequests.AllowDecompression ? (DecompressionMethods.GZip | DecompressionMethods.Deflate) : DecompressionMethods.None,
                Headers = Headers,
                Cookies = Cookies,
                Body = Body,
                Address = GetLocalEndpoint(),
                Files = Files,
                Form = Form
            };
        }

        /// <summary>
        /// Gets the local endpoint
        /// </summary>
        /// <returns></returns>
        private string GetLocalEndpoint()
        {
            if (!string.IsNullOrEmpty(Interface.uMod?.Web?.Configuration?.PreferredEndpoint))
            {
                return Interface.uMod.Web.Configuration.PreferredEndpoint;
            }

            return Interface.uMod?.Server?.LocalAddress != null
                ? Interface.uMod.Server.LocalAddress.ToString()
                : Interface.uMod?.Server?.Address?.ToString();
        }

        /// <summary>
        /// Invoke web request
        /// </summary>
        internal virtual IPromise<Common.Web.WebResponse> Invoke()
        {
            if (_response == null)
            {
                ISettleablePromise<Common.Web.WebResponse> promise = Promise.Create<Common.Web.WebResponse>();
                _client?.EnqueueRequest(GetData(), promise);
                _response = promise;
            }
            return _response;
        }

        /// <summary>
        /// Callback if plugin is unloaded
        /// </summary>
        /// <param name="canceledAction"></param>
        /// <returns></returns>
        public Request Canceled(Action canceledAction)
        {
            _canceledAction = canceledAction;
            return this;
        }

        /// <summary>
        /// Called when the owner plugin was unloaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="manager"></param>
        private void owner_OnRemovedFromManager(IPlugin sender, IPluginManager manager)
        {
            bool pending = true;
            _response.FinallyPeek(() => pending = false);
            if (pending)
            {
                _canceledAction?.Invoke();
            }
        }

        /// <summary>
        /// Dispose of Request
        /// </summary>
        public void Dispose()
        {
            Url = null;
            Body = null;
            Cookies = null;
            Headers = null;
            Files = null;
            Form = null;
            _canceledAction = null;
            if (_removedFromManager != null)
            {
                Event.Remove(ref _removedFromManager);
            }
        }
    }
}
