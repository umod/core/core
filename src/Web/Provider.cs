﻿using System;
using System.Collections.Generic;
using uMod.Plugins;
using uMod.WebSockets;

namespace uMod.Web
{
    public class Provider
    {
        internal Configuration.Web Configuration;
        internal Apps.WebClient Application;
        internal WebSockets.Provider WebSockets;

        private Dictionary<string, string> _panels = new Dictionary<string, string>();
        internal BatchPluginLoader AdminPluginLoader;

        internal Provider(Configuration.Web configuration)
        {
            Configuration = configuration;
            if (Interface.uMod.AppManager.IsInstalled("WebSockets"))
            {
                WebSockets = new WebSockets.Provider(this);
            }
        }

        /// <summary>
        /// Register admin panel
        /// </summary>
        /// <param name="name"></param>
        /// <param name="uri"></param>
        internal void RegisterPanel(string name, string uri)
        {
#if DEBUG
            if (!_panels.ContainsKey(name))
            {
                _panels.Add(name, uri);
            }
            else
            {
                _panels[name] = uri;
            }
#else
            Uri outUri;
            if (Uri.TryCreate(uri, UriKind.Absolute, out outUri)
               && (outUri.Scheme == Uri.UriSchemeHttp || outUri.Scheme == Uri.UriSchemeHttps))
            {
                if (!_panels.ContainsKey(name))
                {
                    _panels.Add(name, uri);
                }
                else
                {
                    _panels[name] = uri;
                }
            }
            else
            {
                throw new InvalidOperationException($"Uri invalid ({uri}). Uri must be absolute and http or https");
            }
#endif

        }

        /// <summary>
        /// Array of session ids currently connected to rcon
        /// </summary>
        private string[] _sessionIds = Pooling.StringPool.Empty;

        /// <summary>
        /// Store session ids for rcon connections
        /// </summary>
        /// <param name="sessionIds"></param>
        internal void RegisterSessions(string[] sessionIds)
        {
            _sessionIds = sessionIds;
        }

        /// <summary>
        /// Checks if specified session id is connection to rcon server
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        internal bool HasSession(string sessionId)
        {
            return Array.IndexOf(_sessionIds, sessionId) > -1;
        }

        /// <summary>
        /// Unregister admin panels
        /// </summary>
        /// <param name="name"></param>
        internal void Unregister(string name)
        {
            _panels.Remove(name);
        }

        /// <summary>
        /// Gets admin panels
        /// </summary>
        /// <returns></returns>
        internal Dictionary<string, string> GetPanels()
        {
            return _panels;
        }
    }
}
