﻿using System;
using System.Collections.Generic;
using uMod.Common;
using uMod.Common.Web;

namespace uMod.Web
{
    public sealed class Client
    {
        private readonly Apps.WebClient _client;
        private readonly IContext _context;

        /// <summary>
        /// Create a plugin web wrapper for the specified plugin
        /// </summary>
        /// <param name="context"></param>
        public Client(IContext context = null)
        {
            _client = Interface.uMod.Web.Application;
            _context = context;
        }

        /// <summary>
        /// Create web request
        /// </summary>
        /// <param name="method"></param>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="body"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Request(string method, string url, IDictionary<string, string> headers = null, string body = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            WebRequestMethod requestMethod = (WebRequestMethod)Enum.Parse(typeof(WebRequestMethod), method);
            return Request(requestMethod, url, headers, body, cookies, timeout);
        }

        /// <summary>
        /// Create web request
        /// </summary>
        /// <param name="method"></param>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Request(string method, string url, IDictionary<string, string> headers = null)
        {
            WebRequestMethod requestMethod = (WebRequestMethod)Enum.Parse(typeof(WebRequestMethod), method);
            return Request(requestMethod, url, headers, null, null, 0f);
        }

        /// <summary>
        /// Create web request
        /// </summary>
        /// <param name="method"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Request(string method, string url)
        {
            WebRequestMethod requestMethod = (WebRequestMethod)Enum.Parse(typeof(WebRequestMethod), method);
            return Request(requestMethod, url, null, null, null, 0f);
        }

        /// <summary>
        /// Create web request
        /// </summary>
        /// <param name="method"></param>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="form"></param>
        /// <param name="files"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Request(string method, string url, IDictionary<string, string> headers = null, IDictionary<string, FormSection> form = null, IDictionary<string, IMultipartFile> files = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            WebRequestMethod requestMethod = (WebRequestMethod)Enum.Parse(typeof(WebRequestMethod), method);
            return Request(requestMethod, url, headers, form, files, cookies, timeout);
        }

        /// <summary>
        /// Create web request
        /// </summary>
        /// <param name="method"></param>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="body"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Request(WebRequestMethod method, string url, IDictionary<string, string> headers = null, string body = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            Request request = Pooling.Pools.Requests.Get().Reset(_client, _context);

            request.Method = method;
            request.Url = url;
            request.Headers = headers;
            request.Timeout = timeout;
            request.Body = body;
            request.Cookies = cookies;

            return request.Invoke();
        }

        /// <summary>
        /// Create web request
        /// </summary>
        /// <param name="method"></param>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Request(WebRequestMethod method, string url, IDictionary<string, string> headers = null)
        {
            Request request = Pooling.Pools.Requests.Get().Reset(_client, _context);

            request.Method = method;
            request.Url = url;
            request.Headers = headers;
            request.Timeout = 0f;
            request.Body = null;
            request.Cookies = null;

            return request.Invoke();
        }

        /// <summary>
        /// Create web request
        /// </summary>
        /// <param name="method"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Request(WebRequestMethod method, string url)
        {
            Request request = Pooling.Pools.Requests.Get().Reset(_client, _context);

            request.Method = method;
            request.Url = url;
            request.Headers = null;
            request.Timeout = 0f;
            request.Body = null;
            request.Cookies = null;

            return request.Invoke();
        }

        /// <summary>
        /// Create web request (with multipart/form-data)
        /// </summary>
        /// <param name="method"></param>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="form"></param>
        /// <param name="files"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Request(WebRequestMethod method, string url, IDictionary<string, string> headers = null, IDictionary<string, FormSection> form = null, IDictionary<string, IMultipartFile> files = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            Request request = Pooling.Pools.Requests.Get().Reset(_client, _context);

            request.Method = method;
            request.Url = url;
            request.Headers = headers;
            request.Timeout = timeout;
            request.Form = form;
            request.Files = files;
            request.Cookies = cookies;

            return request.Invoke();
        }

        /// <summary>
        /// Create GET web request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="body"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Get(string url, IDictionary<string, string> headers = null, string body = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            return Request(WebRequestMethod.GET, url, headers, body, cookies, timeout);
        }

        /// <summary>
        /// Create POST web request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="body"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Post(string url, IDictionary<string, string> headers = null, string body = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            return Request(WebRequestMethod.POST, url, headers, body, cookies, timeout);
        }

        /// <summary>
        /// Create POST web request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Post(string url, IDictionary<string, string> headers = null)
        {
            return Request(WebRequestMethod.POST, url, headers, null, null, 0f);
        }

        /// <summary>
        /// Create POST web request
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Post(string url)
        {
            return Request(WebRequestMethod.POST, url, null, null, null, 0f);
        }

        /// <summary>
        /// Create POST web request (with multipart/form-data)
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="form"></param>
        /// <param name="files"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Post(string url, IDictionary<string, string> headers = null, IDictionary<string, FormSection> form = null, IDictionary<string, IMultipartFile> files = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            return Request(WebRequestMethod.POST, url, headers, form, files, cookies, timeout);
        }

        /// <summary>
        /// Create PUT web request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="body"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Put(string url, IDictionary<string, string> headers = null, string body = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            return Request(WebRequestMethod.PUT, url, headers, body, cookies, timeout);
        }

        /// <summary>
        /// Create PUT web request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Put(string url, IDictionary<string, string> headers = null)
        {
            return Request(WebRequestMethod.PUT, url, headers, null, null, 0f);
        }

        /// <summary>
        /// Create PUT web request
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Put(string url)
        {
            return Request(WebRequestMethod.PUT, url, null, null, null, 0f);
        }

        /// <summary>
        /// Create PUT web request (with multipart/form-data)
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="form"></param>
        /// <param name="files"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Put(string url, IDictionary<string, string> headers = null, IDictionary<string, FormSection> form = null, IDictionary<string, IMultipartFile> files = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            return Request(WebRequestMethod.PUT, url, headers, form, files, cookies, timeout);
        }

        /// <summary>
        /// Create DELETE web request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="body"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Delete(string url, IDictionary<string, string> headers = null, string body = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            return Request(WebRequestMethod.DELETE, url, headers, body, cookies, timeout);
        }

        /// <summary>
        /// Create PATCH web request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="body"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Patch(string url, IDictionary<string, string> headers = null, string body = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            return Request(WebRequestMethod.PATCH, url, headers, body, cookies, timeout);
        }

        /// <summary>
        /// Create HEAD web request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise<WebResponse> Head(string url, IDictionary<string, string> headers = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            return Request(WebRequestMethod.HEAD, url, headers, null, cookies, timeout);
        }

        /// <summary>
        /// Unregister plugin data from websocket provider
        /// </summary>
        internal void Unregister()
        {
            Interface.uMod.Web.Unregister(_context.Name);
        }

        /// <summary>
        /// Register panel from the specified uri with the rcon server
        /// </summary>
        /// <param name="uri"></param>
        public void RegisterPanel(string uri)
        {
            Interface.uMod.Web.RegisterPanel(_context.Name, uri);
        }

        /// <summary>
        /// Checks if specified session is connection to rcon server
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public bool HasSession(string sessionId)
        {
            return Interface.uMod.Web.HasSession(sessionId);
        }
    }
}
