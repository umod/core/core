﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using uMod.Common.WebSockets;
using uMod.Configuration.Toml;
using uMod.IO;
using uMod.Plugins;
using uMod.Utilities;

namespace uMod.Configuration
{
    [AttributeUsage(AttributeTargets.Class)]
    public class WebServerAttribute : Attribute
    {
        public readonly string Name;

        public WebServerAttribute(string name)
        {
            Name = name;
        }
    }

    public enum IWebAuthentication
    {
        None,
        UserPass, // Username/password required per user
        Password, // Single global password per-server
        Jwt
    }

    /// <summary>
    /// Represents a Web server interface
    /// </summary>
    public interface IWebServer
    {
        /// <summary>
        /// Gets the location
        /// </summary>
        [TomlProperty("endpoint")]
        string Endpoint { get; }

        /// <summary>
        /// Gets the protocol
        /// </summary>
        [TomlTypeAlias("web_server")]
        [TomlProperty("protocol")]
        string Protocol { get; } /* ws, wss, http, https */

        /// <summary>
        /// Gets the certificate path
        /// </summary>
        [TomlProperty("certificate")]
        string Certificate { get; }

        /// <summary>
        /// Gets the server path
        /// </summary>
        [TomlProperty("path")]
        string Path { get; }

        /// <summary>
        /// Gets whether or not routing is enabled (connection path must match with parameter support, e.g. "{name}")
        /// </summary>
        [TomlProperty("routing")]
        bool Routing { get; }

        /// <summary>
        /// Gets the server idle timeout in minutes
        /// </summary>
        [TomlProperty("timeout")]
        float IdleTimeoutMinutes { get; }

        /// <summary>
        /// Gets the port
        /// </summary>
        [TomlProperty("port")]
        int Port { get; }
    }

    /// <summary>
    /// Represents a WebSocket server interface
    /// </summary>
    public interface IWebSocketServer : IWebServer
    {
        /// <summary>
        /// Gets the server dual stack option
        /// </summary>
        [TomlProperty("dualstack")]
        bool DualStack { get; }

        /// <summary>
        /// Sends specified logger output to connection
        /// </summary>
        [TomlProperty("logger")]
        string Logger { get; }

        /// <summary>
        /// Gets the connection info associated with this connection configuration
        /// </summary>
        [TomlIgnore]
        ConnectionInfo Connection { get; }

        /// <summary>
        /// Configure connection using configuration
        /// </summary>
        /// <param name="config"></param>
        /// <param name="connection"></param>
        void Configure(Web config, ConnectionInfo connection);
    }

    /// <summary>
    /// Represents a Base connection configuration
    /// </summary>
    [Toml]
    [WebServer("ws")]
    public class WebSocketServer : IWebSocketServer, IWebServer
    {
        /// <summary>
        /// Gets the port
        /// </summary>
        [TomlProperty("port"), DefaultValue(8081)]
        public int Port { get; internal set; } = 8081;

        /// <summary>
        /// Gets the connection info associated with this server configuration
        /// </summary>
        [TomlIgnore]
        public ConnectionInfo Connection { get; protected set; }

        /// <summary>
        /// Gets the endpoint (if empty use preferred)
        /// </summary>
        [TomlProperty("endpoint"), DefaultValue("127.0.0.1")]
        public string Endpoint { get; internal set; }

        /// <summary>
        /// Gets the protocol
        /// </summary>
        [TomlProperty("protocol")]
        public string Protocol { get; internal set; } = "ws";

        /// <summary>
        /// Gets the dual stack option
        /// </summary>
        [TomlProperty("dualstack"), DefaultValue(true)]
        public bool DualStack { get; internal set; } = true;

        /// <summary>
        /// Gets the certificate path
        /// </summary>
        [TomlProperty("certificate")]
        public string Certificate { get; internal set; }

        /// <summary>
        /// Gets the path
        /// </summary>
        [TomlProperty("path")]
        public string Path { get; internal set; }

        /// <summary>
        /// Gets whether or not routing is enabled (connection path must match with parameter support, e.g. "{name}")
        /// </summary>
        [TomlProperty("routing")]
        public bool Routing { get; internal set; } = true;

        /// <summary>
        /// Gets the sockets default log channel
        /// </summary>
        [TomlProperty("logger"), DefaultValue("stack")]
        public string Logger { get; internal set; } = "stack";

        /// <summary>
        /// Gets the socket idle timeout in minutes
        /// </summary>
        [TomlProperty("timeout"), DefaultValue(10)]
        public float IdleTimeoutMinutes { get; internal set; } = 10;

        /// <summary>
        /// Gets rcon password
        /// </summary>
        [TomlProperty("password")]
        internal string Password { get; set; }

        /// <summary>
        /// Gets rcon password
        /// </summary>
        [TomlProperty("env")]
        internal string EnvironmentVar { get; set; }

        /// <summary>
        /// Configure connection using configuration
        /// </summary>
        /// <param name="config"></param>
        /// <param name="connection"></param>
        public virtual void Configure(Web config, ConnectionInfo connection)
        {
            Connection = connection;
            if (!Interface.uMod.AppManager.IsInstalled("WebSockets"))
            {
                return;
            }

            WebSockets.RconAdminPlugin adminPlugin = Interface.uMod.Plugins.Find<WebSockets.RconAdminPlugin>()?.FirstOrDefault();

            if (connection.Name == "rcon")
            {
                Routing = false;
                if (adminPlugin == null)
                {
                    config.RegisterAdminLoader();
                }
            }
        }
    }

    [Toml]
    public class Web : TomlFile
    {
        [TomlProperty("timeout")]
        public float DefaultTimeout;

        [TomlProperty("endpoint")]
        public string PreferredEndpoint;

        /// <summary>
        /// Default socket servers
        /// </summary>
        [TomlProperty("servers")]
        [TomlAggregate]
        public Dictionary<string, IWebServer> Servers;

        internal const string DefaultAdminConnectionName = "rcon";

        /// <summary>
        /// Sets defaults for the web configuration
        /// </summary>
        public Web(string filename) : base(filename)
        {
            DefaultTimeout = 30f;
            PreferredEndpoint = string.Empty;

            Servers = GetDefaultServers();
        }

        private Dictionary<string, IWebServer> GetDefaultServers()
        {
            return new Dictionary<string, IWebServer>()
            {
                { DefaultAdminConnectionName, new WebSocketServer()
                {
                    EnvironmentVar = "rcon.password", // Do not duplicate RCON password in configuration
                    Routing = false
                } }
            };
        }

        /// <summary>
        /// Resolve all IDatabaseConnection interfaces from core and extensions
        /// </summary>
        internal static void ResolveConnectionTypes<T>(TypeManager types) where T : IWebServer
        {
            IEnumerable<Type> webServers = types.Resolve<T>();
            foreach (Type serverType in webServers)
            {
                if (serverType.IsAbstract || serverType.IsInterface)
                {
                    continue;
                }

                if (serverType.GetCustomAttribute<WebServerAttribute>() is WebServerAttribute serverAttribute)
                {
                    Interface.uMod.Application.Resolve(serverType);
                    Interface.uMod.Application.Alias($"{serverAttribute.Name}web_server", serverType);
                }
            }
        }

        /// <summary>
        /// Configure all connections
        /// </summary>
        internal void Initialize()
        {
            if (Servers == null)
            {
                Servers = GetDefaultServers();
            }

            foreach (KeyValuePair<string, IWebServer> kvp in Servers)
            {
                if (kvp.Value is WebSocketServer socketServer)
                {
                    List<string> pathParts = new List<string>();
                    if (!string.IsNullOrEmpty(socketServer.Path))
                    {
                        pathParts.Add(socketServer.Path);
                    }
                    if (!string.IsNullOrEmpty(socketServer.Password))
                    {
                        pathParts.Add(socketServer.Password);
                    }
                    if (!string.IsNullOrEmpty(socketServer.EnvironmentVar))
                    {
                        Interface.uMod.CommandLine.GetArgument(socketServer.EnvironmentVar, out _, out string secret);
                        if (!string.IsNullOrEmpty(secret))
                        {
                            pathParts.Add(secret);
                        }
                    }

#if !DEBUG
                    // Do not allow admin rcon server to launch without password configured
                    if (kvp.Key == DefaultAdminConnectionName && pathParts.Count == 0)
                    {
                        Interface.uMod.LogWarning("Rcon server launch prevented: No password specified.");
                        continue;
                    }
#endif

                    ConnectionInfo connection = new ConnectionInfo()
                    {
                        Name = kvp.Key,
                        Location = socketServer.Endpoint ?? PreferredEndpoint,
                        Port = socketServer.Port,
                        Protocol = socketServer.Protocol,
                        SupportDualStack = socketServer.DualStack,
                        Path = string.Join("/", pathParts.ToArray()),
                        IdleTimeoutMinutes = socketServer.IdleTimeoutMinutes
                    };

                    socketServer.Configure(this, connection);
                }
                /*
                else
                {
                    TODO: Generic http/https server
                } */
            }
        }

        /// <summary>
        /// Ensure rcon websocket server plugin is loaded
        /// </summary>
        internal void RegisterAdminLoader()
        {
            if(Interface.uMod.Web.AdminPluginLoader != null)
            {
                Interface.uMod.Extensions.UnregisterLoader(Interface.uMod.Web.AdminPluginLoader);
            }
            Interface.uMod.Web.AdminPluginLoader = new BatchPluginLoader(Interface.uMod.RootLogger, typeof(WebSockets.RconAdminPlugin));
            Interface.uMod.Extensions.RegisterLoader(Interface.uMod.Web.AdminPluginLoader);
        }
    }
}
