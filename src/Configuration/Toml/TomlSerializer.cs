using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using Tommy;
using uMod.Common;
using uMod.Exceptions;
using uMod.Pooling;
using uMod.Text;
using StringWriter = uMod.Text.StringWriter;

namespace uMod.Configuration.Toml
{
    /// <summary>
    /// Represents a TOML serializer
    /// </summary>
    public class TomlSerializer
    {
        private readonly IApplication _application;
        private readonly TomlContractResolver _resolver;

        public TomlSerializer(IApplication application, TomlContractResolver resolver)
        {
            _application = application;
            _resolver = resolver;
        }

        /// <summary>
        /// Deserialize Toml content to an object of the specified type
        /// </summary>
        /// <param name="toml"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public object Deserialize(string toml, Type type, out IEnumerable<Exception> exceptions)
        {
            List<Exception> exceptionList = Pools.GetList<Exception>();
            try
            {
                TomlTable table;
                using (StringReader reader = new StringReader(toml))
                {
                    try
                    {
                        table = TOML.Parse(reader);
                    }
                    catch (TomlParseException ex)
                    {
                        exceptionList.AddRange(ex.SyntaxErrors.Cast<Exception>());
                        table = ex.ParsedTable;
                    }
                }

                object obj = CreateObject(type);

                if (type == typeof(TomlTable))
                {
                    exceptions = exceptionList.Count > 0 ? exceptionList.ToArray() : Enumerable.Empty<Exception>();

                    return table;
                }

                try
                {
                    Map(obj, table);
                }
                catch (Exception ex)
                {
                    exceptionList.Add(ex);
                }

                exceptions = exceptionList.Count > 0 ? exceptionList.ToArray() : Enumerable.Empty<Exception>();

                return obj;
            }
            finally
            {
                Pools.FreeList(ref exceptionList);
            }
        }

        /// <summary>
        /// Deserialize the specified Toml content and populate the specified object
        /// </summary>
        /// <param name="toml"></param>
        /// <param name="value"></param>
        public void Populate(string toml, object value, out IEnumerable<Exception> exceptions)
        {
            List<Exception> exceptionList = Pools.GetList<Exception>();
            try
            {
                TomlTable table;
                using (StringReader reader = new StringReader(toml))
                {
                    try
                    {
                        table = TOML.Parse(reader);
                    }
                    catch (TomlParseException ex)
                    {
                        exceptionList.AddRange(ex.SyntaxErrors.Cast<Exception>());
                        table = ex.ParsedTable;
                    }
                }

                try
                {
                    Map(value, table);
                }
                catch (Exception ex)
                {
                    exceptionList.Add(ex);
                }

                exceptions = exceptionList.Count > 0 ? exceptionList.ToArray() : Enumerable.Empty<Exception>();
            }
            finally
            {
                Pools.FreeList(ref exceptionList);
            }
        }

        /// <summary>
        /// Map the specified TomlNode to the specified object
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="node"></param>
        private void Map(object obj, TomlNode node)
        {
            TomlObjectContract contract = _resolver.ResolveContract(obj.GetType());

            List<string> assignedFields = new List<string>();
            foreach (string key in node.Keys)
            {
                TomlNode val = node[key];

                if (contract.Properties.TryGetValue(key, out MemberInfo memberInfo))
                {
                    if (memberInfo is PropertyInfo propertyInfo)
                    {
                        SetProperty(propertyInfo, val, obj);
                        assignedFields.Add(propertyInfo.Name);
                    }
                    else if (memberInfo is FieldInfo fieldInfo)
                    {
                        fieldInfo.SetValue(obj, GetValue(val, fieldInfo.FieldType, memberInfo));
                        assignedFields.Add(fieldInfo.Name);
                    }
                }
                else if (val is TomlTable && contract.AggregateMember != null)
                {
                    if (contract.AggregateMember is PropertyInfo propertyInfo)
                    {
                        if (!contract.AggregateMemberReplaced)
                        {
#if NET35 || NET40
                            propertyInfo.SetValue(obj, null, null);
#else
                            propertyInfo.SetValue(obj, null);
#endif
                            contract.AggregateMemberReplaced = true;
                        }

                        SetAggregateProperty(contract, key, propertyInfo, val, obj);
                        assignedFields.Add(propertyInfo.Name);
                    }
                    else if (contract.AggregateMember is FieldInfo fieldInfo)
                    {
                        if (!contract.AggregateMemberReplaced)
                        {
                            fieldInfo.SetValue(obj, null);
                            contract.AggregateMemberReplaced = true;
                        }

                        fieldInfo.SetValue(obj, AggregateValue(contract, key, fieldInfo.GetValue(obj), val, fieldInfo.FieldType));
                        assignedFields.Add(fieldInfo.Name);
                    }
                }
            }

            foreach (KeyValuePair<string, MemberInfo> kvp in contract.Properties)
            {
                // If the field was not assigned but has a default value, assign it the default value
                // https://docs.microsoft.com/en-us/dotnet/api/system.componentmodel.defaultvalueattribute#remarks
                if (!assignedFields.Contains(kvp.Key))
                {
                    DefaultValueAttribute defaultValueAttribute = kvp.Value.GetCustomAttribute<DefaultValueAttribute>(true);
                    if (defaultValueAttribute?.Value != null)
                    {
                        if (kvp.Value is PropertyInfo propertyInfo)
                        {
                            SetProperty(propertyInfo, defaultValueAttribute.Value, obj);
                        }
                        else if (kvp.Value is FieldInfo fieldInfo)
                        {
                            fieldInfo.SetValue(obj, defaultValueAttribute.Value);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Map node to object property and auto-property with missing setter
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="val"></param>
        /// <param name="obj"></param>
        private void SetProperty(PropertyInfo propertyInfo, TomlNode val, object obj)
        {
#if NET35 || NET40
            if (propertyInfo.GetSetMethod() == null)
#else
            if (propertyInfo.SetMethod == null)
#endif
            {
                if (propertyInfo.DeclaringType?.GetField($"<{propertyInfo.Name}>k__BackingField", BindingFlags.NonPublic | BindingFlags.Instance) is FieldInfo backingField)
                {
                    backingField.SetValue(obj, GetValue(val, propertyInfo.PropertyType, propertyInfo));
                }
            }
            else
            {
                propertyInfo.SetValue(obj, GetValue(val, propertyInfo.PropertyType, propertyInfo), null);
            }
        }

        /// <summary>
        /// Map node to object property and auto-property with missing setter
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="val"></param>
        /// <param name="obj"></param>
        private void SetProperty(PropertyInfo propertyInfo, object val, object obj)
        {
#if NET35 || NET40
            if (propertyInfo.GetSetMethod() == null)
#else
            if (propertyInfo.SetMethod == null)
#endif
            {
                if (propertyInfo.DeclaringType?.GetField($"<{propertyInfo.Name}>k__BackingField", BindingFlags.NonPublic | BindingFlags.Instance) is FieldInfo backingField)
                {
                    backingField.SetValue(obj, val);
                }
            }
            else
            {
                propertyInfo.SetValue(obj, val, null);
            }
        }

        /// <summary>
        /// Aggregate extra tables to dictionary
        /// </summary>
        /// <param name="contract"></param>
        /// <param name="key"></param>
        /// <param name="propertyInfo"></param>
        /// <param name="val"></param>
        /// <param name="obj"></param>
        private void SetAggregateProperty(TomlObjectContract contract, string key, PropertyInfo propertyInfo, TomlNode val, object obj)
        {
#if NET35 || NET40
            if (propertyInfo.GetSetMethod() == null)
#else
            if (propertyInfo.SetMethod == null)
#endif
            {
                if (propertyInfo.DeclaringType?.GetField($"<{propertyInfo.Name}>k__BackingField", BindingFlags.NonPublic | BindingFlags.Instance) is FieldInfo backingField)
                {
#if NET35 || NET40
                    backingField.SetValue(obj, AggregateValue(contract, key, propertyInfo.GetValue(obj, null), val, propertyInfo.PropertyType));
#else
                    backingField.SetValue(obj, AggregateValue(contract, key, propertyInfo.GetValue(obj), val, propertyInfo.PropertyType));
#endif
                }
                else
                {
                    return;
                }
            }
            else
            {
#if NET35 || NET40
                propertyInfo.SetValue(obj, AggregateValue(contract, key, propertyInfo.GetValue(obj, null), val, propertyInfo.PropertyType), null);
#else
                propertyInfo.SetValue(obj, AggregateValue(contract, key, propertyInfo.GetValue(obj), val, propertyInfo.PropertyType), null);
#endif
            }
        }

        /// <summary>
        /// Aggregate extra table value to dictionary
        /// </summary>
        /// <param name="contract"></param>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        /// <param name="node"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public object AggregateValue(TomlObjectContract contract, string key, object obj, TomlNode node, Type type)
        {
            if (node is TomlTable)
            {
                if (obj == null)
                {
                    obj = CreateObject(type);
                }

                if (obj is IDictionary dict)
                {
                    Type valueType = type.GetGenericArguments()[1];

                    if (valueType.IsInterface)
                    {
                        MemberInfo memberInfo = valueType.GetMembers().FirstOrDefault(x => x.GetCustomAttribute<TomlTypeAliasAttribute>() != null);

                        if (memberInfo != null)
                        {
                            TomlTypeAliasAttribute tomlTypeAlias = memberInfo.GetCustomAttribute<TomlTypeAliasAttribute>();
                            TomlPropertyAttribute tomlPropertyAttribute = memberInfo.GetCustomAttribute<TomlPropertyAttribute>();

                            if (tomlTypeAlias != null && tomlPropertyAttribute != null)
                            {
                                if (node.HasKey(tomlPropertyAttribute.PropertyName) &&
                                    node[tomlPropertyAttribute.PropertyName] is TomlString tomlString)
                                {
                                    if (_application.TryGetType($"{tomlString.Value}{tomlTypeAlias.PostFix}",
                                        out Type impliedType))
                                    {
                                        valueType = impliedType;
                                    }
                                    else
                                    {
                                        throw new InvalidOperationException($"Cannot create instance of interface {valueType.Name} for {key}. Missing type: {tomlString.Value}{tomlTypeAlias.PostFix}");
                                    }
                                }
                                else
                                {
                                    throw new InvalidOperationException($"Cannot create instance of interface {valueType.Name} for {key}. Node \"{tomlPropertyAttribute.PropertyName}\" missing or invalid");
                                }
                            }
                            else
                            {
                                throw new InvalidOperationException($"Cannot create instance of interface {valueType.Name} for {key}. Add a TomlPropertyAttribute");
                            }
                        }
                        else
                        {
                            throw new InvalidOperationException($"Cannot create instance of interface {valueType.Name} for {key}. Add a TomlTypeAlias");
                        }
                    }

                    object value = GetValue(node, valueType);
                    if (!dict.Contains(key))
                    {
                        dict.Add(key, value);
                    }

                    return dict;
                }
            }

            return null;
        }

        internal Type InferType(TomlNode node)
        {
            if (node == null)
            {
                return null;
            }

            if (node.HasValue)
            {
                switch (node)
                {
                    case TomlString _:
                        return typeof(string);

                    case TomlInteger _:
                        return typeof(long);

                    case TomlFloat _:
                        return typeof(double);

                    case TomlDateTime _:
                        return typeof(DateTime);

                    case TomlBoolean _:
                        return typeof(bool);

                    case TomlArray _:
                        if (node.IsArray)
                        {
                            Type elementType = InferType(node.Children.FirstOrDefault());
                            if (elementType != null)
                            {
                                return elementType.MakeArrayType();
                            }
                        }
                        break;
                }
            }

            if (node is TomlTable)
            {
                return typeof(TomlObject);
            }

            return null;
        }

        /// <summary>
        /// Convert the specified TomlNode to the specified type
        /// </summary>
        /// <param name="node"></param>
        /// <param name="type"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        internal object GetValue(TomlNode node, Type type = null, MemberInfo member = null)
        {
            if (node.HasValue)
            {
                switch (node)
                {
                    case TomlString str:
                        if (type == typeof(char))
                        {
                            return Convert.ChangeType(str.Value, type);
                        }
                        if (type?.IsEnum ?? false)
                        {
                            return Enum.Parse(type, str.Value, true);
                        }
                        return str.Value;

                    case TomlInteger i:
                        return Convert.ChangeType(i.Value, type);

                    case TomlFloat f:
                        return Convert.ChangeType(f.Value, type);

                    case TomlDateTime dt:
                        return Convert.ChangeType(dt.Value, type);

                    case TomlBoolean b:
                        return Convert.ChangeType(b.Value, type);

                    case TomlArray arr:
                        if (type == null)
                        {
                            Type elementType = InferType(arr.RawArray.FirstOrDefault());
                            Array obj = Array.CreateInstance(elementType, arr.ChildrenCount);

                            int x = 0;
                            foreach (TomlNode arrValue in arr.Children)
                            {
                                obj.SetValue(GetValue(arrValue, elementType), x);
                                x++;
                            }

                            return obj;
                        }
                        else if (type.IsArray)
                        {
                            Type elementType = type.GetElementType();
                            Array obj = Array.CreateInstance(elementType, arr.ChildrenCount);

                            int x = 0;
                            foreach (TomlNode arrValue in arr.Children)
                            {
                                obj.SetValue(GetValue(arrValue, elementType), x);
                                x++;
                            }

                            return obj;
                        }
                        else if (type?.IsAssignableFrom<IList>() ?? false)
                        {
                            Type elementType = type.GetGenericArguments()[0];
                            IList list = (IList)CreateObject(type);
                            foreach (TomlNode arrValue in arr.Children)
                            {
                                list.Add(GetValue(arrValue, elementType));
                            }

                            return list;
                        }
                        else if (type?.IsAssignableFrom(typeof(HashSet<>)) ?? false)
                        {
                            Type elementType = type.GetGenericArguments()[0];
                            ICollection list = (ICollection)CreateObject(type);
                            MethodInfo addMethod = type.GetMethod("Add");
                            object[] parameters = { null };
                            if (addMethod != null)
                            {
                                foreach (TomlNode arrValue in arr.Children)
                                {
                                    parameters[0] = GetValue(arrValue, elementType);
                                    addMethod.Invoke(list, parameters);
                                }
                            }

                            return list;
                        }
                        break;
                }
            }

            if (node is TomlTable t && type != null)
            {
                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                {
                    Type valueType = type.GetGenericArguments()[1];
                    IDictionary dict = (IDictionary)CreateObject(type);

                    foreach (string key in node.Keys)
                    {
                        TomlNode subNode = node[key];

                        dict.Add(key, GetValue(subNode, valueType));
                    }

                    return dict;
                }

                if (type.IsInterface && member != null)
                {
                    object tomlPropertyObject = member.GetCustomAttributes(typeof(TomlPropertyAttribute), true)
                        .FirstOrDefault();
                    if (tomlPropertyObject is TomlPropertyAttribute tomlProperty &&
                        tomlProperty.DefaultType != null)
                    {
                        type = tomlProperty.DefaultType;
                    }
                }

                if (type == typeof(TomlObject))
                {
                    TomlObject tomlObject = new TomlObject(node);
                    return tomlObject;
                }

                object obj = CreateObject(type);

                Map(obj, t);
                return obj;
            }

            return null;
        }

        /// <summary>
        /// Instantiates instance of type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private object CreateObject(Type type)
        {
            object obj;
            try
            {
                obj = Activator.CreateInstance(type);
            }
            catch (MissingMethodException missingMethodException)
            {
                throw new TomlMappingException($"No parameterless constructor defined for type: {type.Name}.", missingMethodException);
            }

            return obj;
        }

        /// <summary>
        /// Serialize the specified object to a Toml compatible string
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public string Serialize(object value, Type type = null)
        {
            if (!(value is TomlTable table))
            {
                Type outType = type ?? value.GetType();
                TomlCommentAttribute commentAttribute = null;

                if (type != null)
                {
                    commentAttribute = type.GetCustomAttribute<TomlCommentAttribute>();
                }

                if (commentAttribute == null)
                {
                    commentAttribute = value.GetType().GetCustomAttribute<TomlCommentAttribute>();
                }

                table = ToTable(value, outType, commentAttribute?.Comment);
            }

            PooledStringBuilder stringBuilder = Pools.StringBuilders.Get();
            using (StringWriter stringWriter = new StringWriter(stringBuilder))
            {
                table.WriteTo(stringWriter);
            }

            try
            {
                return stringBuilder.ToString();
            }
            finally
            {
                Pooling.Pools.StringBuilders.Free(stringBuilder);
            }
        }

        /// <summary>
        /// Convert the specified object to a TomlTable
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        private TomlTable ToTable(object value, Type type = null, string comment = null)
        {
            TomlObjectContract contract = _resolver.ResolveContract(value?.GetType() ?? type);
            TomlObjectContract mergedContract = null;
            if (value?.GetType() != null && value.GetType() != type)
            {
                mergedContract = _resolver.ResolveContract(type);
            }

            TomlTable table = new TomlTable()
            {
                Comment = comment
            };

            if (type != null &&
                typeof(IDictionary).IsAssignableFrom(type) &&
                type.GetGenericArguments().Length > 0 &&
                type.GetGenericArguments()[0] == typeof(string) &&
                value is IDictionary valueDict)
            {
                foreach (object key in valueDict.Keys)
                {
                    if (valueDict[key] != null)
                    {
                        TomlNode node = ToNode(table, valueDict[key], valueDict[key].GetType());
                        if (node != null)
                        {
                            table.Add(key as string, node);
                        }
                    }
                }

                return table;
            }

            Dictionary<string, IEnumerable<Attribute>> tomlAttributes = null;
            if (mergedContract != null)
            {
                tomlAttributes = new Dictionary<string, IEnumerable<Attribute>>();
                foreach (KeyValuePair<string, MemberInfo> kvp in mergedContract.Properties)
                {
                    if (kvp.Value is FieldInfo fieldInfo)
                    {
                        tomlAttributes.Add(kvp.Key, fieldInfo.GetCustomAttributes(true).Cast<Attribute>());
                    }
                    else if (kvp.Value is PropertyInfo propertyInfo)
                    {
                        tomlAttributes.Add(kvp.Key, propertyInfo.GetCustomAttributes(true).Cast<Attribute>());
                    }
                }
            }

            foreach (KeyValuePair<string, MemberInfo> kvp in contract.Properties)
            {
                bool aggregate;
                if (kvp.Value is FieldInfo fieldInfo)
                {
                    aggregate = fieldInfo.GetCustomAttribute<TomlAggregateAttribute>() != null;
                    List<Attribute> fieldAttributes = fieldInfo.GetCustomAttributes(true).Cast<Attribute>().ToList();
                    if (mergedContract != null && tomlAttributes.ContainsKey(kvp.Key))
                    {
                        fieldAttributes.AddRange(tomlAttributes[kvp.Key]);
                    }

                    bool skipDefaultAssignment = false;
                    var defaultValueAttribute = fieldInfo.GetCustomAttribute<DefaultValueAttribute>(true);
                    object val = fieldInfo.GetValue(value);
                    if (defaultValueAttribute != null)
                    {
                        object defaultValue = defaultValueAttribute.Value;
                        if (Equals(val, defaultValue))
                        {
                            skipDefaultAssignment = true;
                        }
                    }
                    if (!skipDefaultAssignment && val != null)
                    {
                        TomlNode node = ToNode(table, val, fieldInfo.FieldType, aggregate, fieldAttributes);
                        if (node != null)
                        {
                            table.Add(kvp.Key, node);
                        }
                    }
                }
                else if (kvp.Value is PropertyInfo propertyInfo)
                {
                    aggregate = propertyInfo.GetCustomAttribute<TomlAggregateAttribute>() != null;
                    List<Attribute> propertyAttributes =
                        propertyInfo.GetCustomAttributes(true).Cast<Attribute>().ToList();

                    if (mergedContract != null && tomlAttributes.ContainsKey(kvp.Key))
                    {
                        propertyAttributes.AddRange(tomlAttributes[kvp.Key]);
                    }
                    bool skipDefaultAssignment = false;
                    var defaultValueAttribute = propertyInfo.GetCustomAttribute<DefaultValueAttribute>(true);
                    object val = propertyInfo.GetValue(value, null);
                    if (defaultValueAttribute != null)
                    {
                        object defaultValue = defaultValueAttribute.Value;
                        if (Equals(val, defaultValue))
                        {
                            skipDefaultAssignment = true;
                        }
                    }
                    if (!skipDefaultAssignment && val != null)
                    {
                        TomlNode node = ToNode(table, val, propertyInfo.PropertyType, aggregate, propertyAttributes);
                        if (node != null)
                        {
                            table.Add(kvp.Key, node);
                        }
                    }
                }
            }

            return table;
        }

        /// <summary>
        /// Convert the specified value to a TOML type
        /// </summary>
        /// <param name="parentNode"></param>
        /// <param name="value"></param>
        /// <param name="fieldType"></param>
        /// <param name="aggregate"></param>
        /// <param name="attributes"></param>
        /// <returns></returns>
        internal TomlNode ToNode(TomlNode parentNode, object value, Type fieldType, bool aggregate = false, IEnumerable<Attribute> attributes = null)
        {
            if (value == null)
            {
                return null;
            }

            TomlPropertyAttribute propertyAttribute = attributes?.FirstOrDefault(x => x is TomlPropertyAttribute) as TomlPropertyAttribute;
            TomlCommentAttribute commentAttribute = attributes?.Where(x => x is TomlCommentAttribute).Select(x => (TomlCommentAttribute)x).FirstOrDefault() ?? fieldType.GetCustomAttribute<TomlCommentAttribute>();

            string comment = commentAttribute?.Comment ?? propertyAttribute?.Comment;

            if (!fieldType.IsValueType && !fieldType.IsPrimitive)
            {
                switch (value)
                {
                    case string _:
                        return new TomlString()
                        {
                            Value = value.ToString(),
                            IsMultiline = propertyAttribute?.MultiLine ?? false,
                            Comment = comment
                        };

                    case DateTime dt:
                        return new TomlDateTime()
                        {
                            Value = dt,
                            Comment = comment
                        };

                    default:
                        {
                            if (fieldType.IsArray)
                            {
                                TomlArray arr = new TomlArray
                                {
                                    IsTableArray = (attributes?.Where(x => x is TomlTableArrayAttribute)).Any(),
                                    Comment = comment,
                                };

                                Type elementType = fieldType.GetElementType();
                                foreach (object arrItem in (Array)value)
                                {
                                    if (arrItem != null)
                                    {
                                        arr.RawArray.Add(ToNode(arr, arrItem, elementType));
                                    }
                                }

                                return arr;
                            }
                            if (fieldType.IsAssignableFrom<IList>())
                            {
                                Type[] genericArguments = fieldType.GetGenericArguments();
                                if (genericArguments.Length == 1)
                                {
                                    Type elementType = genericArguments[0];
                                    TomlArray arr = new TomlArray
                                    {
                                        IsTableArray = (attributes?.Where(x => x is TomlTableArrayAttribute)).Any(),
                                        Comment = comment
                                    };

                                    foreach (object arrItem in (IList)value)
                                    {
                                        if (arrItem != null)
                                        {
                                            arr.RawArray.Add(ToNode(arr, arrItem, elementType));
                                        }
                                    }

                                    return arr;
                                }
                            }

                            Type[] interfaces = fieldType.GetInterfaces();
                            if (Array.Exists(interfaces, @interface => @interface.IsAssignableFrom(typeof(ICollection<>))))
                            {
                                Type[] genericArguments = fieldType.GetGenericArguments();
                                if (genericArguments.Length == 1)
                                {
                                    Type elementType = genericArguments[0];
                                    TomlArray arr = new TomlArray
                                    {
                                        IsTableArray = (attributes?.Where(x => x is TomlTableArrayAttribute)).Any(),
                                        Comment = comment
                                    };

                                    MethodInfo enumMethod = fieldType.GetMethod("GetEnumerator");
                                    IEnumerator enumerator = (IEnumerator)enumMethod.Invoke(value, null);

                                    while (enumerator.MoveNext())
                                    {
                                        object arrItem = enumerator.Current;
                                        if (arrItem != null)
                                        {
                                            arr.RawArray.Add(ToNode(arr, arrItem, elementType));
                                        }
                                    }

                                    return arr;
                                }
                            }

                            break;
                        }
                }

                if (fieldType.IsGenericType && fieldType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                {
                    TomlTable table = new TomlTable
                    {
                        Comment = comment
                    };

                    if (aggregate)
                    {
                        foreach (DictionaryEntry entry in (IDictionary)value)
                        {
                            if (entry.Value != null)
                            {
                                parentNode.Add(entry.Key.ToString(),
                                    ToNode(table, entry.Value, entry.Value.GetType()));
                            }
                        }

                        return null;
                    }

                    foreach (DictionaryEntry entry in (IDictionary)value)
                    {
                        if (entry.Value != null)
                        {
                            table.Add(entry.Key.ToString(), ToNode(table, entry.Value, entry.Value.GetType()));
                        }
                    }

                    return table;
                }
                if (fieldType.IsClass || fieldType.IsInterface)
                {
                    return ToTable(value, fieldType, comment);
                }
            }
            else
            {
                switch (value)
                {
                    case int _:
                    case long _:
                    case ulong _:
                    case short _:
                    case ushort _:
                    case uint _:
                        return new TomlInteger()
                        {
                            Value = (long)Convert.ChangeType(value, typeof(long)),
                            IntegerBase = propertyAttribute?.IntegerBase ?? TomlInteger.Base.Decimal,
                            Comment = comment
                        };

                    case char _:
                        return new TomlString()
                        {
                            Value = value.ToString(),
                            Comment = comment,
                            PreferLiteral = true
                        };

                    case float _:
                        return new TomlFloat()
                        {
                            Value = Convert.ToDouble(value),
                            Comment = comment
                        };

                    case decimal _:
                    case double _:
                        return new TomlFloat()
                        {
                            Value = (double)value,
                            Comment = comment
                        };

                    case bool boolValue:
                        return new TomlBoolean()
                        {
                            Value = boolValue,
                            Comment = comment
                        };

                    default:
                        {
                            if (fieldType.IsEnum)
                            {
                                return new TomlString()
                                {
                                    Value = value.ToString().ToLower(),
                                    Comment = comment
                                };
                            }

                            break;
                        }
                }
            }

            return null;
        }
    }
}
