﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using uMod.Pooling;

namespace uMod.Configuration.Toml
{
    /// <summary>
    /// Determines which fields and properties can or should be serialized in TOML
    /// </summary>
    public class TomlContractResolver
    {
        internal Cache<Type, TomlObjectContract> CachedContracts = new Cache<Type, TomlObjectContract>();

        private readonly object _cacheLock = new object();

        /// <summary>
        /// Gets a TomlObjectContract for the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public TomlObjectContract ResolveContract(Type type)
        {
            TomlObjectContract contract;
            lock (_cacheLock)
            {
                if (CachedContracts.TryGetValue(type, out contract))
                {
                    return contract;
                }
            }

            contract = MakeContract(type);

            lock (_cacheLock)
            {
                if (!CachedContracts.ContainsKey(type))
                {
                    CachedContracts.Add(type, contract);
                }
            }

            return contract;
        }

        /// <summary>
        /// Make a contract that represents how data a type should be serialized
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private TomlObjectContract MakeContract(Type type)
        {
            TomlObjectContract contract = new TomlObjectContract(type);
            Dictionary<string, MemberInfo> members = new Dictionary<string, MemberInfo>();

            FieldInfo[] allFields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            PropertyInfo[] allProperties = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);

            List<MemberInfo> allMembers = Pools.GetList<MemberInfo>();
            List<MemberInfo> defaultMembers = Pools.GetList<MemberInfo>();
            try
            {
                allMembers.AddRange(allFields.Where(FilterField).Cast<MemberInfo>());
                allMembers.AddRange(allProperties.Where(FilterProperty).Cast<MemberInfo>());

                FieldInfo[] defaultFields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
                PropertyInfo[] defaultProperties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

                defaultMembers.AddRange(defaultFields.Where(FilterField).Cast<MemberInfo>());
                defaultMembers.AddRange(defaultProperties.Where(FilterProperty).Cast<MemberInfo>());
                foreach (MemberInfo member in allMembers)
                {
                    if (member.GetCustomAttribute<TomlAggregateAttribute>() != null)
                    {
                        contract.AggregateMember = member;
                    }

                    TomlPropertyAttribute propertyAttribute;
                    if (defaultMembers.Contains(member))
                    {
                        if ((propertyAttribute = member.GetCustomAttribute<TomlPropertyAttribute>()) != null)
                        {
                            AddMember(ref members, propertyAttribute.PropertyName ?? member.Name, member);
                        }
                        else
                        {
                            AddMember(ref members, member.Name, member);
                        }
                    }
                    else
                    {
                        if ((propertyAttribute = member.GetCustomAttribute<TomlPropertyAttribute>()) != null)
                        {
                            AddMember(ref members, propertyAttribute.PropertyName ?? member.Name, member);
                        }
                    }
                }
            }
            finally
            {
                Pools.FreeList(ref allMembers);
                Pools.FreeList(ref defaultMembers);
            }

            contract.Properties = members;

            return contract;
        }

        /// <summary>
        /// Prevents argument exception
        /// </summary>
        /// <param name="members"></param>
        /// <param name="name"></param>
        /// <param name="member"></param>
        protected void AddMember(ref Dictionary<string, MemberInfo> members, string name, MemberInfo member)
        {
            if (members.ContainsKey(name))
            {
                members[name] = member;
            }
            else
            {
                members.Add(name, member);
            }
        }

        /// <summary>
        /// Field filter
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        private bool FilterField(FieldInfo field)
        {
            return !field.IsDefined(typeof(CompilerGeneratedAttribute), true) && field.GetCustomAttribute<TomlIgnoreAttribute>() == null;
        }

        /// <summary>
        /// Property filter
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        private bool FilterProperty(PropertyInfo property)
        {
            return property.GetIndexParameters().Length == 0 && !property.IsDefined(typeof(CompilerGeneratedAttribute), true) && property.GetCustomAttribute<TomlIgnoreAttribute>() == null;
        }
    }
}
