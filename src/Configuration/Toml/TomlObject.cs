﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tommy;

namespace uMod.Configuration.Toml
{
    /// <summary>
    /// Simple TOML object
    /// </summary>
    public struct TomlObject : IEquatable<TomlObject>, IEnumerable<KeyValuePair<string, object>>
    {
        /// <summary>
        /// Empty TOML object
        /// </summary>
        public static TomlObject Empty = default;

        /// <summary>
        /// Gets TOML node
        /// </summary>
        internal TomlNode Node;

        /// <summary>
        /// Create new TOML object
        /// </summary>
        /// <param name="filename"></param>
        public TomlObject(TomlNode node = null)
        {
            Node = node;
        }

        /// <summary>
        /// Gets TOML value by string
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object Get(string key)
        {
            return Get<object>(key);
        }

        /// <summary>
        /// Gets TOML value by integer
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object Get(int key)
        {
            return Get<object>(key);
        }

        /// <summary>
        /// Gets TOML value of type by string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T Get<T>(string key) where T : class
        {
            TomlNode node = Node?[key];
            if (node == null)
            {
                return default;
            }

            T val = null;
            if (typeof(T) != typeof(object))
            {
                if (node is TomlTable)
                {
                    val = (T)TomlConvert.Serializer.GetValue(node, typeof(T));
                }
                else if (node is TomlArray)
                {
                    val = (T)TomlConvert.Serializer.GetValue(node, typeof(T));
                }
            }
            else
            {
                val = TomlConvert.Serializer.GetValue(node, TomlConvert.Serializer.InferType(node)) as T;
            }

            return val;
        }

        /// <summary>
        /// Gets TOML value of type by integer
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T Get<T>(int key) where T : class
        {
            TomlNode node = Node?[key];
            if (node == null)
            {
                return default;
            }

            T val = null;
            if (typeof(T) != typeof(object))
            {
                if (node is TomlTable)
                {
                    val = (T)TomlConvert.Serializer.GetValue(node, typeof(T));
                }
                else if (node is TomlArray)
                {
                    val = (T)TomlConvert.Serializer.GetValue(node, typeof(T));
                }
            }
            else
            {
                val = TomlConvert.Serializer.GetValue(node, TomlConvert.Serializer.InferType(node)) as T;
            }

            return val;
        }

        /// <summary>
        /// Sets TOML value by string
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public TomlObject Set(string key, TomlObject value)
        {
            return (TomlObject)SetValue(key, value);
        }

        /// <summary>
        /// Sets TOML value of type by string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public T Set<T>(string key, T value) where T : class
        {
            return (T)SetValue(key, value);
        }

        /// <summary>
        /// Sets TOML value of type by integer
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public T Set<T>(int key, T value) where T : class
        {
            return (T)SetValue(key, value);
        }

        /// <summary>
        /// Sets TOML value by string
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private object SetValue(string key, object value)
        {
            if (value == null)
            {
                if (Node is TomlTable t)
                {
                    t.RawTable.Remove(key);
                }
                else if (Node is TomlArray a && int.TryParse(key, out int intKey))
                {
                    a.RawArray.RemoveAt(intKey);
                }

                return default;
            }

            if (Node == null)
            {
                Node = new TomlTable();
            }

            if (value is TomlObject tomlObject)
            {
                Node[key] = tomlObject.Node;
            }
            else
            {
                Node[key] = TomlConvert.Serializer.ToNode(Node, value, value.GetType());
            }

            return value;
        }

        /// <summary>
        /// Sets TOML value by integer
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private object SetValue(int key, object value)
        {
            if (value == null)
            {
                if (Node is TomlTable t)
                {
                    t.RawTable.Remove(key.ToString());
                }
                else if (Node is TomlArray a)
                {
                    a.RawArray.RemoveAt(key);
                }
                return default;
            }

            if (Node == null)
            {
                Node = new TomlTable();
            }

            Node[key] = TomlConvert.Serializer.ToNode(Node, value, value.GetType());
            return value;
        }

        /// <summary>
        /// Gets or sets a setting on this config by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object this[string key]
        {
            get
            {
                return Get<object>(key);
            }
            set
            {
                SetValue(key, value);
            }
        }

        /// <summary>
        /// Gets or sets a setting on this config by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object this[int key]
        {
            get => Get<object>(key);
            set
            {
                Set(key, value);
            }
        }

        /// <summary>
        /// Gets TOML object enumerator
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IEnumerator<KeyValuePair<string, T>> GetEnumerator<T>() where T : class
        {
            List<KeyValuePair<string, T>> list = new List<KeyValuePair<string, T>>();
            foreach (string key in Node.Keys)
            {
                TomlNode node = Node?[key];
                if (node != null)
                {
                    T val;
                    if (node is TomlTable || node is TomlArray)
                    {
                        val = (T)TomlConvert.Serializer.GetValue(node, typeof(T));
                    }
                    else
                    {
                        val = TomlConvert.Serializer.GetValue(node, TomlConvert.Serializer.InferType(node)) as T;
                    }

                    if (val != null)
                    {
                        list.Add(new KeyValuePair<string, T>(key, val));
                    }
                }
            }

            return list.GetEnumerator();
        }

        /// <summary>
        /// Gets TOML object enumerator
        /// </summary>
        /// <returns></returns>
        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            List<KeyValuePair<string, object>> list = new List<KeyValuePair<string, object>>();
            foreach (string key in Node.Keys)
            {
                TomlNode node = Node?[key];
                if (node != null)
                {
                    object val;
                    if (node is TomlTable)
                    {
                        val = new TomlObject(node);
                    }
                    else
                    {
                        val = TomlConvert.Serializer.GetValue(node, TomlConvert.Serializer.InferType(node));
                    }

                    list.Add(new KeyValuePair<string, object>(key, val));
                }
            }

            return list.GetEnumerator();
        }

        /// <summary>
        /// Gets TOML object enumerator
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Determine if TOML object equals another
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(TomlObject other)
        {
            return Equals(Node, other.Node);
        }

        /// <summary>
        /// Determine if TOML object equals another
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return obj is TomlObject other && Equals(other);
        }

        /// <summary>
        /// Gets TOML object hashcode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (Node != null ? Node.GetHashCode() : 0);
        }

        /// <summary>
        /// Determine if TOML object equals another
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator ==(TomlObject left, TomlObject right)
        {
            return Equals(left, right);
        }

        /// <summary>
        /// Determine if TOML object equals another
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator !=(TomlObject left, TomlObject right)
        {
            return !Equals(left, right);
        }
    }
}
