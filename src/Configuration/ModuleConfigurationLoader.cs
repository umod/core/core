﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using uMod.Common;
using uMod.Common.Database;
using uMod.Database;
using uMod.IO;
using uMod.Plugins.Watchers;

namespace uMod.Configuration
{
    /// <summary>
    /// Reloads global configuration files when they change
    /// </summary>
    internal class ModuleConfigurationLoader
    {
        private readonly Module _module;

        /// <summary>
        /// Create a new ModuleConfigurationLoader class
        /// </summary>
        /// <param name="module"></param>
        public ModuleConfigurationLoader(Module module)
        {
            _module = module;
        }

        /// <summary>
        /// Reload auth configuration
        /// </summary>
        public void LoadAuthConfiguration()
        {
            AuthDriver oldDriver = _module.Auth.Configuration.Driver;

            Auth authConfiguration = DataFile.Load<Auth>(Path.Combine(_module.InstanceDirectory, "auth.toml"));

            if (authConfiguration.HasExceptions)
            {
                authConfiguration.Report();
            }

            _module.Auth.Configuration = authConfiguration;

            if (authConfiguration.Driver != oldDriver)
            {
                _module.InitializeAuth(null, false).Done(delegate ()
                {
                    string oldDriverName = Enum.GetName(typeof(AuthDriver), oldDriver);
                    if (oldDriver == AuthDriver.Database)
                    {
                        oldDriverName = "<old connection>";
                    }

                    if (string.IsNullOrEmpty(oldDriverName))
                    {
                        oldDriverName = "<old driver>";
                    }
                    _module?.RootLogger.Warning(Interface.uMod.Strings.Database.AuthDriverChanged.Interpolate(("old", oldDriverName.ToLower())));
                });
            }
        }

        /// <summary>
        /// Reload database configuration
        /// </summary>
        public void LoadDatabaseConfiguration()
        {
            string oldDefault = _module.Database.Configuration.Default;
            // Close existing connections
            List<string> openConnectionNames = new List<string>();
            if (_module.Database.Client.Connections != null)
            {
                foreach (KeyValuePair<string, Connection> kvp in _module.Database.Client.Connections)
                {
                    if (kvp.Value.State == ConnectionState.Open)
                    {
                        openConnectionNames.Add(kvp.Key);
                        kvp.Value.Close().Fail(_module.RootLogger.Report);
                    }
                }
            }

            Database databaseConfiguration =
                DataFile.Load<Database>(Path.Combine(_module.InstanceDirectory, "database.toml"));

            if (databaseConfiguration.HasExceptions)
            {
                databaseConfiguration.Report();
            }

            _module.Database.Configuration = databaseConfiguration;

            databaseConfiguration.Initialize();

            // Re-open connections
            foreach (string connectionName in openConnectionNames)
            {
                // If connection was not deleted
                if (databaseConfiguration.Connections.ContainsKey(connectionName))
                {
                    _module.Database.Client.Open(connectionName).Fail(_module.RootLogger.Report);
                }
            }

            if (oldDefault != databaseConfiguration.Default)
            {
                _module.InitializeAuth(null, false).Done(delegate ()
                {
                    _module?.RootLogger.Warning(
                        Interface.uMod.Strings.Database.AuthDriverChanged.Interpolate(("old", oldDefault)));
                });
            }
        }

        /// <summary>
        /// Reload logging configuration
        /// </summary>
        public void LoadLoggingConfiguration()
        {
            Logging loggingConfiguration = DataFile.Load<Logging>(Path.Combine(_module.InstanceDirectory, "logging.toml"));

            if (loggingConfiguration.HasExceptions)
            {
                loggingConfiguration.Report();
            }

            loggingConfiguration.Initialize(_module.RootLogger, _module.Application, !_module.CommandLine.HasVariable("nolog")).Done(delegate (ILogger logger)
            {
                _module.RootLogger?.OnRemoved?.Invoke(_module.RootLogger);
                _module.Logging = loggingConfiguration;
                _module.RootLogger = logger;
            }, delegate (Exception ex)
            {
                _module.OnLoggerFallback();
                _module.RootLogger.Report(_module.Strings.Module.LoggerFailure, ex);
            });
        }

        /// <summary>
        /// Reload plugins configuration
        /// </summary>
        public void LoadPluginsConfiguration()
        {
            bool pluginWatchers = Utility.Plugins.Configuration.Watchers.PluginWatchers;
            bool configWatchers = Utility.Plugins.Configuration.Watchers.ConfigWatchers;

            Plugins pluginsConfiguration =
                DataFile.Load<Plugins>(Path.Combine(_module.InstanceDirectory, "plugins.toml"));

            if (pluginsConfiguration.HasExceptions)
            {
                pluginsConfiguration.Report();
            }

            Utility.Plugins.Configuration = pluginsConfiguration;

            // If plugin watchers changed
            if (pluginWatchers != pluginsConfiguration.Watchers.PluginWatchers)
            {
                // Enable
                if (pluginsConfiguration.Watchers.PluginWatchers)
                {
                    SourceWatcher pluginWatcher = new SourceWatcher(_module.PluginDirectory, "*.cs", SearchOption.AllDirectories);
                    _module.Extensions.Manager.RegisterChangeWatcher(pluginWatcher);
                    pluginWatcher.StartWatcher();
                }
                else // Disable
                {
                    IChangeWatcher pluginWatcher = _module.Extensions.Manager.GetChangeWatchers()
                        .FirstOrDefault(x => x is SourceWatcher);
                    if (pluginWatcher != null)
                    {
                        pluginWatcher.StopWatcher();
                        _module.Extensions.Manager.RemoveChangeWatcher(pluginWatcher);
                    }
                }
            }

            // If config watchers changed
            if (configWatchers != pluginsConfiguration.Watchers.ConfigWatchers)
            {
                // Enable
                if (pluginsConfiguration.Watchers.ConfigWatchers)
                {
                    if (_module.ConfigChanges == null)
                    {
                        _module.ConfigChanges = new List<string>();
                    }
                    else
                    {
                        _module.ConfigChanges.Clear();
                    }

                    ConfigWatcher configWatcher = new ConfigWatcher(_module.ConfigDirectory, "*.json,*.toml", SearchOption.AllDirectories);
                    _module.Extensions.Manager.RegisterChangeWatcher(configWatcher);
                    configWatcher.StartWatcher();
                    _module.RootLogger?.Warning(_module.Strings.Module.ConfigWatchersEnabled);
                }
                else //disable
                {
                    IChangeWatcher configWatcher = _module.Extensions.Manager.GetChangeWatchers()
                        .FirstOrDefault(x => x is ConfigWatcher && x.Directory == _module.ConfigDirectory);
                    if (configWatcher != null)
                    {
                        configWatcher.StopWatcher();
                        _module.Extensions.Manager.RemoveChangeWatcher(configWatcher);
                    }
                }
            }
        }

        /// <summary>
        /// Reload telemetry configuration
        /// </summary>
        public void LoadTelemetryConfiguration()
        {
            _module.Telemetry = DataFile.Load<Telemetry>(Path.Combine(_module.InstanceDirectory, "telemetry.toml"));
            if (_module.Telemetry.HasExceptions)
            {
                _module.Telemetry.Report();
            }
        }

        /// <summary>
        /// Reload web configuration
        /// </summary>
        public void LoadWebConfiguration()
        {
            bool hasDefaultAdminPlugin = _module.Web?.Configuration?.Servers?.ContainsKey("rcon") ?? false;
            List<string> currentServers = _module.Web.WebSockets.Configuration.Servers.Select(x => x.Key).ToList();
            Web webConfiguration = DataFile.Load<Web>(Path.Combine(_module.InstanceDirectory, "web.toml"));
            if (webConfiguration.HasExceptions)
            {
                webConfiguration.Report();
            }

            if (_module.AppManager.IsInstalled("WebSockets"))
            {
                foreach (var name in currentServers)
                {
                    if (!webConfiguration.Servers.ContainsKey(name)) // User removed server
                    {
                        _module.Web.WebSockets.Application.EnqueueServerClose(name);
                    }
                }
            }

            _module.Web.Configuration = webConfiguration;
            _module.Web.Configuration.Initialize();

            // rcon connection was removed
            if(hasDefaultAdminPlugin && (!_module.Web?.Configuration?.Servers?.ContainsKey("rcon") ?? false))
            {
                Interface.uMod.Extensions.UnregisterLoader(_module.Web.AdminPluginLoader);
                _module.Web.AdminPluginLoader = null;
            }
        }
    }
}
