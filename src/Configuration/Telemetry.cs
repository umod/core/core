﻿using uMod.Configuration.Toml;
using uMod.IO;

namespace uMod.Configuration
{
    public class AnalyticsProvider
    {
        [TomlProperty("analytics", Comment = "Allow sending helpful, non-identifying usage information")]
        public bool Analytics { get; } = true;
    }

    public class ReportingProvider
    {
        [TomlProperty("exception_reporting", Comment = "Allow sending critical exceptions to developers")]
        public bool Exceptions { get; } = true;
    }

    public class Telemetry : TomlFile
    {
        [TomlProperty("analytics")]
        public AnalyticsProvider Analytics;

        [TomlProperty("reporting")]
        public ReportingProvider Reporting;

        public Telemetry(string filename) : base(filename)
        {
            Analytics = new AnalyticsProvider();
            Reporting = new ReportingProvider();
        }
    }
}
