﻿using System;
using uMod.Common;

namespace uMod
{
    /// <summary>
    /// Asynchronous event using EventArgs for a single generic context
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    public class ContextEventAsyncCallback<TContext> : ContextEventAsyncCallback<EventArgs<TContext>, TContext, TContext> where TContext : class, IContext
    {
        /// <summary>
        /// Create a new asynchronous event object for the specified plugin using the specified callback as the default callback
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="callback"></param>
        public ContextEventAsyncCallback(TContext plugin, Func<bool> callback = null) : base(plugin, callback) { }

        /// <summary>
        /// Create a new asynchronous event object for the specified plugin using the specified callback as the default callback
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="callback"></param>
        public ContextEventAsyncCallback(TContext plugin, Func<TContext, EventArgs<TContext>, bool> callback = null) : base(plugin, callback) { }
    }

    /// <summary>
    /// Generic asynchronous event
    /// </summary>
    /// <typeparam name="TEventArgs"></typeparam>
    /// <typeparam name="TContext"></typeparam>
    /// <typeparam name="TSender"></typeparam>
    public class ContextEventAsyncCallback<TEventArgs, TContext, TSender> : ContextEventCallback<TEventArgs, TContext, TSender> where TEventArgs : EventArgs<TContext> where TContext : class, IContext where TSender : class, IContext
    {
        internal object PromiseLock = new object();

        /// <summary>
        /// Create an asynchronous event object for the specified plugin
        /// </summary>
        /// <param name="plugin"></param>
        public ContextEventAsyncCallback(TContext plugin) : base(plugin) { }

        /// <summary>
        /// Create a new asynchronous event object for the specified plugin using the specified callback as the default callback
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="callback"></param>
        public ContextEventAsyncCallback(TContext plugin, Func<bool> callback) : base(plugin, callback) { }

        /// <summary>
        /// Create a new asynchronous event object for the specified plugin using the specified callback as the default callback
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="callback"></param>
        public ContextEventAsyncCallback(TContext plugin, Func<TSender, TEventArgs, bool> callback) : base(plugin, callback) { }

        /// <summary>
        /// Create a new asynchronous event object for the specified plugin using the specified callback as the default callback
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="callback"></param>
        public ContextEventAsyncCallback(TContext plugin, Action<TSender, TEventArgs> callback) : base(plugin, callback) { }

        /// <summary>
        /// Add callback to be invoked upon event cancellation
        /// </summary>
        /// <param name="canceledDelegate"></param>
        /// <returns></returns>
        public new ContextEventAsyncCallback<TEventArgs, TContext, TSender> Canceled(Action<TSender, TEventArgs> canceledDelegate)
        {
            return base.Canceled(canceledDelegate) as ContextEventAsyncCallback<TEventArgs, TContext, TSender>;
        }

        /// <summary>
        /// Add callback to be invoked before event
        /// </summary>
        /// <param name="beforeDelegate"></param>
        /// <returns></returns>
        public new ContextEventAsyncCallback<TEventArgs, TContext, TSender> Before(Action<TSender, TEventArgs> beforeDelegate)
        {
            return base.Before(beforeDelegate) as ContextEventAsyncCallback<TEventArgs, TContext, TSender>;
        }

        /// <summary>
        /// Add callback to be invoked upon event success
        /// </summary>
        /// <param name="completedDelegate"></param>
        /// <returns></returns>
        public new ContextEventAsyncCallback<TEventArgs, TContext, TSender> Done(Action<TSender, TEventArgs> completedDelegate)
        {
            return base.Done(completedDelegate) as ContextEventAsyncCallback<TEventArgs, TContext, TSender>;
        }

        /// <summary>
        /// Add callback to be invoked after successful event
        /// </summary>
        /// <param name="afterDelegate"></param>
        /// <returns></returns>
        public new ContextEventAsyncCallback<TEventArgs, TContext, TSender> After(Action<TSender, TEventArgs> afterDelegate)
        {
            return base.After(afterDelegate) as ContextEventAsyncCallback<TEventArgs, TContext, TSender>;
        }

        /// <summary>
        /// Add callback to be invoked after event
        /// </summary>
        /// <param name="alwaysDelegate"></param>
        /// <returns></returns>
        public new ContextEventAsyncCallback<TEventArgs, TContext, TSender> Always(Action<TSender, TEventArgs> alwaysDelegate)
        {
            return base.Always(alwaysDelegate) as ContextEventAsyncCallback<TEventArgs, TContext, TSender>;
        }

        /// <summary>
        /// Add callback to be invoked upon event failure
        /// </summary>
        /// <param name="failedDelegate"></param>
        /// <returns></returns>
        public new ContextEventAsyncCallback<TEventArgs, TContext, TSender> Fail(Action<TSender, TEventArgs> failedDelegate)
        {
            return base.Fail(failedDelegate) as ContextEventAsyncCallback<TEventArgs, TContext, TSender>;
        }
    }

    /// <summary>
    /// Promise using EventArgs for a single generic context
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    public class ContextEventCallback<TContext> : ContextEventCallback<EventArgs<TContext>, TContext, TContext> where TContext : class, IContext
    {
    }

    /// <summary>
    /// Generic event
    /// </summary>
    /// <typeparam name="TEventArgs"></typeparam>
    /// <typeparam name="TContext"></typeparam>
    /// <typeparam name="TSender"></typeparam>
    public class ContextEventCallback<TEventArgs, TContext, TSender> : IDisposable where TEventArgs : EventArgs<TContext> where TContext : class, IContext where TSender : class, IContext
    {
        public TContext Context { get; }

        //protected List<Func<TSender, TEventArgs, bool>> queue = new List<Func<TSender, TEventArgs, bool>>();
        //protected virtual List<Func<TSender, TEventArgs, bool>> Queue => queue;
        protected Func<TSender, TEventArgs, bool> Callback;

        public EventState State { get; private set; } = EventState.None;

        protected ICallback<TSender, TEventArgs> BeforeCallback;
        public Event<TSender, TEventArgs> OnBefore;

        protected ICallback<TSender, TEventArgs> FailedCallback;
        public Event<TSender, TEventArgs> OnFailed;

        protected ICallback<TSender, TEventArgs> CompletedCallback;
        public Event<TSender, TEventArgs> OnCompleted;

        protected ICallback<TSender, TEventArgs> CanceledCallback;
        public Event<TSender, TEventArgs> OnCanceled;

        protected ICallback<TSender, TEventArgs> AfterCallback;
        public Event<TSender, TEventArgs> OnAfter;

        protected ICallback<TSender, TEventArgs> AlwaysCallback;
        public Event<TSender, TEventArgs> OnAlways;

        internal ContextEventCallback()
        {
        }

        internal ContextEventCallback(TContext context)
        {
            Context = context;
        }

        internal ContextEventCallback(TContext context, Func<bool> callback)
        {
            Context = context;
            this.Callback = delegate
            {
                return callback();
            };
        }

        internal ContextEventCallback(TContext context, Func<TSender, TEventArgs, bool> callback)
        {
            Context = context;
            this.Callback = callback;
        }

        internal ContextEventCallback(TContext context, Action<TSender, TEventArgs> callback)
        {
            Context = context;
            this.Callback = delegate (TSender sender, TEventArgs args)
            {
                try
                {
                    callback(sender, args);
                    return true;
                }
                catch (Exception ex)
                {
                    Fail(args, ex.Message);
                    return false;
                }
            };
        }

        /// <summary>
        /// Add callback to be invoked upon event cancellation
        /// </summary>
        /// <param name="cancelDelegate"></param>
        /// <returns></returns>
        public virtual ContextEventCallback<TEventArgs, TContext, TSender> Canceled(Action<TSender, TEventArgs> cancelDelegate)
        {
            if (OnCanceled == null)
            {
                OnCanceled = new Event<TSender, TEventArgs>();
            }
            CanceledCallback = OnCanceled.Add(cancelDelegate);
            return this;
        }

        /// <summary>
        /// Add callback to be invoked before event
        /// </summary>
        /// <param name="beforeDelegate"></param>
        /// <returns></returns>
        public virtual ContextEventCallback<TEventArgs, TContext, TSender> Before(Action<TSender, TEventArgs> beforeDelegate)
        {
            if (OnBefore == null)
            {
                OnBefore = new Event<TSender, TEventArgs>();
            }
            BeforeCallback = OnBefore.Add(beforeDelegate);
            return this;
        }

        /// <summary>
        /// Add callback to be invoked upon failure
        /// </summary>
        /// <param name="failedDelegate"></param>
        /// <returns></returns>
        public virtual ContextEventCallback<TEventArgs, TContext, TSender> Fail(Action<TSender, TEventArgs> failedDelegate)
        {
            if (OnFailed == null)
            {
                OnFailed = new Event<TSender, TEventArgs>();
            }
            FailedCallback = OnFailed.Add(failedDelegate);
            return this;
        }

        /// <summary>
        /// Add callback to be invoked upon failure
        /// </summary>
        /// <param name="failedDelegate"></param>
        /// <returns></returns>
        public virtual ContextEventCallback<TEventArgs, TContext, TSender> Failed(Action<TSender, TEventArgs> failedDelegate)
        {
            return Fail(failedDelegate);
        }

        /// <summary>
        /// Add callback to be invoked upon completion
        /// </summary>
        /// <param name="completedDelegate"></param>
        /// <returns></returns>
        public virtual ContextEventCallback<TEventArgs, TContext, TSender> Done(Action<TSender, TEventArgs> completedDelegate)
        {
            return Completed(completedDelegate);
        }

        /// <summary>
        /// Add callback to be invoked upon completion
        /// </summary>
        /// <param name="completedDelegate"></param>
        /// <returns></returns>
        public virtual ContextEventCallback<TEventArgs, TContext, TSender> Completed(Action<TSender, TEventArgs> completedDelegate)
        {
            if (OnCompleted == null)
            {
                OnCompleted = new Event<TSender, TEventArgs>();
            }
            CompletedCallback = OnCompleted.Add(completedDelegate);
            return this;
        }

        /// <summary>
        /// Add callback to be invoked after completion
        /// </summary>
        /// <param name="afterDelegate"></param>
        /// <returns></returns>
        public virtual ContextEventCallback<TEventArgs, TContext, TSender> After(Action<TSender, TEventArgs> afterDelegate)
        {
            if (OnAfter == null)
            {
                OnAfter = new Event<TSender, TEventArgs>();
            }
            AfterCallback = OnAfter.Add(afterDelegate);
            return this;
        }

        /// <summary>
        /// Add callback to be invoked after completion regardless of state
        /// </summary>
        /// <param name="alwaysDelegate"></param>
        /// <returns></returns>
        public virtual ContextEventCallback<TEventArgs, TContext, TSender> Always(Action<TSender, TEventArgs> alwaysDelegate)
        {
            if (OnAlways == null)
            {
                OnAlways = new Event<TSender, TEventArgs>();
            }
            AlwaysCallback = OnAlways.Add(alwaysDelegate);
            return this;
        }

        /// <summary>
        /// Complete event
        /// </summary>
        /// <param name="eventArgs"></param>
        protected virtual void Complete(TEventArgs eventArgs)
        {
            eventArgs?.Complete();
            State = eventArgs?.state ?? EventState.Completed;
        }

        /// <summary>
        /// Fail event
        /// </summary>
        /// <param name="eventArgs"></param>
        protected virtual void Fail(TEventArgs eventArgs)
        {
            eventArgs?.Fail();
            State = eventArgs?.state ?? EventState.Failed;
        }

        /// <summary>
        /// Fail event with message
        /// </summary>
        /// <param name="eventArgs"></param>
        /// <param name="message"></param>
        protected virtual void Fail(TEventArgs eventArgs, string message = "")
        {
            eventArgs?.Fail(message);
            State = eventArgs?.state ?? EventState.Failed;
        }

        /// <summary>
        /// Fail event with promise exception
        /// </summary>
        /// <param name="eventArgs"></param>
        /// <param name="ex"></param>
        internal virtual void Fail(TEventArgs eventArgs, PromiseException ex)
        {
            eventArgs?.Fail(ex.Message);
            State = eventArgs?.state ?? EventState.Failed;
        }

        /// <summary>
        /// Cancel event
        /// </summary>
        /// <param name="eventArgs"></param>
        /// <param name="reason"></param>
        internal virtual void Cancel(TEventArgs eventArgs, string reason = "")
        {
            eventArgs?.Cancel(reason);
            State = eventArgs?.state ?? EventState.Canceled;
        }

        /// <summary>
        /// Reset event
        /// </summary>
        /// <param name="eventArgs"></param>
        private void Reset(TEventArgs eventArgs)
        {
            eventArgs?.Dispose();
            State = EventState.None;
        }

        /// <summary>
        /// Invoke fail callback
        /// </summary>
        /// <param name="eventArgs"></param>
        /// <param name="sender"></param>
        /// <param name="lastException"></param>
        private void FailCallback(TEventArgs eventArgs, TSender sender, Exception lastException)
        {
            if (lastException != null)
            {
                if (lastException is PromiseException exception)
                {
                    Fail(eventArgs, exception);
                }
                else
                {
                    Fail(eventArgs, lastException.Message);
                }
            }
            else
            {
                Fail(eventArgs);
            }
            OnFailed?.Invoke(sender, eventArgs);
        }

        /// <summary>
        /// Perform event invoke
        /// </summary>
        /// <param name="eventArgs"></param>
        /// <param name="sender"></param>
        /// <returns></returns>
        public virtual bool Invoke(TEventArgs eventArgs = null, TSender sender = null)
        {
            if (State == EventState.Completed)
            {
                return true;
            }

            Exception lastException = null;
            bool success = true;
            Reset(eventArgs);

            if (sender == null && Context is TSender context)
            {
                sender = context;
            }

            OnBefore?.Invoke(sender, eventArgs);

            try
            {
                if (!Callback(sender, eventArgs))
                {
                    Cancel(eventArgs);
                }
            }
            catch (PromiseException pEx)
            {
                lastException = pEx;
                success = false;
            }
            catch (Exception ex)
            {
                lastException = ex;
                success = false;
            }

            return Resolve(sender, eventArgs, success, lastException);
        }

        protected virtual bool Resolve(TSender sender, TEventArgs eventArgs, bool success, Exception lastException)
        {
            EventState state = eventArgs?.state ?? EventState.None;

            switch (state)
            {
                case EventState.None:
                    if (success)
                    {
                        Complete(eventArgs);
                        OnCompleted?.Invoke(sender, eventArgs);
                    }
                    else
                    {
                        FailCallback(eventArgs, sender, lastException);
                    }
                    break;

                case EventState.Failed:
                    success = false;
                    FailCallback(eventArgs, sender, lastException);
                    break;

                case EventState.Canceled:
                    success = false;
                    Cancel(eventArgs);
                    OnCanceled?.Invoke(sender, eventArgs);
                    break;

                case EventState.Completed:
                    Complete(eventArgs);
                    OnCompleted?.Invoke(sender, eventArgs);
                    break;
            }

            if (success)
            {
                OnAfter?.Invoke(sender, eventArgs);
            }

            OnAlways?.Invoke(sender, eventArgs);

            return success;
        }

        /// <summary>
        /// Dispose of event
        /// </summary>
        public virtual void Dispose()
        {
            Callback = null;
            OnAlways = OnAfter = OnBefore = OnCanceled = OnCompleted = OnFailed = null;
            AfterCallback = AlwaysCallback = BeforeCallback = CanceledCallback = CompletedCallback = FailedCallback = null;
        }
    }
}
