﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tommy;
using uMod.Common;
using uMod.Common.Web;
using uMod.Configuration.Toml;

namespace uMod.IO
{
    /// <summary>
    /// Represents a generic TOML file
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TomlFile<T> : TomlFile, IDataFile<T> where T : class
    {
        private T _object;

        /// <summary>
        /// Generic object data
        /// </summary>
        [TomlIgnore]
        public T Object
        {
            get => _object; set
            {
                _object = value;
                ObjectType = value?.GetType();
            }
        }

        /// <summary>
        /// Object type
        /// </summary>
        [TomlIgnore]
        public Type ObjectType { get; private set; }

        /// <summary>
        /// Create a new TOML file
        /// </summary>
        /// <param name="filename"></param>
        public TomlFile(string filename) : base(filename)
        {
        }

        /// <summary>
        /// Create a new TOML file of the specified type with optional data
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="type"></param>
        /// <param name="data"></param>
        public TomlFile(string filename, Type type, T data = null) : base(filename)
        {
            Object = data;
            ObjectType = type ?? data?.GetType();
            IsLoaded = data != null;
        }

        /// <summary>
        /// Create a new TOML file with the specified data
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="data"></param>
        public TomlFile(string filename, T data) : this(filename, data.GetType(), data)
        {
        }

        /// <summary>
        /// Serialize object as TOML string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return TomlConvert.SerializeObject(Object);
        }

        /// <summary>
        /// Deserialize object from TOML string
        /// </summary>
        /// <param name="toml"></param>
        public override void FromString(string toml)
        {
            if (Object == null)
            {
                if (typeof(T) == typeof(object))
                {
                    Object = (T)TomlConvert.DeserializeObject(toml, ObjectType, out Exceptions);
                }
                else
                {
                    Object = TomlConvert.DeserializeObject<T>(toml, out Exceptions);
                }
            }
            else
            {
                TomlConvert.PopulateObject(toml, Object, out Exceptions);
            }
        }

        /// <summary>
        /// Convert TOML file to specified data format
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        public IDataFile<T> Convert(DataFormat format)
        {
            return DataSystem.Convert(this, format);
        }

        /// <summary>
        /// Converts data file to specified data format with data file cast
        /// </summary>
        /// <typeparam name="TDataFile"></typeparam>
        /// <param name="format"></param>
        /// <returns></returns>
        public IDataFile<T> Convert<TDataFile>(DataFormat format) where TDataFile : IDataFile<T>
        {
            return (TDataFile)Convert(format);
        }

        /// <summary>
        /// Converts data file to specified type
        /// </summary>
        /// <typeparam name="TConvert"></typeparam>
        /// <returns></returns>
        public IDataFile<TConvert> As<TConvert>() where TConvert : class, T
        {
            return DataSystem.MakeDataFile(path, DataFormat.Toml, typeof(TConvert), _object as TConvert);
        }

        /// <summary>
        /// Load TOML file asynchronously
        /// </summary>
        /// <returns></returns>
        public IPromise<T> LoadAsync()
        {
            return LoadFile().Then(() => Object);
        }

        /// <summary>
        /// Save TOML file asynchronously
        /// </summary>
        /// <returns></returns>
        public IPromise<T> SaveAsync()
        {
            return SaveFile().Then(() => Object);
        }

        public new IPromise<IDataFile<T>> LoadFile()
        {
            return Load<IDataFile<T>>(this);
        }

        public new IPromise<IDataFile<T>> SaveFile()
        {
            return Save<IDataFile<T>>(this);
        }
    }

    /// <summary>
    /// Represents a TOML file
    /// </summary>
    public class TomlFile : DataFile
    {
        /// <summary>
        /// Gets toml file extension
        /// </summary>
        [TomlIgnore]
        public override string Extension => "toml";

        /// <summary>
        /// Gets toml parse exceptions generated during deserialization
        /// </summary>
        [TomlIgnore]
        public IEnumerable<Exception> Exceptions;

        /// <summary>
        /// Gets whether or not toml file parsed with exceptions
        /// </summary>
        [TomlIgnore]
        public bool HasExceptions { get => Exceptions != null && Exceptions.Any(); }

        /// <summary>
        /// Create new TOML file
        /// </summary>
        /// <param name="filename"></param>
        public TomlFile(string filename) : base(filename)
        {
        }

        /// <summary>
        /// Serialize object as TOML string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return TomlConvert.SerializeObject(this);
        }

        /// <summary>
        /// Deserialize object from TOML string
        /// </summary>
        /// <param name="toml"></param>
        public override void FromString(string toml)
        {
            TomlConvert.PopulateObject(toml, this, out Exceptions);
        }

        /// <summary>
        /// Converts text file to file attachment
        /// </summary>
        /// <param name="contentType"></param>
        /// <returns></returns>
        public override IMultipartFile ToAttachment(string contentType = null)
        {
            return new FileAttachment($"{Filename}.{Extension}", ToString(), !string.IsNullOrEmpty(contentType) ? contentType : "text/plain");
        }

        /// <summary>
        /// Reports toml parsing exceptions to specified logger
        /// </summary>
        /// <param name="logger"></param>
        internal void Report(ILogger logger = null)
        {
            if (logger == null)
            {
                logger = Interface.uMod.RootLogger;
            }
            foreach (Exception exception in Exceptions)
            {
                if (exception is TomlSyntaxException syntaxException)
                {
                    logger.Error(
                        $"{Filename}.{Extension}: ({syntaxException.Line}, {syntaxException.Column}): {syntaxException.Message}");
                }
                else
                {
                    logger.Error(
                        $"{Filename}.{Extension}: Mapping failure: {exception.Message}"); // TODO: Localization
                }
            }
        }
    }
}
