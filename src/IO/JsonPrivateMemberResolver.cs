﻿extern alias References;
using System.Reflection;
using References::Newtonsoft.Json;
using References::Newtonsoft.Json.Serialization;

namespace uMod.IO
{
    public class JsonPrivateMemberResolver : DefaultContractResolver
    {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty prop = base.CreateProperty(member, memberSerialization);
            if (prop.Writable)
            {
                return prop;
            }

            PropertyInfo property = member as PropertyInfo;
            bool hasPrivateSetter = property?.GetSetMethod(true) != null;
            prop.Writable = hasPrivateSetter;
            return prop;
        }
    }
}
