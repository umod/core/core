﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace uMod.IO
{
    internal static class PathUtils
    {
        internal static string InvalidPathChars = new string(Path.GetInvalidPathChars());

        /// <summary>
        /// Escape path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        internal static string Escape(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return "\"\"";
            }

            path = Regex.Replace(path, @"(\\*)" + "\"", @"$1\$0");
            path = Regex.Replace(path, @"^(.*\s.*?)(\\*)$", "\"$1$2$2\"");
            return path;
        }

        /// <summary>
        /// Makes the specified name safe for use in a filename
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string Sanitize(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                path = path.Replace('\\', Path.DirectorySeparatorChar).Replace('/', Path.DirectorySeparatorChar);
                path = Regex.Replace(path, "[" + Regex.Escape(InvalidPathChars) + "]", "_");
                path = Regex.Replace(path, @"\.+", ".");
                return path.TrimStart('.');
            }

            return string.Empty;
        }

        /// <summary>
        /// Check if file path is in working directory
        /// </summary>
        /// <param name="filename"></param>
        public static string AssertValid(string filename)
        {
            filename = Sanitize(filename);
            if (!Path.GetFullPath(filename).StartsWith(Interface.uMod.InstanceDirectory, StringComparison.Ordinal))
            {
                throw new Exception($"Access denied. Path must be within 'umod' directory!{Environment.NewLine}File: {filename}");
            }

            return filename;
        }

        /// <summary>
        /// Determine if path is in configuration folder
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static bool IsConfig(string filename)
        {
            return Path.GetFullPath(Sanitize(filename)).StartsWith(Interface.uMod.ConfigDirectory, StringComparison.Ordinal);
        }

        /// <summary>
        /// Get data category of path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static DataCategory GetDataCategory(string path)
        {
            string fullPath = Path.GetFullPath(Sanitize(path));

            if (fullPath.StartsWith(Interface.uMod.ConfigDirectory, StringComparison.Ordinal))
            {
                return DataCategory.Configuration;
            }
            else if (fullPath.StartsWith(Interface.uMod.LangDirectory, StringComparison.Ordinal))
            {
                return DataCategory.Localization;
            }

            return DataCategory.Data;
        }

        /// <summary>
        /// Get directory of data category
        /// </summary>
        /// <param name="dataCategory"></param>
        /// <returns></returns>
        public static string GetDataDirectory(DataCategory dataCategory)
        {
            switch (dataCategory)
            {
                case DataCategory.Configuration:
                    return Interface.uMod.ConfigDirectory;

                case DataCategory.Localization:
                    return Interface.uMod.LangDirectory;
            }

            return Interface.uMod.DataDirectory;
        }
    }
}
