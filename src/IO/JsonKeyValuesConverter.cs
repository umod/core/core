﻿extern alias References;

using System;
using System.Collections.Generic;
using System.Linq;
using References::Newtonsoft.Json;

namespace uMod.IO
{
    /// <summary>
    /// A mechanism to convert a keyvalues dictionary to and from json
    /// </summary>
    public class JsonKeyValuesConverter : JsonConverter
    {
        /// <summary>
        /// Returns if this converter can convert the specified type or not
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Dictionary<string, object>) || objectType == typeof(List<object>);
        }

        /// <summary>
        /// Throws generic exception with specified message
        /// </summary>
        /// <param name="message"></param>
        private void Throw(string message)
        {
            throw new Exception(message);
        }

        /// <summary>
        /// Reads an instance of the specified type from json
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="objectType"></param>
        /// <param name="existingValue"></param>
        /// <param name="serializer"></param>
        /// <returns></returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (objectType == typeof(Dictionary<string, object>))
            {
                // Get the dictionary to populate
                Dictionary<string, object> dict = existingValue as Dictionary<string, object> ?? new Dictionary<string, object>();
                if (reader.TokenType == JsonToken.StartArray)
                {
                    return dict;
                }

                // Read until end of object
                while (reader.Read() && reader.TokenType != JsonToken.EndObject)
                {
                    // Read property name
                    if (reader.TokenType != JsonToken.PropertyName)
                    {
                        Throw("Unexpected token: " + reader.TokenType);
                    }

                    string propname = reader.Value as string;
                    if (!reader.Read())
                    {
                        Throw("Unexpected end of json");
                    }

                    // What type of object are we reading?
                    switch (reader.TokenType)
                    {
                        case JsonToken.String:
                        case JsonToken.Float:
                        case JsonToken.Boolean:
                        case JsonToken.Bytes:
                        case JsonToken.Date:
                        case JsonToken.Null:
                            dict[propname] = reader.Value;
                            break;

                        case JsonToken.Integer:
                            string value = reader.Value.ToString();
                            if (int.TryParse(value, out int result))
                            {
                                dict[propname] = result;
                            }
                            else
                            {
                                dict[propname] = value;
                            }

                            break;

                        case JsonToken.StartObject:
                            dict[propname] = serializer.Deserialize<Dictionary<string, object>>(reader);
                            break;

                        case JsonToken.StartArray:
                            dict[propname] = serializer.Deserialize<List<object>>(reader);
                            break;

                        default:
                            Throw("Unexpected token: " + reader.TokenType);
                            break;
                    }
                }

                // Return it
                return dict;
            }
            if (objectType == typeof(List<object>))
            {
                // Get the list to populate
                List<object> list = existingValue as List<object> ?? new List<object>();

                // Read until end of array
                while (reader.Read() && reader.TokenType != JsonToken.EndArray)
                {
                    // What type of object are we reading?
                    switch (reader.TokenType)
                    {
                        case JsonToken.String:
                        case JsonToken.Float:
                        case JsonToken.Boolean:
                        case JsonToken.Bytes:
                        case JsonToken.Date:
                        case JsonToken.Null:
                            list.Add(reader.Value);
                            break;

                        case JsonToken.Integer:
                            string value = reader.Value.ToString();
                            if (int.TryParse(value, out int result))
                            {
                                list.Add(result);
                            }
                            else
                            {
                                list.Add(value);
                            }

                            break;

                        case JsonToken.StartObject:
                            list.Add(serializer.Deserialize<Dictionary<string, object>>(reader));
                            break;

                        case JsonToken.StartArray:
                            list.Add(serializer.Deserialize<List<object>>(reader));
                            break;

                        default:
                            Throw("Unexpected token: " + reader.TokenType);
                            break;
                    }
                }

                // Return it
                return list;
            }
            return existingValue;
        }

        /// <summary>
        /// Writes an instance of the specified type to json
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="serializer"></param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            // Get the dictionary to write
            if (value is Dictionary<string, object> dict)
            {
                // Start object
                writer.WriteStartObject();

                // Simply loop through and serialize
                foreach (KeyValuePair<string, object> pair in dict.OrderBy(i => i.Key))
                {
                    writer.WritePropertyName(pair.Key, true);
                    serializer.Serialize(writer, pair.Value);
                }

                // End object
                writer.WriteEndObject();
            }
            else if (value is List<object> list) // Get the list to write
            {
                // Start array
                writer.WriteStartArray();

                // Simply loop through and serialize
                foreach (object t in list)
                {
                    serializer.Serialize(writer, t);
                }

                // End array
                writer.WriteEndArray();
            }
        }
    }
}
