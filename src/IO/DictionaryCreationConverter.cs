﻿extern alias References;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using References::Newtonsoft.Json.Converters;

namespace uMod.IO
{
    /// <summary>
    /// Custom creation converter to create dictionary with case insensitive keys
    /// </summary>
    public class CaseInsensitiveDictionaryCreationConverter : CustomCreationConverter<IDictionary>
    {
        /// <summary>
        /// Dictionary comparer
        /// </summary>
        private readonly IEqualityComparer<string> _comparer;

        public CaseInsensitiveDictionaryCreationConverter()
        {
            _comparer = StringComparer.InvariantCultureIgnoreCase;
        }

        /// <summary>
        /// Checks if specified type is converted by this converter
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public override bool CanConvert(Type objectType)
        {
            return HasCompatibleInterface(objectType)
                   && HasCompatibleConstructor(objectType);
        }

        /// <summary>
        /// Check if specified type is a dictionary
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        private static bool HasCompatibleInterface(Type objectType)
        {
            return objectType
                .GetInterfaces()
                .Where(i => HasGenericTypeDefinition(i, typeof(IDictionary<,>)))
                .Any(i => typeof(string).IsAssignableFrom(i.GetGenericArguments().First()));
        }

        /// <summary>
        /// Check if specified type has the specified generic type definition
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="typeDefinition"></param>
        /// <returns></returns>
        private static bool HasGenericTypeDefinition(Type objectType, Type typeDefinition)
        {
            return objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeDefinition;
        }

        /// <summary>
        /// Check if compatible constructor exists
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        private static bool HasCompatibleConstructor(Type objectType)
        {
            return objectType.GetConstructor(new[] { typeof(IEqualityComparer<string>) }) != null;
        }

        /// <summary>
        /// Create new dictionary
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public override IDictionary Create(Type objectType)
        {
            if (_comparer == null)
            {
                return Activator.CreateInstance(objectType) as IDictionary;
            }

            return Activator.CreateInstance(objectType, _comparer) as IDictionary;
        }
    }
}
