﻿using System;
using System.IO;
using uMod.Common.Web;
using uMod.Utilities;

namespace uMod.IO
{
    /// <summary>
    /// Represents a binary file
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BinaryFile<T> : DataFile<T> where T : class
    {
        private T _object;

        /// <summary>
        /// Gets or sets the object
        /// </summary>
        public override T Object
        {
            get => _object; set
            {
                _object = value;
                ObjectType = value?.GetType();
            }
        }

        /// <summary>
        /// Converts data file to specified type
        /// </summary>
        /// <typeparam name="TConvert"></typeparam>
        /// <returns></returns>
        public override IDataFile<TConvert> As<TConvert>()
        {
            return DataSystem.MakeDataFile(path, DataFormat.Binary, typeof(TConvert), _object as TConvert);
        }

        /// <summary>
        /// Gets or sets the object type
        /// </summary>
        public override Type ObjectType { get; protected set; }

        /// <summary>
        /// Gets the extension associated with file type
        /// </summary>
        public override string Extension => "dat";

        /// <summary>
        /// Create new binary file
        /// </summary>
        /// <param name="filename"></param>
        public BinaryFile(string filename) : base(filename)
        {
        }

        /// <summary>
        /// Create new binary file of type and with optional default data
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="type"></param>
        /// <param name="data"></param>
        public BinaryFile(string filename, Type type, T data = null) : this(filename)
        {
            Object = data;
            ObjectType = type ?? data?.GetType();
            IsLoaded = data != null;
        }

        /// <summary>
        /// Create new binary file with optional default data
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="data"></param>
        public BinaryFile(string filename, T data) : this(filename, data.GetType(), data)
        {
        }

        /// <summary>
        /// Convert object to serialized string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            using (MemoryStream stream = new MemoryStream())
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                if (Object is IBinaryObject binaryObject)
                {
                    binaryObject.Write(writer);
                }
                else
                {
                    ReflectionUtility.Invoke(Object, "Write", writer);
                }
                return System.Convert.ToBase64String(stream.ToArray());
            }
        }

        /// <summary>
        /// Deserialize binary string
        /// </summary>
        /// <param name="binary"></param>
        public override void FromString(string binary)
        {
            byte[] bytes = System.Convert.FromBase64String(binary);
            using (MemoryStream stream = new MemoryStream(bytes))
            using (BinaryReader reader = new BinaryReader(stream))
            {
                if (Object == null)
                {
                    Object = Activator.CreateInstance<T>();
                }

                if (Object is IBinaryObject binaryObject)
                {
                    binaryObject.Read(reader);
                }
                else
                {
                    ReflectionUtility.Invoke(Object, "Read", reader);
                }
            }
        }

        /// <summary>
        /// Converts binary file to file attachment
        /// </summary>
        /// <param name="contentType"></param>
        /// <returns></returns>
        public override IMultipartFile ToAttachment(string contentType = null)
        {
            return new FileAttachment($"{Filename}.{Extension}", ToString(), !string.IsNullOrEmpty(contentType) ? contentType : "application/octet-stream");
        }
    }
}
