﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tommy;
using uMod.Configuration.Toml;

namespace uMod.IO
{
    /// <summary>
    /// Represents a synchronized TOML file
    /// </summary>
    public class TomlSyncFile : TomlFile, IEnumerable<KeyValuePair<string, object>>
    {
        /// <summary>
        /// Gets TOML object
        /// </summary>
        [TomlIgnore]
        protected TomlObject Root;

        /// <summary>
        /// TOML serializer
        /// </summary>
        [TomlIgnore]
        public TomlSerializer Serializer { get; protected set; }

        /// <summary>
        /// Create new TOML synchronized file
        /// </summary>
        /// <param name="filename"></param>
        public TomlSyncFile(string filename, TomlSerializer serializer = null) : base(filename)
        {
            Serializer = serializer ?? TomlConvert.Serializer;
        }

        /// <summary>
        /// Gets or sets a setting on this config by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object this[string key]
        {
            get
            {
                return Get(key);
            }
            set
            {
                Set(value, key);
            }
        }

        /// <summary>
        /// Gets or sets a setting on this config by key
        /// </summary>
        /// <param name="key"></param>
        /// <param name="key2"></param>
        /// <returns></returns>
        public object this[string key, string key2]
        {
            get
            {
                return Get(key, key2);
            }
            set
            {
                Set(value, key, key2);
            }
        }

        public object this[string key, string key2, string key3]
        {
            get
            {
                return Get(key, key2, key3);
            }
            set
            {
                Set(value, key, key2, key3);
            }
        }

        /// <summary>
        /// Gets or sets a setting on this config by key
        /// </summary>
        /// <param name="key"></param>
        /// <param name="key2"></param>
        /// <returns></returns>
        public object this[string key, int key2]
        {
            get
            {
                return Get(key, key2);
            }
            set
            {
                Set(value, key, key2);
            }
        }

        public object this[string key, string key2, int key3]
        {
            get
            {
                return Get(key, key2, key3);
            }
            set
            {
                Set(value, key, key2, key3);
            }
        }

        public void Set(object val, params object[] keys)
        {
            if (keys.Length < 1)
            {
                throw new ArgumentException("Keys must not be empty");
            }

            if (Root == TomlObject.Empty)
            {
                Root = new TomlObject(new TomlTable());
            }

            object lastNode = Root;

            for (int i = 0; i < keys.Length; i++)
            {
                object key = keys[i];
                if (key is string stringKey)
                {
                    object lastVal = null;
                    if (lastNode is TomlObject lastTomlObject)
                    {
                        lastVal = lastTomlObject.Get(stringKey);
                    }
                    
                    if (i == keys.Length - 1)
                    {
                        if (lastNode is TomlObject lastTomlNode)
                        {
                            lastTomlNode.Set(stringKey, val);
                        }

                        return;
                    }
                    else
                    {
                        if (lastVal is TomlObject tomlObject)
                        {
                            lastNode = tomlObject;
                            continue;
                        }
                        if (lastVal?.GetType().IsArray ?? false)
                        {
                            lastNode = lastVal;
                            continue;
                        }

                        if (lastNode is TomlObject lastTomlNode)
                        {
                            TomlObject newNode = keys[i + 1] is int ?
                                new TomlObject(new TomlArray()) :
                                new TomlObject(new TomlTable());
                            
                            lastTomlNode.Set(stringKey, newNode);
                            lastNode = newNode;
                        }
                    }
                }
                else if (key is int intKey)
                {
                    object lastVal = null;
                    if (lastNode is TomlObject lastTomlObject)
                    {
                        lastVal = lastTomlObject.Get(intKey);
                    }
                    else if (lastNode?.GetType().IsArray ?? false)
                    {
                        lastVal = lastNode;
                    }

                    if (i == keys.Length - 1)
                    {
                        if (lastNode is TomlObject lastTomlObject2)
                        {
                            lastTomlObject2.Set(intKey, val);
                        }
                    }
                    else
                    {
                        if (lastVal is TomlObject tomlObject)
                        {
                            lastNode = tomlObject;
                            continue;
                        }

                        break;
                    }
                }
            }
        }

        public object Get(params object[] keys)
        {
            if (keys.Length < 1)
            {
                throw new ArgumentException("Keys must not be empty");
            }

            if (Root == TomlObject.Empty)
            {
                return null;
            }

            object lastNode = Root;

            for (int i = 0; i < keys.Length; i++)
            {
                object key = keys[i];
                if (key is string stringKey)
                {
                    object lastVal = null;
                    if (lastNode is TomlObject lastTomlObject)
                    {
                        lastVal = lastTomlObject.Get(stringKey);
                    }
                    
                    if (i == keys.Length - 1)
                    {
                        return lastVal;
                    }

                    if (lastVal is TomlObject tomlObject)
                    {
                        lastNode = tomlObject;
                        continue;
                    }
                    if (lastVal?.GetType().IsArray ?? false)
                    {
                        lastNode = lastVal;
                        continue;
                    }

                    return null;
                }

                if (key is int intKey)
                {
                    object lastVal = null;
                    if (lastNode is TomlObject lastTomlObject)
                    {
                        lastVal = lastTomlObject.Get(intKey);
                    }
                    else if (lastNode?.GetType().IsArray ?? false)
                    {
                        lastVal = lastNode;
                    }

                    if (i == keys.Length - 1)
                    {
                        if (lastVal is TomlObject tomlObject)
                        {
                            return tomlObject.Get(intKey);
                        }
                        if (lastVal?.GetType().IsArray ?? false)
                        {
                            Array arr = (Array)lastVal;
                            return arr.GetValue(intKey);
                        }

                        return lastVal;
                    }
                    else
                    {
                        if (lastVal is TomlObject tomlObject)
                        {
                            lastNode = tomlObject;
                            continue;
                        }

                        return null;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Load configuration file asynchronously
        /// </summary>
        /// <returns></returns>
        public new IPromise<TomlSyncFile> LoadFile()
        {
            return Load<TomlSyncFile>(this);
        }

        /// <summary>
        /// Save configuration file asynchronously
        /// </summary>
        /// <returns></returns>
        public new IPromise<TomlSyncFile> SaveFile()
        {
            return Save<TomlSyncFile>(this);
        }

        /// <summary>
        /// Serialized object as TOML string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return TomlConvert.SerializeObject(Root.Node);
        }

        /// <summary>
        /// Deserialize object from TOML string
        /// </summary>
        /// <param name="toml"></param>
        public override void FromString(string toml)
        {
            TomlTable val = TomlConvert.DeserializeObject<TomlTable>(toml);
            if (Root == TomlObject.Empty)
            {
                Root = new TomlObject(val);
            }
            else
            {
                Root.Node = val;
            }
        }

        #region IEnumerable

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            if (Root == TomlObject.Empty)
            {
                return Enumerable.Empty<KeyValuePair<string, object>>().GetEnumerator();
            }
            return Root.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            if (Root == TomlObject.Empty)
            {
                return Enumerable.Empty<KeyValuePair<string, object>>().GetEnumerator();
            }
            return Root.GetEnumerator();
        }

        #endregion IEnumerable
    }
}
