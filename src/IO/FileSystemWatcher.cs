using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Permissions;
#if !NET35 && !NET40
using System.Threading.Tasks;
#endif
using uMod.Common;

namespace uMod.IO
{
    /// <summary>
    /// Represents possible file change states
    /// </summary>
    public enum FileChangeType
    {
        Created,
        Deleted,
        Changed
    }

    /// <summary>
    /// Represents a filesystem watcher
    /// </summary>
    public sealed class FileSystemWatcher : IDisposable
    {
        private readonly ILogger _logger;

        /// <summary>
        /// Root path being watched
        /// </summary>
        public string Path => _path;

        /// <summary>
        /// File filter
        /// </summary>
        public string Filter { get; }

        private readonly string[] _filters;

        /// <summary>
        /// Callback when changes occur
        /// </summary>
        internal Action<string, FileChangeType> NotifyCallback;

        /// <summary>
        /// Current file state
        /// </summary>
        public Dictionary<string, Dictionary<string, DateTime>> Files =
            new Dictionary<string, Dictionary<string, DateTime>>();

        private readonly HashSet<string> _foundFiles = new HashSet<string>();
        private readonly HashSet<string> _deletedFiles = new HashSet<string>();
        private readonly string _path;
        private int _index;
        private readonly SearchOption _searchOption;
        private readonly Func<string, bool> _directoryFilter = null;

        /// <summary>
        /// Create a new filesystem watcher
        /// </summary>
        /// <param name="module"></param>
        /// <param name="logger"></param>
        /// <param name="path"></param>
        /// <param name="filter"></param>
        public FileSystemWatcher(ILogger logger, string path, Action<string, FileChangeType> callback, SearchOption searchOption, string filter = "*.*", Func<string, bool> directoryFilter = null)
        {
            _logger = logger;
            _path = path;
            Filter = filter;
            if (!string.IsNullOrEmpty(Filter) && Filter.IndexOf(',') > -1)
            {
                _filters = Filter.Split(',');
                for (int i = 0; i < _filters.Length; i++)
                {
                    _filters[i] = _filters[i].Trim();
                }
            }
            else
            {
                _filters = new[] { Filter };
            }
            NotifyCallback = callback;
            _searchOption = searchOption;
            _directoryFilter = directoryFilter;
            Initialize();
        }

        /// <summary>
        /// Gets enumerable list of files
        /// </summary>
        /// <returns></returns>
        private IEnumerable<string> GetFiles(string filter = null)
        {
#if NET35
            return Directory.GetFiles(_path, filter ?? Filter, _searchOption);
#else
            return Directory.EnumerateFiles(_path, filter ?? Filter, _searchOption);
#endif
        }

        private List<System.IO.FileSystemWatcher> watchers = new List<System.IO.FileSystemWatcher>();

        /// <summary>
        /// Create initial file state
        /// </summary>
#if !NETSTANDARD && !NETCOREAPP3_1 && !NET5_0 && !NET6_0
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
#endif
        private void Initialize()
        {
            foreach (string filter in _filters)
            {
                if (!Files.TryGetValue(filter, out Dictionary<string, DateTime> fileList))
                {
                    Files.Add(filter, fileList = new Dictionary<string, DateTime>());
                }
                foreach (string filePath in GetFiles(filter))
                {
                    if (_directoryFilter != null && !_directoryFilter(filePath))
                    {
                        continue;
                    }
                    fileList.Add(filePath, File.GetLastWriteTime(filePath));
                }
            }

            foreach (string filter in _filters)
            {
                System.IO.FileSystemWatcher fsWatcher =
                    new System.IO.FileSystemWatcher
                    {
                        Path = _path,
                        NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName,
                        Filter = filter
                    };

                fsWatcher.Changed += Handle;
                fsWatcher.Created += Handle;
                fsWatcher.Deleted += Handle;
                fsWatcher.Renamed += Handle;
                fsWatcher.Error += HandleError;
                fsWatcher.EnableRaisingEvents = true;
                if (_searchOption == SearchOption.AllDirectories)
                {
                    fsWatcher.IncludeSubdirectories = true;
                }
                GC.KeepAlive(fsWatcher);
                watchers.Add(fsWatcher);
            }
        }

        private void Handle(object sender, FileSystemEventArgs e)
        {
            for (_index = 0; _index < _filters.Length; _index++)
            {
                NotifyChanges(_filters[_index]);
            }
        }

        private void HandleError(object sender, ErrorEventArgs e)
        {
            Interface.uMod.NextTick(() =>
            {
                Interface.uMod.LogException("FSWatcher error", e.GetException());
            });
        }

        public void Close()
        {
            foreach (var fsWatcher in watchers)
            {
                try
                {
                    fsWatcher.EnableRaisingEvents = false;
                    fsWatcher.Changed -= Handle;
                    fsWatcher.Created -= Handle;
                    fsWatcher.Deleted -= Handle;
                    fsWatcher.Renamed -= Handle;
                    fsWatcher.Error -= HandleError;
                    fsWatcher.Dispose();
                }
                catch (Exception)
                {
                    // Ignore
                }
            }
        }

        private DateTime _lastDateTime;
        /// <summary>
        /// Notify changes
        /// </summary>
        private void NotifyChanges(string filter)
        {
            _foundFiles.Clear();
            _deletedFiles.Clear();

            if (!Files.TryGetValue(filter, out Dictionary<string, DateTime> fileList))
            {
                Files.Add(filter, fileList = new Dictionary<string, DateTime>());
            }

            foreach (string filePath in GetFiles(filter))
            {
                if (_directoryFilter != null && !_directoryFilter(filePath))
                {
                    continue;
                }
                if (fileList.TryGetValue(filePath, out DateTime dateTime))
                {
                    _lastDateTime = File.GetLastWriteTime(filePath);
                    if (_lastDateTime != dateTime)
                    {
                        // File modified
                        NotifyCallback.Invoke(filePath, FileChangeType.Changed);
                        fileList[filePath] = _lastDateTime;
                    }
                }
                else
                {
                    // File created
                    NotifyCallback.Invoke(filePath, FileChangeType.Created);
                    fileList.Add(filePath, File.GetLastWriteTime(filePath));
                }

                _foundFiles.Add(filePath);
            }

            foreach (string file in fileList.Keys)
            {
                if (_foundFiles.Contains(file))
                {
                    continue;
                }

                _deletedFiles.Add(file);
            }

            if (_deletedFiles.Count == 0)
            {
                return;
            }

            foreach (string deletedPath in _deletedFiles)
            {
                // File deleted
                NotifyCallback.Invoke(deletedPath, FileChangeType.Deleted);
                fileList.Remove(deletedPath);
            }
        }

        public void Dispose()
        {
            Close();
        }
    }
}
