﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using uMod.Common;
using uMod.Libraries;
using uMod.Plugins;
using uMod.Utilities;

namespace uMod.IO.Schema
{
    /// <summary>
    /// Represents a locale schematic reader
    /// </summary>
    internal class LocaleSchemaReader : SchemaReader
    {
        private readonly Lang _lang;

        /// <summary>
        /// Creates a new instance of the LocaleSchemaReader class
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="logger"></param>
        /// <param name="typeMediator"></param>
        /// <param name="migration"></param>
        /// <param name="pluginFiles"></param>
        public LocaleSchemaReader(Plugin plugin, ILogger logger, TypeMediator typeMediator, SchemaMigration migration, PluginFiles pluginFiles) : base(plugin, logger, typeMediator, migration, pluginFiles)
        {
            _lang = Interface.uMod.Libraries.Get<Lang>();
        }

        /// <summary>
        /// Gets a list of versionable locale files
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IVersionableFile> GetLocaleFiles()
        {
            if (Mediator.TryGetTypes("Locale*", out IEnumerable<Type> localeTypes))
            {
                GetDataFileContracts<LocalizationAttribute>(localeTypes, out Dictionary<string, List<IVersionableFile>> autoLoadLocales, out Dictionary<string, List<IVersionableFile>> locales);

                return autoLoadLocales.SelectMany(x => x.Value);
            }

            return null;
        }

        /// <summary>
        /// Load locale localization
        /// </summary>
        /// <param name="promise"></param>
        public void ReadSchema(ISettleablePromise<IEnumerable<object>> promise)
        {
            if (Mediator.TryGetTypes("Locale*", out IEnumerable<Type> localeTypes))
            {
                GetDataFileContracts<LocalizationAttribute>(localeTypes, out Dictionary<string, List<IVersionableFile>> autoLoadLocales, out Dictionary<string, List<IVersionableFile>> locales);

                Dictionary<string, Queue<KeyValuePair<IVersionable, IVersionable>>> allUpgrades = Migration.PrepareFileUpgrades("lang", autoLoadLocales, locales);

                Dictionary<string, List<IVersionableFile>> nonUpgradedLocales = autoLoadLocales.Where(x => !allUpgrades.ContainsKey(x.Key)).ToDictionary(k => k.Key, v => v.Value);

                if (allUpgrades.Count > 0)
                {
                    List<object> finalData = new List<object>();
                    int currentUpgrade = 0;

                    Migration.AddToTransaction(allUpgrades.Select(x => x.Value.Select(y => y.Key).FirstOrDefault()));
                    foreach (KeyValuePair<string, Queue<KeyValuePair<IVersionable, IVersionable>>> kvp in allUpgrades)
                    {
                        ISettleablePromise<IEnumerable<object>> localePromise = Promise.Create<IEnumerable<object>>();
                        localePromise.Done(delegate (IEnumerable<object> data)
                        {
                            currentUpgrade++;
                            finalData.AddRange(data);
                            if (currentUpgrade == allUpgrades.Count)
                            {
                                ReadLocaleAction(nonUpgradedLocales, promise, null, finalData);
                            }
                        }, delegate (Exception ex)
                        {
                            Migration.Rollback();
                            promise.ReportRejected(ex);
                        });

                        if (kvp.Value != null)
                        {
                            Migration.TryUpgrades(kvp.Value).Done(delegate ()
                            {
                                ReadLocaleAction(autoLoadLocales, localePromise, kvp.Key);
                            }, delegate (Exception ex)
                            {
                                Migration.Rollback();
                                promise.ReportRejected(ex);
                            });
                        }
                        else
                        {
                            ReadLocaleAction(autoLoadLocales, localePromise, kvp.Key);
                        }
                    }
                }
                else
                {
                    ReadLocaleAction(autoLoadLocales, promise);
                }
            }
            else
            {
                promise.ReportRejected(new Exception(Interface.uMod.Strings.Plugin.LocaleTypesMissing));
            }
        }

        /// <summary>
        /// Read locale files
        /// </summary>
        /// <param name="autoLoadLocale"></param>
        /// <param name="promise"></param>
        /// <param name="name"></param>
        private void ReadLocaleAction(Dictionary<string, List<IVersionableFile>> autoLoadLocale, ISettleablePromise<IEnumerable<object>> promise, string name = null, List<object> additionalLocales = null)
        {
            Dictionary<string, IVersionableFile> autoload;
            if (!string.IsNullOrEmpty(name))
            {
                autoload = autoLoadLocale.Where(x => x.Key.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    .ToDictionary(x => x.Key, x => x.Value.OrderByDescending(v => v.Version).FirstOrDefault());
            }
            else
            {
                autoload = autoLoadLocale.ToDictionary(x => x.Key,
                    x => x.Value.OrderByDescending(v => v.Version).FirstOrDefault());
            }

            Dictionary<Type, string> autoLoadedLocalizationContracts = autoload.ToDictionary(x => x.Value.Type, x => x.Value.Filename);
            Dictionary<Type, string> autoLoadFiles = new Dictionary<Type, string>();

            foreach (KeyValuePair<Type, string> kvp in autoLoadedLocalizationContracts)
            {
                IEnumerable<Type> localeTypes = Plugin.GetType()
                    .GetNestedTypesOfInterface(kvp.Key, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

                Type defaultLocale = localeTypes.FirstOrDefault(x =>
                    x.GetCustomAttribute<LocaleAttribute>() is LocaleAttribute localeAttribute &&
                    (string.IsNullOrEmpty(localeAttribute.Locale) ||
                     localeAttribute.Locale.Equals(Lang.DefaultLang, StringComparison.InvariantCultureIgnoreCase)));

                string[] languages = _lang.GetLanguages();
                List<string> foundLanguages = new List<string>();

                foreach (string language in languages)
                {
                    string path = kvp.Value.Replace($"en{Path.DirectorySeparatorChar}", $"{language}{Path.DirectorySeparatorChar}");

                    if (File.Exists(path))
                    {
                        foundLanguages.Add(language);
                        Type localeType = localeTypes.FirstOrDefault(x =>
                                              x.GetCustomAttribute<LocaleAttribute>() is LocaleAttribute localeAttribute &&
                                              localeAttribute.Locale.Equals(language)) ?? defaultLocale;

                        if (localeType != null)
                        {
                            autoLoadFiles.Add(localeType, path);
                        }
                    }
                }

                foreach (Type localeType in localeTypes)
                {
                    if (localeType.GetCustomAttribute<LocaleAttribute>() is LocaleAttribute localeAttribute && !foundLanguages.Contains(localeAttribute.Locale))
                    {
                        autoLoadFiles.Add(localeType, kvp.Value.Replace($"en{Path.DirectorySeparatorChar}", $"{localeAttribute.Locale}{Path.DirectorySeparatorChar}"));
                    }
                }
            }

            if (autoLoadFiles.Count == 0)
            {
                promise.ReportResolved(Enumerable.Empty<object>());
            }

            foreach (KeyValuePair<Type, string> kvp in autoLoadFiles)
            {
                Files.AddFile(kvp.Key, Files.Localization.GetDataFile(kvp.Key, kvp.Value));
            }

            Files.Localization.ReadObjects(autoLoadFiles, true)
                .Done(delegate (IEnumerable<object> readData)
                {
                    foreach (KeyValuePair<Type, string> kvp in autoLoadFiles)
                    {
                        LogTomlExceptions(Files.Localization.GetDataFile(kvp.Key, kvp.Value));
                    }

                    foreach (object data in readData)
                    {
                        if (data != null && data.GetType().IsClass)
                        {
                            Mediator.When().Needs(data.GetType()).Bind(data);
                        }
                    }

                    VersionStore.Instance.Update(autoload.Values, "lang");

                    if (additionalLocales != null)
                    {
                        additionalLocales.AddRange(readData);
                        promise.ReportResolved(additionalLocales);
                    }
                    else
                    {
                        promise.ReportResolved(readData);
                    }
                }, promise.ReportRejected);
        }
    }
}
