using System;
using System.Collections.Generic;
using uMod.Common;
using uMod.Plugins;

namespace uMod.IO.Schema
{
    /// <summary>
    /// Represents a configuration schematic migration
    /// </summary>
    internal class ConfigSchemaMigration : SchemaMigration
    {
        /// <summary>
        /// Creates a new instance of the ConfigSchemaMigration class
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="files"></param>
        /// <param name="application"></param>
        /// <param name="dispatcher"></param>
        public ConfigSchemaMigration(IPlugin plugin, ILogger logger, PluginFiles files, IApplication application, IChainDispatcher dispatcher) : base(plugin, logger, files, application, dispatcher)
        {
        }

        /// <summary>
        /// Try to upgrade a configuration file using the specified upgrade path
        /// </summary>
        /// <param name="upgradePath"></param>
        /// <returns></returns>
        public override IPromise<object> TryUpgrade(Queue<KeyValuePair<IVersionable, IVersionable>> upgradePath)
        {
            return Promise.Create<object>((resolve, reject) =>
            {
                Dispatcher.Dispatch(delegate
                {
                    object lastConfig = null;
                    IVersionable lastDataDescription = null;
                    IEnumerator<KeyValuePair<IVersionable, IVersionable>> enumerator = upgradePath.GetEnumerator();
                    IDataFile<object> originalDataFile = null;
                    IDataFile<object> lastDataFile = null;
                    string originalFile = string.Empty;
                    bool fileRenamed = false;
                    bool upgraded = false;
                    while (enumerator.MoveNext())
                    {
                        try
                        {
                            lastDataFile = LoadFile(enumerator.Current.Key);
                            if (string.IsNullOrEmpty(originalFile))
                            {
                                originalFile = lastDataFile?.Filename;
                                originalDataFile = lastDataFile;
                            }
                            lastConfig = lastDataFile?.Object;

                            IDataFile<object> nextDataFile = PrepareUpgrade(enumerator.Current.Value);

                            if (lastConfig != null)
                            {
                                upgraded = true;
                                Plugin.CallHook("OnConfigUpgrade", lastConfig, nextDataFile.Object);
                            }

                            lastDataDescription = enumerator.Current.Value;
                            lastConfig = nextDataFile.Object;
                        }
                        catch (Exception ex)
                        {
                            reject(ex);
                            return true;
                        }
                    }

                    if (lastDataFile != null && lastDataFile.Filename != originalFile)
                    {
                        fileRenamed = true;
                    }

                    if (lastDataDescription != null && upgraded)
                    {
                        if (lastDataDescription is IVersionableFile lastVersionableFile)
                        {
                            resolve(
                                Files.Configuration.WriteObject(lastVersionableFile.Filename, lastConfig)
                                    .Then(() =>
                                    {
                                        if (fileRenamed)
                                        {
                                            originalDataFile?.Delete();
                                        }

                                        return lastConfig;
                                    })
                            );
                        }
                        else
                        {
                            reject(new Exception(Interface.uMod.Strings.Plugin.ConfigVersionEmpty));
                        }
                    }
                    else
                    {
                        resolve(lastConfig);
                    }

                    return true;
                });
            });
        }

        /// <summary>
        /// Gets a configuration file from the specified versionable
        /// </summary>
        /// <param name="versionable"></param>
        /// <returns></returns>
        public override IDataFile<object> GetDataFile(IVersionable versionable)
        {
            if (!(versionable is IVersionableFile versionableFile))
            {
                return default;
            }

            return Files.Configuration.GetDataFile(versionableFile.Type, versionableFile.Filename);
        }

        /// <summary>
        /// Gets implied type for upgrade path schematic
        /// </summary>
        /// <param name="abstractType"></param>
        /// <returns></returns>
        public override Type GetImpliedType(Type abstractType)
        {
            return abstractType;
        }

        /// <summary>
        /// Prepare a data file to receive an upgrade for the specified versionable
        /// </summary>
        /// <param name="versionable"></param>
        /// <returns></returns>
        private IDataFile<object> PrepareUpgrade(IVersionable versionable)
        {
            if (!(versionable is IVersionableFile versionableFile))
            {
                return default;
            }

            IDataFile<object> nextDataFile = Files.Configuration.GetDataFile(versionableFile.Type, versionableFile.Filename);
            if (nextDataFile.Object == null)
            {
                nextDataFile.Object = Application.Make<object>(versionableFile.Type);
                nextDataFile.OnCreateObject?.Invoke(nextDataFile);
            }

            return nextDataFile;
        }
    }
}
