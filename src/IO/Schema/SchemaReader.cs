﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Tommy;
using uMod.Common;
using uMod.Libraries;
using uMod.Plugins;
using uMod.Utilities;

namespace uMod.IO.Schema
{
    /// <summary>
    /// Represents a file schematic reader
    /// </summary>
    internal abstract class SchemaReader
    {
        protected readonly Plugin Plugin;
        protected readonly ILogger Logger;
        protected readonly TypeMediator Mediator;
        protected readonly SchemaMigration Migration;
        protected readonly PluginFiles Files;

        /// <summary>
        /// Creates a new instance of the SchemaReader class
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="logger"></param>
        /// <param name="typeMediator"></param>
        /// <param name="migration"></param>
        /// <param name="pluginFiles"></param>
        protected SchemaReader(Plugin plugin, ILogger logger, TypeMediator typeMediator, SchemaMigration migration, PluginFiles pluginFiles)
        {
            Plugin = plugin;
            Logger = logger;
            Mediator = typeMediator;
            Migration = migration;
            Files = pluginFiles;
        }

        /// <summary>
        /// Gets the file name for the specified schematic type and data file attribute
        /// </summary>
        /// <param name="type"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public string GetDataFileName(Type type, DataFileAttribute attribute)
        {
            if (attribute is LocalizationAttribute)
            {
                string name = Plugin.Name;
                if (type.GetCustomAttributeIncludingBaseInterfaces<LocalizationAttribute>() is LocalizationAttribute localizationAttribute)
                {
                    if (!string.IsNullOrEmpty(localizationAttribute.Name))
                    {
                        name = $"{Plugin.Name}_{localizationAttribute.Name}";
                    }
                }

                string lang = Lang.DefaultLang;
                if (type.GetCustomAttribute<LocaleAttribute>() is LocaleAttribute localeAttribute)
                {
                    lang = localeAttribute.Locale;
                }

                if (!string.IsNullOrEmpty(attribute.Name))
                {
                    return $"{lang}{Path.DirectorySeparatorChar}{Plugin.Name}_{attribute.Name}";
                }

                return $"{lang}{Path.DirectorySeparatorChar}{name}";
            }

            if (!string.IsNullOrEmpty(attribute.Name))
            {
                return $"{Plugin.Name}/{attribute.Name}";
            }

            return Plugin.Name;
        }

        /// <summary>
        /// Gets contracts from the specified types
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="types"></param>
        /// <param name="autoLoadFiles"></param>
        /// <param name="allFiles"></param>
        protected void GetDataFileContracts<T>(IEnumerable<Type> types, out Dictionary<string, List<IVersionableFile>> autoLoadFiles, out Dictionary<string, List<IVersionableFile>> allFiles) where T : DataFileAttribute
        {
            autoLoadFiles = new Dictionary<string, List<IVersionableFile>>();
            allFiles = new Dictionary<string, List<IVersionableFile>>();
            // Obtain a list of all data file schematics
            foreach (Type dataType in types)
            {
                T dataFileAttribute = dataType.GetCustomAttribute<T>();
                if (dataFileAttribute == null)
                {
                    continue;
                }

                string name = GetDataFileName(dataType, dataFileAttribute);
                VersionNumber dataFileVersionNumber = VersionNumber.Empty;

                if (!string.IsNullOrEmpty(dataFileAttribute.Version) && !VersionNumber.TryParse(dataFileAttribute.Version, out dataFileVersionNumber))
                {
                    Logger.Warning(Interface.uMod.Strings.Plugin.MetadataVersionInvalid.Interpolate(
                        ("plugin", Plugin.Name),
                        ("type", dataType.Name),
                        ("version", dataFileAttribute.Version)
                    ));
                    continue;
                }

                // Create new plugin data description
                IVersionableFile dataFileDescription = new PluginMeta.PluginDataDescription()
                {
                    Name = dataFileAttribute.Name ?? Plugin.Name,
                    Filename = name,
                    Type = dataType,
                    Version = dataFileVersionNumber,
                    Attribute = dataFileAttribute
                };

                if (dataFileAttribute.AutoLoad)
                {
                    if (!autoLoadFiles.TryGetValue(dataFileDescription.Name, out List<IVersionableFile> autoLoadedversions))
                    {
                        autoLoadFiles.Add(dataFileDescription.Name, autoLoadedversions = new List<IVersionableFile>());
                    }

                    autoLoadedversions.Add(dataFileDescription);
                }

                if (!allFiles.TryGetValue(dataFileDescription.Name, out List<IVersionableFile> allVersions))
                {
                    allFiles.Add(dataFileDescription.Name, allVersions = new List<IVersionableFile>());
                }

                allVersions.Add(dataFileDescription);
            }

            // If autoload is not specified on any schematic or on multiple schematics with the same name, only auto-load the latest
            if (typeof(T) == typeof(ConfigAttribute))
            {
                foreach (KeyValuePair<string, List<IVersionableFile>> kvp in allFiles)
                {
                    bool isAutoLoaded;
                    // If only one schematic with the specified name is already auto-loaded, use that schematic
                    if ((isAutoLoaded = autoLoadFiles.TryGetValue(kvp.Key,
                            out List<IVersionableFile> autoLoadedversions)) && autoLoadedversions.Count == 1)
                    {
                        continue;
                    }

                    // Get the latest version schematic with the highest version number
                    IVersionableFile latestVersionableFile;
                    if (isAutoLoaded && autoLoadedversions != null)
                    {
                        latestVersionableFile = autoLoadedversions.OrderByDescending(x => x.Version).FirstOrDefault();
                    }
                    else
                    {
                        latestVersionableFile = kvp.Value.OrderBy(x => x.Version).FirstOrDefault();
                    }

                    if (latestVersionableFile == null)
                    {
                        continue;
                    }

                    if (isAutoLoaded && autoLoadedversions != null) // If multiple schematics are auto-loaded, clear them
                    {
                        autoLoadedversions.Clear();
                    }
                    else
                    {
                        autoLoadFiles.Add(kvp.Key, autoLoadedversions = new List<IVersionableFile>());
                    }

                    autoLoadedversions.Add(latestVersionableFile);
                }
            }
            else
            {
                foreach (KeyValuePair<string, List<IVersionableFile>> kvp in allFiles)
                {
                    if (autoLoadFiles.TryGetValue(kvp.Key, out List<IVersionableFile> autoLoadedversions))
                    {
                        continue;
                    }
                    // Get the latest version schematic with the highest version number
                    IVersionableFile latestVersionableFile = kvp.Value.OrderBy(x => x.Version).FirstOrDefault();

                    if (latestVersionableFile == null)
                    {
                        continue;
                    }

                    autoLoadFiles.Add(kvp.Key, autoLoadedversions = new List<IVersionableFile>());

                    autoLoadedversions.Add(latestVersionableFile);
                }
            }
        }

        /// <summary>
        /// Logs t
        /// </summary>
        /// <param name="type"></param>
        /// <param name="filename"></param>
        protected void LogTomlExceptions(IDataFile file)
        {
            if (file is TomlFile tomlFile && tomlFile.HasExceptions)
            {
                tomlFile.Report(Logger);
            }
        }
    }
}
