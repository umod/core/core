﻿using System;
using System.Collections.Generic;
using System.Linq;
using uMod.Common;
using uMod.Plugins;
using uMod.Utilities;

namespace uMod.IO.Schema
{
    /// <summary>
    /// Represents a configuration schematic reader
    /// </summary>
    internal class ConfigSchemaReader : SchemaReader
    {
        /// <summary>
        /// Creates a new instance of the ConfigSchemaReader class
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="logger"></param>
        /// <param name="typeMediator"></param>
        /// <param name="migration"></param>
        /// <param name="pluginFiles"></param>
        public ConfigSchemaReader(Plugin plugin, ILogger logger, TypeMediator typeMediator, SchemaMigration migration, PluginFiles pluginFiles) : base(plugin, logger, typeMediator, migration, pluginFiles)
        {
        }

        /// <summary>
        /// Load dynamic configuration
        /// </summary>
        /// <param name="promise"></param>
        public IPromise ReadSchema()
        {
            if (Mediator.TryGetTypes("Config*", out IEnumerable<Type> configTypes))
            {
                GetDataFileContracts<ConfigAttribute>(configTypes, out Dictionary<string, List<IVersionableFile>> autoLoadConfig, out Dictionary<string, List<IVersionableFile>> allConfigs);

                Dictionary<string, Queue<KeyValuePair<IVersionable, IVersionable>>> allUpgrades = Migration.PrepareFileUpgrades("config", autoLoadConfig, allConfigs);

                if (allUpgrades.Count > 0)
                {
                    Migration.AddToTransaction(allUpgrades.Select(x => x.Value.Select(y => y.Key).FirstOrDefault()));
                    foreach (KeyValuePair<string, Queue<KeyValuePair<IVersionable, IVersionable>>> kvp in allUpgrades)
                    {
                        if (kvp.Value != null)
                        {
                            return Migration.TryUpgrade(kvp.Value)
                                .Then(() =>
                                {
                                    if (Plugin._manager.Exists(Plugin.Name))
                                    {
                                        return ReadConfigAction(autoLoadConfig, kvp.Key);
                                    }

                                    return null;
                                })
                                .CatchPeek((Exception ex) =>
                                {
                                    Migration.Rollback();
                                });
                        }

                        return ReadConfigAction(autoLoadConfig, kvp.Key);
                    }
                }

                return ReadConfigAction(autoLoadConfig);
            }

            return Promise.Reject(new Exception(Interface.uMod.Strings.Plugin.ConfigTypesMissing));
        }

        /// <summary>
        /// Read configuration schema
        /// </summary>
        /// <param name="autoLoadConfig"></param>
        /// <param name="promise"></param>
        /// <param name="name"></param>
        private IPromise ReadConfigAction(Dictionary<string, List<IVersionableFile>> autoLoadConfig, string name = null)
        {
            if (!Plugin._manager.Exists(Plugin.Name))
            {
                return Promise.Resolve();
            }

            IEnumerable<KeyValuePair<string, List<IVersionableFile>>> autoLoadConfigFiltered = string.IsNullOrEmpty(name)
                ? autoLoadConfig
                : autoLoadConfig.Where(x => x.Key.Equals(name, StringComparison.InvariantCultureIgnoreCase));

            Dictionary<string, IVersionableFile> autoload = autoLoadConfigFiltered.ToDictionary(
                x => x.Key,
                x => x.Value.OrderByDescending(v => v.Version).FirstOrDefault()
            );

            Dictionary<Type, string> autoLoadFiles = autoload.ToDictionary(x => x.Value.Type, x => x.Value.Filename);
            List<Type> boundTypes = new List<Type>();
            List<ICallback<IDataObject>> createdCallbacks = new List<ICallback<IDataObject>>();
            List<IPromise> callbackOperations = new List<IPromise>();

            foreach (var kvp in autoLoadFiles)
            {
                IDataFile<object> dataFile = Files.Configuration.GetDataFile(kvp.Key, kvp.Value);
                Files.AddFile(kvp.Key, dataFile);
                ICallback<IDataObject> callback = dataFile.OnCreateObject?.Add(delegate
                {
                    if (dataFile?.Object == null)
                    {
                        return;
                    }

                    if (dataFile.ObjectType?.IsClass ?? false)
                    {
                        Mediator.When().Needs(dataFile.ObjectType).Bind(dataFile.Object);
                        boundTypes.Add(dataFile.ObjectType);
                    }

                    callbackOperations.Add(Promise.Create<object>((resolve, reject) =>
                    {
                        Interface.uMod.NextTick(() =>
                        {
                            if (dataFile?.Object == null)
                            {
                                resolve(null);
                                return;
                            }

                            if (Plugin?.IsSubscribedWith("OnConfigCreate", dataFile.ObjectType) == true)
                            {
                                Plugin.CallHook("OnConfigCreate", dataFile.Object);

                                resolve(dataFile.SaveAsync().CatchPeek((ex) =>
                                {
                                    Logger.Report(ex);
                                }));
                                return;
                            }

                            resolve(null);
                        });
                    }));
                });

                if (callback != null)
                {
                    createdCallbacks.Add(callback);
                }
            }

            return Files.Configuration.ReadObjects(autoLoadFiles, true)
                .Then((IEnumerable<object> readData) =>
                {
                    if (!Plugin._manager.Exists(Plugin.Name))
                    {
                        return null;
                    }

                    foreach (var kvp in autoLoadFiles)
                    {
                        LogTomlExceptions(Files.Configuration.GetDataFile(kvp.Key, kvp.Value));
                    }

                    foreach (var callback in createdCallbacks)
                    {
                        callback.Remove();
                    }

                    foreach (var data in readData)
                    {
                        if (data != null && data.GetType().IsClass && !boundTypes.Contains(data.GetType()))
                        {
                            Mediator.When().Needs(data.GetType()).Bind(data);
                        }
                    }

                    VersionStore.Instance.Update(autoload.Values, "config");
                    boundTypes.Clear();

                    return Promise.All(callbackOperations);
                });
        }
    }
}
