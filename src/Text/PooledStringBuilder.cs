﻿using System;
using System.Collections.Generic;

namespace uMod.Text
{
    /// <summary>
    /// Represents a fast string builder
    /// </summary>
    public class PooledStringBuilder
    {
        const int InitialCapacity = 32;
        const int MaxBufferLen = 2560;

        private string _stringGenerated = string.Empty;
        private bool _isGenerated;

        /// <summary>
        /// Working mutable string
        /// </summary>
        private char[] _buffer;
        private int _bufferPos;
        private int _capacity;
        private readonly int _maxLen;

        /// <summary>
        /// Temporary string used for the Replace method
        /// </summary>
        private List<char> _replacement;

        /// <summary>
        /// Create a new instance of the StringBuilder class
        /// </summary>
        public PooledStringBuilder() : this(32)
        {
        }

        /// <summary>
        /// Create a new instance of the StringBuilder class
        /// </summary>
        /// <param name="initialCapacity"></param>
	    public PooledStringBuilder(int initialCapacity = InitialCapacity, int maxLen = MaxBufferLen)
        {
            _maxLen = maxLen;
            _buffer = new char[_capacity = initialCapacity];
        }

        /// <summary>
        /// Gets string builder length
        /// </summary>
        public int Length => _buffer.Length;

        /// <summary>
        /// Check if string builder is Empty
        /// </summary>
        public bool IsEmpty => _isGenerated ? (_stringGenerated == null) : (_bufferPos == 0);

        /// <summary>
        /// Build the final string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (_isGenerated)
            {
                return _stringGenerated;
            }

            _stringGenerated = new string(_buffer, 0, _bufferPos);
            _isGenerated = true;
            return _stringGenerated;
        }

        /// <summary>
        /// Clear string builder
        /// </summary>
        /// <returns></returns>
        public PooledStringBuilder Clear()
        {
            _bufferPos = 0;
            _isGenerated = false;
            if (_capacity > _maxLen)
            {
                DeallocateBuffer();
            }
            return this;
        }

        /// <summary>
        /// Append specified string (and new line)
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public PooledStringBuilder AppendLine(string value)
        {
            Append(value);
            Append(Environment.NewLine);
            return this;
        }

        /// <summary>
        /// Append specified string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public PooledStringBuilder Append(string value)
        {
            ReallocateBuffer(value.Length);
            int n = value.Length;
            for (int i = 0; i < n; i++)
            {
                _buffer[_bufferPos + i] = value[i];
            }

            _bufferPos += n;
            _isGenerated = false;
            return this;
        }

        /// <summary>
        /// Append the specified buffer
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="index"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public PooledStringBuilder Append(char[] buffer, int index, int count)
        {
            if (buffer == null)
            {
                if (index == 0 && count == 0)
                {
                    return this;
                }

                throw new ArgumentNullException(nameof(buffer));
            }

            if (count == 0)
            {
                return this;
            }

            if (index < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }

            if (count > buffer.Length - index)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }

            ReallocateBuffer(count);
            int n = count;
            for (int i = 0; i < n; i++)
            {
                _buffer[_bufferPos + i] = buffer[i];
            }

            _bufferPos += n;
            _isGenerated = false;
            return this;
        }

        /// <summary>
        /// Append specified object (after ToString)
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
	    public PooledStringBuilder Append(object value)
        {
            Append(value.ToString());
            return this;
        }

        /// <summary>
        /// Append specified char
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public PooledStringBuilder Append(char value)
        {
            ReallocateBuffer(1);
            _buffer[_bufferPos] = value;

            _bufferPos += 1;
            _isGenerated = false;
            return this;
        }

        /// <summary>
        /// Append specified integer
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public PooledStringBuilder Append(int value)
        {
            ReallocateBuffer(16);

            if (value < 0)
            {
                value = -value;
                _buffer[_bufferPos++] = '-';
            }

            int charCount = 0;
            do
            {
                _buffer[_bufferPos++] = (char)('0' + value % 10);
                value /= 10;
                charCount++;
            } while (value != 0);

            for (int i = charCount / 2 - 1; i >= 0; i--)
            {
                char c = _buffer[_bufferPos - i - 1];
                _buffer[_bufferPos - i - 1] = _buffer[_bufferPos - charCount + i];
                _buffer[_bufferPos - charCount + i] = c;
            }
            _isGenerated = false;
            return this;
        }

        /// <summary>
        /// Append specified float
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public PooledStringBuilder Append(float value)
        {
            return Append((double)value);
        }

        /// <summary>
        /// Append specified double
        /// </summary>
        /// <param name="valueF"></param>
        /// <returns></returns>
        public PooledStringBuilder Append(double value)
        {
            _isGenerated = false;
            ReallocateBuffer(32);

            if (value == 0)
            {
                _buffer[_bufferPos++] = '0';
                return this;
            }

            if (value < 0)
            {
                value = -value;
                _buffer[_bufferPos++] = '-';
            }

            int digitCount = 0;
            while (value < 1000000)
            {
                value *= 10;
                digitCount++;
            }

            long valueLong = (long)Math.Round(value);

            int charCount = 0;
            bool isLeadingZero = true;
            while (valueLong != 0 || digitCount >= 0)
            {
                if (valueLong % 10 != 0 || digitCount <= 0)
                {
                    isLeadingZero = false;
                }

                if (!isLeadingZero)
                {
                    _buffer[_bufferPos + (charCount++)] = (char)('0' + valueLong % 10);
                }

                if (--digitCount == 0 && !isLeadingZero)
                {
                    _buffer[_bufferPos + (charCount++)] = '.';
                }

                valueLong /= 10;
            }

            _bufferPos += charCount;

            for (int i = charCount / 2 - 1; i >= 0; i--)
            {
                char c = _buffer[_bufferPos - i - 1];
                _buffer[_bufferPos - i - 1] = _buffer[_bufferPos - charCount + i];
                _buffer[_bufferPos - charCount + i] = c;
            }

            return this;
        }

        /// <summary>
        /// Replace specified string with specified new string
        /// </summary>
        /// <param name="oldStr"></param>
        /// <param name="newStr"></param>
        /// <returns></returns>
        public PooledStringBuilder Replace(string oldStr, string newStr)
        {
            if (_bufferPos == 0)
            {
                return this;
            }

            if (_replacement == null)
            {
                _replacement = new List<char>();
            }

            for (int i = 0; i < _bufferPos; i++)
            {
                bool shouldReplace = false;
                if (_buffer[i] == oldStr[0])
                {
                    int k = 1;
                    while (k < oldStr.Length && _buffer[i + k] == oldStr[k])
                    {
                        k++;
                    }

                    shouldReplace = (k >= oldStr.Length);
                }
                if (shouldReplace)
                {
                    i += oldStr.Length - 1;
                    if (newStr == null)
                    {
                        continue;
                    }

                    for (int k = 0; k < newStr.Length; k++)
                    {
                        _replacement.Add(newStr[k]);
                    }
                }
                else
                {
                    _replacement.Add(_buffer[i]);
                }
            }

            ReallocateBuffer(_replacement.Count - _bufferPos);
            for (int k = 0; k < _replacement.Count; k++)
            {
                _buffer[k] = _replacement[k];
            }

            _bufferPos = _replacement.Count;
            _replacement.Clear();
            _isGenerated = false;
            return this;
        }

        /// <summary>
        /// Reallocate buffer
        /// </summary>
        /// <param name="length"></param>
	    private void ReallocateBuffer(int length, int index = 0)
        {
            if (_bufferPos + length <= _capacity)
            {
                return;
            }

            _capacity = Math.Max(_capacity + length, _capacity * 2);
            char[] newChars = new char[_capacity];
            _buffer.CopyTo(newChars, index);
            _buffer = newChars;
        }

        /// <summary>
        /// Deallocate old buffer, reallocate at initial capacity
        /// </summary>
        private void DeallocateBuffer()
        {
            _buffer = new char[_capacity = InitialCapacity];
        }
    }
}
