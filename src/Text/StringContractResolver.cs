﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using uMod.Pooling;

namespace uMod.Text
{
    /// <summary>
    /// Represents a string contract resolver
    /// </summary>
    internal class StringContractResolver
    {
        /// <summary>
        /// Cache of string contracts
        /// </summary>
        internal static Cache<Type, StringObjectContract> CachedContracts = new Cache<Type, StringObjectContract>();

        private static readonly object _cacheLock = new object();

        /// <summary>
        /// Gets a TomlObjectContract for the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static StringObjectContract ResolveContract(Type type)
        {
            StringObjectContract contract;
            lock (_cacheLock)
            {
                if (CachedContracts.TryGetValue(type, out contract))
                {
                    return contract;
                }
            }

            contract = GetContract(type);

            // Check if contract already added by a different thread
            lock (_cacheLock)
            {
                if (!CachedContracts.ContainsKey(type))
                {
                    CachedContracts.Add(type, contract);
                }
            }

            return contract;
        }

        /// <summary>
        /// Gets a dictionary of fields and properties to be serialized
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static StringObjectContract GetContract(Type type)
        {
            StringObjectContract contract = new StringObjectContract(type);
            Dictionary<string, PropertyInfo> properties = new Dictionary<string, PropertyInfo>();
            Dictionary<string, FieldInfo> fields = new Dictionary<string, FieldInfo>();

            FieldInfo[] allFields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            PropertyInfo[] allProperties = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);

            List<PropertyInfo> filteredProperties = Pools.GetList<PropertyInfo>() ?? new List<PropertyInfo>();
            List<FieldInfo> filteredFields = Pools.GetList<FieldInfo>() ?? new List<FieldInfo>();
            List<MemberInfo> defaultMembers = Pools.GetList<MemberInfo>() ?? new List<MemberInfo>();

            try
            {
                filteredFields.AddRange(allFields.Where(FilterField));
                filteredProperties.AddRange(allProperties.Where(FilterProperty));

                FieldInfo[] defaultFields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
                PropertyInfo[] defaultProperties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

                defaultMembers.AddRange(defaultFields.Where(FilterField).Cast<MemberInfo>());
                defaultMembers.AddRange(defaultProperties.Where(FilterProperty).Cast<MemberInfo>());
                PlaceholderAttribute propertyAttribute = null;
                foreach (PropertyInfo member in filteredProperties)
                {
                    if (defaultMembers.Contains(member))
                    {
                        if ((propertyAttribute = member.GetCustomAttribute<PlaceholderAttribute>()) != null)
                        {
                            properties.Add(propertyAttribute.Name ?? member.Name, member);
                        }
                        else
                        {
                            properties.Add(member.Name, member);
                        }
                    }
                    else
                    {
                        if ((propertyAttribute = member.GetCustomAttribute<PlaceholderAttribute>()) != null)
                        {
                            properties.Add(propertyAttribute.Name ?? member.Name, member);
                        }
                    }
                }

                foreach (FieldInfo member in filteredFields)
                {
                    if (defaultMembers.Contains(member))
                    {
                        if ((propertyAttribute = member.GetCustomAttribute<PlaceholderAttribute>()) != null)
                        {
                            fields.Add(propertyAttribute.Name ?? member.Name, member);
                        }
                        else
                        {
                            fields.Add(member.Name, member);
                        }
                    }
                    else
                    {
                        if ((propertyAttribute = member.GetCustomAttribute<PlaceholderAttribute>()) != null)
                        {
                            fields.Add(propertyAttribute.Name ?? member.Name, member);
                        }
                    }
                }
            }
            finally
            {
                Pools.FreeList(ref filteredProperties);
                Pools.FreeList(ref filteredFields);
                Pools.FreeList(ref defaultMembers);
            }

            contract.Properties = properties;
            contract.Fields = fields;

            return contract;
        }

        /// <summary>
        /// Field filter
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        private static bool FilterField(FieldInfo field)
        {
            return !field.IsDefined(typeof(CompilerGeneratedAttribute), true) && field.GetCustomAttribute<PlaceholderIgnoreAttribute>() == null;
        }

        /// <summary>
        /// Property filter
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        private static bool FilterProperty(PropertyInfo property)
        {
            return property.GetIndexParameters().Length == 0 && !property.IsDefined(typeof(CompilerGeneratedAttribute), true) && property.GetCustomAttribute<PlaceholderIgnoreAttribute>() == null;
        }
    }
}
