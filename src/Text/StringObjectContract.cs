﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace uMod.Text
{
    /// <summary>
    /// Represents a string object contract
    /// </summary>
    internal class StringObjectContract
    {
        /// <summary>
        /// The type that this contract describes
        /// </summary>
        public readonly Type Type;

        /// <summary>
        /// The properties of the contract to be formatted
        /// </summary>
        public Dictionary<string, PropertyInfo> Properties;

        /// <summary>
        /// The properties of the contract to be formatted
        /// </summary>
        public Dictionary<string, FieldInfo> Fields;

        /// <summary>
        /// Create a StringObjectContract
        /// </summary>
        /// <param name="type"></param>
        public StringObjectContract(Type type)
        {
            Type = type;
        }
    }
}
