﻿using System;
using System.Globalization;
using System.IO;
using System.Text;

namespace uMod.Text
{
    [Serializable]
    public class StringWriter : TextWriter
    {
        private static UnicodeEncoding _encoding;
        private PooledStringBuilder _sb;
        private bool _isOpen;

        /// <summary>
        /// Create a new instance of the StringWriter class
        /// </summary>
        public StringWriter()
          : this(new PooledStringBuilder(), CultureInfo.CurrentCulture)
        {
        }

        /// <summary>
        /// Create a new instance of the StringWriter class with the specified format provider
        /// </summary>
        /// <param name="formatProvider"></param>
        public StringWriter(IFormatProvider formatProvider)
          : this(new PooledStringBuilder(), formatProvider)
        {
        }

        /// <summary>
        /// Create a new instance of the StringWriter class with the specified string builder
        /// </summary>
        /// <param name="sb"></param>
        public StringWriter(PooledStringBuilder sb)
          : this(sb, CultureInfo.CurrentCulture)
        {
        }

        /// <summary>
        /// Create a new instance of the StringWriter class with the specified string builder and format provider
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="formatProvider"></param>
        public StringWriter(PooledStringBuilder sb, IFormatProvider formatProvider)
          : base(formatProvider)
        {
            _sb = sb ?? throw new ArgumentNullException(nameof(sb));
            _isOpen = true;
        }

        /// <summary>
        /// Close the current and underlying stream
        /// </summary>
        public override void Close() => Dispose(true);

        /// <summary>
        /// Releases the unmanaged resources used by the StringWriter and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            _isOpen = false;
            base.Dispose(disposing);
        }

        /// <summary>
        /// Gets the Encoding in which the output is written.
        /// </summary>
        public override Encoding Encoding
        {
            get
            {
                return _encoding ?? (_encoding = new UnicodeEncoding(false, false));
            }
        }

        /// <summary>
        /// Returns the underlying StringBuilder
        /// </summary>
        /// <returns></returns>
        public virtual PooledStringBuilder GetStringBuilder() => _sb;

        /// <summary>
        /// Writes a character to the string.
        /// </summary>
        /// <param name="value"></param>
        public override void Write(char value)
        {
            if (!_isOpen)
            {
                throw new ObjectDisposedException(null);
            }

            _sb.Append(value);
        }

        /// <summary>
        /// Writes a subarray of characters to the string.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="index"></param>
        /// <param name="count"></param>
        public override void Write(char[] buffer, int index, int count)
        {
            if (!_isOpen)
            {
                throw new ObjectDisposedException(null);
            }

            if (buffer == null)
            {
                throw new ArgumentNullException(nameof(buffer));
            }

            if (index < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }

            if (buffer.Length - index < count)
            {
                throw new ArgumentException();
            }

            _sb.Append(buffer, index, count);
        }

        /// <summary>
        /// Writes a string to the current string.
        /// </summary>
        /// <param name="value"></param>
        public override void Write(string value)
        {
            if (!_isOpen)
            {
                throw new ObjectDisposedException(null);
            }

            if (value == null)
            {
                return;
            }

            _sb.Append(value);
        }

        /// <summary>
        /// Returns a string containing the characters written to the current StringWriter so far.
        /// </summary>
        /// <returns></returns>
        public override string ToString() => _sb.ToString();
    }
}
