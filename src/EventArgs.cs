﻿using System;
using uMod.Common;

namespace uMod
{
    /// <summary>
    /// Pass-thru object to communicate basic state information between hook calls for implicit context
    /// </summary>
    public abstract class EventArgs : System.EventArgs, IEventArgs
    {
        /// <summary>
        /// Array of EventState names
        /// </summary>
        internal static string[] EventNames = Enum.GetNames(typeof(EventState));

        /// <summary>
        /// Determine if event state is canceled
        /// </summary>
        public abstract bool Canceled { get; }

        /// <summary>
        /// Determine if event state is completed
        /// </summary>
        public abstract bool Completed { get; }

        /// <summary>
        /// Determine if event state is failed
        /// </summary>
        public abstract bool Failed { get; }

        /// <summary>
        /// Determine if event state is started
        /// </summary>
        public abstract bool Started { get; }

        /// <summary>
        /// An explanation for the current event state
        /// </summary>
        public abstract string StateReason { get; protected set; }

        /// <summary>
        /// Get the event state context
        /// </summary>
        /// <returns></returns>
        public abstract IContext GetContext();

        /// <summary>
        /// Get the event state
        /// </summary>
        public abstract EventState State { get; internal set; }

        /// <summary>
        /// Gets the state name of the hook
        /// </summary>
        public abstract string StateName { get; }

        /// <summary>
        /// Dispose of event args
        /// </summary>
        public abstract void Dispose();

        /// <summary>
        /// Cancel event
        /// </summary>
        public abstract void Cancel();

        /// <summary>
        /// Cancel event with reason
        /// </summary>
        /// <param name="reason"></param>
        public abstract void Cancel(string reason);

        /// <summary>
        /// Start event
        /// </summary>
        internal abstract void Start();

        /// <summary>
        /// Bind event to context
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        internal abstract EventArgs Bind(IContext context);

        /// <summary>
        /// Complete event
        /// </summary>
        internal abstract void Complete();

        /// <summary>
        /// Fail event with exception
        /// </summary>
        /// <param name="ex"></param>
        public abstract void Fail(Exception ex);

        /// <summary>
        /// Fail event
        /// </summary>
        public abstract void Fail();

        /// <summary>
        /// Fail event with exception
        /// </summary>
        /// <param name="message"></param>
        public abstract void Fail(string message);
    }

    /// <summary>
    /// Pass-thru object to communicate basic state information between hook calls for explicit context
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EventArgs<T> : EventArgs, IEventArgs<T> where T : class, IContext
    {
        internal EventState state = EventState.None;
        internal T context;
        protected string stateReason;
        protected readonly object Lock = new object();

        /// <summary>
        /// Get explicit event context
        /// </summary>
        public T Context
        {
            get => context;
            private set
            {
                lock (Lock)
                {
                    context = value;
                }
            }
        }

        /// <summary>
        /// Get event state
        /// </summary>
        public override EventState State
        {
            get => state;
            internal set
            {
                lock (Lock)
                {
                    state = value;
                }
            }
        }

        /// <summary>
        /// Gets the state name of the hook
        /// </summary>
        public override string StateName
        {
            get
            {
                switch (state)
                {
                    case EventState.None:
                        return string.Empty;
                }

                return Enum.GetName(typeof(EventState), state);
            }
        }

        /// <summary>
        /// An explanation for the current event state
        /// </summary>
        public override string StateReason
        {
            get => stateReason;
            protected set
            {
                lock (Lock)
                {
                    stateReason = value;
                }
            }
        }

        /// <summary>
        /// Determine if event is canceled
        /// </summary>
        public override bool Canceled => state == EventState.Canceled;

        /// <summary>
        /// Determine if event is completed
        /// </summary>
        public override bool Completed => state == EventState.Completed;

        /// <summary>
        /// Determine if event is failed
        /// </summary>
        public override bool Failed => state == EventState.Failed;

        /// <summary>
        /// Determine if event is started
        /// </summary>
        public override bool Started => state == EventState.Started;

        /// <summary>
        /// Cancel event
        /// </summary>
        public override void Cancel()
        {
            lock (Lock)
            {
                state = EventState.Canceled;
            }
        }

        /// <summary>
        /// Cancel event with reason
        /// </summary>
        /// <param name="reason"></param>
        public override void Cancel(string reason)
        {
            lock (Lock)
            {
                state = EventState.Canceled;
                stateReason = reason;
            }
        }

        /// <summary>
        /// Start event
        /// </summary>
        internal override void Start()
        {
            lock (Lock)
            {
                state = EventState.Started;
                stateReason = string.Empty;
            }
        }

        /// <summary>
        /// Bind event to implicit context
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        internal override EventArgs Bind(IContext context)
        {
            if (context is T castedContext)
            {
                lock (Lock)
                {
                    this.context = castedContext;
                }
            }
            return this;
        }

        /// <summary>
        /// Bind event to explicit context
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        internal EventArgs<T> Bind(T context)
        {
            lock (Lock)
            {
                this.context = context;
            }
            return this;
        }

        /// <summary>
        /// Complete event
        /// </summary>
        internal override void Complete()
        {
            lock (Lock)
            {
                state = EventState.Completed;
                stateReason = string.Empty;
            }
        }

        /// <summary>
        /// Fail event with exception
        /// </summary>
        /// <param name="ex"></param>
        public override void Fail(Exception ex)
        {
            lock (Lock)
            {
                state = EventState.Failed;
                stateReason = ex.Message;
            }
        }

        /// <summary>
        /// Fail event
        /// </summary>
        public override void Fail()
        {
            lock (Lock)
            {
                state = EventState.Failed;
            }
        }

        /// <summary>
        /// Fail event with message
        /// </summary>
        /// <param name="message"></param>
        public override void Fail(string message)
        {
            lock (Lock)
            {
                state = EventState.Failed;
                stateReason = message;
            }
        }

        /// <summary>
        /// Dispose of event
        /// </summary>
        public override void Dispose()
        {
            lock (Lock)
            {
                context = null;
                state = EventState.None;
                stateReason = string.Empty;
            }
        }

        /// <summary>
        /// Get event context
        /// </summary>
        /// <returns></returns>
        public override IContext GetContext()
        {
            return context;
        }

        /// <summary>
        /// Create empty event
        /// </summary>
        public EventArgs() { }

        /// <summary>
        /// Create event with optional context
        /// </summary>
        /// <param name="context"></param>
        public EventArgs(T context = null) : this()
        {
            this.context = context;
        }

        /// <summary>
        /// Create event with optional context and state
        /// </summary>
        /// <param name="context"></param>
        /// <param name="state"></param>
        internal EventArgs(T context = null, EventState state = EventState.None) : this(context)
        {
            this.state = state;
        }
    }
}
