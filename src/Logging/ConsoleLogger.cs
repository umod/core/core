﻿using System;
using uMod.Common;
using uMod.Threading;

namespace uMod.Logging
{
    /// <summary>
    /// A logger that write to the system console
    /// </summary>
    [Logger("console")]
    public class ConsoleLogger : TextLogger
    {
        /// <summary>
        /// Creates a new instance of the ConsoleLogger class
        /// </summary>
        public ConsoleLogger() : base(true)
        {
        }

        /// <summary>
        /// Processes the specified message
        /// </summary>
        /// <param name="message"></param>
        protected override void ProcessMessage(LogMessage message, string defaultMessage = null)
        {
            if (message.Level > LogLevel)
            {
                return;
            }

            if (string.IsNullOrEmpty(defaultMessage))
            {
                defaultMessage = Format.Interpolate(message);
            }

            if (!MainThreadState.IsMain)
            {
                Interface.uMod.NextTick(delegate()
                {
                    Console.WriteLine(defaultMessage);
                });
            }
            else
            {
                Console.WriteLine(defaultMessage);
            }
        }
    }
}
