﻿using uMod.Common;

namespace uMod.Logging
{
    [Logger("native")]
    public class NativeLogger : TextLogger
    {
        private readonly NativeDebugCallback _callback;

        /// <summary>
        /// Creates a new instance of the NativeLogger class
        /// </summary>
        public NativeLogger() : base(true)
        {
            _callback = Interface.uMod.debugCallback;
        }

        /// <summary>
        /// Processes the specified message
        /// </summary>
        /// <param name="message"></param>
        protected override void ProcessMessage(LogMessage message, string defaultMessage = null)
        {
            if (message.Level > LogLevel)
            {
                return;
            }

            if (string.IsNullOrEmpty(defaultMessage))
            {
                defaultMessage = Format.Interpolate(message);
            }

            _callback?.Invoke(defaultMessage);
        }
    }
}
