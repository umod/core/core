﻿using System;
using System.Collections.Generic;
using uMod.Text;
using uMod.Common;

namespace uMod.Logging
{
    [AttributeUsage(AttributeTargets.Class)]
    public class LoggerAttribute : Attribute
    {
        public readonly string Name;

        public LoggerAttribute(string name)
        {
            Name = name;
        }
    }

    /// <summary>
    /// Represents a logger
    /// </summary>
    public abstract class Logger : ILogger
    {
        // The message queue
        protected Queue<KeyValuePair<LogMessage,string>> MessageQueue;

        // Should messages be processed immediately and on the same thread?
        private readonly bool _processImediately;

        public IEvent<ILogger> OnAdded { get; } = new Event<ILogger>();
        public IEvent<ILogger> OnRemoved { get; } = new Event<ILogger>();
        public IEvent<ILogger> OnConfigure { get; } = new Event<ILogger>();

        public LogLevel LogLevel { get; internal set; } = LogLevel.Debug;

        /// <summary>
        /// Creates a new instance of the Logger class
        /// </summary>
        /// <param name="processImediately"></param>
        protected Logger(bool processImediately)
        {
            // Initialize
            _processImediately = processImediately;
            if (!processImediately)
            {
                MessageQueue = new Queue<KeyValuePair<LogMessage,string>>();
            }
        }

        /// <summary>
        /// Creates a log message from the specified arguments
        /// </summary>
        /// <param name="type"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected LogMessage CreateLogMessage(LogLevel type, string message)
        {
            LogMessage msg = new LogMessage
            {
                Level = type,
                Message = message,
                Date = DateTime.Now
            };

            return msg;
        }

        /// <summary>
        /// Writes a message to this logger
        /// </summary>
        /// <param name="level"></param>
        /// <param name="message"></param>
        public virtual void Write(LogLevel level, string message)
        {
            if (level > LogLevel)
            {
                return;
            }
            // Create the structure and pass to overload
            Write(CreateLogMessage(level, message));
        }

        /// <summary>
        /// Writes a message to this logger
        /// </summary>
        /// <param name="message"></param>
        public virtual void Write(LogMessage message, string defaultMessage = null)
        {
            if (message.Level > LogLevel)
            {
                return;
            }

            // If we're set to process immediately, do so, otherwise enqueue
            if (_processImediately)
            {
                ProcessMessage(message, defaultMessage);
            }
            else
            {
                MessageQueue.Enqueue(new KeyValuePair<LogMessage, string>(message, defaultMessage));
            }
        }

        /// <summary>
        /// Processes the specified message
        /// </summary>
        /// <param name="message"></param>
        protected virtual void ProcessMessage(LogMessage message, string defaultMessage = null)
        {
        }

        protected LogMessage CreateReportMessage(string message, Exception ex)
        {
            string formatted = ExceptionHandler.FormatException(ex);
            if (formatted != null)
            {
                return new LogMessage()
                {
                    Date = DateTime.Now,
                    Level = LogLevel.Error,
                    Message = $"{Environment.NewLine}{formatted}"
                };
            }

            Exception outerEx = ex;
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
            }

            PooledStringBuilder stringBuilder = Pooling.Pools.StringBuilders?.Get() ?? new PooledStringBuilder();
            if (outerEx.GetType() != ex.GetType())
            {
                stringBuilder.AppendLine($"ExType: {outerEx.GetType().Name}");
            }

            if (!string.IsNullOrEmpty(message))
            {
                stringBuilder.Append($"{message} ");
            }

            if (!string.IsNullOrEmpty(ex.StackTrace))
            {
                stringBuilder.Append($"({ex.GetType().Name}: {ex.Message})\n{ex.StackTrace}");
            }
            else
            {
                stringBuilder.Append($"({ex.GetType().Name}: {ex.Message})");
            }

            try
            {
                return new LogMessage()
                {
                    Date = DateTime.Now,
                    Level = LogLevel.Error,
                    Message = stringBuilder.ToString()
                };
            }
            finally
            {
                Pooling.Pools.StringBuilders?.Free(stringBuilder);
            }
        }

        /// <summary>
        /// Writes an exception to this logger
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        public virtual void Report(string message, Exception ex)
        {
            Write(CreateReportMessage(message, ex));

            // Used for testing but some tests fail when exception rethrown

            /*
            if (Interface.uMod.IsTesting && !ex.Message.StartsWith("Test"))
            {
                throw ex;
            }
            */
        }

        public virtual void Report(Exception ex)
        {
            Write(CreateReportMessage(string.Empty, ex));
        }

        public void Debug(string message)
        {
            Write(LogLevel.Debug, message);
        }

        public void Info(string message)
        {
            Write(LogLevel.Info, message);
        }

        public void Notice(string message)
        {
            Write(LogLevel.Notice, message);
        }

        public void Warning(string message)
        {
            Write(LogLevel.Warning, message);
        }

        public void Error(string message)
        {
            Write(LogLevel.Error, message);
        }

        public void Critical(string message)
        {
            Write(LogLevel.Critical, message);
        }

        public void Alert(string message)
        {
            Write(LogLevel.Alert, message);
        }

        public void Emergency(string message)
        {
            Write(LogLevel.Emergency, message);
        }
    }
}
