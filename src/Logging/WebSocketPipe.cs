﻿extern alias References;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using References::Newtonsoft.Json;
using uMod.Common;
using uMod.Common.WebSockets;
using uMod.Libraries;

namespace uMod.Logging
{
    public class WebSocketPipe : TextLogger
    {
        public string ServerName {get;set;}

        private Apps.WebSockets _app;

        private Timer _timerLib;

        public WebSocketPipe(string serverName) : base(true)
        {
            ServerName = serverName;
            _app = Interface.uMod.Web.WebSockets.Application;
            _timerLib = Interface.uMod.Libraries.Get<Timer>();
        }

        private string GetRconLogLevel(LogLevel logLevel)
        {
            switch (logLevel)
            {
                case LogLevel.Emergency:
                case LogLevel.Alert:
                case LogLevel.Critical:
                case LogLevel.Error:
                    return "Error";
                case LogLevel.Warning:
                case LogLevel.Notice:
                    return "Warning";
                case LogLevel.Info:
                    return "Log";
            }

            return "Generic";
        }

        private List<Packet> _broadcastQueue = new List<Packet>();
        private ITimer _timer;

        protected override void ProcessMessage(LogMessage message, string defaultMessage = null)
        {
            if (string.IsNullOrEmpty(defaultMessage))
            {
                defaultMessage = Format.Interpolate(message);
            }

            if (Format != null)
            {
                _broadcastQueue.Add(new Packet()
                {
                    Message = defaultMessage,
                    Name = ServerName,
                    Type = GetRconLogLevel(message.Level)
                });

                // Timer is null or destroyed
                if (_timer?.Destroyed ?? true)
                {
                    _timer = _timerLib.Repeat(0.2f,0, BroadcastQueue);
                }
            }
        }

        /// <summary>
        /// Delays the broadcast of log messages to server to minimize overhead assuming log messages may be created at an incredible rate
        /// </summary>
        private void BroadcastQueue()
        {
            if (_broadcastQueue.Count > 0)
            {
                _app.EnqueueAggregateBroadcast(ServerName, _broadcastQueue.ToArray());

                _broadcastQueue.Clear();
            }
        }
    }
}
