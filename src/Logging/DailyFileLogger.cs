using System;
using System.IO;
using uMod.Common;
using uMod.IO;

namespace uMod.Logging
{
    /// <summary>
    /// A logger that writes to a set of files that rotate by day
    /// </summary>
    [Logger("daily")]
    public class DailyFileLogger : ThreadedTextLogger, IFileLogger
    {
        /// <summary>
        /// Stores when logging started to rotate file name only when necessary
        /// </summary>
        private DateTime _started = DateTime.MinValue;

        /// <summary>
        /// Cached log file name
        /// </summary>
        private string _logFileName;

        /// <summary>
        /// Gets the directory to write log files to
        /// </summary>
        public string Directory { get; protected set; }

        /// <summary>
        /// Gets the file name
        /// </summary>
        public string Path { get; internal set; }

        // The active writer
        private StreamWriter _writer;

        /// <summary>
        /// Creates a new instance of the DailyFileLogger class
        /// </summary>
        public DailyFileLogger()
        {
            Directory = Interface.uMod.LogDirectory;

            OnConfigure.Add(delegate
            {
                if (string.IsNullOrEmpty(Path))
                {
                    Path = "umod_{date|yyyy-MM-dd}.log";
                }

                PathUtils.AssertValid(Directory);
            });
        }

        /// <summary>
        /// Gets the filename for the specified date
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private string GetLogFilename(DateTime date)
        {
            if (!string.IsNullOrEmpty(_logFileName) &&
               _started.Year == date.Year &&
               _started.Month == date.Month &&
               _started.Day == date.Day)
            {
                return _logFileName;
            }

            string path = PathUtils.AssertValid(System.IO.Path.Combine(Directory, Path?.Interpolate("date", date) ?? $"umod_{date:yyyy-MM-dd}.log"));
            string directory = Utility.GetDirectoryName(path);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }

            _started = date;

            if (string.IsNullOrEmpty(path))
            {
                return _logFileName = path;
            }

            return _logFileName = path;
        }

        /// <summary>
        /// Begins a batch process operation
        /// </summary>
        protected override void BeginBatchProcess()
        {
            // Open the writer
            _writer = new StreamWriter(new FileStream(GetLogFilename(DateTime.Now), FileMode.Append, FileAccess.Write, FileShare.ReadWrite));
        }

        /// <summary>
        /// Processes the specified message
        /// </summary>
        /// <param name="message"></param>
        protected override void ProcessMessage(LogMessage message, string defaultMessage = null)
        {
            if (string.IsNullOrEmpty(defaultMessage))
            {
                defaultMessage = Format.Interpolate(message);
            }

            if (Format != null)
            {
                _writer?.WriteLine(defaultMessage);
            }
        }

        /// <summary>
        /// Finishes a batch process operation
        /// </summary>
        protected override void FinishBatchProcess()
        {
            // Close the writer
            _writer?.Close();
            _writer?.Dispose();
            _writer = null;
        }
    }
}
