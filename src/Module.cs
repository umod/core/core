extern alias References;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using References::Newtonsoft.Json;
using uMod.Apps;
using uMod.Common;
using uMod.Configuration;
using uMod.Exceptions;
using uMod.Extensions;
using uMod.IO;
using uMod.Libraries;
using uMod.Localization;
using uMod.Logging;
using uMod.Plugins;
using uMod.Plugins.Watchers;
using uMod.Text;
using uMod.Threading;
using uMod.Utilities;
using Timer = uMod.Libraries.Timer;

namespace uMod
{
    public delegate void NativeDebugCallback(string message);

    /// <summary>
    /// Responsible for core uMod logic
    /// </summary>
    public sealed class Module : IModule
    {
        internal Dictionary<string, KeyValuePair<IPlugin, Plugins.HookMethod>> BaseHookMethods =
            new Dictionary<string, KeyValuePair<IPlugin, Plugins.HookMethod>>();

        public IChainDispatcher Dispatcher;
        internal IChainDispatcher FileDispatcher;

        /// <summary>
        /// The current uMod version
        /// </summary>
        public static readonly VersionNumber Version =
            new VersionNumber(Assembly.GetExecutingAssembly().GetName().Version);

        /// <summary>
        /// Gets the main logger
        /// </summary>
        public ILogger RootLogger { get; internal set; }

        /// <summary>
        /// Gets the universal server
        /// </summary>
        public IServer Server => Universal.Server;

        /// <summary>
        /// Represents loaded event after loading completed
        /// </summary>
        public IEvent OnLoaded { get; } = new Event();

        /// <summary>
        /// Represents logging initialization event
        /// </summary>
        public IEvent OnLoggingInitialized { get; } = new Event();

        /// <summary>
        /// Gets the data file system
        /// </summary>
        [Obsolete("Use FileSystem.Data instead")]
        public DataFileSystem DataFileSystem { get; private set; }

        /// <summary>
        /// Gets the root directory
        /// </summary>
        public string RootDirectory { get; private set; }

        /// <summary>
        /// Gets the extension directory
        /// </summary>
        public string ExtensionDirectory { get; private set; }

        /// <summary>
        /// Gets the instance directory
        /// </summary>
        public string InstanceDirectory { get; private set; }

        /// <summary>
        /// Gets the application directory
        /// </summary>
        public string AppDirectory { get; private set; }

        /// <summary>
        /// Gets the configuration directory
        /// </summary>
        public string ConfigDirectory { get; private set; }

        /// <summary>
        /// Gets the data directory
        /// </summary>
        public string DataDirectory { get; private set; }

        /// <summary>
        /// Gets the lang directory
        /// </summary>
        public string LangDirectory { get; private set; }

        /// <summary>
        /// Gets the log directory
        /// </summary>
        public string LogDirectory { get; private set; }

        /// <summary>
        /// Gets the plugin directory
        /// </summary>
        public string PluginDirectory { get; private set; }

        /// <summary>
        /// Gets the test directory
        /// </summary>
        public string TestDirectory { get; private set; }

        /// <summary>
        /// Gets the number of seconds since the server started
        /// </summary>
        public float Now => _getTimeSinceStartup();

        /// <summary>
        /// This is true if the server is shutting down
        /// </summary>
        public bool IsShuttingDown
        {
            get => _isShuttingDown;
            private set => _isShuttingDown = value;
        }

        /// <summary>
        /// The command line
        /// </summary>
        public CommandLine CommandLine;

        internal TypeManager Types;

        /// <summary>
        /// Determines whether or not module is in testing mode
        /// </summary>
        public bool IsTesting => Types.IsTesting;

        /// <summary>
        /// uMod Service Container
        /// </summary>
        public IApplication Application { get; private set; }

        public Configuration.Logging Logging { get; internal set; }
        public Database.Provider Database { get; private set; }
        public Auth.Provider Auth { get; private set; }
        internal Plugins.PluginProvider _plugins;
        public IPluginProvider Plugins { get => _plugins; }
        public IExtensionProvider Extensions { get; private set; }
        public ILibraryProvider Libraries { get; private set; }
        public Web.Provider Web { get; private set; }
        public Telemetry Telemetry { get; internal set; }

        [Obsolete("Use Plugins.Manager")]
        public IPluginManager RootPluginManager => _plugins.Manager;

        /// <summary>
        /// List of queued config changes
        /// </summary>
        internal List<string> ConfigChanges = new List<string>();

        /// <summary>
        /// Universal library
        /// </summary>
        internal Universal Universal;

        /// <summary>
        /// Permission library
        /// </summary>
        private Permission _libperm;

        /// <summary>
        /// Timer library
        /// </summary>
        internal Timer _libtimer;

        /// <summary>
        /// Lang library
        /// </summary>
        private Lang _liblang;

        /// <summary>
        /// Locale library
        /// </summary>
        private Locale _liblocale;

        /// <summary>
        /// Timer instance for watcher
        /// </summary>
        private Timer.TimerInstance _timerInstance;

        /// <summary>
        /// App manager
        /// </summary>
        internal AppManager AppManager;

        // Extension implemented delegates
        private Func<float> _getTimeSinceStartup;

        // Thread safe NextTick callback queue
        private Queue<Action> _nextTickQueue = new Queue<Action>();
        private Queue<Action> _lastTickQueue = new Queue<Action>();
        private readonly object _nextTickLock = new object();

        // Allow extensions to register a method to be called every frame
        internal readonly NativeDebugCallback debugCallback;

        internal bool ForceSandbox = false;
        private Action<float> _onFrame;
        private Stopwatch _timer;
        private readonly ModuleConfigurationLoader _configurationLoader;
        internal bool _isShuttingDown;

        /// <summary>
        /// Gets the current game engine
        /// </summary>
        public GameEngine GameEngine { get; internal set; } = GameEngine.None;

        /// <summary>
        /// Creates a new instance of the uMod class
        /// </summary>
        /// <param name="debugCallback"></param>
        public Module(NativeDebugCallback debugCallback)
        {
            this.debugCallback = debugCallback;
            _configurationLoader = new ModuleConfigurationLoader(this);
        }

        private void Setup(IInitializationInfo info = null)
        {
            Utility.Time = new TimeUtility();
            Utility.Random = new RandomUtility();
            Utility.Interpolator = new StringInterpolater();
            Utility.Formatter = new Formatter();

            OnLoaded.Add(delegate()
            {
                CallHook("InitLogging");
            });

            // Set the root directory, where the server is installed
            if (info?.Environment != null && info.Environment.TryGetValue("directory.root", out string rootDirectory) &&
                !string.IsNullOrEmpty(rootDirectory))
            {
                RootDirectory = rootDirectory;
            }
            else
            {
                RootDirectory = Environment.CurrentDirectory;
                if (RootDirectory.StartsWith(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)))
                {
                    RootDirectory = AppDomain.CurrentDomain.BaseDirectory;
                }
            }

            if (RootDirectory == null)
            {
                throw new RootDirectoryNotFound("Could not identify root directory"); // TODO: Localization
            }

            MainThreadState.Initialize();
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings { Culture = CultureInfo.InvariantCulture };

            // Set the default instance directory
            InstanceDirectory = Path.Combine(RootDirectory, "umod");

            // Parse command-line to set instance directory
            string format = null, instanceDirectory = null;
            CommandLine = new CommandLine(Environment.GetCommandLineArgs());
            if (CommandLine.HasVariable("umod.directory"))
            {
                CommandLine.GetArgument("umod.directory", out instanceDirectory, out format);
            }
            else if (CommandLine.HasVariable("force-sandbox"))
            {
                ForceSandbox = true;
            }
            else if (CommandLine.HasVariable("oxide.directory"))
            {
                CommandLine.GetArgument("oxide.directory", out instanceDirectory, out format);
            }

            if (!string.IsNullOrEmpty(instanceDirectory))
            {
                InstanceDirectory = Path.Combine(RootDirectory,
                    Utility.CleanPath(string.Format(format, instanceDirectory)));
            }

            // Set and create core directories, if needed
            if (info?.Environment != null &&
                info.Environment.TryGetValue("directory.extension", out string extensionDirectory) &&
                !string.IsNullOrEmpty(extensionDirectory))
            {
                ExtensionDirectory = extensionDirectory;
            }
            else
            {
                ExtensionDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            }

            if (ExtensionDirectory == null || !Directory.Exists(ExtensionDirectory))
            {
                throw new ExtensionDirectoryNotFound("Could not identify extension directory"); // TODO: Localization
            }

            if (!Directory.Exists(InstanceDirectory))
            {
                Directory.CreateDirectory(InstanceDirectory);
            }

            AppDirectory = Path.Combine(InstanceDirectory, "apps");
            if (!Directory.Exists(AppDirectory))
            {
                Directory.CreateDirectory(AppDirectory);
            }

            ConfigDirectory = Path.Combine(InstanceDirectory, "config");
            if (!Directory.Exists(ConfigDirectory))
            {
                Directory.CreateDirectory(ConfigDirectory);
            }

            DataDirectory = Path.Combine(InstanceDirectory, "data");
            if (!Directory.Exists(DataDirectory))
            {
                Directory.CreateDirectory(DataDirectory);
            }

            LangDirectory = Path.Combine(InstanceDirectory, "lang");
            if (!Directory.Exists(LangDirectory))
            {
                Directory.CreateDirectory(LangDirectory);
            }

            LogDirectory = Path.Combine(InstanceDirectory, "logs");
            if (!Directory.Exists(LogDirectory))
            {
                Directory.CreateDirectory(LogDirectory);
            }

            PluginDirectory = Path.Combine(InstanceDirectory, "plugins");
            if (!Directory.Exists(PluginDirectory))
            {
                Directory.CreateDirectory(PluginDirectory);
            }
        }

        private void PerformOxideUpgrade()
        {
            // Move files from "oxide" directory to "oxide_backup" directory
            string oxideDirectory = Path.Combine(RootDirectory, "oxide");
            if (Directory.Exists(oxideDirectory))
            {
                string newOxideDirectory = Path.Combine(RootDirectory, "oxide_backup");
                Directory.Move(oxideDirectory, newOxideDirectory);
                // Copy "oxide" directory contents to "umod" directory
                FileUtility.CopyDirectory(RootLogger, newOxideDirectory, Path.Combine(RootDirectory, "umod"));
            }

            // Prepare file upgrades
            Dictionary<string, string> upgradePaths = new Dictionary<string, string>()
            {
                {
                    Path.Combine(InstanceDirectory, "oxide.config.json"),
                    Path.Combine(InstanceDirectory, "umod.config.json")
                },
                {Path.Combine(DataDirectory, "oxide.lang.data"), Path.Combine(DataDirectory, "umod.lang.data")},
                {Path.Combine(DataDirectory, "oxide.users.data"), Path.Combine(DataDirectory, "umod.users.data")},
                {Path.Combine(DataDirectory, "oxide.groups.data"), Path.Combine(DataDirectory, "umod.groups.data")},
            };

            foreach (KeyValuePair<string, string> kvp in upgradePaths)
            {
                if (!Utility.TryUpgrade(kvp.Key, kvp.Value))
                {
                    RootLogger?.Warning($"Unable to upgrade file: {kvp.Key}"); // TODO: Localization
                }
            }
        }

        private void SetupSandbox()
        {
            if (File.Exists(Path.Combine(InstanceDirectory, "umod.sandbox")))
            {
                ForceSandbox = true;
            }

            if (!ForceSandbox)
            {
                if (File.Exists(Path.Combine(ExtensionDirectory, "umod.sandbox")))
                {
                    ForceSandbox = true;
                }
            }

            if (!ForceSandbox)
            {
                if (File.Exists(Path.Combine(RootDirectory, "umod.sandbox")))
                {
                    ForceSandbox = true;
                }
            }
        }

        private void CleanupOxideFiles()
        {
            // Run cleanup of old files
            foreach (string extPath in Directory.GetFiles(ExtensionDirectory, "Oxide.*.dll")) // TODO: Remove this cleanup eventually
            {
                Cleanup.Add(extPath);
            }

            Cleanup.Run(RootLogger);
        }

        private void SetupApplication()
        {
            // Set up service container
            Application = new Application();
            Application.BindSingleton(typeof(IModule), this);

            // Set up universal library
            Universal = new Universal(Application, RootLogger);

            // Set up localization
            _liblang = new Lang(Application, RootLogger);
            _liblocale = new Locale(Application);
            LoadLocalization();

            Types = new TypeManager(RootLogger);
            if (IsTesting)
            {
                RegisterFallbackEngineClock();
            }
        }

        private void SetupCore(IInitializationInfo info = null)
        {
            // Set up app manager
            AppManager = new AppManager(RootLogger, CommandLine.HasVariable("noagent"), info);
            // If apps are not installed, try to install them with agent
            try
            {
                AppManager.ValidateInstall();
            }
            catch (Exception exception)
            {
                RootLogger.Report(Strings.Apps.ValidationFailure, exception);
            }

            // Load database provider
            Configuration.Database.ResolveConnectionTypes(Types);
            Configuration.Database databaseConfiguration =
                DataFile.Load<Configuration.Database>(Path.Combine(InstanceDirectory, "database.toml"));
            if (databaseConfiguration.HasExceptions)
            {
                databaseConfiguration.Report();
            }

            Database = new Database.Provider(databaseConfiguration);

            // Load web provider
            Configuration.Web.ResolveConnectionTypes<IWebServer>(Types);
            Configuration.Web.ResolveConnectionTypes<IWebSocketServer>(Types);
            Configuration.Web webConfiguration = DataFile.Load<Configuration.Web>(Path.Combine(InstanceDirectory, "web.toml"));
            if (webConfiguration.HasExceptions)
            {
                webConfiguration.Report();
            }
            Web = new Web.Provider(webConfiguration);

            // Load auth provider
            Configuration.Auth authConfiguration = DataFile.Load<Configuration.Auth>(Path.Combine(InstanceDirectory, "auth.toml"));
            if (authConfiguration.HasExceptions)
            {
                authConfiguration.Report();
            }
            Auth = new Auth.Provider(authConfiguration);
            Dispatcher = new StackDispatcher(RootLogger);
            FileDispatcher = new Dispatcher(RootLogger);

            IAppInfo compilerAppInfo = AppManager.IsInstalled("Compiler") ? AppManager["Compiler"] : null;

            Extensions = new ExtensionProvider(ExtensionDirectory, Application, RootLogger);
            Libraries = new LibraryProvider(Extensions);
            Configuration.Plugins pluginsConfiguration =
                DataFile.Load<Configuration.Plugins>(Path.Combine(InstanceDirectory, "plugins.toml"));
            if (pluginsConfiguration.HasExceptions)
            {
                pluginsConfiguration.Report();
            }
            // Load plugins provider
            Utility.Plugins = _plugins = new PluginProvider(this, Strings, Extensions, compilerAppInfo,
                pluginsConfiguration, RootLogger);

            Telemetry = DataFile.Load<Telemetry>(Path.Combine(InstanceDirectory, "telemetry.toml"));

            if (Telemetry.HasExceptions)
            {
                Telemetry.Report();
            }
        }

        private void SetupLibraries()
        {
            // Register libraries
            Extensions.Manager.RegisterLibrary("Universal", Universal);
            Extensions.Manager.RegisterLibrary("Lang", _liblang);
            Extensions.Manager.RegisterLibrary("Locale", _liblocale);
            Extensions.Manager.RegisterLibrary("Timer", _libtimer = new Timer(this, Application));
            Extensions.Manager.RegisterLibrary("Permission", _libperm = new Permission(Application));
        }

        private void SetupApps()
        {
            if (AppManager.IsInstalled("WebClient"))
            {
                IAppInfo webClientInfo = AppManager["WebClient"];
                Web.Application = new WebClient(webClientInfo, RootLogger);
                if (!Web.Application.Initialize())
                {
                    Web.Application.OnFailed(Interface.uMod.Strings.Apps.StartFailure);
                }
                Extensions.Manager.RegisterLibrary("WebRequests",
                    new WebRequests(Application, RootLogger, Web.Application));
            }
            else if (!IsTesting)
            {
                RootLogger?.Warning(Strings.Apps.StartWithout.Interpolate("app", "web client"));
            }

            if (AppManager.IsInstalled("Database"))
            {
                Database.Initialize();
                IAppInfo databaseInfo = AppManager["Database"];
                Database.Application = new Apps.Database(databaseInfo, RootLogger);
                if (!Database.Application.Initialize())
                {
                    Database.Application.OnFailed(Interface.uMod.Strings.Apps.StartFailure);
                }
            }
            else if (!IsTesting)
            {
                RootLogger?.Warning(Strings.Apps.StartWithout.Interpolate("app", "database"));
            }

            if (AppManager.IsInstalled("WebSockets"))
            {
                Web.WebSockets.Initialize();
                IAppInfo websocketsInfo = AppManager["WebSockets"];
                Web.WebSockets.Application = new Apps.WebSockets(this, websocketsInfo, RootLogger);
                if (!Web.WebSockets.Application.Initialize())
                {
                    Web.WebSockets.Application.OnFailed(Interface.uMod.Strings.Apps.StartFailure);
                }
            }
            else if (!IsTesting)
            {
                RootLogger?.Warning(Strings.Apps.StartWithout.Interpolate("app", "websockets"));
            }
        }

        private void LoadExtensions(IInitializationInfo info = null)
        {
            // Load core extensions
            RootLogger?.Info(Strings.Module.LoadingCoreExtensions);
            if (info != null && info?.Extensions?.Length > 0)
            {
                foreach (Type extensionType in info.Extensions)
                {
                    try
                    {
                        if (Activator.CreateInstance(extensionType) is IExtension extension)
                        {
                            Extensions.Manager.LoadExtension(extension, true);
                        }
                    }
                    catch (Exception exception)
                    {
                        RootLogger?.Report(Strings.Extension.LoadFailed.Interpolate(("name", extensionType.Name)), exception);
                    }
                }
            }

            Extensions.Manager.LoadFrom(ExtensionDirectory, true);
        }

        private void LoadLoggingConfig()
        {
            // Initialize logging
            Configuration.Logging.ResolveLoggerTypes(Application, Types);
            Logging = DataFile.Load<Configuration.Logging>(Path.Combine(InstanceDirectory, "logging.toml"));
        }

        private void InitializeUniversalAndPlugins(IInitializationInfo info = null)
        {
            // Initialize universal API
            Universal.Initialize(info);
            _plugins.LoadOnce(Extensions.GetLoaders().ToArray());
        }

        private void FinalizeLoading(IInitializationInfo info = null)
        {
            Logging.Initialize(RootLogger, Application, !CommandLine.HasVariable("nolog")).Done(delegate (ILogger logger)
            {
                RootLogger = logger;
                InitializeAuth(info);
            }, delegate (Exception ex)
            {
                OnLoggerFallback();
                RootLogger.Report(Strings.Module.LoggerFailure, ex);
                InitializeAuth(info);
            });
        }

        private void InitializeDataSystem()
        {
#pragma warning disable CS0618 // Type or member is obsolete
            DataFileSystem = new DataFileSystem(DataDirectory);
#pragma warning restore CS0618 // Type or member is obsolete
        }

        private void CreatePluginManager()
        {
            _plugins.CreateManager();
        }

        private void ValidateCommandLine()
        {
            // Warn if using obsolete command-line argument(s)
            if (CommandLine.HasVariable("oxide.directory"))
            {
                RootLogger?.Warning(Strings.Module.OxideDirectoryObsolete);
            }

            // Check for and warn if logging is disabled
            if (CommandLine.HasVariable("nolog"))
            {
                RootLogger?.Warning(Strings.Module.NoLogWarning);
            }
        }

        /// <summary>
        /// Initializes a new instance of the uMod class
        /// </summary>
        public void Load(IInitializationInfo info = null)
        {
            Setup(info);

            // Add core logger
            RootLogger = new PipeLogger();
            /*
            StackLogger stackLogger = new StackLogger();
            SingleFileLogger fileLogger = new SingleFileLogger();
            fileLogger.OnAdded.Invoke(fileLogger);
            fileLogger.OnConfigure.Invoke(fileLogger);
            RootLogger = stackLogger;
            stackLogger.AddLogger(fileLogger);
            */

            SetupSandbox();

            PerformOxideUpgrade();

            SetupApplication();

            SetupCore(info);

            ValidateCommandLine();

            // Set up core managers and data file system
            RootLogger?.Info(Strings.Module.Loading.Interpolate("version", Version));

            CleanupOxideFiles();

            CreatePluginManager();

            InitializeDataSystem();

            SetupLibraries();

            SetupApps();

            LoadExtensions(info);

            LoadLoggingConfig();

            InitializeUniversalAndPlugins(info);

            FinalizeLoading(info);
        }

        private void LoadLocalization()
        {
            IDataFile<CoreLocalization> dataFile = FileSystem.Localization.GetDataFile<CoreLocalization>(
                $"{Lang.DefaultLang}{Path.DirectorySeparatorChar}umod");

            if (!dataFile.Exists)
            {
                dataFile.Object = new CoreLocalization();
                dataFile.OnCreateObject?.Invoke(dataFile);
                dataFile.Save();
            }
            else
            {
                dataFile.Load();
            }

            _liblocale.RegisterLocale(dataFile.Object, Universal);

            foreach (string language in _liblang.GetLanguages())
            {
                if (language != Lang.DefaultLang)
                {
                    dataFile = FileSystem.Localization.GetDataFile<CoreLocalization>(
                        $"{language}{Path.DirectorySeparatorChar}umod");

                    if (dataFile.Exists)
                    {
                        dataFile.Load();
                        _liblocale.RegisterLocale(dataFile.Object, Universal, language);
                    }
                }
            }
        }

        internal IPromise InitializeAuth(IInitializationInfo info = null, bool loadDomain = true)
        {
            return Auth.Initialize(RootLogger, info)
                .Then(() =>
                {
                    _libperm.SetProvider(Auth.Manager);
                    _liblang.SetProvider(Auth.Manager);
                    _liblocale.SetProvider(Auth.Manager);

                    if (Auth.Configuration.Groups.CreateDefaultGroups)
                    {
                        // Set up default permission groups
                        int rank = 0;
                        foreach (string defaultGroup in Auth.Configuration.Groups)
                        {
                            if (!_libperm.GroupExists(defaultGroup))
                            {
                                _libperm.CreateGroup(defaultGroup, defaultGroup, rank++);
                            }
                        }
                    }

                    Universal.playerManager.PlayerCleanup(Auth);
                    Universal.playerManager.LoadPlayers(Auth.Manager);
                    if (loadDomain)
                    {
                        NextTick(LoadUserDomain);
                    }
                })
                .CatchPeek((Exception exception) =>
                {
                    RootLogger?.Report(Strings.Module.AuthFailure, exception);
                });
        }

        /// <summary>
        /// Gets the core locale provider
        /// </summary>
        /// <returns></returns>
        public CoreLocalization Strings => _liblocale.GetLocale<CoreLocalization>(_liblang.GetServerLanguage(), Universal);

        /// <summary>
        /// Gets the core locale provider for the specified language
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public CoreLocalization GetStrings(string lang = null)
        {
            if (lang == null)
            {
                lang = _liblang.GetServerLanguage();
            }

            return _liblocale.GetLocale<CoreLocalization>(lang, Universal);
        }

        /// <summary>
        /// Get the core locale provider for the specified player
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public CoreLocalization GetStrings(IPlayer player)
        {
            return _liblocale.GetLocale<CoreLocalization>(_liblang.GetLanguage(player?.Id), Universal);
        }

        /// <summary>
        /// Load user land extensions and plugins
        /// </summary>
        private void LoadUserDomain()
        {
            _plugins.IsInitialized = false;

            // Load all extensions
            RootLogger?.Info(Strings.Module.LoadingUserExtensions);
            Extensions.Manager.LoadFrom(ExtensionDirectory);
            ValidateClock();
            if (AppManager.IsInstalled("Compiler"))
            {
                if (!IsTesting)
                {
                    // Ensure data serialization contracts (with generics)
                    // Specified with .NET can be deserialized with .NET Framework
#if !NET5_0 && !NETCOREAPP3_1
                    AssemblyResolver.RedirectAssembly("System.Private.CoreLib", "mscorlib");
#endif
                }

                AppDomain.CurrentDomain.AssemblyResolve += delegate(object sender, ResolveEventArgs args)
                {
                    string assemblyName = Utility.ReflectedName(args.Name);
                    foreach (IExtension extension in Extensions.All)
                    {
                        if (extension.Name == assemblyName)
                        {
                            return extension.GetType().Assembly;
                        }
                    }

                    string fullPath = Path.Combine(InstanceDirectory, $"{assemblyName}.dll");
                    if (File.Exists(fullPath))
                    {
                        return Assembly.LoadFile(fullPath);
                    }
                    fullPath = Path.Combine(ExtensionDirectory, $"{assemblyName}.dll");
                    if (File.Exists(fullPath))
                    {
                        return Assembly.LoadFile(fullPath);
                    }

                    return null;
                };

                InitializeChangeWatchers();

                // Register .cs plugin loader
                Extensions.Manager.RegisterPluginLoader(_plugins.Loader);
                _plugins.Loader.AddReferences();
            }
            else if (!IsTesting)
            {
                RootLogger.Warning(Strings.Apps.StartWithout.Interpolate("app", "compiler"));
            }

            // Load all plugin watchers for extensions
            foreach (IExtension ext in Extensions.Manager.GetExtensions())
            {
                if (AppManager.IsInstalled("Compiler"))
                {
                    ext.LoadPluginWatchers(PluginDirectory);
                }

                ext.Configure();
            }

            // Load all plugins
            RootLogger?.Info(Strings.Module.LoadingPlugins);
            Plugins.LoadAll(true);

            if (AppManager.IsInstalled("Compiler"))
            {
                // Set up events for all changes watchers
                RegisterChangeWatcherEvents();
            }

            OnLoaded?.Invoke();
        }

        /// <summary>
        /// Check for a reliable clock, else set primitive timer
        /// </summary>
        private void ValidateClock()
        {
            if (_getTimeSinceStartup == null)
            {
                RegisterFallbackEngineClock();
                RootLogger?.Warning(Strings.Module.ClockFailure);
            }
        }

        /// <summary>
        /// Initializes logger fallback
        /// </summary>
        internal void OnLoggerFallback()
        {
            // Logger was not initialized
            if (RootLogger is PipeLogger pipeLogger)
            {
                Configuration.Logging.ResolveLoggerTypes(Application, Types);
                if (Logging == null)
                {
                    Logging = new Configuration.Logging(Path.Combine(InstanceDirectory, "logging.toml"), this, Application);
                }

                Logging.FileLogging = CommandLine.HasVariable("nolog");
                StackLogger stackLogger = new StackLogger();
                DailyFileLogger fileLogger = new DailyFileLogger();
                NativeLogger nativeLogger = null;
                if (debugCallback != null)
                {
                    nativeLogger = new NativeLogger();
                    nativeLogger.OnAdded.Invoke(nativeLogger);
                    nativeLogger.OnConfigure.Invoke(nativeLogger);
                }
                fileLogger.OnAdded.Invoke(fileLogger);
                fileLogger.OnConfigure.Invoke(fileLogger);
                stackLogger.AddLogger(fileLogger);
                if (nativeLogger != null)
                {
                    stackLogger.AddLogger(nativeLogger);
                }

                RootLogger = stackLogger;
                pipeLogger.Pipe(RootLogger);
            }
        }

        /// <summary>
        /// Register fallback engine clock
        /// </summary>
        private void RegisterFallbackEngineClock()
        {
            _timer = new Stopwatch();
            _timer.Start();
            _getTimeSinceStartup = FallbackTime;
        }

        private float FallbackTime()
        {
            return (float)_timer.Elapsed.TotalSeconds;
        }

        /// <summary>
        /// Initialize change watchers
        /// </summary>
        private void InitializeChangeWatchers()
        {
            // Register .cs plugin watcher
            if (_plugins.Configuration.Watchers.PluginWatchers)
            {
                SourceWatcher pluginWatcher = new SourceWatcher(PluginDirectory, "*.cs", SearchOption.AllDirectories);
                Extensions.Manager.RegisterChangeWatcher(pluginWatcher);
                pluginWatcher.StartWatcher();
            }
            else
            {
                RootLogger?.Warning(Strings.Module.PluginWatchersDisabled);
            }

            // Register module configuration watcher
            ConfigWatcher moduleConfigWatcher = new ConfigWatcher(InstanceDirectory, "*.toml", SearchOption.TopDirectoryOnly);
            moduleConfigWatcher.AddMappings("auth", "database", "logging", "plugins", "telemetry", "web");
            Extensions.Manager.RegisterChangeWatcher(moduleConfigWatcher);
            moduleConfigWatcher.StartWatcher();

            if (_plugins.Configuration.Watchers.ConfigWatchers)
            {
                // Register plugin configuration file watchers
                ConfigWatcher configWatcher = new ConfigWatcher(ConfigDirectory, "*.json,*.toml", SearchOption.AllDirectories);
                Extensions.Manager.RegisterChangeWatcher(configWatcher);
                configWatcher.StartWatcher();
                RootLogger?.Warning(Strings.Module.ConfigWatchersEnabled);
            }
        }

        /// <summary>
        /// Set up events for all change watchers
        /// </summary>
        private void RegisterChangeWatcherEvents()
        {
            foreach (IChangeWatcher watcher in Extensions.Manager.GetChangeWatchers())
            {
                if (watcher is SourceWatcher)
                {
                    watcher.OnAdded.Add(watcher_OnPluginAdded);
                    watcher.OnChanged.Add(watcher_OnPluginChanged);
                    watcher.OnRemoved.Add(watcher_OnPluginRemoved);
                }
                else if (watcher is ConfigWatcher)
                {
                    watcher.OnChanged.Add(watcher_OnConfigChanged);
                }
            }
        }

        /// <summary>
        /// Register a library using the specified name and library
        /// </summary>
        /// <param name="name"></param>
        /// <param name="library"></param>
        [Obsolete("Use Library.Register instead")]
        public void RegisterLibrary(string name, ILibrary library)
        {
            Libraries.Register(name, library);
        }

        /// <summary>
        /// Gets the library by the specified name or generic type
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [Obsolete("Use Libraries.Get instead")]
        public T GetLibrary<T>(string name = null) where T : class, ILibrary => Libraries.Get<T>(name);

        /// <summary>
        /// Gets the library by the specified name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [Obsolete("Use Libraries.Get instead")]
        public ILibrary GetLibrary(string name) => Libraries.Get(name);

        /// <summary>
        /// Gets loaded extensions
        /// </summary>
        /// <returns></returns>
        [Obsolete("Use Extensions.All instead")]
        public IEnumerable<IExtension> GetAllExtensions() => Extensions.All;

        /// <summary>
        /// Gets an extension with the specified name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [Obsolete("Use Extensions.Get (or Extensions[name]) instead")]
        public IExtension GetExtension(string name) => Extensions.Get(name);

        /// <summary>
        /// Gets an extension with the specified name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        [Obsolete("Use Extensions.Get instead")]
        public T GetExtension<T>(string name = null) where T : class, IExtension => Extensions.Manager.GetExtension<T>(name ?? typeof(T).Name);

        /// <summary>
        /// Gets all loaded extensions
        /// </summary>
        /// <returns></returns>
        [Obsolete("Use Extensions.GetLoaders instead")]
        public IEnumerable<IPluginLoader> GetPluginLoaders() => Extensions.GetLoaders();

        #region Logging

        /// <summary>
        /// Logs a formatted debug message to the root logger
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public void LogDebug(string format, params object[] args) => RootLogger?.Write(LogLevel.Debug, args?.Length > 0 ? string.Format(format, args) : format);

        /// <summary>
        /// Logs a formatted error message to the root logger
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public void LogError(string format, params object[] args) => RootLogger?.Write(LogLevel.Error, args?.Length > 0 ? string.Format(format, args) : format);

        /// <summary>
        /// Logs an exception to the root logger
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        /// <returns></returns>
        public void LogException(string message, Exception ex) => RootLogger?.Report(message, ex);

        /// <summary>
        /// Logs a formatted info message to the root logger
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public void LogInfo(string format, params object[] args) => RootLogger?.Write(LogLevel.Info, args?.Length > 0 ? string.Format(format, args) : format);

        /// <summary>
        /// Logs a formatted warning message to the root logger
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public void LogWarning(string format, params object[] args) => RootLogger?.Write(LogLevel.Warning, args?.Length > 0 ? string.Format(format, args) : format);

        #endregion Logging

        #region Plugin Management

        /// <summary>
        /// Scans for all available plugins and attempts to load them
        /// </summary>
        [Obsolete("Use Plugins.LoadAll instead")]
        public void LoadAllPlugins(bool init = false)
        {
            _plugins.LoadAll(init);
        }

        /// <summary>
        /// Register specified plugin loader
        /// </summary>
        /// <param name="loader"></param>
        [Obsolete("Use Extensions.RegisterLoader instead")]
        public void RegisterLoader(IPluginLoader loader)
        {
            Extensions.RegisterLoader(loader);
        }

        /// <summary>
        /// Unregister specified plugin loader
        /// </summary>
        /// <param name="loader"></param>
        [Obsolete("Use Extensions.UnregisterLoader instead")]
        public void UnregisterLoader(IPluginLoader loader)
        {
            Extensions.UnregisterLoader(loader);
        }

        /// <summary>
        /// Unloads the plugin by the given name
        /// </summary>
        /// <param name="name"></param>
        [Obsolete("Use Plugins.Unload instead")]
        public bool UnloadPlugin(string name)
        {
            return _plugins.Unload(name);
        }

        /// <summary>
        /// Loads a plugin by the given name
        /// </summary>
        /// <param name="name"></param>
        [Obsolete("Use Plugins.Load instead")]
        public bool LoadPlugin(string name)
        {
            return _plugins.Load(name);
        }

        /// <summary>
        /// Reloads the plugin by the given name
        /// </summary>
        /// <param name="name"></param>
        [Obsolete("Use Plugins.Reload instead")]
        public bool ReloadPlugin(string name)
        {
            return _plugins.Reload(name);
        }

        #endregion Plugin Management

        #region Extension Management

        /// <summary>
        /// Load the specified extension
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        [Obsolete("Use Extensions.Load instead")]
        public bool LoadExtension(IExtension extension)
        {
            return Extensions.Load(extension);
        }

        /// <summary>
        /// Load the specified extension type
        /// </summary>
        /// <param name="extensionType"></param> A
        /// <returns></returns>
        [Obsolete("Use Extensions.Load instead")]
        public bool LoadExtension(Type extensionType)
        {
            return Extensions.Load(extensionType);
        }

        /// <summary>
        /// Load the specified extension assembly by file name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [Obsolete("Use Extensions.Load instead")]
        public bool LoadExtension(string name)
        {
            return Extensions.Load(name);
        }

        /// <summary>
        /// Unload the specified extension
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        [Obsolete("Use Extensions.Unload instead")]
        public bool UnloadExtension(IExtension extension)
        {
            return Extensions.Unload(extension);
        }

        /// <summary>
        /// Unload the specified extension assembly by file name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [Obsolete("Use Extensions.Unload instead")]
        public bool UnloadExtension(string name)
        {
            return Extensions.Unload(name);
        }

        /// <summary>
        /// Reload the specified extension
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        [Obsolete("Use Extensions.Reload instead")]
        public bool ReloadExtension(IExtension extension)
        {
            return Extensions.Reload(extension);
        }

        /// <summary>
        /// Reload the specified extension assembly by file name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [Obsolete("Use Extensions.Reload instead")]
        public bool ReloadExtension(string name)
        {
            return Extensions.Reload(name);
        }

        #endregion Extension Management

        #region Hook Calling

        /// <summary>
        /// Calls a hook
        /// </summary>
        /// <param name="hookname"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public object CallHook(string hookname, params object[] args)
        {
            if (BaseHookMethods != null && BaseHookMethods.TryGetValue(hookname, out KeyValuePair<IPlugin, Plugins.HookMethod> baseHook) && baseHook.Value != null)
            {
                return baseHook.Value.Invoke(baseHook.Key, args);
            }

            HookName hookName;
            lock (HookName.Cache.HookNameLock)
            {
                hookName = HookName.Cache.Get(hookname);
            }

            return !hookName.IsSkipped ? _plugins?.Manager?.CallHook(hookName, args) : null;
        }

        /// <summary>
        /// Calls a hook
        /// </summary>
        /// <param name="hookname"></param>
        /// <param name="args"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        internal object CallHook(string hookname, object[] args, IEventArgs e)
        {
            HookName hookName;
            lock (HookName.Cache.HookNameLock)
            {
                hookName = HookName.Cache.Get(hookname);
            }

            return !hookName.IsSkipped ? _plugins?.Manager?.CallHook(hookName, args, e) : null;
        }

        /// <summary>
        /// Calls a hook
        /// </summary>
        /// <param name="hookname"></param>
        /// <param name="args"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        internal object CallHook(IHookName hookname, object[] args, IEventArgs e)
        {
            return _plugins?.Manager?.CallHook(hookname, args, e);
        }

        /// <summary>
        /// Calls a deprecated hook and prints a warning
        /// </summary>
        /// <param name="oldHook"></param>
        /// <param name="newHook"></param>
        /// <param name="expireDate"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public object CallDeprecatedHook(string oldHook, string newHook, DateTime expireDate, params object[] args)
        {
            return _plugins?.Manager?.CallDeprecatedHook(oldHook, newHook, expireDate, args);
        }

        /// <summary>
        /// Calls a deprecated hook and prints a warning
        /// </summary>
        /// <param name="oldHook"></param>
        /// <param name="newHook"></param>
        /// <param name="expireDate"></param>
        /// <param name="args"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public object CallDeprecatedHook(IHookName oldHook, IHookName newHook, DateTime expireDate, object[] args, IEventArgs e)
        {
            return _plugins?.Manager?.CallDeprecatedHook(oldHook.ToString(), newHook.ToString(), expireDate, args, e);
        }

        #endregion Hook Calling

        /// <summary>
        /// Queues a callback to be called in the next server frame
        /// </summary>
        /// <param name="callback"></param>
        public void NextTick(Action callback)
        {
            lock (_nextTickLock)
            {
                _nextTickQueue.Enqueue(callback);
            }
        }

        /// <summary>
        /// Registers a callback which will be called every server frame
        /// </summary>
        /// <param name="callback"></param>
        public void OnFrame(Action<float> callback) => _onFrame += callback;

        /// <summary>
        /// Called every server frame, implemented by an engine-specific extension
        /// </summary>
        public void OnFrame(float delta)
        {
            // Call any callbacks queued for this frame
            if (_nextTickQueue.Count > 0)
            {
                Queue<Action> queued;
                lock (_nextTickLock)
                {
                    queued = _nextTickQueue;
                    _nextTickQueue = _lastTickQueue;
                    _lastTickQueue = queued;
                }

                while (queued.Any())
                {
                    try
                    {
                        Action item = queued.Dequeue();
                        item();
                    }
                    catch (Exception exception)
                    {
                        RootLogger?.Report(Strings.Module.NextTickFailure, exception);
                    }
                }
            }

            // Update libraries
            _libtimer.Update(delta);

            // Do not update plugin watchers or call OnFrame in plugins until servers starts ticking
            if (_plugins.IsInitialized)
            {
                // Update extensions
                try
                {
                    _onFrame?.Invoke(delta);
                }
                catch (Exception ex)
                {
                    RootLogger?.Report(Strings.Module.OnFrameFailure.Interpolate("exception", ex.GetType().Name), ex);
                }

                if (_plugins.Loader?.FramePlugins == null)
                {
                    return;
                }

                // Call OnFrame hook in plugins
                lock (_plugins.Loader.FramePluginLock)
                {
                    foreach (IPlugin plugin in _plugins.Loader.FramePlugins)
                    {
                        plugin.CallHook("OnFrame", delta);
                    }
                }
            }
        }

        /// <summary>
        /// Called when the server is saving
        /// </summary>
        public void OnSave() => _libperm.SaveData();

        /// <summary>
        /// Called when the server is shutting down
        /// </summary>
        public void OnShutdown()
        {
            if (!_isShuttingDown)
            {
                _libperm.SaveData();
                _isShuttingDown = true;
                _plugins.UnloadAll();

                foreach (IChangeWatcher changeWatcher in Extensions.Manager.GetChangeWatchers())
                {
                    changeWatcher.StopWatcher();
                }

                foreach (IExtension extension in Extensions.Manager.GetExtensions())
                {
                    extension.OnShutdown();
                }

                foreach (string name in Extensions.Manager.GetLibraryNames())
                {
                    Extensions.Manager.GetLibrary(name).Shutdown();
                }

                Web?.Application?.Shutdown();
                Database?.Application?.Shutdown();
                _plugins?.Application?.Shutdown();

                RootLogger?.OnRemoved?.Invoke(RootLogger);
                Dispatcher.Stop();
                FileDispatcher.Stop();
            }
        }

        /// <summary>
        /// Called by an engine-specific extension to register the engine clock
        /// </summary>
        /// <param name="method"></param>
        public void RegisterEngineClock(Func<float> method) => _getTimeSinceStartup = method;

        #region Change Watchers

        /// <summary>
        /// Called when a watcher has reported a change in a plugin configuration
        /// </summary>
        /// <param name="name"></param>
        private void watcher_OnConfigChanged(string name)
        {
            try
            {
                switch (name)
                {
                    case "auth":
                        _configurationLoader.LoadAuthConfiguration();
                        return;
                    case "database":
                        _configurationLoader.LoadDatabaseConfiguration();
                        return;
                    case "logging":
                        _configurationLoader.LoadLoggingConfiguration();
                        return;
                    case "plugins":
                        _configurationLoader.LoadPluginsConfiguration();
                        return;
                    case "telemetry":
                        _configurationLoader.LoadTelemetryConfiguration();
                        return;
                    case "web":
                        _configurationLoader.LoadWebConfiguration();
                        return;
                }
            }
            catch (Exception ex)
            {
                RootLogger?.Report(ex);
            }

            string originalPath = name;
            string configName = Path.GetFileNameWithoutExtension(name);
            if (name.IndexOf('_') > -1)
            {
                name = name.Substring(0, name.IndexOf('_'));
            }

            if (name.IndexOf(Path.DirectorySeparatorChar) > -1)
            {
                name = name.Substring(0, name.IndexOf(Path.DirectorySeparatorChar));
            }

            if (_plugins.Manager.GetPlugin(name) is Plugin plugin)
            {
                if (_plugins.Manager.IsSubscribedToHook("OnConfigChanged", plugin))
                {
                    plugin.ConfigChanged(configName, originalPath);
                }
                else
                {
                    Plugins.Reload(name);
                }
            }
            else
            {
                RootLogger?.Warning($"Plugin {name} does not exist");
            }
        }

        /// <summary>
        /// Called when a watcher has reported a change in a plugin source
        /// </summary>
        /// <param name="name"></param>
        private void watcher_OnPluginAdded(string name)
        {
            if (_timerInstance != null && !_timerInstance.Destroyed)
            {
                _timerInstance.Destroy();
            }
            _timerInstance = _libtimer.Once(0.4f, delegate ()
            {
                _plugins.Compiler.Paused = false;
            });

            _plugins.Compiler.Paused = true;
            Plugins.Load(name);
        }

        /// <summary>
        /// Called when a watcher has reported a change in a plugin source
        /// </summary>
        /// <param name="name"></param>
        private void watcher_OnPluginChanged(string name)
        {
            Plugins.Reload(name);
        }

        /// <summary>
        /// Called when a watcher has reported a change in a plugin source
        /// </summary>
        /// <param name="name"></param>
        private void watcher_OnPluginRemoved(string name)
        {
            // Cancel any existing compilations first
            _plugins.Compiler.OnScriptRemove(Interface.uMod.PluginDirectory, name);
            Plugins.Unload(name);
        }

        #endregion Change Watchers
    }
}
