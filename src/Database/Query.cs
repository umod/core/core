﻿extern alias References;

using System;
using System.Collections;
using System.Collections.Generic;
using References::Newtonsoft.Json;
using uMod.Common;
using uMod.Common.Database;
using uMod.Pooling;
using CommandType = uMod.Common.Database.CommandType;

namespace uMod.Database
{
    public class Query : IQuery, IDisposable
    {
        internal static DynamicPool QueryPool = Pools.Default<Common.Database.Query>();

        /// <summary>
        /// Gets the plugin name
        /// </summary>
        public string Plugin { get; internal set; }

        /// <summary>
        /// Gets the connection name
        /// </summary>
        public string Connection { get; internal set; }

        /// <summary>
        /// Gets the query type
        /// </summary>
        public QueryType QueryType { get; internal set; }

        /// <summary>
        /// Gets the sql
        /// </summary>
        public string Sql { get; internal set; }

        /// <summary>
        /// Gets the type name
        /// </summary>
        public string TypeName { get; internal set; }

        /// <summary>
        /// Gets the query parameters
        /// </summary>
        public object Parameters { get; internal set; }

        /// <summary>
        /// Gets the transaction name
        /// </summary>
        public string Transaction { get; internal set; }

        /// <summary>
        /// Gets whether or not query is buffered
        /// </summary>
        public bool Buffered { get; internal set; }

        /// <summary>
        /// Gets the query timeout
        /// </summary>
        public int Timeout { get; internal set; }

        /// <summary>
        /// Gets the command type
        /// </summary>
        public CommandType CommandType { get; internal set; }

        /// <summary>
        /// Gets the mapping split configuration
        /// </summary>
        public string SplitOn { get; internal set; }

        /// <summary>
        /// Gets the type to property mapping
        /// </summary>
        public string[] Mapping { get; internal set; }

        /// <summary>
        /// Query Plugin
        /// </summary>
        public IContext Context { get; internal set; }

        /// <summary>
        /// Query ObjectModel
        /// </summary>
        public ObjectModel Model { get; internal set; }

        private readonly Provider _database;

        /// <summary>
        /// Create new Query
        /// </summary>
        /// <param name="database"></param>
        /// <param name="context"></param>
        internal Query(Provider database, IContext context)
        {
            _database = database;
            Context = context;
        }

        /// <summary>
        /// Get intermediate WebRequestData for web client
        /// </summary>
        /// <returns></returns>
        internal Common.Database.Query GetData()
        {
            if (Parameters != null)
            {
                if (Parameters.GetType().IsClass)
                {
                    Parameters = JsonConvert.SerializeObject(Parameters);
                }
            }

            Common.Database.Query query = QueryPool.Get<Common.Database.Query>();

            query.Connection = Connection;
            query.QueryType = QueryType;
            query.Sql = Sql;
            query.TypeName = TypeName;
            query.Parameters = Parameters;
            query.Transaction = Transaction;
            query.Buffered = Buffered;
            query.Timeout = Timeout.Equals(0) ? _database.Configuration.Timeout : Timeout;
            query.CommandType = CommandType;
            query.Model = Model;

            return query;
        }

        public IPromise Invoke()
        {
            ISettleablePromise<object> promise = Promise.Create<object>();
            _database?.Application?.EnqueueQuery(GetData(), promise);

            return promise;
        }

        /// <summary>
        /// Dispose of Query
        /// </summary>
        public void Dispose()
        {
            Parameters = null;
        }
    }

    /// <summary>
    /// Represents a database query
    /// </summary>
    public class Query<T> : IQuery, IDisposable
    {
        /// <summary>
        /// Gets the plugin name
        /// </summary>
        public string Plugin { get; internal set; }

        /// <summary>
        /// Gets the connection name
        /// </summary>
        public string Connection { get; internal set; }

        /// <summary>
        /// Gets the query type
        /// </summary>
        public QueryType QueryType { get; internal set; }

        /// <summary>
        /// Gets the sql
        /// </summary>
        public string Sql { get; internal set; }

        /// <summary>
        /// Gets the type name
        /// </summary>
        public string TypeName { get; internal set; }

        /// <summary>
        /// Gets the query parameters
        /// </summary>
        public object Parameters { get; internal set; }

        /// <summary>
        /// Gets the transaction name
        /// </summary>
        public string Transaction { get; internal set; }

        /// <summary>
        /// Gets whether or not query is buffered
        /// </summary>
        public bool Buffered { get; internal set; }

        /// <summary>
        /// Gets the query timeout
        /// </summary>
        public int Timeout { get; internal set; }

        /// <summary>
        /// Gets the command type
        /// </summary>
        public CommandType CommandType { get; internal set; }

        /// <summary>
        /// Gets the mapping split configuration
        /// </summary>
        public string SplitOn { get; internal set; }

        /// <summary>
        /// Gets the type to property mapping
        /// </summary>
        public string[] Mapping { get; internal set; }

        /// <summary>
        /// Query Plugin
        /// </summary>
        public readonly IContext Context;

        /// <summary>
        /// Query ObjectModel
        /// </summary>
        public ObjectModel Model { get; internal set; }

        private readonly Provider _database;

        /// <summary>
        /// Create new Query
        /// </summary>
        /// <param name="database"></param>
        /// <param name="context"></param>
        internal Query(Provider database, IContext context)
        {
            _database = database;
            Context = context;
        }

        /// <summary>
        /// Get intermediate WebRequestData for web client
        /// </summary>
        /// <returns></returns>
        internal Common.Database.Query GetData()
        {
            Common.Database.Query query = Query.QueryPool.Get<Common.Database.Query>();
            query.Connection = Connection;
            query.QueryType = QueryType;
            query.Sql = Sql;
            query.TypeName = TypeName;
            query.Parameters = Parameters;
            query.Transaction = Transaction;
            query.Buffered = Buffered;
            query.Timeout = Timeout.Equals(0) ? _database.Configuration.Timeout : Timeout;
            query.CommandType = CommandType;
            query.SplitOn = SplitOn;
            query.Model = Model;
            query.Mapping = Mapping;

            return query;
        }

        /// <summary>
        /// Send query to database application
        /// </summary>
        public IPromise<T> Invoke()
        {
            ISettleablePromise<object> promise = Promise.Create<object>();
            _database?.Application?.EnqueueQuery(GetData(), promise);

            return promise.Then((result) =>
            {
                // If the result is not the expected type but is a string, assume it's JSON and deserialize it
                if (result.GetType() != typeof(T) && result is string)
                {
                    result = JsonConvert.DeserializeObject<T>(result.ToString());
                }

                // Attempt to convert a list of objects to a strongly-typed list if necessary
                if (typeof(IList).IsAssignableFrom(typeof(T)) && result.GetType() == typeof(List<object>) && typeof(T) != typeof(object))
                {
                    result = result.ToType<T>();
                }

                return (T)result;
            });
        }

        /// <summary>
        /// Dispose of Query
        /// </summary>
        public void Dispose()
        {
            Parameters = null;
        }
    }
}
