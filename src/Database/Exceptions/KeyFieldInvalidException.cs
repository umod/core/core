﻿using System;

namespace uMod.Database.Exceptions
{
    public class KeyFieldInvalidException : Exception
    {
        public KeyFieldInvalidException()
        {
        }

        public KeyFieldInvalidException(string message) : base(message)
        {
        }

        public KeyFieldInvalidException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
