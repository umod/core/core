﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using uMod.Common;
using uMod.Common.Database;
using uMod.Configuration;
using uMod.Database.Exceptions;
using uMod.Pooling;
using CommandType = uMod.Common.Database.CommandType;

namespace uMod.Database
{
    /// <summary>
    /// Represents a database client
    /// </summary>
    public sealed class Client
    {
        /// <summary>
        /// Database client context
        /// </summary>
        internal readonly IContext Context;

        /// <summary>
        /// Database client connection
        /// </summary>
        internal readonly object ConnectionLock = new object();

        /// <summary>
        /// Dictionary of established connections
        /// </summary>
        internal Dictionary<string, Connection> Connections;

        /// <summary>
        /// Connect event
        /// </summary>
        public IEvent<Connection> OnConnected { get; } = new Event<Connection>();

        /// <summary>
        /// Disconnect event
        /// </summary>
        public IEvent<Connection> OnDisconnected { get; } = new Event<Connection>();

        /// <summary>
        /// Database provider
        /// </summary>
        internal Provider Database;

        /// <summary>
        /// Object model resolver
        /// </summary>
        private ObjectModelResolver _modelResolver;

        /// <summary>
        /// List of connection names once previously connected
        /// </summary>
        private readonly List<ConnectionInfo> _wasConnected = new List<ConnectionInfo>();

        /// <summary>
        /// Object model resolver
        /// </summary>
        internal ObjectModelResolver ModelResolver
        {
            get
            {
                if (_modelResolver != null)
                {
                    return _modelResolver;
                }

                return _modelResolver = new ObjectModelResolver();
            }
        }

        /// <summary>
        /// Create a plugin web wrapper for the specified plugin
        /// </summary>
        /// <param name="database"></param>
        /// <param name="context"></param>
        public Client(Provider database, IContext context = null)
        {
            Database = database;
            Context = context;
            OnConnected.Add(delegate (Connection connection)
            {
                lock (ConnectionLock)
                {
                    if (Connections == null)
                    {
                        Connections = new Dictionary<string, Connection>();
                    }
                    if (Connections.ContainsKey(connection.Name))
                    {
                        Connections[connection.Name] = connection;
                    }
                    else
                    {
                        Connections.Add(connection.Name, connection);
                    }
                }
            });

            OnDisconnected.Add(delegate (Connection connection)
            {
                lock (ConnectionLock)
                {
                    if (Connections?.ContainsKey(connection.Name) ?? false)
                    {
                        Connections.Remove(connection.Name);
                    }
                }
            });
        }

        /// <summary>
        /// Open connection using specified connection info
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public IPromise<Connection> Open(ConnectionInfo info)
        {
            Connection connection = null;
            lock (ConnectionLock)
            {
                Connections?.TryGetValue(info.Name, out connection);
            }

            if (connection != null)
            {
                return connection.Open();
            }

            connection = new Connection(Database, this, info);
            return connection.Open();
        }

        /// <summary>
        /// Open default connection using globally configured connection
        /// </summary>
        /// <returns></returns>
        public IPromise<Connection> Open()
        {
            return Open(Database.Configuration.Default);
        }

        /// <summary>
        /// Open connection using globally configured connection info
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IPromise<Connection> Open(string name)
        {
            if (string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(Database.Configuration.Default))
            {
                return Open(Database.Configuration.Default);
            }
            if (!string.IsNullOrEmpty(name) && Database.Configuration.Connections.TryGetValue(name, out IDatabaseConnection dbConnection))
            {
                return Open(dbConnection.Connection);
            }

            return Promise.Reject<Connection>(new ConnectionException(Interface.uMod.Strings.Database.ConnectionMissing.Interpolate("name", name)));
        }

        /// <summary>
        /// Create and open connection using specified parameters
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="connectionString"></param>
        /// <param name="persistent"></param>
        /// <returns></returns>
        public IPromise<Connection> Open(string name, ConnectionType type, string connectionString, bool persistent = true)
        {
            ConnectionInfo connectionInfo = new ConnectionInfo
            {
                Name = name,
                ConnectionString = connectionString,
                Type = type,
                Persistent = persistent
            };

            if (!_wasConnected.Contains(connectionInfo))
            {
                _wasConnected.Add(connectionInfo);
            }

            return Open(connectionInfo);
        }

        /// <summary>
        /// Run all migrations for a given connection
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public IPromise Migrate(Connection connection, MigrationDirection direction)
        {
            IDictionary<Type, MigrationAttribute> migrations = Context.GetType()
                .GetNestedTypeDictionaryWithAttribute<MigrationAttribute>(BindingFlags.NonPublic | BindingFlags.Public |
                                                                          BindingFlags.Instance);

            if (migrations != null && migrations.Count > 0)
            {
                List<IPromise> migrationTasks = new List<IPromise>(migrations.Count);

                foreach (KeyValuePair<Type, MigrationAttribute> kvp in migrations)
                {
                    if (kvp.Value.Type == ConnectionType.None ||
                        kvp.Value.Type == connection.Info.Type)
                    {
                        migrationTasks.Add(connection.Migrate(kvp.Key, direction));
                    }
                }

                return Promise.All(migrationTasks);
            }

            return Promise.Resolve();
        }

        /// <summary>
        /// Removes all related connection info from database memory
        /// </summary>
        internal void RemoveConnections()
        {
            foreach (ConnectionInfo connectionInfo in _wasConnected)
            {
                Database.Application.EnqueueDisconnect(connectionInfo, true);
            }

            _wasConnected.Clear();
        }
    }

    /// <summary>
    /// Connection class
    /// </summary>
    public sealed class Connection : INameable
    {
        /// <summary>
        /// Connection client
        /// </summary>
        internal readonly Client Client;

        /// <summary>
        /// Connection info
        /// </summary>
        internal readonly ConnectionInfo Info;

        /// <summary>
        /// Connection state
        /// </summary>
        public ConnectionState State { get; internal set; } = ConnectionState.Closed;

        /// <summary>
        /// Gets connection name
        /// </summary>
        public string Name => Info.Name;

        internal Provider Database;

        /// <summary>
        /// Create new connection
        /// </summary>
        /// <param name="database"></param>
        /// <param name="client"></param>
        /// <param name="info"></param>
        public Connection(Provider database, Client client, ConnectionInfo info)
        {
            Database = database;
            Client = client;
            Info = info;
        }

        /// <summary>
        /// Open connection
        /// </summary>
        /// <returns></returns>
        public IPromise<Connection> Open()
        {
            return Promise.Create<Connection>((resolve, reject) =>
            {
                Client.Database.Application.EnqueueConnect(Info, delegate (ConnectionState state, string message)
                {
                    if (state != State && state == ConnectionState.Open)
                    {
                        Client.OnConnected?.Invoke(this);
                    }
                    State = state;
                    if (state == ConnectionState.Open)
                    {
                        resolve(this);
                    }
                    else
                    {
                        reject(new ConnectionException(message));
                    }
                });
            });
        }

        /// <summary>
        /// Close connection
        /// </summary>
        /// <returns></returns>
        public IPromise<Connection> Close()
        {
            return Promise.Create<Connection>((resolve, reject) =>
            {
                Client.Database.Application.EnqueueDisconnect(Info, false, delegate (ConnectionState state, string message)
                {
                    if (state != State && state == ConnectionState.Closed)
                    {
                        Client.OnDisconnected?.Invoke(this);
                    }
                    State = state;
                    if (state == ConnectionState.Closed)
                    {
                        resolve(this);
                    }
                    else
                    {
                        reject(new ConnectionException(message));
                    }
                });
            });
        }

        /// <summary>
        /// Check connection status
        /// </summary>
        /// <param name="statusCallback"></param>
        public void CheckStatus(Action<ConnectionState> statusCallback = null)
        {
            Client.Database.Application.EnqueueStatusCheck(Info, delegate (ConnectionState state)
            {
                bool triggerDisconnected = false, triggerConnected = false;
                if (State == ConnectionState.Open && state != ConnectionState.Open)
                {
                    triggerDisconnected = true;
                }
                if (State != ConnectionState.Open && state == ConnectionState.Open)
                {
                    triggerConnected = true;
                }
                State = state;
                if (triggerConnected)
                {
                    Client.OnConnected?.Invoke(this);
                }
                else if (triggerDisconnected)
                {
                    Client.OnDisconnected?.Invoke(this);
                }
                statusCallback?.Invoke(state);
            });
        }

        /// <summary>
        /// Create transaction
        /// </summary>
        /// <param name="transactionName"></param>
        /// <returns></returns>
        public Transaction Transaction(string transactionName = null)
        {
            if (!Info.Persistent)
            {
                throw new TransactionException($"Cannot create transaction on non-persistent connection \"{Name}\"");
            }

            return new Transaction(Database, this, transactionName);
        }

        /// <summary>
        /// Create strongly-typed transaction
        /// </summary>
        /// <typeparam name="TTransaction"></typeparam>
        /// <param name="transactionName"></param>
        /// <returns></returns>
        public Transaction Transaction<TTransaction>(string transactionName = null) where TTransaction : Transaction
        {
            if (!Info.Persistent)
            {
                throw new TransactionException($"Cannot create transaction on non-persistent connection \"{Name}\"");
            }

            object[] parameters = ArrayPool.Get(2);
            parameters[0] = this;
            parameters[1] = transactionName;

            try
            {
                return Interface.uMod.Application.Make<TTransaction>(typeof(TTransaction), parameters);
            }
            finally
            {
                ArrayPool.Free(parameters);
            }
        }

        /// <summary>
        /// Perform migration in specified direction
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="direction"></param>
        /// <returns></returns>
        public IPromise Migrate<T>(MigrationDirection direction) where T : Migration
        {
            return Migrate(typeof(T), direction);
        }

        /// <summary>
        /// Perform migration in the specified direction
        /// </summary>
        /// <param name="migrationType"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public IPromise Migrate(Type migrationType, MigrationDirection direction)
        {
            object[] parameters = ArrayPool.Get(1);
            parameters[0] = this;
            Migration migration;
            try
            {
                migration = Interface.uMod.Application.Make<Migration>(migrationType, parameters);
            }
            finally
            {
                ArrayPool.Free(parameters);
            }

            if (migration is MigrationMigration)
            {
                return RunMigration(migration, direction);
            }

            return Migration.EnsureMigrationTableExists(this).Then(() => RunMigration(migration, direction));
        }

        /// <summary>
        /// Run migration
        /// </summary>
        /// <param name="migration"></param>
        /// <param name="direction"></param>
        private IPromise RunMigration(Migration migration, MigrationDirection direction)
        {
            IPromise promise;

            if (migration is MigrationMigration)
            {
                promise = migration.Commit(direction);
            }
            else if (direction == MigrationDirection.Up)
            {
                promise = Migration.CheckTableExists(this, migration)
                    .Then((bool found) =>
                    {
                        if (!found)
                        {
                            return migration.Commit(direction);
                        }

                        return null;
                    });
            }
            else
            {
                promise = migration.Commit(direction);
            }

            return promise.CatchPeek((ex) =>
            {
                Interface.uMod.LogException($"Migration \"{migration.Name ?? migration.GetType().Name}\" failed: ", ex);
            });
        }

        /// <summary>
        /// Invoke specified query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private IPromise Invoke(Query query)
        {
            return query.Invoke();
        }

        /// <summary>
        /// Invoke specified generic query
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        private IPromise<T> Invoke<T>(Query<T> query)
        {
            return query.Invoke();
        }

        /// <summary>
        /// Gets result type element name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        internal string GetTypeName<T>()
        {
            if (typeof(IList).IsAssignableFrom(typeof(T)))
            {
                return typeof(T).GetGenericArguments()[0].FullName;
            }

            return typeof(T).FullName;
        }

        /// <summary>
        /// Gets an object model from the specified generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        internal ObjectModel GetTypeModel<T>()
        {
            Type type = typeof(T);

            if (typeof(IList).IsAssignableFrom(type))
            {
                type = type.GetGenericArguments()[0];
            }
            else if
                (typeof(IDictionary).IsAssignableFrom(type))
            {
                type = type.GetGenericArguments()[1];
            }

            if (type.IsClass && type.GetCustomAttribute<ModelAttribute>() != null)
            {
                return Client.ModelResolver.ResolveModel(type);
            }

            return null;
        }

        /// <summary>
        /// Create a new query
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise Query(string sql, object parameters = null, int timeout = 0)
        {
            return Invoke(new Query(Database, Client.Context)
            {
                Plugin = Client.Context?.Name,
                Connection = Info.Name,
                QueryType = QueryType.Query,
                Sql = sql,
                TypeName = null,
                Parameters = parameters,
                Transaction = null,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text
            });
        }

        /// <summary>
        /// Create a new query that returns multiple strongly-typed results
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise<T> Query<T>(string sql, object parameters = null, int timeout = 0)
        {
            return Invoke(new Query<T>(Database, Client.Context)
            {
                Plugin = Client.Context?.Name,
                Connection = Info.Name,
                QueryType = QueryType.Query,
                Sql = sql,
                TypeName = GetTypeName<T>(),
                Parameters = parameters,
                Transaction = null,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text,
                Model = GetTypeModel<T>()
            });
        }

        /// <summary>
        /// Create a new query that returns multiple strongly-typed results with multi-mapping
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="mapping"></param>
        /// <param name="splitOn"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise<T> Query<T>(string sql, IEnumerable<string> mapping, string splitOn = "Id", object parameters = null, int timeout = 0)
        {
            return Invoke(new Query<T>(Database, Client.Context)
            {
                Plugin = Client.Context?.Name,
                Connection = Info.Name,
                QueryType = QueryType.Query,
                Sql = sql,
                TypeName = GetTypeName<T>(),
                Parameters = parameters,
                Transaction = null,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text,
                Mapping = mapping.ToArray(),
                SplitOn = splitOn,
                Model = GetTypeModel<T>()
            });
        }

        /// <summary>
        /// Create a new query that returns first result
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise QueryFirst(string sql, object parameters = null, int timeout = 0)
        {
            return Invoke(new Query(Database, Client.Context)
            {
                Plugin = Client.Context?.Name,
                Connection = Info.Name,
                QueryType = QueryType.QueryFirst,
                Sql = sql,
                TypeName = null,
                Parameters = parameters,
                Transaction = null,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text
            });
        }

        /// <summary>
        /// Create a new query that returns first strongly-typed result
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise<T> QueryFirst<T>(string sql, object parameters = null, int timeout = 0)
        {
            return Invoke(new Query<T>(Database, Client.Context)
            {
                Plugin = Client.Context?.Name,
                Connection = Info.Name,
                QueryType = QueryType.QueryFirst,
                Sql = sql,
                TypeName = GetTypeName<T>(),
                Parameters = parameters,
                Transaction = null,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text,
                Model = GetTypeModel<T>()
            });
        }

        /// <summary>
        /// Create a new query that returns a single result
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise QuerySingle(string sql, object parameters = null, int timeout = 0)
        {
            return Invoke(new Query(Database, Client.Context)
            {
                Plugin = Client.Context?.Name,
                Connection = Info.Name,
                QueryType = QueryType.QuerySingle,
                Sql = sql,
                TypeName = null,
                Parameters = parameters,
                Transaction = null,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text
            });
        }

        /// <summary>
        /// Create a new query that returns a single strongly-typed result
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise<T> QuerySingle<T>(string sql, object parameters = null, int timeout = 0)
        {
            return Invoke(new Query<T>(Database, Client.Context)
            {
                Plugin = Client.Context?.Name,
                Connection = Info.Name,
                QueryType = QueryType.QuerySingle,
                Sql = sql,
                TypeName = GetTypeName<T>(),
                Parameters = parameters,
                Transaction = null,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text,
                Model = GetTypeModel<T>()
            });
        }

        /// <summary>
        /// Create a new non-query with no return rows
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise Execute(string sql, object parameters = null, int timeout = 0)
        {
            return Invoke(new Query(Database, Client.Context)
            {
                Plugin = Client.Context?.Name,
                Connection = Info.Name,
                QueryType = QueryType.Execute,
                Sql = sql,
                TypeName = null,
                Parameters = parameters,
                Transaction = null,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text
            });
        }

        /// <summary>
        /// Create a new non-query with no return rows (but strongly-typed result)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise<T> Execute<T>(string sql, object parameters = null, int timeout = 0)
        {
            return Invoke(new Query<T>(Database, Client.Context)
            {
                Plugin = Client.Context?.Name,
                Connection = Info.Name,
                QueryType = QueryType.Execute,
                Sql = sql,
                TypeName = GetTypeName<T>(),
                Parameters = parameters,
                Transaction = null,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text,
                Model = GetTypeModel<T>()
            });
        }

        /// <summary>
        /// Create a new non-query that returns a scalar value
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise ExecuteScalar(string sql, object parameters = null, int timeout = 0)
        {
            return Invoke(new Query(Database, Client.Context)
            {
                Plugin = Client.Context?.Name,
                Connection = Info.Name,
                QueryType = QueryType.ExecuteScalar,
                Sql = sql,
                TypeName = null,
                Parameters = parameters,
                Transaction = null,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text
            });
        }

        /// <summary>
        /// Create a new non-query that returns a scalar value
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public IPromise<T> ExecuteScalar<T>(string sql, object parameters = null, int timeout = 0)
        {
            return Invoke(new Query<T>(Database, Client.Context)
            {
                Plugin = Client.Context?.Name,
                Connection = Info.Name,
                QueryType = QueryType.ExecuteScalar,
                Sql = sql,
                TypeName = null,
                Parameters = parameters,
                Transaction = null,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text
            });
        }
    }
}
