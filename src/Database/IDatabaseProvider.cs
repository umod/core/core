using System;
using System.Collections.Generic;
using uMod.Plugins;

namespace uMod.Database
{
    public interface IDatabaseProvider
    {
        LegacyConnection OpenDb(string file, Plugin plugin, bool persistent = false);

        void CloseDb(LegacyConnection db);

        Sql NewSql();

        void Query(Sql sql, LegacyConnection db, Action<List<Dictionary<string, object>>> callback);

        void ExecuteNonQuery(Sql sql, LegacyConnection db, Action<int> callback = null);

        void Insert(Sql sql, LegacyConnection db, Action<int> callback = null);

        void Update(Sql sql, LegacyConnection db, Action<int> callback = null);

        void Delete(Sql sql, LegacyConnection db, Action<int> callback = null);
    }
}
