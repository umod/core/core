﻿using System.Globalization;
using System.Threading;

namespace uMod.Threading
{
    public static class MainThreadState
    {
        /// <summary>
        /// Main thread
        /// </summary>
        internal static readonly int MainThreadId = Thread.CurrentThread.ManagedThreadId;

        /// <summary>
        /// Determine if current thread is main thread
        /// </summary>
        public static bool IsMain => MainThreadId == Thread.CurrentThread.ManagedThreadId;

        /// <summary>
        /// Set culture settings for thread and JSON handling
        /// </summary>
        internal static void Initialize()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
        }
    }
}
