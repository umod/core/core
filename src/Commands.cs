using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using uMod.Auth;
using uMod.Command;
using uMod.Common;
using uMod.Common.Command;
using uMod.Configuration;
using uMod.Libraries;
using uMod.Localization;
using uMod.Logging;
using uMod.Plugins;
using uMod.Pooling;
using uMod.Text;

namespace uMod
{
    /// <summary>
    /// Universal commands for all supported games
    /// </summary>
    public class Commands
    {
        // Libraries and references
        internal readonly Universal Universal = Interface.uMod.Libraries.Get<Universal>();
        internal readonly Lang Lang = Interface.uMod.Libraries.Get<Lang>();
        internal readonly Permission Permission = Interface.uMod.Libraries.Get<Permission>();
        internal readonly IPluginManager PluginManager = Interface.uMod.Plugins.Manager;
        internal Func<IPlayer, IArgs, CommandState> VersionCallback;
        internal Plugin Plugin;
        internal PluginCommands _commandHandler;
        private List<string> _commandList;

        #region Command Initialization

        private const string DebugPermission = "umod.debug";
        private const string AgentPermission = "umod.agent";
        private const string LocalePermission = "umod.locale";
        private const string PluginsPermission = "umod.plugins";
        private const string LoadPermission = "umod.load";
        private const string UnloadPermission = "umod.unload";
        private const string GrantPermission = "umod.grant";
        private const string GroupPermission = "umod.group";
        private const string RevokePermission = "umod.revoke";
        private const string ShowPermission = "umod.show";
        private const string UserGroupPermission = "umod.usergroup";
        private const string ShutdownPermission = "umod.shutdown";
        private const string SavePermission = "umod.save";
        private const string ImportPermission = "umod.import";

        public void Initialize(IPlugin plugin, Func<IPlayer, IArgs, CommandState> versionCallback = null)
        {
            Plugin = plugin as Plugin;
            _commandHandler = Plugin?.Commands as PluginCommands;
            VersionCallback = versionCallback;
            _commandList = new List<string>(); // Reduces instantiation of lists

            CreateCommand(CreatePrefixedCommandList(_commandList, "debug {bool?}", false), DebugCommand, DebugPermission);
            if (!Interface.uMod.AppManager.AgentDisabled)
            {
                CreateCommand("umod", ModCommand, AgentPermission);
            }

            // Add core plugin commands
            CreateCommand(CreatePrefixedCommandList(_commandList, "locale", false), LocaleCommand, LocalePermission);
            CreateCommand(CreatePrefixedCommandList(_commandList, "plugins", true), PluginsCommand, PluginsPermission);
            CreateCommand(CreatePrefixedCommandList(_commandList, "load", "plugin.load"), LoadCommand, LoadPermission);
            CreateCommand(CreatePrefixedCommandList(_commandList, "reload", "plugin.reload"), ReloadCommand, LoadPermission);
            CreateCommand(CreatePrefixedCommandList(_commandList, "unload", "plugin.unload"), UnloadCommand, UnloadPermission);

            // Add core permission commands
            CreateCommand(CreatePrefixedCommandList(_commandList, "grant", "perm.grant"), GrantCommand, GrantPermission);
            CreateCommand(CreatePrefixedCommandList(_commandList, "group", "perm.group"), GroupCommand, GroupPermission);
            CreateCommand(CreatePrefixedCommandList(_commandList, "revoke", "perm.revoke"), RevokeCommand, RevokePermission);
            CreateCommand(CreatePrefixedCommandList(_commandList, "show", "perm.show"), ShowCommand, ShowPermission);
            CreateCommand(CreatePrefixedCommandList(_commandList, "usergroup", "perm.usergroup"), UserGroupCommand, UserGroupPermission);

            // Add core misc commands
            CreateCommand(CreatePrefixedCommandList(_commandList, "lang", true), LangCommand, string.Empty);
            CreateCommand(CreatePrefixedCommandList(_commandList, "shutdown", "quit"), ShutdownCommand, ShutdownPermission);
            CreateCommand(CreatePrefixedCommandList(_commandList, "save", false), SaveCommand, SavePermission);
            CreateCommand(CreatePrefixedCommandList(_commandList, "import", false), ImportCommand, ImportPermission);
            CreateCommand(CreatePrefixedCommandList(_commandList, "version", false), VersionProxyCallback, string.Empty);
        }

        private void CreateCommand(string cmdString, Func<IPlayer, IArgs, CommandState> callback, string permission)
        {
            ICommandDefinition commandDefinition = new CommandDefinition(cmdString);
            ICommandInfo command = new CommandInfo(commandDefinition, _commandHandler.CreateCallback(commandDefinition, callback), permission);
            _commandHandler.Add(command);
        }

        private void CreateCommand(IEnumerable<string> commands, Func<IPlayer, IArgs, CommandState> callback, string permission)
        {
            ICommandDefinition commandDefinition = new CommandDefinition(commands);
            ICommandInfo command = new CommandInfo(commandDefinition, _commandHandler.CreateCallback(commandDefinition, callback), permission);
            _commandHandler.Add(command);
        }

        private CommandState VersionProxyCallback(IPlayer player, IArgs args)
        {
            if (VersionCallback != null && VersionCallback.Invoke(player, args) == CommandState.Completed)
            {
                return CommandState.Completed;
            }

            return VersionCommand(player, args);
        }

        private IEnumerable<string> CreatePrefixedCommandList(List<string> commandList, string command, object globalCommand)
        {
            commandList.Clear();
            commandList.AddRange(new[]
            {
                $"umod.{command}",
                $"u.{command}",
                $"oxide.{command}",
                $"o.{command}",
            });

            switch (globalCommand)
            {
                case bool b when b:
                    commandList.Add(command);
                    break;
                case string _:
                    commandList.Add(globalCommand.ToString());
                    break;
            }

            return commandList;
        }

        #endregion Command Initialization

        #region uMod Command

        /// <summary>
        /// Called when the "umod" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState ModCommand(IPlayer player, IArgs args)
        {
            if (args.Length == 0)
            {
                return VersionCommand(player, args);
            }

            CoreLocalization localization = Interface.uMod.GetStrings(player);

            string subCommand = args[0];
            string additionalArgs = string.Empty;

            switch (subCommand.ToLower())
            {
                case "show":
                case "require":
                case "update":
                case "old":
                case "obsolete":
                case "outdated":
                case "suggests":
                case "fund":
                case "depends":
                case "prohibits":
                case "status":
                case "licenses":
                case "uninstall":
                case "remove":
                    return Exec(new Args(args), additionalArgs);
                case "debug":
                case "grant":
                case "group":
                case "locale":
                case "lang":
                case "load":
                case "plugins":
                case "reload":
                case "revoke":
                //case "show": TODO: resolve conflict between agent show and umod.show
                case "unload":
                case "usergroup":
                case "version":
                case "save":
                case "shutdown":
                case "import":
                    return ProxyCommand(subCommand, player, args);
            }

            player.Reply(localization.Command.AgentCommandNotFound.Interpolate(("command", subCommand)));
            return CommandState.Unrecognized;
        }

        /// <summary>
        /// Proxy general umod command to intended internal command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="player"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private CommandState ProxyCommand(string command, IPlayer player, IArgs args)
        {
            ICommandInfo cmdInfo = _commandHandler.GetCommand($"umod.{command}");
            if (cmdInfo == null)
            {
                if (Interface.uMod.Plugins.Configuration.Commands.UnknownCommands)
                {
                    player?.Message(Interface.uMod.GetStrings(player).Command.UnknownCommand
                        .Interpolate("command", $"umod {command}"));
                }

                return CommandState.Unrecognized;
            }

            IArgs newArgs = args.Shift(cmdInfo.Definition);

            if (cmdInfo.Gates != null)
            {
                if (!player.IsServer && !player.IsAdmin && !cmdInfo.Authorize(Plugin.gate, Plugin, player))
                {
                    if (Interface.uMod.Plugins.Configuration.Commands.DeniedCommands)
                    {
                        player.Message(Interface.uMod.GetStrings(player).Command.PermissionDenied
                            .Interpolate("command", $"umod {command}"));
                    }

                    return CommandState.Completed;
                }
            }

            switch (command.ToLower())
            {
                case "debug":
                    return DebugCommand(player, newArgs);
                case "grant":
                    return GrantCommand(player, newArgs);
                case "group":
                    return GroupCommand(player, newArgs);
                case "locale":
                    return LocaleCommand(player, newArgs);
                case "lang":
                    return LangCommand(player, newArgs);
                case "load":
                    return LoadCommand(player, newArgs);
                case "plugins":
                    return PluginsCommand(player, newArgs);
                case "reload":
                    return ReloadCommand(player, newArgs);
                case "revoke":
                    return RevokeCommand(player, newArgs);
                //case "show": TODO: resolve conflict between agent show and umod.show
                //    return ShowCommand(player, args.Shift());
                case "unload":
                    return UnloadCommand(player, newArgs);
                case "usergroup":
                    return UserGroupCommand(player, newArgs);
                case "version":
                    return VersionCommand(player, newArgs);
                case "save":
                    return SaveCommand(player, newArgs);
                case "shutdown":
                    return ShutdownCommand(player, newArgs);
                case "import":
                    return ImportCommand(player, newArgs);
            }

            return CommandState.Unrecognized;
        }

        /// <summary>
        /// Change the logging level of the currently loaded loggers
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public CommandState DebugCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);

            if (args.HasArgument("bool"))
            {
                bool isDebug = args.GetBool("bool");
                if (Interface.uMod.RootLogger is Logger logger)
                {
                    if (isDebug)
                    {
                        logger.LogLevel = LogLevel.Debug;
                    }
                    else
                    {
                        logger.LogLevel = LogLevel.Info;
                    }

                    if (logger is StackLogger stackLogger)
                    {
                        foreach (ILogger subLogger in stackLogger.GetLoggers())
                        {
                            if (subLogger is Logger subLoggerImpl)
                            {
                                if (isDebug)
                                {
                                    subLoggerImpl.LogLevel = LogLevel.Debug;
                                }
                                else
                                {
                                    subLoggerImpl.LogLevel = LogLevel.Info;
                                }
                            }
                        }
                    }
                }
            }

            if (Interface.uMod.RootLogger.LogLevel == LogLevel.Debug)
            {
                Interface.uMod.LogInfo(localization.Command.LogLevel.Interpolate(("level", "Debug")));
            }
            else
            {
                Interface.uMod.LogInfo(localization.Command.LogLevel.Interpolate(("level", Enum.GetName(typeof(LogLevel), Interface.uMod.RootLogger.LogLevel))));
            }

            return CommandState.Completed;
        }

        /// <summary>
        /// Execute agent command
        /// </summary>
        /// <param name="commandArgs"></param>
        /// <returns></returns>
        private CommandState Exec(IArgs args, string additionalArgs)
        {
            Interface.uMod.Dispatcher.Dispatch(delegate()
            {
                try
                {
                    string arguments = string.Join(" ", args.Arguments.Select(x => x.Value.ToString()).ToArray());
                    arguments = $"{arguments} --scope-server {additionalArgs}";

                    ProcessStartInfo processStartInfo = new ProcessStartInfo()
                    {
                        FileName = Interface.uMod.AppManager.AgentPath,
                        Arguments = arguments,
                        CreateNoWindow = true,
                        WindowStyle = ProcessWindowStyle.Hidden,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        UseShellExecute = false
                    };

                    Process process = new Process
                    {
                        StartInfo = processStartInfo,
                        EnableRaisingEvents = true
                    };

                    process.OutputDataReceived += (sender, e) =>
                    {
                        string message = e?.Data;
                        if (!string.IsNullOrEmpty(message))
                        {
                            if (message.StartsWith("Error:"))
                            {
                                string messagePart = message.Substring("Error: ".Length);
                                Interface.uMod.LogError(messagePart);
                            }
                            else
                            {
                                Interface.uMod.LogInfo(message);
                            }
                        }
                    };
                    process.ErrorDataReceived += (sender, e) =>
                    {
                        string message = e?.Data;
                        if (string.IsNullOrEmpty(message))
                        {
                            return;
                        }

                        Interface.uMod.LogError(message);
                    };

                    process.Start();
                    process.BeginOutputReadLine();
                    process.BeginErrorReadLine();

                    process.WaitForExit();
                }
                finally
                {
                    Pools.CommandArgs.Free(args);
                }

                return true;
            });

            return CommandState.Completed;
        }

        #endregion

        #region Grant Command

        /// <summary>
        /// Called when the "grant" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState GrantCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length < 3)
            {
                player.Reply(localization.CommandUsage.UsageGrant);
                return CommandState.Canceled;
            }

            string mode = args[0];
            string name = args[1].Sanitize();
            string perm = args[2];

            if (!Permission.PermissionExists(perm))
            {
                player.Reply(localization.Permission.PermissionNotFound.Interpolate("permission", perm));
                return CommandState.Canceled;
            }

            if (mode.Equals("group"))
            {
                if (!Permission.GroupExists(name))
                {
                    player.Reply(localization.Group.GroupNotFound.Interpolate("group", name));
                    return CommandState.Canceled;
                }

                if (Permission.GroupHasPermission(name, perm))
                {
                    player.Reply(localization.Group.GroupAlreadyHasPermission.Interpolate(("group", name), ("permission", perm)));
                    return CommandState.Canceled;
                }

                Permission.GrantGroupPermission(name, perm, null);
                player.Reply(localization.Group.GroupPermissionGranted.Interpolate(("group", name), ("permission", perm)));
            }
            else if (mode.Equals("user") || mode.Equals("player"))
            {
                IPlayer[] foundPlayers = Universal.Players.FindPlayers(name).ToArray();
                if (foundPlayers.Length > 1)
                {
                    player.Reply(localization.Player.PlayersFound.Interpolate("playerList", string.Join(", ", foundPlayers.Select(p => p.Name).ToArray())));
                    return CommandState.Canceled;
                }

                IPlayer target = foundPlayers.Length == 1 ? foundPlayers[0] : null;
                if (target == null && !Permission.UserIdValid(name))
                {
                    player.Reply(localization.Player.PlayerNotFound.Interpolate("name", name));
                    return CommandState.Canceled;
                }

                string playerId = name;
                if (target != null)
                {
                    playerId = target.Id;
                    name = target.Name;
                    Permission.UpdateNickname(playerId, name);
                }

                if (Permission.UserHasPermission(name, perm))
                {
                    player.Reply(localization.PlayerPermission.PlayerAlreadyHasPermission.Interpolate(("player", $"{name} ({playerId})"), ("permission", perm)));
                    return CommandState.Canceled;
                }

                if (Permission.GrantUserPermission(playerId, perm, null))
                {
                    player.Reply(localization.PlayerPermission.PlayerPermissionGranted.Interpolate(("player", $"{name} ({playerId})"), ("permission", perm)));
                }
                else
                {
                    player.Reply(localization.PlayerPermission.PlayerPermissionGrantFail.Interpolate(("player", $"{name} ({playerId})"), ("permission", perm)));
                }
            }
            else
            {
                player.Reply(localization.CommandUsage.UsageGrant);
                return CommandState.Canceled;
            }

            return CommandState.Completed;
        }

        #endregion Grant Command

        // TODO: GrantAllCommand (grant all permissions from user(s)/group(s))

        #region Group Command

        /// <summary>
        /// Called when the "group" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState GroupCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length < 2)
            {
                player.Reply(localization.CommandUsage.UsageGroup);
                player.Reply(localization.CommandUsage.UsageGroupParent);
                player.Reply(localization.CommandUsage.UsageGroupRemove);
                return CommandState.Canceled;
            }

            string mode = args[0];
            string group = args[1];
            string title = args.Length >= 3 ? args[2] : "";
            int rank = args.Length == 4 ? int.Parse(args[3]) : 0;

            if (mode.Equals("add"))
            {
                if (Permission.GroupExists(group))
                {
                    player.Reply(localization.Group.GroupAlreadyExists.Interpolate("group", group));
                    return CommandState.Canceled;
                }

                Permission.CreateGroup(group, title, rank);
                player.Reply(localization.Group.GroupCreated.Interpolate("group", group));
            }
            else if (mode.Equals("remove"))
            {
                if (!Permission.GroupExists(group))
                {
                    player.Reply(localization.Group.GroupNotFound.Interpolate("group", group));
                    return CommandState.Canceled;
                }

                Permission.RemoveGroup(group);
                player.Reply(localization.Group.GroupDeleted.Interpolate("group", group));
            }
            else if (mode.Equals("set"))
            {
                if (!Permission.GroupExists(group))
                {
                    player.Reply(localization.Group.GroupNotFound.Interpolate("group", group));
                    return CommandState.Canceled;
                }

                Permission.SetGroupTitle(group, title);
                Permission.SetGroupRank(group, rank);
                player.Reply(localization.Group.GroupChanged.Interpolate("group", group));
            }
            else if (mode.Equals("parent"))
            {
                if (args.Length <= 2)
                {
                    player.Reply(localization.CommandUsage.UsageGroupParent);
                    return CommandState.Canceled;
                }

                if (!Permission.GroupExists(group))
                {
                    player.Reply(localization.Group.GroupNotFound.Interpolate("group", group));
                    return CommandState.Canceled;
                }

                string parent = args[2];
                if (!string.IsNullOrEmpty(parent) && !Permission.GroupExists(parent))
                {
                    player.Reply(localization.Group.GroupParentNotFound.Interpolate("group", group));
                    return CommandState.Canceled;
                }

                if (Permission.SetGroupParent(group, parent))
                {
                    player.Reply(localization.Group.GroupParentChanged.Interpolate(("group", group), ("parent", parent)));
                }
                else
                {
                    player.Reply(localization.Group.GroupParentNotChanged.Interpolate("group", group));
                }
            }
            else
            {
                player.Reply(localization.CommandUsage.UsageGroup);
                player.Reply(localization.CommandUsage.UsageGroupParent);
                player.Reply(localization.CommandUsage.UsageGroupRemove);
                return CommandState.Canceled;
            }

            return CommandState.Completed;
        }

        #endregion Group Command

        #region Lang Command

        public CommandState LocaleCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);

            if (args.Length < 1)
            {
                player.Reply(localization.CommandUsage.UsageLocale);
                return CommandState.Canceled;
            }

            string pluginName = args[0];
            IPlugin plugin = Utility.Plugins.Get(pluginName);
            if (plugin is Plugin pluginImpl)
            {
                if (args.Length == 2 && args[1].Equals("clear", StringComparison.InvariantCultureIgnoreCase))
                {
                    pluginImpl._locales.Clear();
                    return CommandState.Completed;
                }

                IEnumerable<string> localeNames = pluginImpl._locales.GetLocaleNames();
                List<string> paths = new List<string>();
                foreach (string localeName in localeNames)
                {
                    string localeAttributeName = localeName.Replace(plugin.Name, string.Empty).Trim('_');
                    string[] languages = Lang.GetLanguages(plugin, localeAttributeName);
                    foreach (string language in languages)
                    {
                        string path = Path.Combine(Interface.uMod.LangDirectory, language);
                        string jsonPath = Path.Combine(path, $"{localeName}.json");
                        if (File.Exists(jsonPath))
                        {
                            paths.Add(jsonPath);
                        }
                        else
                        {
                            string tomlPath = Path.Combine(path, $"{localeName}.toml");
                            if (File.Exists(tomlPath))
                            {
                                paths.Add(tomlPath);
                            }
                        }
                    }
                }

                PooledStringBuilder sb = Pools.StringBuilders.Get();
                try
                {
                    sb.AppendLine(localization.Locale.FoundFiles.Interpolate(("count", paths.Count)));
                    foreach (string path in paths)
                    {
                        sb.AppendLine(
                            $"  {path.Replace(Interface.uMod.LangDirectory, string.Empty).Trim('/').Trim('\\')}");
                    }

                    player.Reply(sb.ToString());
                }
                finally
                {
                    Pools.StringBuilders.Free(sb);
                }
            }
            else
            {
                player.Reply(localization.Plugin.NotFound.Interpolate(("plugin", pluginName)));
                return CommandState.Canceled;
            }


            return CommandState.Completed;
        }

        /// <summary>
        /// Called when the "lang" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState LangCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length < 1)
            {
                player.Reply(localization.CommandUsage.UsageLang);
                return CommandState.Canceled;
            }

            if (player.IsServer)
            {
                if (!Lang.HasLanguage(args[0]))
                {
                    player.Reply(localization.Server.ServerLanguageNotFoundOrEmpty.Interpolate("language", args[0]));
                }

                Lang.SetServerLanguage(args[0]);
                player.Reply(localization.Server.ServerLanguage.Interpolate("language", Lang.GetServerLanguage()));
            }
            else
            {
                if (Lang.HasLanguage(args[0]))
                {
                    Lang.SetLanguage(args[0], player.Id);
                    player.Reply(localization.Player.PlayerLanguage.Interpolate("language", args[0]));
                }
                else
                {
                    player.Reply(localization.Player.PlayerLanguageNotFound.Interpolate("language", args[0]));
                }
            }

            return CommandState.Completed;
        }

        #endregion Lang Command

        #region Load Command

        /// <summary>
        /// Called when the "load" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState LoadCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length < 1)
            {
                player.Reply(localization.CommandUsage.UsageLoad);
                return CommandState.Canceled;
            }

            if (args[0].Equals("*") || args[0].ToLower().Equals("all"))
            {
                Interface.uMod.Plugins.LoadAll();
                return CommandState.Canceled;
            }

            Interface.uMod.Plugins.Load(args.Cast<string>());

            return CommandState.Completed;
        }

        #endregion Load Command

        #region Plugins Command

        /// <summary>
        /// Called when the "plugins" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState PluginsCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            IPlugin[] loadedPlugins = PluginManager.GetPlugins().Where(pl => !pl.IsCorePlugin).ToArray();
            HashSet<string> loadedPluginNames = new HashSet<string>(loadedPlugins.Select(pl => pl.Name));
            Dictionary<string, string> unloadedPluginErrors = new Dictionary<string, string>();
            foreach (IPluginLoader loader in Interface.uMod.Extensions.GetLoaders())
            {
                foreach (FileInfo file in loader.ScanDirectory(Interface.uMod.PluginDirectory).Where(f => !loadedPluginNames.Contains(Utility.GetFileNameWithoutExtension(f.Name))))
                {
                    string pluginName = Utility.GetFileNameWithoutExtension(file.Name);
                    unloadedPluginErrors[pluginName] = loader.PluginErrors.TryGetValue(file.Name, out string msg)
                        ? msg
                        : Interface.uMod.Strings.Plugin.Unloaded;
                }

                foreach (string pluginName in loader.Scan().Where(f => !loadedPluginNames.Contains(f)))
                {
                    unloadedPluginErrors[pluginName] = loader.PluginErrors.TryGetValue(pluginName, out string msg)
                        ? msg
                        : Interface.uMod.Strings.Plugin.Unloaded;
                }
            }

            int totalPluginCount = loadedPlugins.Length + unloadedPluginErrors.Count;
            if (totalPluginCount < 1)
            {
                player.Reply(Interface.uMod.GetStrings(player).Plugin.PluginsNotFound);
                return CommandState.Canceled;
            }

            int count = loadedPlugins.Length + unloadedPluginErrors.Count;
            string pluginNoun = localization.Plugin.Plugin.Choice(count).ToLower();
            string output = localization.Plugin.ListingHeader.Interpolate(("count", count), ("noun", pluginNoun));
            int number = 1;
            foreach (IPlugin plugin in loadedPlugins.Where(p => p.Filename != null))
            {
                string titleCard = localization.Plugin.TitleCard
                    .Interpolate(("title", plugin.Title), ("version", plugin.Version), ("author", plugin.Author));
                output += $"\n  {number++:00} {titleCard} ({plugin.Diagnostics.TotalHookTime:0.00}s) - {plugin.Filename.Basename()}";
            }

            foreach (string pluginName in unloadedPluginErrors.Keys)
            {
                output += $"\n  {number++:00} {pluginName} - {unloadedPluginErrors[pluginName]}";
            }

            player.Reply(output);

            return CommandState.Completed;
        }

        #endregion Plugins Command

        #region Reload Command

        /// <summary>
        /// Called when the "reload" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState ReloadCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length < 1)
            {
                player.Reply(localization.CommandUsage.UsageReload);
                return CommandState.Canceled;
            }

            if (args[0].Equals("*") || args[0].ToLower().Equals("all"))
            {
                Interface.uMod.Plugins.ReloadAll();
                return CommandState.Completed;
            }

            Interface.uMod.Plugins.Reload(args.Cast<string>());

            return CommandState.Completed;
        }

        #endregion Reload Command

        #region Revoke Command

        /// <summary>
        /// Called when the "revoke" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState RevokeCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length < 3)
            {
                player.Reply(localization.CommandUsage.UsageRevoke);
                return CommandState.Canceled;
            }

            string mode = args[0];
            string name = args[1].Sanitize();
            string perm = args[2];

            if (mode.Equals("group"))
            {
                if (!Permission.GroupExists(name))
                {
                    player.Reply(localization.Group.GroupNotFound.Interpolate("group", name));
                    return CommandState.Canceled;
                }

                if (!Permission.GroupHasPermission(name, perm))
                {
                    player.Reply(localization.Group.GroupDoesNotHavePermission.Interpolate(("group", name), ("permission", perm)));
                    return CommandState.Canceled;
                }

                if (Permission.IsGroupPermissionInherited(name, perm))
                {
                    string permissionGroup = Permission.GetGroupPermissionGroup(name, perm);
                    if (!string.IsNullOrEmpty(permissionGroup))
                    {
                        player.Reply(localization.Group.GroupPermissionInherited.Interpolate(("group", name), ("permission", perm), ("parentGroup", permissionGroup)));
                        return CommandState.Canceled;
                    }
                }

                Permission.RevokeGroupPermission(name, perm);
                player.Reply(localization.Group.GroupPermissionRevoked.Interpolate(("group", name), ("permission", perm)));
            }
            else if (mode.Equals("user") || mode.Equals("player"))
            {
                IPlayer[] foundPlayers = Universal.Players.FindPlayers(name).ToArray();
                if (foundPlayers.Length > 1)
                {
                    player.Reply(localization.Player.PlayersFound.Interpolate("playerList", string.Join(", ", foundPlayers.Select(p => p.Name).ToArray())));
                    return CommandState.Canceled;
                }

                IPlayer target = foundPlayers.Length == 1 ? foundPlayers[0] : null;
                if (target == null && !Permission.UserIdValid(name))
                {
                    player.Reply(localization.Player.PlayerNotFound.Interpolate("player", name));
                    return CommandState.Canceled;
                }

                string playerId = name;
                if (target != null)
                {
                    playerId = target.Id;
                    name = target.Name;
                    Permission.UpdateNickname(playerId, name);
                }

                if (!Permission.UserHasPermission(playerId, perm))
                {
                    player.Reply(localization.PlayerPermission.PlayerDoesNotHavePermission.Interpolate(("player", name), ("permission", perm)));
                    return CommandState.Canceled;
                }

                if (Permission.IsUserPermissionInherited(playerId, perm))
                {
                    string permissionGroup = Permission.GetUserPermissionGroup(playerId, perm);
                    if (!string.IsNullOrEmpty(permissionGroup))
                    {
                        player.Reply(localization.PlayerPermission.PlayerPermissionInherited.Interpolate(("player", name), ("permission", perm), ("group", permissionGroup)));
                        return CommandState.Canceled;
                    }
                }

                if (Permission.RevokeUserPermission(playerId, perm))
                {
                    player.Reply(localization.PlayerPermission.PlayerPermissionRevoked.Interpolate(("player", $"{name} ({playerId})"), ("permission", perm)));
                }
                else
                {
                    player.Reply(localization.PlayerPermission.PlayerPermissionRevokeFail.Interpolate(("player", $"{name} ({playerId})"), ("permission", perm)));
                }

            }
            else
            {
                player.Reply(localization.CommandUsage.UsageRevoke);
                return CommandState.Canceled;
            }

            return CommandState.Completed;
        }

        #endregion Revoke Command

        // TODO: RevokeAllCommand (revoke all permissions from user(s)/group(s))

        #region Show Command

        /// <summary>
        /// Called when the "show" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState ShowCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length < 1)
            {
                player.Reply(localization.CommandUsage.UsageShow);
                player.Reply(localization.CommandUsage.UsageShowName);
                return CommandState.Canceled;
            }

            string mode = args[0];
            string name = args.Length == 2 ? args[1].Sanitize() : string.Empty;

            if (mode.Equals("perms"))
            {
                player.Reply(localization.Permission.Permission.Choice(2) + ":\n" + string.Join(", ", Permission.GetPermissions()));
            }
            else if (mode.Equals("perm"))
            {
                if (args.Length < 2 || string.IsNullOrEmpty(name))
                {
                    player.Reply(localization.CommandUsage.UsageShow);
                    player.Reply(localization.CommandUsage.UsageShowName);
                    return CommandState.Canceled;
                }

                string[] users = Permission.GetPermissionUsers(name);
                string[] groups = Permission.GetPermissionGroups(name);
                List<string> extendedUserData = Pools.GetList<string>();
                try
                {
                    foreach (string userId in users)
                    {
                        IPlayer playerName = Interface.uMod.Server.PlayerManager.FindPlayerById(userId);
                        if (playerName != null)
                        {
                            extendedUserData.Add($"{userId}({playerName.Name})");
                        }
                        else
                        {
                            extendedUserData.Add($"{userId}(Unnamed)");
                        }
                    }

                    string result = localization.Permission.PermissionPlayers.Interpolate("permission", name) + ":\n";
                    result += extendedUserData.Count > 0
                        ? string.Join(", ", extendedUserData.ToArray())
                        : localization.Permission.NoPermissionPlayers;
                    result += $"\n\n{localization.Permission.PermissionGroups.Interpolate("permission", name)}:\n";
                    result += groups.Length > 0
                        ? string.Join(", ", groups)
                        : localization.Permission.NoPermissionGroups;
                    player.Reply(result);
                }
                finally
                {
                    Pools.FreeList(ref extendedUserData);
                }
            }
            else if (mode.Equals("user") || mode.Equals("player"))
            {
                if (args.Length < 2 || string.IsNullOrEmpty(name))
                {
                    player.Reply(localization.CommandUsage.UsageShow);
                    player.Reply(localization.CommandUsage.UsageShowName);
                    return CommandState.Canceled;
                }

                IPlayer[] foundPlayers = Universal.Players.FindPlayers(name).ToArray();
                if (foundPlayers.Length > 1)
                {
                    player.Reply(localization.Player.PlayersFound.Interpolate("playerList", string.Join(", ", foundPlayers.Select(p => p.Name).ToArray())));
                    return CommandState.Canceled;
                }

                IPlayer target = foundPlayers.Length == 1 ? foundPlayers[0] : null;
                if (target == null && !Permission.UserIdValid(name))
                {
                    player.Reply(localization.Player.PlayerNotFound.Interpolate("player", name));
                    return CommandState.Canceled;
                }

                string playerId = name;
                if (target != null)
                {
                    playerId = target.Id;
                    name = target.Name;
                    Permission.UpdateNickname(playerId, name);
                    name += $" ({playerId})";
                }

                string[] perms = Permission.GetUserPermissions(playerId);
                string[] groups = Permission.GetUserGroups(playerId);
                string result = localization.PlayerPermission.PlayerPermissions.Interpolate("player", name) + ":\n";
                result += perms.Length > 0 ? string.Join(", ", perms) : localization.PlayerPermission.NoPlayerPermissions;
                result += $"\n\n{localization.PlayerGroup.PlayerGroups.Interpolate("player", name)}:\n";
                result += groups.Length > 0 ? string.Join(", ", groups) : localization.PlayerGroup.NoPlayerGroups;
                player.Reply(result);
            }
            else if (mode.Equals("group"))
            {
                if (args.Length < 2 || string.IsNullOrEmpty(name))
                {
                    player.Reply(localization.CommandUsage.UsageShow);
                    player.Reply(localization.CommandUsage.UsageShowName);
                    return CommandState.Canceled;
                }

                if (!Permission.GroupExists(name))
                {
                    player.Reply(localization.Group.GroupNotFound.Interpolate("group", name));
                    return CommandState.Canceled;
                }

                string[] users = Permission.GetUsersInGroup(name);
                string[] perms = Permission.GetGroupPermissions(name);

                string result = localization.Group.GroupPlayers.Interpolate("group", name) + ":\n";
                result += users.Length > 0 ? string.Join(", ", users) : localization.Group.NoPlayersInGroup;
                result += $"\n\n{localization.Group.GroupPermissions.Interpolate("group", name)}:\n";
                result += perms.Length > 0 ? string.Join(", ", perms) : localization.Group.NoGroupPermissions;

                string parent = Permission.GetGroupParent(name);
                while (Permission.GroupExists(parent))
                {
                    result += $"\n{localization.Group.ParentGroupPermissions.Interpolate("group", name)}:\n";
                    result += string.Join(", ", Permission.GetGroupPermissions(parent));
                    parent = Permission.GetGroupParent(parent);
                }
                player.Reply(result);
            }
            else if (mode.Equals("groups"))
            {
                player.Reply(localization.Group.Group.Choice(2) + ":\n" + string.Join(", ", Permission.GetGroups()));
            }
            else
            {
                player.Reply(localization.CommandUsage.UsageShow);
                player.Reply(localization.CommandUsage.UsageShowName);
                return CommandState.Canceled;
            }

            return CommandState.Completed;
        }

        #endregion Show Command

        #region Unload Command

        /// <summary>
        /// Called when the "unload" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState UnloadCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length < 1)
            {
                player.Reply(localization.CommandUsage.UsageUnload);
                return CommandState.Canceled;
            }

            if (args[0].Equals("*") || args[0].ToLower().Equals("all"))
            {
                Interface.uMod.Plugins.UnloadAll();
                return CommandState.Canceled;
            }

            Interface.uMod.Plugins.Unload(args.Cast<string>());

            return CommandState.Completed;
        }

        #endregion Unload Command

        #region User Group Command

        /// <summary>
        /// Called when the "usergroup" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState UserGroupCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length < 3)
            {
                player.Reply(localization.CommandUsage.UsageUserGroup);
                return CommandState.Canceled;
            }

            string mode = args[0];
            string name = args[1].Sanitize();
            string group = args[2];

            IPlayer[] foundPlayers = Universal.Players.FindPlayers(name).ToArray();
            if (foundPlayers.Length > 1)
            {
                player.Reply(localization.Player.PlayersFound.Interpolate("playerList", string.Join(", ", foundPlayers.Select(p => p.Name).ToArray())));
                return CommandState.Canceled;
            }

            IPlayer target = foundPlayers.Length == 1 ? foundPlayers[0] : null;
            if (target == null && !Permission.UserIdValid(name))
            {
                player.Reply(localization.Player.PlayerNotFound.Interpolate(("player", name)));
                return CommandState.Canceled;
            }

            string playerId = name;
            if (target != null)
            {
                playerId = target.Id;
                name = target.Name;
                Permission.UpdateNickname(playerId, name);
                name += $"({playerId})";
            }

            if (!Permission.GroupExists(group))
            {
                player.Reply(localization.Group.GroupNotFound.Interpolate("group", group));
                return CommandState.Canceled;
            }

            if (mode.Equals("add"))
            {
                if (Permission.AddUserGroup(playerId, group))
                {
                    player.Reply(localization.PlayerGroup.PlayerAddedToGroup.Interpolate(("player", name), ("group", group)));
                }
                else
                {
                    player.Reply(localization.PlayerGroup.PlayerAddGroupFail.Interpolate(("player", name), ("group", group)));
                }
            }
            else if (mode.Equals("remove"))
            {
                if (Permission.RemoveUserGroup(playerId, group))
                {
                    player.Reply(localization.PlayerGroup.PlayerRemovedFromGroup.Interpolate(("player", name), ("group", group)));
                }
                else
                {
                    player.Reply(localization.PlayerGroup.PlayerRemoveGroupFail.Interpolate(("player", name), ("group", group)));
                }
            }
            else
            {
                player.Reply(localization.CommandUsage.UsageUserGroup);
                return CommandState.Canceled;
            }

            return CommandState.Completed;
        }

        #endregion User Group Command

        // TODO: UserGroupAllCommand (add/remove all users to/from group)

        #region Version Command

        /// <summary>
        /// Called when the "version" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState VersionCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            string format = Universal.FormatText(localization.Server.Version);
            string message = format.Interpolate(("version", Module.Version), ("game", Universal.Game),
                ("serverVersion", Universal.Server.Version), ("protocol", Universal.Server.Protocol));
            player.Reply(message);
            return CommandState.Completed;
        }

        #endregion Version Command

        #region Save Command

        public CommandState SaveCommand(IPlayer player, IArgs args)
        {
            Interface.uMod.OnSave();
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            player.Reply(localization.Data.DataSaved);
            return CommandState.Completed;
        }

        #endregion Save Command

        #region Shutdown Command

        private CommandState ShutdownCommand(IPlayer player, IArgs args)
        {
            Universal.Server.Shutdown(); // TODO: Implement argument handling for optional save true/false and delay
            return CommandState.Completed;
        }

        #endregion Shutdown Command

        #region Import Command

        private CommandState ImportCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length > 0)
            {
                string sourceName = args[0];

                string connectionName = null;

                bool isSourceConnection = Interface.uMod.Database.Configuration.Connections.ContainsKey(sourceName);

                if (sourceName == "protobuf" || sourceName == "json" || isSourceConnection)
                {
                    if (isSourceConnection)
                    {
                        connectionName = sourceName;
                        sourceName = "database";
                    }
                    AuthDriver sourceDriver;
                    try
                    {
                        sourceDriver = (AuthDriver)Enum.Parse(typeof(AuthDriver), sourceName, true);
                    }
                    catch (ArgumentException)
                    {
                        player.Reply(localization.Command.DriverInvalid.Interpolate("driver", sourceName));
                        return CommandState.Canceled;
                    }

                    string targetName = Enum.GetName(typeof(AuthDriver), Interface.uMod.Auth.Configuration.Driver);
                    if (Interface.uMod.Auth.Configuration.Driver == AuthDriver.Database)
                    {
                        targetName = Interface.uMod.Database.Configuration.Default;
                    }

                    AuthSeeder seeder = new AuthSeeder();

                    player.Reply(localization.Command.ImportStarted.Interpolate(("sourceDriver", connectionName ?? sourceName), ("targetDriver", targetName)));
                    IPromise authJob = seeder.Invoke(sourceDriver, connectionName);
                    if (authJob != null)
                    {
                        authJob.Done(delegate
                        {
                            player.Reply(localization.Command.ImportSuccess);
                        }, delegate(Exception exception)
                        {
                            player.Reply(
                                localization.Command.ImportFailureErrors.Interpolate("errors", exception.Message));
                        });
                    }
                    else
                    {
                        player.Reply(localization.Command.ImportFailureInvalid);
                    }

                    return CommandState.Completed;
                }
            }

            player.Reply(localization.CommandUsage.UsageImport);
            return CommandState.Canceled;
        }

        #endregion Import Command
    }
}
