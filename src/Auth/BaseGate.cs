using System;
using System.Collections;
using System.Collections.Generic;
using uMod.Common;
using uMod.Pooling;

namespace uMod.Auth
{
    /// <summary>
    /// Implements a basic authorization gate abstraction
    /// </summary>
    public abstract class BaseGate : IGate
    {
        /// <summary>
        /// List containing all policies handled by this gate
        /// </summary>
        public abstract IEnumerable<string> Policies { get; }

        /// <summary>
        /// Determine if gate allows the specified policy
        /// </summary>
        /// <param name="policy"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public abstract bool Allows(string policy, object[] arguments);

        /// <summary>
        /// Determine if gate denies the specified policy
        /// </summary>
        /// <param name="policy"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public abstract bool Denies(string policy, object[] arguments);

        /// <summary>
        /// Determine if gate allows all of the specified policies
        /// </summary>
        /// <param name="policies"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public virtual bool All(IEnumerable<string> policies, object[] arguments)
        {
            foreach (string policy in policies)
            {
                if (!Allows(policy, arguments))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Determine if gate allows any of the specified policies
        /// </summary>
        /// <param name="policies"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public virtual bool Any(IEnumerable<string> policies, object[] arguments)
        {
            foreach (string policy in policies)
            {
                if (Allows(policy, arguments))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Determine if gate supports the specified policy
        /// </summary>
        /// <param name="policy"></param>
        /// <returns></returns>
        public abstract bool Has(string policy);

        /// <summary>
        /// Register the specified policy using the specified callback
        /// </summary>
        /// <param name="policy"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public abstract IGate Register(string policy, Func<bool> callback);

        /// <summary>
        /// Register the specified policies
        /// </summary>
        /// <param name="policies"></param>
        /// <returns></returns>
        public virtual IGate Register(IDictionary<string, Func<bool>> policies)
        {
            foreach (KeyValuePair<string, Func<bool>> kvp in policies)
            {
                Register(kvp.Key, kvp.Value);
            }

            return this;
        }

        #region Allows Overloads

        /// <summary>
        /// Determine if the gate allows the specified policy
        /// </summary>
        /// <param name="policy"></param>
        /// <returns></returns>
        public bool Allows(string policy)
        {
            return Allows(policy, null);
        }

        /// <summary>
        /// Determine if the gate allows the specified policy
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <param name="policy"></param>
        /// <param name="arg1"></param>
        /// <returns></returns>
        public bool Allows<TArg1>(string policy, TArg1 arg1)
        {
            object[] array = ArrayPool.Get(1);
            array[0] = arg1;
            try
            {
                return Allows(policy, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate allows the specified policy
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <param name="policy"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <returns></returns>
        public bool Allows<TArg1, TArg2>(string policy, TArg1 arg1, TArg2 arg2)
        {
            object[] array = ArrayPool.Get(2);
            array[0] = arg1;
            array[1] = arg2;
            try
            {
                return Allows(policy, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate allows the specified policy
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <param name="policy"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <returns></returns>
        public bool Allows<TArg1, TArg2, TArg3>(string policy, TArg1 arg1, TArg2 arg2, TArg3 arg3)
        {
            object[] array = ArrayPool.Get(3);
            array[0] = arg1;
            array[1] = arg2;
            array[2] = arg3;
            try
            {
                return Allows(policy, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate allows the specified policy
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <typeparam name="TArg4"></typeparam>
        /// <param name="policy"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <returns></returns>
        public bool Allows<TArg1, TArg2, TArg3, TArg4>(string policy, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4)
        {
            object[] array = ArrayPool.Get(4);
            array[0] = arg1;
            array[1] = arg2;
            array[2] = arg3;
            array[3] = arg4;
            try
            {
                return Allows(policy, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate allows the specified policy
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <typeparam name="TArg4"></typeparam>
        /// <typeparam name="TArg5"></typeparam>
        /// <param name="policy"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <param name="arg5"></param>
        /// <returns></returns>
        public bool Allows<TArg1, TArg2, TArg3, TArg4, TArg5>(string policy, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5)
        {
            object[] array = ArrayPool.Get(4);
            array[0] = arg1;
            array[1] = arg2;
            array[2] = arg3;
            array[3] = arg4;
            array[4] = arg5;
            try
            {
                return Allows(policy, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        #endregion Allows Overloads

        #region Denies Overloads

        /// <summary>
        /// Determine if the gate denies the specified policy
        /// </summary>
        /// <param name="policy"></param>
        /// <returns></returns>
        public bool Denies(string policy)
        {
            return Denies(policy, null);
        }

        /// <summary>
        /// Determine if the gate denies the specified policy
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <param name="policy"></param>
        /// <param name="arg1"></param>
        /// <returns></returns>
        public bool Denies<TArg1>(string policy, TArg1 arg1)
        {
            object[] array = ArrayPool.Get(1);
            array[0] = arg1;

            try
            {
                return Denies(policy, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate denies the specified policy
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <param name="policy"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <returns></returns>
        public bool Denies<TArg1, TArg2>(string policy, TArg1 arg1, TArg2 arg2)
        {
            object[] array = ArrayPool.Get(2);
            array[0] = arg1;
            array[1] = arg2;
            try
            {
                return Denies(policy, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate denies the specified policy
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <param name="policy"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <returns></returns>
        public bool Denies<TArg1, TArg2, TArg3>(string policy, TArg1 arg1, TArg2 arg2, TArg3 arg3)
        {
            object[] array = ArrayPool.Get(3);
            array[0] = arg1;
            array[1] = arg2;
            array[2] = arg3;
            try
            {
                return Denies(policy, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate denies the specified policy
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <typeparam name="TArg4"></typeparam>
        /// <param name="policy"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <returns></returns>
        public bool Denies<TArg1, TArg2, TArg3, TArg4>(string policy, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4)
        {
            object[] array = ArrayPool.Get(4);
            array[0] = arg1;
            array[1] = arg2;
            array[2] = arg3;
            array[3] = arg4;
            try
            {
                return Denies(policy, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate denies the specified policy
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <typeparam name="TArg4"></typeparam>
        /// <typeparam name="TArg5"></typeparam>
        /// <param name="policy"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <param name="arg5"></param>
        /// <returns></returns>
        public bool Denies<TArg1, TArg2, TArg3, TArg4, TArg5>(string policy, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5)
        {
            object[] array = ArrayPool.Get(4);
            array[0] = arg1;
            array[1] = arg2;
            array[2] = arg3;
            array[3] = arg4;
            array[4] = arg5;
            try
            {
                return Denies(policy, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        #endregion Denies Overloads

        #region All Implementation

        /// <summary>
        /// Determine if the gate allows all of the specified policies
        /// </summary>
        /// <param name="policies"></param>
        /// <returns></returns>
        public bool All(IEnumerable<string> policies)
        {
            return All(policies, null);
        }

        /// <summary>
        /// Determine if the gate allows all of the specified policies
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <param name="policies"></param>
        /// <param name="arg1"></param>
        /// <returns></returns>
        public bool All<TArg1>(IEnumerable<string> policies, TArg1 arg1)
        {
            object[] array = ArrayPool.Get(1);
            array[0] = arg1;

            try
            {
                return All(policies, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate allows all of the specified policies
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <param name="policies"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <returns></returns>
        public bool All<TArg1, TArg2>(IEnumerable<string> policies, TArg1 arg1, TArg2 arg2)
        {
            object[] array = ArrayPool.Get(1);
            array[0] = arg1;
            array[1] = arg2;

            try
            {
                return All(policies, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate allows all of the specified policies
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <param name="policies"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <returns></returns>
        public bool All<TArg1, TArg2, TArg3>(IEnumerable<string> policies, TArg1 arg1, TArg2 arg2, TArg3 arg3)
        {
            object[] array = ArrayPool.Get(1);
            array[0] = arg1;
            array[1] = arg2;
            array[2] = arg3;
            try
            {
                return All(policies, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate allows all of the specified policies
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <typeparam name="TArg4"></typeparam>
        /// <param name="policies"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <returns></returns>
        public bool All<TArg1, TArg2, TArg3, TArg4>(IEnumerable<string> policies, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4)
        {
            object[] array = ArrayPool.Get(1);
            array[0] = arg1;
            array[1] = arg2;
            array[2] = arg3;
            array[3] = arg4;
            try
            {
                return All(policies, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate allows all of the specified policies
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <typeparam name="TArg4"></typeparam>
        /// <typeparam name="TArg5"></typeparam>
        /// <param name="policies"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <param name="arg5"></param>
        /// <returns></returns>
        public bool All<TArg1, TArg2, TArg3, TArg4, TArg5>(IEnumerable<string> policies, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5)
        {
            object[] array = ArrayPool.Get(1);
            array[0] = arg1;
            array[1] = arg2;
            array[2] = arg3;
            array[3] = arg4;
            array[4] = arg5;
            try
            {
                return All(policies, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        #endregion All Implementation

        #region Any Implementation

        /// <summary>
        /// Determine if the gate allows any of the specified policies
        /// </summary>
        /// <param name="policies"></param>
        /// <returns></returns>
        public bool Any(IEnumerable<string> policies)
        {
            return Any(policies, null);
        }

        /// <summary>
        /// Determine if the gate allows any of the specified policies
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <param name="policies"></param>
        /// <param name="arg1"></param>
        /// <returns></returns>
        public bool Any<TArg1>(IEnumerable<string> policies, TArg1 arg1)
        {
            object[] array = ArrayPool.Get(1);
            array[0] = arg1;
            try
            {
                return Any(policies, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate allows any of the specified policies
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <param name="policies"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <returns></returns>
        public bool Any<TArg1, TArg2>(IEnumerable<string> policies, TArg1 arg1, TArg2 arg2)
        {
            object[] array = ArrayPool.Get(1);
            array[0] = arg1;
            array[1] = arg2;
            try
            {
                return Any(policies, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate allows any of the specified policies
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <param name="policies"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <returns></returns>
        public bool Any<TArg1, TArg2, TArg3>(IEnumerable<string> policies, TArg1 arg1, TArg2 arg2, TArg3 arg3)
        {
            object[] array = ArrayPool.Get(1);
            array[0] = arg1;
            array[1] = arg2;
            array[2] = arg3;
            try
            {
                return Any(policies, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate allows any of the specified policies
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <typeparam name="TArg4"></typeparam>
        /// <param name="policies"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <returns></returns>
        public bool Any<TArg1, TArg2, TArg3, TArg4>(IEnumerable<string> policies, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4)
        {
            object[] array = ArrayPool.Get(1);
            array[0] = arg1;
            array[1] = arg2;
            array[2] = arg3;
            array[3] = arg4;
            try
            {
                return Any(policies, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate allows any of the specified policies
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <typeparam name="TArg4"></typeparam>
        /// <typeparam name="TArg5"></typeparam>
        /// <param name="policies"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <param name="arg5"></param>
        /// <returns></returns>
        public bool Any<TArg1, TArg2, TArg3, TArg4, TArg5>(IEnumerable<string> policies, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5)
        {
            object[] array = ArrayPool.Get(1);
            array[0] = arg1;
            array[1] = arg2;
            array[2] = arg3;
            array[3] = arg4;
            array[4] = arg5;
            try
            {
                return Any(policies, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        #endregion Any Implementation

        #region None Implementation

        /// <summary>
        /// Determine if the gate denies all of the specified policies
        /// </summary>
        /// <param name="policies"></param>
        /// <returns></returns>
        public bool None(IEnumerable<string> policies)
        {
            return None(policies, null);
        }

        /// <summary>
        /// Determine if the gate denies all of the specified policies
        /// </summary>
        /// <param name="policies"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public bool None(IEnumerable<string> policies, object[] arguments)
        {
            foreach (string policy in policies)
            {
                if (Allows(policy, arguments))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Determine if the gate denies all of the specified policies
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <param name="policies"></param>
        /// <param name="arg1"></param>
        /// <returns></returns>
        public bool None<TArg1>(IEnumerable<string> policies, TArg1 arg1)
        {
            object[] array = ArrayPool.Get(1);
            array[0] = arg1;
            try
            {
                return None(policies, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate denies all of the specified policies
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <param name="policies"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <returns></returns>
        public bool None<TArg1, TArg2>(IEnumerable<string> policies, TArg1 arg1, TArg2 arg2)
        {
            object[] array = ArrayPool.Get(1);
            array[0] = arg1;
            array[1] = arg2;
            try
            {
                return None(policies, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate denies all of the specified policies
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <param name="policies"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <returns></returns>
        public bool None<TArg1, TArg2, TArg3>(IEnumerable<string> policies, TArg1 arg1, TArg2 arg2, TArg3 arg3)
        {
            object[] array = ArrayPool.Get(1);
            array[0] = arg1;
            array[1] = arg2;
            array[2] = arg3;
            try
            {
                return None(policies, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate denies all of the specified policies
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <typeparam name="TArg4"></typeparam>
        /// <param name="policies"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <returns></returns>
        public bool None<TArg1, TArg2, TArg3, TArg4>(IEnumerable<string> policies, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4)
        {
            object[] array = ArrayPool.Get(1);
            array[0] = arg1;
            array[1] = arg2;
            array[2] = arg3;
            array[3] = arg4;
            try
            {
                return None(policies, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        /// <summary>
        /// Determine if the gate denies all of the specified policies
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <typeparam name="TArg4"></typeparam>
        /// <typeparam name="TArg5"></typeparam>
        /// <param name="policies"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <param name="arg5"></param>
        /// <returns></returns>
        public bool None<TArg1, TArg2, TArg3, TArg4, TArg5>(IEnumerable<string> policies, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4,
            TArg5 arg5)
        {
            object[] array = ArrayPool.Get(1);
            array[0] = arg1;
            array[1] = arg2;
            array[2] = arg3;
            array[3] = arg4;
            array[4] = arg5;
            try
            {
                return Any(policies, array);
            }
            finally
            {
                ArrayPool.Free(array);
            }
        }

        #endregion None Implementation

        #region Enumerable

        public IEnumerator<string> GetEnumerator() => Policies.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => Policies.GetEnumerator();

        #endregion Enumerable
    }
}
