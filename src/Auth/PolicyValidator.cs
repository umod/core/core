﻿using System.Collections.Generic;
using System.Linq;
using uMod.Common;
using uMod.Plugins;

namespace uMod.Auth
{
    /// <summary>
    /// Validates policies
    /// </summary>
    public class PolicyValidator : IPolicyValidator
    {
        /// <summary>
        /// List of policy gates
        /// </summary>
        public IEnumerable<IPolicy> Gates { get; }

        private IEnumerator<IPolicy> _gateEnumerator;
        internal bool IsGated;

        /// <summary>
        /// Create a new instance of the PolicyValidator class
        /// </summary>
        /// <param name="gates"></param>
        public PolicyValidator(IEnumerable<IPolicy> gates)
        {
            Gates = SetupGates(gates);
        }

        /// <summary>
        /// Create a new instance of the PolicyValidator class
        /// </summary>
        /// <param name="permission"></param>
        public PolicyValidator(string permission)
        {
            Gates = SetupGates(ConvertPermission(permission));
        }

        /// <summary>
        /// Create a new instance of the PolicyValidator class
        /// </summary>
        /// <param name="permissions"></param>
        public PolicyValidator(params string[] permissions)
        {
            Gates = SetupGates(ConvertPermission(permissions));
        }

        /// <summary>
        /// Convert permission strings to policy (default ALL)
        /// </summary>
        /// <param name="permissions"></param>
        /// <returns></returns>
        private IEnumerable<IPolicy> ConvertPermission(params string[] permissions)
        {
            List<IPolicy> gates = null;

            if (permissions != null && permissions.Length > 0)
            {
                gates = new List<IPolicy> {new AllAttribute(permissions)};
            }

            return gates;
        }

        /// <summary>
        /// Setup gate optimizations
        /// </summary>
        /// <param name="gates"></param>
        /// <returns></returns>
        private IEnumerable<IPolicy> SetupGates(IEnumerable<IPolicy> gates)
        {
            if (gates != null && gates.Any())
            {
                IsGated = true;
                _gateEnumerator = gates.GetEnumerator();
                return gates;
            }

            return null;
        }

        /// <summary>
        /// Authorize policy
        /// </summary>
        /// <param name="gate"></param>
        /// <param name="context"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public bool Authorize(IGate gate, IContext context, params object[] arguments)
        {
            if (!IsGated)
            {
                return true;
            }

            _gateEnumerator.Reset();
            while (_gateEnumerator.MoveNext())
            {
                switch (_gateEnumerator.Current)
                {
                    // Use permission gate
                    case AllowsAttribute allowsAttribute when allowsAttribute.gateType == null:
                        {
                            if (!gate.Allows(allowsAttribute.permission, arguments))
                            {
                                return false;
                            }

                            break;
                        }
                    // Use custom gate
                    case AllowsAttribute allowsAttribute:
                        {
                            if (Interface.uMod.Application.TryInject(_gateEnumerator.Current.GateType, context.GetType(), null, out object param) && param is IGate customGate)
                            {
                                if (!customGate.Allows(allowsAttribute.permission, arguments))
                                {
                                    return false;
                                }
                            }
                            else if (Interface.uMod.Application.TryGetSingleton(_gateEnumerator.Current.GateType, out customGate))
                            {
                                if (!customGate.Allows(allowsAttribute.permission, arguments))
                                {
                                    return false;
                                }
                            }

                            break;
                        }
                    // Use permission gate
                    case DeniesAttribute deniesAttribute when deniesAttribute.gateType == null:
                        {
                            if (!gate.Denies(deniesAttribute.permission, arguments))
                            {
                                return false;
                            }

                            break;
                        }
                    // Use custom gate
                    case DeniesAttribute deniesAttribute:
                        {
                            if (Interface.uMod.Application.TryInject(deniesAttribute.gateType, context.GetType(), null, out object param) && param is IGate customGate)
                            {
                                if (!customGate.Denies(deniesAttribute.permission, arguments))
                                {
                                    return false;
                                }
                            }
                            else if (Interface.uMod.Application.TryGetSingleton(deniesAttribute.gateType, out customGate))
                            {
                                if (!customGate.Denies(deniesAttribute.permission, arguments))
                                {
                                    return false;
                                }
                            }

                            break;
                        }
                    // Use permission gate
                    case AnyAttribute anyAttribute when anyAttribute.gateType == null:
                        {
                            if (!gate.Any(anyAttribute.permissions, arguments))
                            {
                                return false;
                            }

                            break;
                        }
                    // Use custom gate
                    case AnyAttribute anyAttribute:
                        {
                            if (Interface.uMod.Application.TryInject(anyAttribute.gateType, context.GetType(), null, out object param) && param is IGate customGate)
                            {
                                if (!customGate.Any(anyAttribute.permissions, arguments))
                                {
                                    return false;
                                }
                            }
                            else if (Interface.uMod.Application.TryGetSingleton(anyAttribute.gateType, out customGate))
                            {
                                if (!customGate.Any(anyAttribute.permissions, arguments))
                                {
                                    return false;
                                }
                            }

                            break;
                        }
                    // Use permission gate
                    case AllAttribute allAttribute when allAttribute.gateType == null:
                        {
                            if (!gate.All(allAttribute.permissions, arguments))
                            {
                                return false;
                            }

                            break;
                        }
                    // Use custom gate
                    case AllAttribute allAttribute:
                        {
                            if (Interface.uMod.Application.TryInject(allAttribute.gateType, context.GetType(), null, out object param) && param is IGate customGate)
                            {
                                if (!customGate.All(allAttribute.permissions, arguments))
                                {
                                    return false;
                                }
                            }
                            else if (Interface.uMod.Application.TryGetSingleton(allAttribute.gateType, out customGate))
                            {
                                if (!customGate.All(allAttribute.permissions, arguments))
                                {
                                    return false;
                                }
                            }

                            break;
                        }
                    // Use permission gate
                    case NoneAttribute noneAttribute when noneAttribute.gateType == null:
                        {
                            if (!gate.None(noneAttribute.permissions, arguments))
                            {
                                return false;
                            }

                            break;
                        }
                    // Use custom gate
                    case NoneAttribute noneAttribute:
                        {
                            if (Interface.uMod.Application.TryInject(noneAttribute.gateType, context.GetType(), null, out object param) && param is IGate customGate)
                            {
                                if (!customGate.None(noneAttribute.permissions, arguments))
                                {
                                    return false;
                                }
                            }
                            else if (Interface.uMod.Application.TryGetSingleton(noneAttribute.gateType, out customGate))
                            {
                                if (!customGate.None(noneAttribute.permissions, arguments))
                                {
                                    return false;
                                }
                            }

                            break;
                        }
                }
            }

            return true;
        }
    }
}
