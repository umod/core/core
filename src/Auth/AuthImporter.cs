﻿using System;
using System.Collections.Generic;
using uMod.Configuration;

namespace uMod.Auth
{
    internal class AuthImporter
    {
        public List<string> Errors = new List<string>();

        public AuthImporter()
        {
        }

        public bool HasErrors
        {
            get
            {
                return Errors?.Count > 0;
            }
        }

        public void AddError(string error)
        {
            Errors.Add(error);
        }

        public IPromise Invoke(string source, string target)
        {
            string oldConnectionName = null;
            string newConnectionName = null;

            // Get source connection/driver name
            if (Interface.uMod.Database.Configuration.Connections.ContainsKey(source))
            {
                oldConnectionName = target;
                source = "database";
            }

            // Get target connection/driver name
            if (Interface.uMod.Database.Configuration.Connections.ContainsKey(target))
            {
                newConnectionName = target;
                target = "database";
            }

            // Get source driver
            AuthDriver sourceDriver;
            try
            {
                sourceDriver = (AuthDriver)Enum.Parse(typeof(AuthDriver), source, true);
            }
            catch (ArgumentException)
            {
                return null;
            }

            // Get target driver
            AuthDriver targetDriver;
            try
            {
                targetDriver = (AuthDriver)Enum.Parse(typeof(AuthDriver), target, true);
            }
            catch (ArgumentException)
            {
                return null;
            }

            AuthSeeder seeder = new AuthSeeder();

            if (oldConnectionName != newConnectionName)
            {
                return seeder.Invoke(sourceDriver, targetDriver, oldConnectionName);
            }

            return Promise.Resolve();
        }
    }
}
