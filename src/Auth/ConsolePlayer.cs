﻿using System;
using uMod.Common;

namespace uMod.Auth
{
    public abstract class ConsolePlayer : IAuthorizable, IIdentity, IEquatable<IIdentity>, IEquatable<IPlayer>
    {
        public bool IsAdmin => true;
        public bool IsModerator => true;
        public string Id => "server_console";

        /// <summary>
        /// Stub
        /// </summary>
        /// <param name="permission"></param>
        /// <returns></returns>
        public bool Can(string permission) => true;

        /// <summary>
        /// Stub
        /// </summary>
        /// <param name="permission"></param>
        /// <returns></returns>
        public bool HasPermission(string permission) => true;

        /// <summary>
        /// Stub
        /// </summary>
        /// <param name="permission"></param>
        public bool GrantPermission(string permission)
        {
            return true;
        }

        /// <summary>
        /// Stub
        /// </summary>
        /// <param name="permission"></param>
        public bool RevokePermission(string permission)
        {
            return false;
        }

        /// <summary>
        /// Stub
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public bool BelongsToGroup(string group) => false;

        /// <summary>
        /// Stub
        /// </summary>
        /// <param name="group"></param>
        public bool AddToGroup(string group)
        {
            return true;
        }

        /// <summary>
        /// Stub
        /// </summary>
        /// <param name="group"></param>
        public bool RemoveFromGroup(string group)
        {
            return false;
        }

        /// <summary>
        /// Determines if player's unique ID is equal to another player's unique ID
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(IIdentity other) => Id == other?.Id;

        /// <summary>
        /// Determines if player's unique ID is equal to another player's unique ID
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(IPlayer other) => Id == other?.Id;

        /// <summary>
        /// Determines if player's object is equal to another player's object
        /// </summary>
        /// <param name="identityObj"></param>
        /// <returns></returns>
        public override bool Equals(object identityObj) => identityObj is IIdentity identity && Id == identity.Id;

        /// <summary>
        /// Gets the hash code of the player's unique ID
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode() => Id.GetHashCode();
    }
}
