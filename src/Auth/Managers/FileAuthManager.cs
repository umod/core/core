using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using uMod.Common;
using uMod.IO;
using uMod.Pooling;

namespace uMod.Auth.Managers
{
    internal class FileAuthManager : BaseAuthProvider, IAuthManager
    {
        /// <summary>
        /// User data file
        /// </summary>
        private IDataFile<Dictionary<string, UserData>> _userDataFile;

        /// <summary>
        /// User data
        /// </summary>
        private Dictionary<string, UserData> _userdata;

        /// <summary>
        /// Group data file
        /// </summary>
        private IDataFile<Dictionary<string, GroupData>> _groupDataFile;

        /// <summary>
        /// Group data
        /// </summary>
        private Dictionary<string, GroupData> _groupdata;

        /// <summary>
        /// Lang data file
        /// </summary>
        private IDataFile<LangData> _langDataFile;

        /// <summary>
        /// Lang data
        /// </summary>
        internal static LangData LangData;

        /// <summary>
        /// Expiration data
        /// </summary>
        internal static ExpirationData ExpirationData;

        /// <summary>
        /// Determine if provider is loaded
        /// </summary>
        public bool IsLoaded => (_userDataFile?.IsLoaded ?? false) && (_groupDataFile?.IsLoaded ?? false);

        public IEvent<Player> PlayerCreated => throw new NotImplementedException();

        public IEvent<Player> PlayerChanged => throw new NotImplementedException();

        public IEvent<PlayerGroup> PlayerGroupGranted => throw new NotImplementedException();

        public IEvent<PlayerGroup> PlayerGroupRevoked => throw new NotImplementedException();

        public IEvent<PlayerPermission> PlayerPermissionGranted => throw new NotImplementedException();

        public IEvent<PlayerPermission> PlayerPermissionRevoked => throw new NotImplementedException();

        public IEvent<Group> GroupCreated => throw new NotImplementedException();

        public IEvent<Group> GroupRemoved => throw new NotImplementedException();

        public IEvent<Group> GroupChanged => throw new NotImplementedException();

        public IEvent<GroupPermission> GroupPermissionGranted => throw new NotImplementedException();

        public IEvent<GroupPermission> GroupPermissionRevoked => throw new NotImplementedException();

        public IEvent<PlayerGroup[]> PlayerGroupsGranted => throw new NotImplementedException();

        public IEvent<PlayerGroup[]> PlayerGroupsRevoked => throw new NotImplementedException();

        public IEvent<PlayerPermission[]> PlayerPermissionsGranted => throw new NotImplementedException();

        public IEvent<PlayerPermission[]> PlayerPermissionsRevoked => throw new NotImplementedException();

        public FileAuthManager(Provider provider, ILogger logger) : base(provider, logger)
        {
        }

        /// <summary>
        /// Removes players with invalid player IDs
        /// </summary>
        public void CleanUp()
        {
            if (!IsLoaded || validate == null)
            {
                return;
            }

            string[] invalid = _userdata?.Keys.Where(k => !validate(k)).ToArray();
            if (!(invalid?.Length > 0))
            {
                return;
            }

            foreach (string i in invalid)
            {
                _userdata.Remove(i);
            }
        }

        #region File Management

        /// <summary>
        /// Load auth data
        /// </summary>
        /// <param name="authData"></param>
        /// <returns></returns>
        public IPromise Load(AuthData authData = AuthData.All)
        {
            switch (authData)
            {
                case AuthData.Groups:
                    return LoadGroups();

                case AuthData.Users:
                    return Promise.All(
                        LoadUsers(),
                        LoadLang()
                    );

                case AuthData.All:
                default:
                    return Promise.All(
                        LoadUsers(),
                        LoadGroups(),
                        LoadLang()
                    );
            }
        }

        /// <summary>
        /// Load player data
        /// </summary>
        /// <returns></returns>
        private IPromise LoadUsers()
        {
            _userDataFile = DataSystem.MakeDataFile<Dictionary<string, UserData>>(
                Path.Combine(Interface.uMod.DataDirectory, "umod.users"),
                Auth.Configuration.Driver.ToDataFormat()
            );

            if (_userDataFile.Exists)
            {
                return _userDataFile.LoadAsync()
                    .Then((Dictionary<string, UserData> data) =>
                    {
                        _userdata = new Dictionary<string, UserData>(data, StringComparer.InvariantCultureIgnoreCase);
                        _userDataFile.Object = _userdata;
                        data.Clear();
                    });
            }

            _userdata = _userDataFile.Object = new Dictionary<string, UserData>(StringComparer.InvariantCultureIgnoreCase);
            _userDataFile.OnCreateObject?.Invoke(_userDataFile);
            return _userDataFile.SaveAsync();
        }

        private IPromise LoadLang()
        {
            _langDataFile = DataSystem.MakeDataFile<LangData>(
                Path.Combine(Interface.uMod.DataDirectory, "umod.lang"),
                Auth.Configuration.Driver.ToDataFormat()
            );

            if (_langDataFile.Exists)
            {
                return _langDataFile.LoadAsync()
                    .Then((LangData data) =>
                    {
                        LangData = data;
                    });
            }

            LangData = _langDataFile.Object = new LangData();
            _langDataFile.OnCreateObject?.Invoke(_langDataFile);
            return _langDataFile.SaveAsync();
        }

        /// <summary>
        /// Load groups
        /// </summary>
        /// <returns></returns>
        private IPromise LoadGroups()
        {
            _groupDataFile = DataSystem.MakeDataFile<Dictionary<string, GroupData>>(
                Path.Combine(Interface.uMod.DataDirectory, "umod.groups"),
                Auth.Configuration.Driver.ToDataFormat()
            );

            if (_groupDataFile.Exists)
            {
                return _groupDataFile.LoadAsync()
                    .Then((Dictionary<string, GroupData> data) =>
                    {
                        _groupdata = new Dictionary<string, GroupData>(data, StringComparer.InvariantCultureIgnoreCase);
                        _groupDataFile.Object = _groupdata;
                        data.Clear();
                    });
            }

            _groupdata = _groupDataFile.Object = new Dictionary<string, GroupData>(StringComparer.InvariantCultureIgnoreCase);
            _groupDataFile.OnCreateObject?.Invoke(_groupDataFile);
            return _groupDataFile.SaveAsync();
        }

        /// <summary>
        /// Save auth data
        /// </summary>
        /// <param name="authData"></param>
        /// <returns></returns>
        public IPromise Save(AuthData authData = AuthData.All)
        {
            switch (authData)
            {
                case AuthData.Groups:
                    return SaveGroups();

                case AuthData.Users:
                    return Promise.All(
                        SaveUsers(),
                        SaveLang()
                    );

                case AuthData.All:
                default:
                    return Promise.All(
                        Save(AuthData.Users),
                        Save(AuthData.Groups)
                    );
            }
        }

        /// <summary>
        /// Save users
        /// </summary>
        /// <returns></returns>
        private IPromise SaveUsers()
        {
            if (_userDataFile?.IsLoaded ?? false)
            {
                return _userDataFile?.SaveAsync();
            }

            return null;
        }

        private IPromise SaveLang()
        {
            if (_langDataFile?.IsLoaded ?? false)
            {
                return _langDataFile?.SaveAsync();
            }

            return null;
        }

        /// <summary>
        /// Save groups
        /// </summary>
        /// <returns></returns>
        private IPromise SaveGroups()
        {
            if (_groupDataFile?.IsLoaded ?? false)
            {
                return _groupDataFile?.SaveAsync();
            }

            return null;
        }

        #endregion File Management

        #region Server Methods

        /// <summary>
        /// Gets server language
        /// </summary>
        /// <returns></returns>
        public string GetServerLanguage()
        {
            return LangData.Lang;
        }

        /// <summary>
        /// Sets server language
        /// </summary>
        /// <param name="language"></param>
        public void SetServerLanguage(string language)
        {
            if (!string.IsNullOrEmpty(language) && !language.Equals(LangData.Lang))
            {
                LangData.Lang = language;
                SaveLang().Done();
            }
        }

        #endregion Server Methods

        #region User Methods

        /// <summary>
        /// Gets a dictionary of player IDs and names
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, string> GetUsers()
        {
            return _userdata.ToDictionary(x => x.Key, x => x.Value.LastSeenNickname, StringComparer.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Gets player language
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public string GetLanguage(string playerId)
        {
            if (string.IsNullOrEmpty(playerId) || !LangData.UserData.TryGetValue(playerId, out string lang))
            {
                return LangData.Lang;
            }

            return lang;
        }

        /// <summary>
        /// Sets player language
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="language"></param>
        public void SetLanguage(string playerId, string language)
        {
            if (!string.IsNullOrEmpty(language) && !string.IsNullOrEmpty(playerId))
            {
                if (!LangData.UserData.TryGetValue(playerId, out string currentLang) || !language.Equals(currentLang))
                {
                    LangData.UserData[playerId] = language;
                    SaveLang().Done();
                }
            }
        }

        /// <summary>
        /// Returns if the specified player exists
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public bool UserExists(string playerId)
        {
            return _userdata.ContainsKey(playerId);
        }

        /// <summary>
        /// Updates the player name
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="name"></param>
        /// <param name="oldName"></param>
        /// <returns></returns>
        public bool UpdateName(string playerId, string name, out string oldName)
        {
            UserData player = GetPlayerData(playerId);

            oldName = player.LastSeenNickname;
            player.LastSeenNickname = name;

            return true;
        }

        /// <summary>
        /// Triggered when player reconnects
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="name"></param>
        /// <param name="oldName"></param>
        /// <returns></returns>
        public bool UserReconnect(string playerId, string name, out string oldName)
        {
            return UpdateName(playerId, name, out oldName);
        }

        /// <summary>
        /// Updates the player last login time (not implemented)
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public bool UpdateLastLogin(string playerId)
        {
            return false;
        }

        /// <summary>
        /// Gets the data for the specified player
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public IAuthPlayer GetUserData(string playerId)
        {
            if (!_userdata.TryGetValue(playerId, out UserData data))
            {
                _userdata.Add(playerId, data = new UserData());
            }

            return data;
        }

        /// <summary>
        /// Gets the data for the specified player
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public UserData GetPlayerData(string playerId)
        {
            if (!_userdata.TryGetValue(playerId, out UserData data))
            {
                _userdata.Add(playerId, data = new UserData());
            }

            return data;
        }

        #region Querying

        /// <summary>
        /// Returns if the specified player has the specified permission
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool UserHasPermission(string playerId, string perm)
        {
            if (string.IsNullOrEmpty(perm))
            {
                return false;
            }

            // Always allow the server console
            if (playerId.Equals("server_console"))
            {
                return true;
            }

            perm = perm.ToLower();

            // First, get the player data
            UserData data = GetPlayerData(playerId);

            // Check if they have the perm
            if (data.Perms.Contains(perm))
            {
                return true;
            }

            // Check if their group has the perm
            return GroupsHavePermission(data.Groups, perm);
        }

        /// <summary>
        /// Check if player has a group
        /// </summary>
        /// <param name="playerId"></param>
        public bool UserHasAnyGroup(string playerId)
        {
            return UserExists(playerId) && GetPlayerData(playerId).Groups.Count > 0;
        }

        /// <summary>
        /// Get if the player belongs to given group
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool UserHasGroup(string playerId, string name)
        {
            if (!GroupExists(name))
            {
                return false;
            }

            return GetPlayerData(playerId).Groups.Contains(name.ToLower());
        }

        /// <summary>
        /// Returns if the specified player permission is inherited from a group
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool IsUserPermissionInherited(string playerId, string perm)
        {
            if (string.IsNullOrEmpty(perm))
            {
                return false;
            }

            perm = perm.ToLower();

            // First, get the player data
            UserData data = GetPlayerData(playerId);

            // Check if they have the perm
            if (data.Perms.Contains(perm))
            {
                return false;
            }

            // Check if their group has the perm
            return GroupsHavePermission(data.Groups, perm);
        }

        /// <summary>
        /// Returns the group that a specified player permission is inherited from
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public string GetUserPermissionGroup(string playerId, string perm)
        {
            if (string.IsNullOrEmpty(perm))
            {
                return null;
            }

            perm = perm.ToLower();

            // First, get the player data
            UserData data = GetPlayerData(playerId);

            // Check if they have the perm
            if (data.Perms.Contains(perm))
            {
                return null;
            }

            foreach (string group in data.Groups)
            {
                string permissionGroup = GetGroupPermissionGroup(group, perm);
                if (!string.IsNullOrEmpty(permissionGroup))
                {
                    return permissionGroup;
                }
            }

            return null;
        }

        /// <summary>
        /// Returns the group to which the specified player belongs
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public string[] GetUserGroups(string playerId)
        {
            return GetPlayerData(playerId).Groups.ToArray();
        }

        /// <summary>
        /// Returns the permissions which the specified player has
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public string[] GetUserPermissions(string playerId)
        {
            UserData data = GetPlayerData(playerId);
            List<string> perms = data.Perms.ToList();
            foreach (string group in data.Groups)
            {
                perms.AddRange(GetGroupPermissions(group));
            }

            return perms.Distinct().ToArray();
        }

        /// <summary>
        /// Returns the players with given permission
        /// </summary>
        /// <param name="perm"></param>
        /// <returns></returns>
        public string[] GetPermissionUsers(string perm)
        {
            if (string.IsNullOrEmpty(perm))
            {
                return StringPool.Empty;
            }

            perm = perm.ToLower();
            List<string> users = Pools.GetList<string>();
            try
            {
                foreach (KeyValuePair<string, UserData> data in _userdata)
                {
                    if (data.Value.Perms.Contains(perm))
                    {
                        users.Add(data.Key);
                    }
                }

                return users.ToArray();
            }
            finally
            {
                Pools.FreeList(ref users);
            }
        }

        #endregion Querying

        #region User Permissions

        /// <summary>
        /// Set the group to which the specified player belongs
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool AddUserGroup(string playerId, string name)
        {
            if (!GroupExists(name))
            {
                return false;
            }

            UserData data = GetPlayerData(playerId);
            if (!data.Groups.Add(name.ToLower()))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Set the group to which the specified player belongs
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool RemoveUserGroup(string playerId, string name)
        {
            if (!GroupExists(name))
            {
                return false;
            }

            UserData data = GetPlayerData(playerId);
            if (name.Equals("*"))
            {
                if (data.Groups.Count <= 0)
                {
                    return false;
                }

                data.Groups.Clear();
                return true;
            }

            if (!data.Groups.Remove(name.ToLower()))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Grants the specified permission to the specified player
        /// </summary>
        /// <param name="id"></param>
        /// <param name="perm"></param>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public bool GrantUserPermission(string id, string perm, IPlugin plugin)
        {
            // Check if it is even a permission
            if (!PermissionExists(perm, plugin))
            {
                return false;
            }

            // Get the player data
            UserData data = GetPlayerData(id);

            perm = perm.ToLower();

            if (perm.EndsWith("*"))
            {
                HashSet<string> perms;
                if (plugin == null)
                {
                    perms = new HashSet<string>(Permset.Values.SelectMany(v => v));
                }
                else if (!Permset.TryGetValue(plugin, out perms))
                {
                    return false;
                }

                if (perm.Equals("*"))
                {
                    if (!perms.Aggregate(false, (c, s) => c | data.Perms.Add(s)))
                    {
                        return false;
                    }
                }
                else
                {
                    perm = perm.TrimEnd('*');
                    if (!perms.Where(s => s.StartsWith(perm)).Aggregate(false, (c, s) => c | data.Perms.Add(s)))
                    {
                        return false;
                    }
                }
                return true;
            }

            // Add the permission
            return data.Perms.Add(perm);
        }

        /// <summary>
        /// Revokes the specified permission from the specified player
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="perm"></param>
        public bool RevokeUserPermission(string playerId, string perm)
        {
            if (string.IsNullOrEmpty(perm))
            {
                return false;
            }

            // Get the player data
            UserData data = GetPlayerData(playerId);

            perm = perm.ToLower();

            if (perm.EndsWith("*"))
            {
                if (perm.Equals("*"))
                {
                    if (data.Perms.Count <= 0)
                    {
                        return false;
                    }

                    data.Perms.Clear();
                }
                else
                {
                    perm = perm.TrimEnd('*');
                    if (data.Perms.RemoveWhere(s => s.StartsWith(perm)) <= 0)
                    {
                        return false;
                    }
                }

                return true;
            }

            // Remove the permission
            return data.Perms.Remove(perm);
        }

        #endregion User Permissions

        #endregion User Methods

        #region Group Methods

        /// <summary>
        /// Migrate permissions from one group to another
        /// </summary>
        /// <param name="newGroup"></param>
        /// <param name="oldGroup"></param>
        public void MigrateGroup(string oldGroup, string newGroup)
        {
            if (IsLoaded && GroupExists(oldGroup))
            {
                foreach (string perm in GetGroupPermissions(oldGroup))
                {
                    GrantGroupPermission(newGroup, perm, null);
                }

                if (GetUsersInGroup(oldGroup).Length == 0)
                {
                    RemoveGroup(oldGroup);
                }
            }
        }

        /// <summary>
        /// Rename group
        /// </summary>
        /// <param name="oldGroup"></param>
        /// <param name="newGroup"></param>
        public void RenameGroup(string oldGroup, string newGroup)
        {
            if (IsLoaded && GroupExists(oldGroup))
            {
                // Do not allow group overwrite, new name must not exist already
                if (_groupdata.TryGetValue(oldGroup, out GroupData data) && !_groupdata.ContainsKey(newGroup))
                {
                    _groupdata.Add(newGroup, data);
                    _groupdata.Remove(oldGroup);
                }
            }
        }

        #region Querying

        /// <summary>
        /// Returns if the specified group has the specified permission or not
        /// </summary>
        /// <param name="groups"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool GroupsHavePermission(IEnumerable<string> groups, string perm)
        {
            return groups.Any(group => GroupHasPermission(group, perm));
        }

        /// <summary>
        /// Returns if the specified group has the specified permission or not
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool GroupHasPermission(string name, string perm)
        {
            if (string.IsNullOrEmpty(perm) || !GroupExists(name))
            {
                return false;
            }

            if (!_groupdata.TryGetValue(name.ToLower(), out GroupData group))
            {
                return false;
            }

            // Check if the group has the perm
            return group.Perms.Contains(perm.ToLower()) || GroupHasPermission(group.ParentGroup, perm);
        }

        /// <summary>
        /// Checks if specified permission belongs to group or parent group
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool IsGroupPermissionInherited(string name, string perm)
        {
            if (!GroupHasPermission(name, perm))
            {
                return false;
            }

            if (!_groupdata.TryGetValue(name.ToLower(), out GroupData group))
            {
                return false;
            }

            return !group.Perms.Contains(perm.ToLower());
        }

        /// <summary>
        /// Returns the parent group that the specified permission belongs to
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public string GetGroupPermissionGroup(string name, string perm)
        {
            if (!GroupExists(name) || string.IsNullOrEmpty(perm))
            {
                return null;
            }

            if (!_groupdata.TryGetValue(name.ToLower(), out GroupData group))
            {
                return null;
            }

            if (group.Perms.Contains(perm.ToLower()))
            {
                return name;
            }

            return GetGroupPermissionGroup(group.ParentGroup, perm);
        }

        /// <summary>
        /// Returns the permissions which the specified group has, with optional transversing of parent groups
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parents"></param>
        /// <returns></returns>
        public string[] GetGroupPermissions(string name, bool parents = false)
        {
            if (!GroupExists(name))
            {
                return StringPool.Empty;
            }

            if (!_groupdata.TryGetValue(name.ToLower(), out GroupData group))
            {
                return StringPool.Empty;
            }

            List<string> perms = group.Perms.ToList();
            if (parents)
            {
                perms.AddRange(GetGroupPermissions(group.ParentGroup));
            }

            return perms.Distinct().ToArray();
        }

        /// <summary>
        /// Returns the groups with given permission
        /// </summary>
        /// <returns></returns>
        public string[] GetPermissionGroups(string perm)
        {
            if (string.IsNullOrEmpty(perm))
            {
                return StringPool.Empty;
            }

            bool wildcard = perm.EndsWith("*");

            perm = perm.ToLower();
            List<string> groups = Pools.GetList<string>();
            try
            {
                foreach (KeyValuePair<string, GroupData> data in _groupdata)
                {
                    if (wildcard)
                    {
                        if (data.Value.Perms.Any(x =>
                        {
                            if (perm.EndsWith("*"))
                            {
                                return x.StartsWith(perm.Trim('*'));
                            }
                            return x.Equals(perm, StringComparison.InvariantCultureIgnoreCase);
                        }))
                        {
                            groups.Add(data.Key);
                        }
                    }
                    else
                    {
                        if (data.Value.Perms.Contains(perm, StringComparer.InvariantCultureIgnoreCase))
                        {
                            groups.Add(data.Key);
                        }
                    }
                }

                return groups.ToArray();
            }
            finally
            {
                Pools.FreeList(ref groups);
            }
        }

        /// <summary>
        /// Returns if the specified group exists or not
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public bool GroupExists(string group)
        {
            return !string.IsNullOrEmpty(group) && (group.Equals("*") || _groupdata.ContainsKey(group.ToLower()));
        }

        /// <summary>
        /// Returns existing groups
        /// </summary>
        /// <returns></returns>
        public string[] GetGroups()
        {
            return _groupdata.Keys.ToArray();
        }

        /// <summary>
        /// Returns players in specified group
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public string[] GetUsersInGroup(string group)
        {
            if (!GroupExists(group))
            {
                return StringPool.Empty;
            }

            return _userdata.Where(u => u.Value.Groups.Contains(group.ToLower())).Select(u => $"{u.Key} ({u.Value.LastSeenNickname})").ToArray();
        }

        /// <summary>
        /// Gets the parent of the specified group
        /// </summary>
        /// <param name="group"></param>
        public string GetGroupParent(string group)
        {
            if (!GroupExists(group))
            {
                return string.Empty;
            }

            return !_groupdata.TryGetValue(group.ToLower(), out GroupData data) ? string.Empty : data.ParentGroup;
        }

        /// <summary>
        /// Returns the title of the specified group
        /// </summary>
        /// <param name="group"></param>
        public string GetGroupTitle(string group)
        {
            if (!GroupExists(group))
            {
                return string.Empty;
            }

            // First, get the group data
            if (!_groupdata.TryGetValue(group.ToLower(), out GroupData data))
            {
                return string.Empty;
            }

            // Return the group title
            return data.Title;
        }

        /// <summary>
        /// Returns the rank of the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public int GetGroupRank(string group)
        {
            if (!GroupExists(group))
            {
                return 0;
            }

            // First, get the group data
            if (!_groupdata.TryGetValue(group.ToLower(), out GroupData data))
            {
                return 0;
            }

            // Return the group rank
            return data.Rank;
        }

        #endregion Querying

        #region Group Permissions

        /// <summary>
        /// Grant the specified permission to the specified group
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public bool GrantGroupPermission(string name, string perm, IPlugin plugin)
        {
            // Check if it is even a permission
            if (!PermissionExists(perm, plugin) || !GroupExists(name))
            {
                return false;
            }

            // Get the group data
            if (!_groupdata.TryGetValue(name.ToLower(), out GroupData data))
            {
                return false;
            }

            perm = perm.ToLower();

            if (perm.EndsWith("*"))
            {
                HashSet<string> perms;
                if (plugin == null)
                {
                    perms = new HashSet<string>(Permset.Values.SelectMany(v => v));
                }
                else if (!Permset.TryGetValue(plugin, out perms))
                {
                    return false;
                }

                if (perm.Equals("*"))
                {
                    if (!perms.Aggregate(false, (c, s) => c | data.Perms.Add(s)))
                    {
                        return false;
                    }
                }
                else
                {
                    perm = perm.TrimEnd('*').ToLower();
                    if (!perms.Where(s => s.StartsWith(perm)).Aggregate(false, (c, s) => c | data.Perms.Add(s)))
                    {
                        return false;
                    }
                }
                return true;
            }

            // Add the permission
            return data.Perms.Add(perm);
        }

        /// <summary>
        /// Revokes the specified permission from the specified player
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        public bool RevokeGroupPermission(string name, string perm)
        {
            if (!GroupExists(name) || string.IsNullOrEmpty(perm))
            {
                return false;
            }

            // Get the group data
            if (!_groupdata.TryGetValue(name.ToLower(), out GroupData data))
            {
                return false;
            }

            // Remove the permission
            return data.Perms.Remove(perm);
        }

        #endregion Group Permissions

        #region Group Management

        /// <summary>
        /// Creates the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <param name="title"></param>
        /// <param name="rank"></param>
        /// <param name="parent"></param>
        public bool CreateGroup(string group, string title, int rank, string parent = "")
        {
            // Check if it already exists
            if (GroupExists(group) || string.IsNullOrEmpty(group))
            {
                return false;
            }

            // Create the data
            GroupData data = new GroupData { Title = title, Rank = rank, ParentGroup = parent };

            // Add the group
            _groupdata.Add(group.ToLower(), data);

            return true;
        }

        /// <summary>
        /// Removes the specified group
        /// </summary>
        /// <param name="group"></param>
        public bool RemoveGroup(string group)
        {
            // Check if it even exists
            if (!GroupExists(group))
            {
                return false;
            }

            group = group.ToLower();

            // Remove the group
            bool removed = _groupdata.Remove(group);

            if (removed)
            {
                // Set children to having no parent group
                foreach (GroupData child in _groupdata.Values.Where(x => x.ParentGroup == group))
                {
                    child.ParentGroup = string.Empty;
                }
            }

            // Remove group from players
            bool changed = _userdata.Values.Aggregate(false, (current, userData) => current | userData.Groups.Remove(group));
            if (changed)
            {
                SaveUsers();
            }

            return removed;
        }

        /// <summary>
        /// Sets the title of the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <param name="title"></param>
        public bool SetGroupTitle(string group, string title)
        {
            if (!GroupExists(group))
            {
                return false;
            }

            group = group.ToLower();

            // First, get the group data
            if (!_groupdata.TryGetValue(group, out GroupData data))
            {
                return false;
            }

            // Change the title
            if (data.Title == title)
            {
                return true;
            }

            data.Title = title;

            return true;
        }

        /// <summary>
        /// Sets the rank of the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <param name="rank"></param>
        public bool SetGroupRank(string group, int rank)
        {
            if (!GroupExists(group))
            {
                return false;
            }

            group = group.ToLower();

            // First, get the group data
            if (!_groupdata.TryGetValue(group, out GroupData data))
            {
                return false;
            }

            // Change the rank
            if (data.Rank == rank)
            {
                return true;
            }

            data.Rank = rank;

            return true;
        }

        /// <summary>
        /// Sets the parent of the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <param name="parent"></param>
        public bool SetGroupParent(string group, string parent)
        {
            if (!GroupExists(group))
            {
                return false;
            }

            group = group.ToLower();

            // First, get the group data
            if (!_groupdata.TryGetValue(group, out GroupData data))
            {
                return false;
            }

            if (string.IsNullOrEmpty(parent))
            {
                data.ParentGroup = null;
                return true;
            }

            if (!GroupExists(parent) || group.Equals(parent.ToLower()))
            {
                return false;
            }

            parent = parent.ToLower();

            if (!string.IsNullOrEmpty(data.ParentGroup) && data.ParentGroup.Equals(parent))
            {
                return true;
            }

            if (HasCircularParent(group, parent))
            {
                return false;
            }

            // Change the parent group
            data.ParentGroup = parent;

            return true;
        }

        /// <summary>
        /// Determine if group relationship is circular
        /// </summary>
        /// <param name="group"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        private bool HasCircularParent(string group, string parent)
        {
            // Get parent data
            if (!_groupdata.TryGetValue(parent, out GroupData parentData))
            {
                return false;
            }

            // Check for circular reference
            List<string> groups = Pools.GetList<string>();
            try
            {
                groups.Add(group);
                groups.Add(parent);
                while (!string.IsNullOrEmpty(parentData.ParentGroup))
                {
                    // Found itself?
                    if (groups.Contains(parentData.ParentGroup))
                    {
                        return true;
                    }

                    groups.Add(parentData.ParentGroup);

                    // Get next parent
                    if (!_groupdata.TryGetValue(parentData.ParentGroup, out parentData))
                    {
                        return false;
                    }
                }

                return false;
            }
            finally
            {
                Pools.FreeList(ref groups);
            }
        }

        #endregion Group Management

        #endregion Group Methods
    }
}
