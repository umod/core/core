using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using uMod.Auth.Managers;
using uMod.Common.Database;
using uMod.Configuration;
using uMod.Database;
using uMod.Database.Exceptions;
using uMod.IO;
using static uMod.Libraries.Lang;

namespace uMod.Auth
{
    /// <summary>
    /// Data transformer interface
    /// </summary>
    public interface IDataTransformer
    {
        IPromise Invoke();
    }

    /// <summary>
    /// Auth seeder to convert from one driver to another
    /// </summary>
    public class AuthTransformer : IDataTransformer
    {
        /// <summary>
        /// Old auth driver
        /// </summary>
        protected readonly AuthDriver OldDriver;

        /// <summary>
        /// Old auth driver connection name
        /// </summary>
        protected readonly string OldConnectionName;

        /// <summary>
        /// New auth driver
        /// </summary>
        protected readonly AuthDriver Driver;

        protected Database.Provider Database;

        /// <summary>
        /// Create new auth transformer object
        /// </summary>
        /// <param name="database"></param>
        /// <param name="oldDriver"></param>
        /// <param name="newDriver"></param>
        public AuthTransformer(Database.Provider database, AuthDriver oldDriver, AuthDriver newDriver, string connectionName = null)
        {
            Database = database;
            Driver = newDriver;
            OldDriver = oldDriver;
            OldConnectionName = connectionName;
        }

        /// <summary>
        /// Invoke transformer
        /// </summary>
        /// <returns></returns>
        public IPromise Invoke()
        {
            switch (Driver)
            {
                case AuthDriver.Database:
                    return new SqlAuthTransformer(Database, OldDriver, OldConnectionName).Invoke();

                case AuthDriver.Protobuf:
                    return new ProtobufAuthTransformer(Database, OldDriver).Invoke();

                case AuthDriver.Json:
                    return new JsonAuthTransformer(Database, OldDriver).Invoke();
            }

            return Promise.Resolve();
        }
    }

    /// <summary>
    /// Base auth conversion
    /// </summary>
    internal abstract class AuthConvertTransformer : IDataTransformer
    {
        /// <summary>
        /// Old auth driver
        /// </summary>
        protected AuthDriver OldDriver;

        protected Database.Provider Database;

        /// <summary>
        /// Create a new auth conversion object
        /// </summary>
        /// <param name="database"></param>
        /// <param name="oldDriver"></param>
        protected AuthConvertTransformer(Database.Provider database, AuthDriver oldDriver)
        {
            Database = database;
            OldDriver = oldDriver;
        }

        /// <summary>
        /// Invoke transformer
        /// </summary>
        /// <returns></returns>
        public abstract IPromise Invoke();

        /// <summary>
        /// Convert specified file of format to another format
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fromFormat"></param>
        /// <param name="toFormat"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        protected IPromise ConvertDataFile<T>(DataFormat fromFormat, DataFormat toFormat, string path) where T : class
        {
            IDataFile<T> sourceFile = DataSystem.MakeDataFile<T>(path, fromFormat);
            if (sourceFile.Exists)
            {
                return sourceFile.LoadFile()
                    .Then((IDataFile<T> file) =>
                    {
                        IDataFile<T> protoUserFile = sourceFile.Convert(toFormat);
                        protoUserFile.Save();
                        sourceFile.Delete();
                    });
            }

            return Promise.Reject<IDataFile<T>>(new FileNotFoundException(Interface.uMod.Strings.Exceptions.FileNotFound, path));
        }

        /// <summary>
        /// Load data file at specified path and of the specified format
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="format"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        protected IPromise<IDataFile<T>> LoadDataFile<T>(DataFormat format, string path) where T : class
        {
            IDataFile<T> file = DataSystem.MakeDataFile<T>(path, format);
            if (file.Exists)
            {
                return file.LoadFile();
            }

            return Promise.Reject<IDataFile<T>>(new FileNotFoundException(Interface.uMod.Strings.Exceptions.FileNotFound, path));
        }

        /// <summary>
        /// Populate player data from database connection
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        protected IPromise<Dictionary<string, UserData>> PopulateUsersFromConnection(Connection connection, LangData langData, ExpirationData expirationData)
        {
            Dictionary<string, UserData> userData = new Dictionary<string, UserData>();
            return connection.Query<List<Player>>("SELECT * FROM umod_players")
                .Then((List<Player> players) =>
                {
                    foreach (Player player in players)
                    {
                        userData.Add(player.Id, new UserData()
                        {
                            LastSeenNickname = player.Name,
                            Groups = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase),
                            Perms = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase),
                        });

                        if (player.Language != DefaultLang && (!langData?.UserData?.ContainsKey(player.Id) ?? false))
                        {
                            langData.UserData.Add(player.Id, player.Language);
                        }
                    }

                    return Promise.All(
                        PopulateUserGroupsFromConnection(connection, userData, expirationData),
                        PopulateUserPermissionsFromConnection(connection, userData, expirationData)
                    );
                })
                .Then(() => userData);
        }

        /// <summary>
        /// Populate user groups from database connection
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="userData"></param>
        /// <returns></returns>
        protected IPromise PopulateUserGroupsFromConnection(Connection connection, Dictionary<string, UserData> userData, ExpirationData expirationData)
        {
            return connection.Query<List<PlayerGroup>>("SELECT * FROM umod_player_groups")
                .Then((List<PlayerGroup> playerGroups) =>
                {
                    foreach (PlayerGroup playerGroup in playerGroups)
                    {
                        if (userData.ContainsKey(playerGroup.PlayerId))
                        {
                            userData[playerGroup.PlayerId].Groups.Add(playerGroup.GroupName);
                        }

                        if (playerGroup.ValidUntil != null && expirationData != null && !expirationData.PlayerGroups.ContainsKey($"{playerGroup.PlayerId}.{playerGroup.GroupName}"))
                        {
                            expirationData.PlayerGroups.Add($"{playerGroup.PlayerId}.{playerGroup.GroupName}", playerGroup.ValidUntil);
                        }
                    }
                });
        }

        /// <summary>
        /// Populate user permissions from database connection
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="userData"></param>
        /// <returns></returns>
        protected IPromise PopulateUserPermissionsFromConnection(Connection connection, Dictionary<string, UserData> userData, ExpirationData expirationData)
        {
            return connection.Query<List<PlayerPermission>>("SELECT * FROM umod_player_permissions")
                .Then(delegate (List<PlayerPermission> playerPermissions)
                {
                    foreach (PlayerPermission playerPermission in playerPermissions)
                    {
                        if (userData.ContainsKey(playerPermission.PlayerId))
                        {
                            userData[playerPermission.PlayerId].Perms.Add(playerPermission.Permission);
                        }

                        if (playerPermission.ValidUntil != null && expirationData != null && !expirationData.PlayerPermissions.ContainsKey($"{playerPermission.PlayerId}.{playerPermission.Permission}"))
                        {
                            expirationData.PlayerPermissions.Add($"{playerPermission.PlayerId}.{playerPermission.Permission}", playerPermission.ValidUntil);
                        }
                    }
                });
        }

        /// <summary>
        /// Populate groups from database connection
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        protected IPromise<Dictionary<string, GroupData>> PopulateGroupsFromConnection(Connection connection, ExpirationData expirationData)
        {
            Dictionary<string, GroupData> groupData = new Dictionary<string, GroupData>();

            return connection.Query<List<Group>>("SELECT * FROM umod_groups")
                .Then(delegate (List<Group> groups)
                {
                    foreach (Group group in groups)
                    {
                        groupData.Add(group.Name, new GroupData()
                        {
                            ParentGroup = group.ParentGroup,
                            Rank = group.Rank,
                            Title = group.Title,
                            Perms = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase)
                        });
                    }

                    return PopulateGroupPermissionsFromConnection(connection, groupData, expirationData);
                })
                .Then(() => groupData);
        }

        /// <summary>
        /// Populate group permissions from database connection
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="groupData"></param>
        /// <returns></returns>
        protected IPromise PopulateGroupPermissionsFromConnection(Connection connection, Dictionary<string, GroupData> groupData, ExpirationData expirationData)
        {
            return connection.Query<List<GroupPermission>>("SELECT * FROM umod_group_permissions")
                .Then((List<GroupPermission> groupPermissions) =>
                {
                    foreach (GroupPermission groupPermission in groupPermissions)
                    {
                        if (groupData.ContainsKey(groupPermission.GroupName))
                        {
                            groupData[groupPermission.GroupName].Perms.Add(groupPermission.Permission);
                        }

                        if (groupPermission.ValidUntil != null && expirationData != null && !expirationData.GroupPermissions.ContainsKey($"{groupPermission.GroupName}.{groupPermission.Permission}"))
                        {
                            expirationData.GroupPermissions.Add(
                                $"{groupPermission.GroupName}.{groupPermission.Permission}", groupPermission.ValidUntil);
                        }
                    }
                });
        }
    }

    /// <summary>
    /// Json auth transformer
    /// </summary>
    internal class JsonAuthTransformer : AuthConvertTransformer
    {
        /// <summary>
        /// Create new json auth transformer
        /// </summary>
        /// <param name="database"></param>
        /// <param name="oldDriver"></param>
        public JsonAuthTransformer(Database.Provider database, AuthDriver oldDriver) : base(database, oldDriver)
        {
        }

        /// <summary>
        /// Invoke transformer
        /// </summary>
        /// <returns></returns>
        public override IPromise Invoke()
        {
            switch (OldDriver)
            {
                case AuthDriver.Database:
                    return SeedFromSql();

                case AuthDriver.Protobuf:
                    return SeedFromProtobuf();
            }

            return null;
        }

        /// <summary>
        /// Seed json file from database connection
        /// </summary>
        /// <returns></returns>
        private IPromise SeedFromSql()
        {
            return Database.Client.Open()
                .Then((Connection connection) =>
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        LangData langData = new LangData();

                        return Promise.All(
                            PopulateGroupsFromConnection(connection, null)
                                .Then((Dictionary<string, GroupData> groupData) =>
                                {
                                    JsonFile<Dictionary<string, GroupData>> groupFile = new JsonFile<Dictionary<string, GroupData>>(Path.Combine(Interface.uMod.DataDirectory, "umod.groups"), groupData);
                                    groupFile.Save();
                                }),
                            PopulateUsersFromConnection(connection, langData, null)
                                .Then((Dictionary<string, UserData> userData) =>
                                {
                                    JsonFile<Dictionary<string, UserData>> userFile = new JsonFile<Dictionary<string, UserData>>(Path.Combine(Interface.uMod.DataDirectory, "umod.users"), userData);
                                    userFile.Save();
                                    if (Interface.uMod.Auth.Manager is FileAuthManager)
                                    {
                                        FileAuthManager.LangData = langData;
                                    }
                                })
                        );
                    }
                    else
                    {
                        throw new ConnectionException(Interface.uMod.Strings.Database.ImportFailure.Interpolate(("connection", Database.Configuration.Default)));
                    }
                });
        }

        /// <summary>
        /// Seed json file from protobuf file
        /// </summary>
        /// <returns></returns>
        private IPromise SeedFromProtobuf()
        {
            return Promise.All(
                ConvertDataFile<Dictionary<string, GroupData>>(DataFormat.Proto, DataFormat.Json, Path.Combine(Interface.uMod.DataDirectory, "umod.groups")),
                ConvertDataFile<Dictionary<string, UserData>>(DataFormat.Proto, DataFormat.Json, Path.Combine(Interface.uMod.DataDirectory, "umod.users"))
            );
        }
    }

    /// <summary>
    /// Database auth seeder
    /// </summary>
    internal class SqlAuthTransformer : AuthConvertTransformer
    {
        private readonly string OldConnectionName;

        /// <summary>
        /// Create a new database auth seeder
        /// </summary>
        /// <param name="database"></param>
        /// <param name="oldDriver"></param>
        public SqlAuthTransformer(Database.Provider database, AuthDriver oldDriver, string connectionName = null) : base(database, oldDriver)
        {
            OldConnectionName = connectionName;
        }

        /// <summary>
        /// Invoke transformer
        /// </summary>
        /// <returns></returns>
        public override IPromise Invoke()
        {
            return Database.Client.Open()
                .Then((Connection connection) => connection.Migrate<AuthMigration>(MigrationDirection.Up))
                .Then(() =>
                {
                    switch (OldDriver)
                    {
                        case AuthDriver.Json:
                            return SeedFromJson();

                        case AuthDriver.Protobuf:
                            return SeedFromProtobuf();

                        case AuthDriver.Database:
                            return SeedFromSql(OldConnectionName);

                        default:
                            throw new Exception("Unknown driver");
                    }
                });
        }

        private IPromise SeedFromSql(string connectionName = null)
        {
            LangData langData = new LangData();
            ExpirationData expirationData = new ExpirationData();

            return Database.Client.Open(connectionName)
                .Then((Connection connection) =>
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        return Promise.All<object>(
                            PopulateGroupsFromConnection(connection, expirationData),
                            PopulateUsersFromConnection(connection, langData, expirationData)
                        );
                    }

                    throw new ConnectionException(Interface.uMod.Strings.Database.ImportFailure.Interpolate(("connection", !string.IsNullOrEmpty(connectionName) ? connectionName : Database.Configuration.Default)));
                })
                .Then((data) =>
                {
                    object[] dataArray = data.ToArray();
                    Dictionary<string, GroupData> groupData = (Dictionary<string, GroupData>)dataArray[0];
                    Dictionary<string, UserData> userData = (Dictionary<string, UserData>)dataArray[1];

                    return SeedFromData(userData, groupData, langData, expirationData);
                });
        }

        /// <summary>
        /// Seed database from json file
        /// </summary>
        private IPromise SeedFromJson()
        {
            return LoadDataFile<LangData>(DataFormat.Json, Path.Combine(Interface.uMod.DataDirectory, "umod.lang"))
                .Then(
                    file => SeedGroupsFrom(DataFormat.Json, file.Object),
                    exception => SeedGroupsFrom(DataFormat.Json)
                );
        }

        /// <summary>
        /// Seed database from protobuf file
        /// </summary>
        private IPromise SeedFromProtobuf()
        {
            return LoadDataFile<LangData>(DataFormat.Proto, Path.Combine(Interface.uMod.DataDirectory, "umod.lang"))
                .Then(
                    file => SeedGroupsFrom(DataFormat.Proto, file.Object),
                    exception => SeedGroupsFrom(DataFormat.Proto)
                );
        }

        /// <summary>
        /// Seed groups from specified format to database
        /// </summary>
        /// <param name="format"></param>
        /// <param name="langData"></param>
        private IPromise SeedGroupsFrom(DataFormat format, LangData langData = null)
        {
            return LoadDataFile<Dictionary<string, GroupData>>(format, Path.Combine(Interface.uMod.DataDirectory, "umod.groups"))
                .Then(
                    file => SeedUsersFrom(format, file.Object, langData),
                    exception => SeedUsersFrom(format, null, langData)
                );
        }

        /// <summary>
        /// Seed users from specified format to database
        /// </summary>
        /// <param name="format"></param>
        /// <param name="groupData"></param>
        /// <param name="langData"></param>
        private IPromise SeedUsersFrom(DataFormat format, Dictionary<string, GroupData> groupData = null, LangData langData = null)
        {
            return LoadDataFile<Dictionary<string, UserData>>(format, Path.Combine(Interface.uMod.DataDirectory, "umod.users"))
                .Then(file => SeedFromData(file.Object, groupData, langData));
        }

        /// <summary>
        /// Seed data to database
        /// </summary>
        /// <param name="userData"></param>
        /// <param name="groupData"></param>
        /// <param name="langData"></param>
        private IPromise SeedFromData(Dictionary<string, UserData> userData, Dictionary<string, GroupData> groupData = null, LangData langData = null, ExpirationData expirationData = null)
        {
            if (langData == null)
            {
                langData = new LangData();
            }

            if (expirationData == null)
            {
                expirationData = new ExpirationData();
            }

            if (groupData == null)
            {
                groupData = new Dictionary<string, GroupData>();
            }

            return Database.Client.Open()
                .Then((Connection connection) =>
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        List<Group> groups = new List<Group>();
                        List<GroupPermission> groupPermissions = new List<GroupPermission>();
                        List<Player> players = new List<Player>();
                        List<PlayerGroup> playerGroups = new List<PlayerGroup>();
                        List<PlayerPermission> playerPermissions = new List<PlayerPermission>();

                        foreach (KeyValuePair<string, GroupData> kvp in groupData)
                        {
                            groups.Add(new Group()
                            {
                                Name = kvp.Key,
                                Rank = kvp.Value.Rank,
                                Title = kvp.Value.Title,
                                ParentGroup = kvp.Value.ParentGroup
                            });

                            foreach (string permission in kvp.Value.Perms)
                            {
                                DateTime? validUntil = null;
                                expirationData?.GroupPermissions.TryGetValue($"{kvp.Key}.{permission}", out validUntil);
                                groupPermissions.Add(new GroupPermission()
                                {
                                    GroupName = kvp.Key,
                                    Permission = permission,
                                    ValidUntil = validUntil
                                });
                            }
                        }

                        foreach (KeyValuePair<string, UserData> kvp in userData)
                        {
                            langData.UserData.TryGetValue(kvp.Key, out string playerLanguage);
                            players.Add(new Player()
                            {
                                Id = kvp.Key,
                                Name = kvp.Value.LastSeenNickname,
                                Language = playerLanguage ?? DefaultLang
                            });

                            foreach (string group in kvp.Value.Groups)
                            {
                                DateTime? validUntil = null;
                                expirationData?.PlayerGroups.TryGetValue($"{kvp.Key}.{group}", out validUntil);
                                playerGroups.Add(new PlayerGroup()
                                {
                                    PlayerId = kvp.Key,
                                    GroupName = group,
                                    ValidUntil = validUntil
                                });
                            }

                            foreach (string permission in kvp.Value.Perms)
                            {
                                DateTime? validUntil = null;
                                expirationData?.PlayerPermissions.TryGetValue($"{kvp.Key}.{permission}", out validUntil);
                                playerPermissions.Add(new PlayerPermission()
                                {
                                    PlayerId = kvp.Key,
                                    Permission = permission,
                                    ValidUntil = validUntil
                                });
                            }
                        }

                        Transaction transaction = connection.Transaction();
                        if (players.Count > 0)
                        {
                            transaction.Execute(connection.Info.Type == ConnectionType.MySQL
                                ? "INSERT INTO umod_players (Id, Name, Language) VALUES (@Id, @Name, @Language) ON DUPLICATE KEY UPDATE Name=VALUES(Name), Language=VALUES(Language);"
                                : "INSERT OR IGNORE INTO umod_players (Id, Name, Language) VALUES (@Id, @Name, @Language);", players.ToArray());
                        }
                        if (groups.Count > 0)
                        {
                            transaction.Execute(connection.Info.Type == ConnectionType.MySQL
                                ? "INSERT INTO umod_groups (Name, Title, Rank, ParentGroup) VALUES (@Name, @Title, @Rank, @ParentGroup) ON DUPLICATE KEY UPDATE Title=VALUES(Title), Rank=VALUES(Rank), ParentGroup=VALUES(ParentGroup);"
                                : "INSERT OR IGNORE INTO umod_groups (Name, Title, Rank, ParentGroup) VALUES (@Name, @Title, @Rank, @ParentGroup);", groups.ToArray());
                        }
                        if (groupPermissions.Count > 0)
                        {
                            transaction.Execute("INSERT INTO umod_group_permissions (GroupName, Permission) VALUES (@GroupName, @Permission);", groupPermissions.Select(x => x.ValidUntil == null).ToArray());
                            transaction.Execute("INSERT INTO umod_group_permissions (GroupName, Permission, ValidUntil) VALUES (@GroupName, @Permission, @ValidUntil);", groupPermissions.Select(x => x.ValidUntil != null).ToArray());
                        }
                        if (playerGroups.Count > 0)
                        {
                            transaction.Execute("INSERT INTO umod_player_groups (PlayerId, GroupName) VALUES (@PlayerId, @GroupName);", playerGroups.Select(x => x.ValidUntil == null).ToArray());
                            transaction.Execute("INSERT INTO umod_player_groups (PlayerId, GroupName, ValidUntil) VALUES (@PlayerId, @GroupName, @ValidUntil);", playerGroups.Select(x => x.ValidUntil != null).ToArray());
                        }
                        if (playerPermissions.Count > 0)
                        {
                            transaction.Execute("INSERT INTO umod_player_permissions (PlayerId, Permission) VALUES (@PlayerId, @Permission);", playerPermissions.Select(x => x.ValidUntil == null).ToArray());
                            transaction.Execute("INSERT INTO umod_player_permissions (PlayerId, Permission, ValidUntil) VALUES (@PlayerId, @Permission, @ValidUntil);", playerPermissions.Select(x => x.ValidUntil != null).ToArray());
                        }
                        return transaction.Commit();
                    }

                    throw new ConnectionException(Interface.uMod.Strings.Database.ImportFailure.Interpolate(("connection", Database.Configuration.Default)));
                });
        }
    }

    /// <summary>
    /// Protobuf auth transformer
    /// </summary>
    internal class ProtobufAuthTransformer : AuthConvertTransformer
    {
        /// <summary>
        /// Create protobuf auth transformer
        /// </summary>
        /// <param name="database"></param>
        /// <param name="oldDriver"></param>
        public ProtobufAuthTransformer(Database.Provider database, AuthDriver oldDriver) : base(database, oldDriver)
        {
        }

        /// <summary>
        /// Invoke transformer
        /// </summary>
        /// <returns></returns>
        public override IPromise Invoke()
        {
            switch (OldDriver)
            {
                case AuthDriver.Json:
                    return SeedFromJson();

                case AuthDriver.Database:
                    return SeedFromSql();
            }

            return null;
        }

        /// <summary>
        /// Seed protofuf file from json file
        /// </summary>
        /// <returns></returns>
        private IPromise SeedFromJson()
        {
            return Promise.All(
                ConvertDataFile<Dictionary<string, GroupData>>(DataFormat.Json, DataFormat.Proto, Path.Combine(Interface.uMod.DataDirectory, "umod.groups")),
                ConvertDataFile<Dictionary<string, UserData>>(DataFormat.Json, DataFormat.Proto, Path.Combine(Interface.uMod.DataDirectory, "umod.users"))
            );
        }

        /// <summary>
        /// Seed protobuf from database
        /// </summary>
        /// <returns></returns>
        private IPromise SeedFromSql()
        {
            return Database.Client.Open()
                .Then(delegate (Connection connection)
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        LangData langData = new LangData();

                        return Promise.All<object>(
                            PopulateGroupsFromConnection(connection, null)
                                .Then(delegate (Dictionary<string, GroupData> groupData)
                                {
                                    ProtoFile<Dictionary<string, GroupData>> groupFile = new ProtoFile<Dictionary<string, GroupData>>(Path.Combine(Interface.uMod.DataDirectory, "umod.groups"), groupData);
                                    groupFile.Save();
                                }),
                            PopulateUsersFromConnection(connection, langData, null)
                                .Then(delegate (Dictionary<string, UserData> userData)
                                {
                                    ProtoFile<Dictionary<string, UserData>> userFile = new ProtoFile<Dictionary<string, UserData>>(Path.Combine(Interface.uMod.DataDirectory, "umod.users"), userData);
                                    userFile.Save();
                                    if (Interface.uMod.Auth.Manager is FileAuthManager)
                                    {
                                        FileAuthManager.LangData = langData;
                                    }
                                })
                        );
                    }

                    throw new ConnectionException(Interface.uMod.Strings.Database.ImportFailure.Interpolate(("connection", Database.Configuration.Default)));
                });
        }
    }
}
