using System;
using System.Collections.Generic;
using System.Linq;
using uMod.Auth.Managers;
using uMod.Common;
using uMod.Configuration;
using uMod.Pooling;
using uMod.Utilities;

namespace uMod.Auth
{
    public class PlayerManager<T> : PlayerManager where T : class, IPlayer
    {
        #region Initialization

        private readonly IDictionary<string, T> _allPlayers;
        private readonly IDictionary<string, T> _connectedPlayers;

        /// <summary>
        /// Creates a new PlayerManager object
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        public PlayerManager(IApplication application, ILogger logger) : base(application, logger, new ActivatorFactory(typeof(T)), typeof(T))
        {
            _allPlayers = new Dictionary<string, T>();
            _connectedPlayers = new Dictionary<string, T>();
        }

        internal override void LoadPlayers(IAuthManager authManager)
        {
            object[] constructorArgs = ArrayPool.Get(2);
            try
            {
                foreach (KeyValuePair<string, string> kvp in authManager.GetUsers())
                {
                    if (_allPlayers.ContainsKey(kvp.Key))
                    {
                        continue;
                    }

                    if (authManager.Validate != null && !((bool)authManager.Validate?.Invoke(kvp.Key)))
                    {
                        continue;
                    }

                    constructorArgs[0] = kvp.Key;
                    constructorArgs[1] = kvp.Value;
                    T player = PlayerFactory.Make<T>(typeof(T), constructorArgs);
                    _allPlayers.Add(kvp.Key, player);
                }
            }
            finally
            {
                ArrayPool.Free(constructorArgs);
            }
        }

        #endregion Initialization

        #region Player Finding

        /// <summary>
        /// Gets all players
        /// </summary>
#if NET35
        public override IEnumerable<IPlayer> All => _allPlayers.Values.Cast<IPlayer>();
#else
        public override IEnumerable<IPlayer> All => _allPlayers.Values;
#endif

        /// <summary>
        /// Gets all connected players
        /// </summary>
#if NET35
        public override IEnumerable<IPlayer> Connected => _connectedPlayers.Values.Cast<IPlayer>();
#else
        public override IEnumerable<IPlayer> Connected => _connectedPlayers.Values;
#endif

        /// <summary>
        /// Gets all sleeping players
        /// </summary>

#if NET35
        public override IEnumerable<IPlayer> Sleeping => _allPlayers.Where(x => x.Value.IsSleeping).Select(x => x.Value).Cast<IPlayer>();
#else
        public override IEnumerable<IPlayer> Sleeping => _allPlayers.Where(x => x.Value.IsSleeping).Select(x => x.Value);
#endif

        /// <summary>
        /// Determine if specified string is a matching search key for the specified player
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        protected virtual bool IsPlayerKeyMatch(string partialNameOrIdOrIp, T player, int playerFilter = 0)
        {
            if ((playerFilter & (int)PlayerFilter.Id) != 0 && player.Id == partialNameOrIdOrIp)
            {
                return true;
            }

            if ((playerFilter & (int)PlayerFilter.Name) != 0 &&
                !string.IsNullOrEmpty(player.Name) &&
                player.Name.IndexOf(partialNameOrIdOrIp, StringComparison.OrdinalIgnoreCase) >= 0)
            {
                return true;
            }

            if ((playerFilter & (int)PlayerFilter.Address) != 0 &&
                !string.IsNullOrEmpty(player.Address) &&
                player.Address.Equals(partialNameOrIdOrIp, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Finds a single player given unique ID
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public override IPlayer FindPlayerById(string playerId)
        {
            return _allPlayers.TryGetValue(playerId, out T player) ? player : null;
        }

        /// <summary>
        /// Finds a single connected player given game object
        /// </summary>
        /// <param name="playerObj"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        public override IPlayer FindPlayerByObj(object playerObj, int playerFilter = 1)
        {
            IEnumerable<T> players = Filter((playerFilter & (int)PlayerFilter.Connected) != 0 ? _connectedPlayers.Values : _allPlayers.Values, playerFilter);
            return players.FirstOrDefault(p => p.Object == playerObj);
        }

        /// <summary>
        /// Finds any number of players given a partial name, unique ID, or IP address (case-insensitive, partial names accepted)
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        public override IEnumerable<IPlayer> FindPlayers(string partialNameOrIdOrIp, int playerFilter = 0)
        {
            playerFilter = ValidatePlayerFilter(playerFilter);
            // Try by ID first to prevent unnecessary loops
            if ((playerFilter & (int)PlayerFilter.Id) != 0)
            {
                IPlayer idMatch = FindPlayerById(partialNameOrIdOrIp);
                if (idMatch != null)
                {
                    yield return idMatch;
                    yield break;
                }
            }

            IEnumerable<T> players = (playerFilter & (int)PlayerFilter.Connected) != 0 ? _connectedPlayers.Values : _allPlayers.Values;

            if (ShouldFilter(playerFilter))
            {
                players = Filter(players, playerFilter);
            }

            foreach (T player in players)
            {
                if (IsPlayerKeyMatch(partialNameOrIdOrIp, player, playerFilter))
                {
                    yield return player;
                }
            }
        }

        public override IEnumerable<IPlayer> FindPlayers(int playerFilter = 0)
        {
            IEnumerable<T> players = (playerFilter & (int)PlayerFilter.Connected) != 0 ? _connectedPlayers.Values : _allPlayers.Values;

            if (ShouldFilter(playerFilter))
            {
                players = Filter(players, playerFilter);
            }

            foreach (T player in players)
            {
                yield return player;
            }
        }

        #endregion Player Finding

        #region Player Creation

        /// <summary>
        /// Creates or updates implied IPlayer instance when a player joins
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="playerName"></param>
        public override void PlayerJoin(string playerId, string playerName)
        {
            if (_allPlayers.TryGetValue(playerId, out T player))
            {
                player.Name = playerName;
            }
            else
            {
                object[] constructorArgs = ArrayPool.Get(2);
                try
                {
                    constructorArgs[0] = playerId;
                    constructorArgs[1] = playerName;

                    player = PlayerFactory.Make<T>(typeof(T), constructorArgs);
                    _allPlayers.Add(playerId, player);
                }
                catch (Exception exception)
                {
                    Logger.Report("Failed to create player", exception);
                }
                finally
                {
                    ArrayPool.Free(constructorArgs);
                }
            }

            Interface.uMod.Auth.Manager.UserReconnect(playerId, playerName, out _);
            AutomaticGrouping(player);
        }

        /// <summary>
        /// Creates a new bound IPlayer instance and sets player as connected
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="gamePlayer"></param>
        public override void PlayerConnected<TValue>(string playerId, TValue gamePlayer)
        {
            if (_allPlayers.TryGetValue(playerId, out T player))
            {
                player.Reconnect(gamePlayer);
            }
            else
            {
                object[] constructorArgs = ArrayPool.Get(1);
                try
                {
                    constructorArgs[0] = gamePlayer;
                    player = PlayerFactory.Make<T>(typeof(T), constructorArgs);
                }
                finally
                {
                    ArrayPool.Free(constructorArgs);
                }
            }

            if (player != null)
            {
                Interface.uMod.Auth.Manager.UserReconnect(playerId, player.Name, out _);
                if (_allPlayers.ContainsKey(playerId))
                {
                    _allPlayers[playerId] = player;
                }
                else
                {
                    _allPlayers.Add(playerId, player);
                }

                if (_connectedPlayers.ContainsKey(playerId))
                {
                    _connectedPlayers[playerId] = player;
                }
                else
                {
                    _connectedPlayers.Add(playerId, player);
                }
            }
            else
            {
                Logger.Error(Interface.uMod.Strings.Player.CreateFailure);
            }
        }

        /// <summary>
        /// Removes player from connected player list on disconnection
        /// </summary>
        /// <param name="playerId"></param>
        public override void PlayerDisconnected(string playerId)
        {
            _connectedPlayers.Remove(playerId);
        }

        #endregion Player Creation
    }

    /// <summary>
    /// Represents a generic player manager
    /// </summary>
    public class PlayerManager : IPlayerManager
    {
        #region Initialization

        protected readonly IFactory PlayerFactory;
        private readonly Type _impliedPlayerType;
        private readonly IDictionary<string, IPlayer> _allPlayers;
        private readonly IDictionary<string, IPlayer> _connectedPlayers;
        protected readonly ILogger Logger;

        /// <summary>
        /// Creates a new PlayerManager object
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        public PlayerManager(IApplication application, ILogger logger)
        {
            Logger = logger;
            _allPlayers = new Dictionary<string, IPlayer>();
            _connectedPlayers = new Dictionary<string, IPlayer>();
            PlayerFactory = new ActivatorFactory<IPlayer>();

            if (!application.TryGetType("IPlayer", out _impliedPlayerType))
            {
                Logger?.Warning(Interface.uMod.Strings.Player.MissingPlayerType);
            }
        }

        protected PlayerManager(IApplication application, ILogger logger, IFactory factory, Type impliedPlayerType)
        {
            Logger = logger;
            PlayerFactory = factory ?? new ActivatorFactory(typeof(IPlayer));
            _impliedPlayerType = impliedPlayerType;
        }

        internal virtual void LoadPlayers(IAuthManager authManager)
        {
            object[] constructorArgs = ArrayPool.Get(2);
            try
            {
                foreach (KeyValuePair<string, string> kvp in authManager.GetUsers())
                {
                    if (_allPlayers.ContainsKey(kvp.Key))
                    {
                        continue;
                    }

                    if (authManager.Validate != null && !(bool)authManager.Validate?.Invoke(kvp.Key))
                    {
                        continue;
                    }

                    constructorArgs[0] = kvp.Key;
                    constructorArgs[1] = kvp.Value;
                    IPlayer player = PlayerFactory.Make<IPlayer>(_impliedPlayerType, constructorArgs);
                    _allPlayers.Add(kvp.Key, player);
                }
            }
            finally
            {
                ArrayPool.Free(constructorArgs);
            }
        }

        #endregion Initialization

        #region Player Finding

        /// <summary>
        /// Gets all players
        /// </summary>
        public virtual IEnumerable<IPlayer> All => _allPlayers.Values;

        /// <summary>
        /// Gets all connected players
        /// </summary>
        public virtual IEnumerable<IPlayer> Connected => _connectedPlayers.Values;

        /// <summary>
        /// Gets all sleeping players
        /// </summary>
        public virtual IEnumerable<IPlayer> Sleeping => _allPlayers.Where(x => x.Value.IsSleeping).Select(x => x.Value);

        /// <summary>
        /// Enable default key filters
        /// </summary>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        protected int ValidatePlayerFilter(int playerFilter)
        {
            // If no id, name, or address filter specified then default to id and name
            if ((playerFilter & (int)PlayerFilter.Id) == 0 &&
               (playerFilter & (int)PlayerFilter.Name) == 0 &&
               (playerFilter & (int)PlayerFilter.Address) == 0)
            {
                playerFilter |= (int)PlayerFilter.Id;
                playerFilter |= (int)PlayerFilter.Name;
            }

            return playerFilter;
        }

        /// <summary>
        /// Filter player enumerable with specified filter
        /// </summary>
        /// <param name="players"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        protected IEnumerable<T> Filter<T>(IEnumerable<T> players, int playerFilter = 0) where T : IPlayer
        {
            // Skip filtering if default filters are provided
            if (playerFilter != 0 && playerFilter != (int)PlayerFilter.Connected)
            {
                foreach (T player in players)
                {
                    if ((playerFilter & (int)PlayerFilter.Admin) != 0 && !player.IsAdmin)
                    {
                        continue;
                    }

                    if ((playerFilter & (int)PlayerFilter.Alive) != 0 && !player.IsAlive)
                    {
                        continue;
                    }

                    if ((playerFilter & (int)PlayerFilter.Banned) != 0 && !player.IsBanned)
                    {
                        continue;
                    }

                    if ((playerFilter & (int)PlayerFilter.Dead) != 0 && !player.IsDead)
                    {
                        continue;
                    }

                    if ((playerFilter & (int)PlayerFilter.Moderator) != 0 && !player.IsModerator)
                    {
                        continue;
                    }

                    if ((playerFilter & (int)PlayerFilter.Sleeping) != 0 && !player.IsSleeping)
                    {
                        continue;
                    }

                    yield return player;
                }
            }
            else
            {
                foreach (T player in players)
                {
                    yield return player;
                }
            }
        }

        /// <summary>
        /// Determine if specified string is a matching search key for the specified player
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        protected virtual bool IsPlayerKeyMatch(string partialNameOrIdOrIp, IPlayer player, int playerFilter = 0)
        {
            if ((playerFilter & (int)PlayerFilter.Id) != 0 && player.Id == partialNameOrIdOrIp)
            {
                return true;
            }

            if ((playerFilter & (int)PlayerFilter.Name) != 0 &&
                !string.IsNullOrEmpty(player.Name) &&
                player.Name.IndexOf(partialNameOrIdOrIp, StringComparison.OrdinalIgnoreCase) >= 0)
            {
                return true;
            }

            if ((playerFilter & (int)PlayerFilter.Address) != 0 &&
                !string.IsNullOrEmpty(player.Address) &&
                player.Address.Equals(partialNameOrIdOrIp, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Finds a single player given unique ID
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public virtual IPlayer FindPlayerById(string playerId)
        {
            return _allPlayers.TryGetValue(playerId, out IPlayer player) ? player : null;
        }

        /// <summary>
        /// Finds a single connected player given game object
        /// </summary>
        /// <param name="playerObj"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        public virtual IPlayer FindPlayerByObj(object playerObj, int playerFilter = 1)
        {
            IEnumerable<IPlayer> players = Filter((playerFilter & (int)PlayerFilter.Connected) != 0 ? _connectedPlayers.Values : _allPlayers.Values, playerFilter);
            return players.FirstOrDefault(p => p.Object == playerObj);
        }

        /// <summary>
        /// Finds a single connected player given game object
        /// </summary>
        /// <param name="playerObj"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        public virtual IPlayer FindPlayerByObj(object playerObj, PlayerFilter playerFilter)
        {
            return FindPlayerByObj(playerObj, (int)playerFilter);
        }

        /// <summary>
        /// Finds a single player given a partial name, unique ID, or IP address (case-insensitive, partial names accepted, multiple matches returns null)
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        public virtual IPlayer FindPlayer(string partialNameOrIdOrIp, PlayerFilter playerFilter)
        {
            return FindPlayer(partialNameOrIdOrIp, (int)playerFilter);
        }

        /// <summary>
        /// Finds a single player given a partial name, unique ID, or IP address (case-insensitive, partial names accepted, multiple matches returns null)
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        public virtual IPlayer FindPlayer(string partialNameOrIdOrIp, int playerFilter = 0)
        {
            playerFilter = ValidatePlayerFilter(playerFilter);
            // Try by ID first to prevent unnecessary loops
            if ((playerFilter & (int)PlayerFilter.Id) != 0)
            {
                IPlayer idMatch = FindPlayerById(partialNameOrIdOrIp);
                if (idMatch != null)
                {
                    return idMatch;
                }
            }

            IPlayer[] players = FindPlayers(partialNameOrIdOrIp, playerFilter).ToArray();
            return players.Length == 1 ? players[0] : null;
        }

        /// <summary>
        /// Optimization to determine if additional filtering is necessary
        /// </summary>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        protected virtual bool ShouldFilter(int playerFilter)
        {
            return (playerFilter & (int)PlayerFilter.Admin) != 0 ||
                   (playerFilter & (int)PlayerFilter.Moderator) != 0 ||
                   (playerFilter & (int)PlayerFilter.Banned) != 0 ||
                   (playerFilter & (int)PlayerFilter.Alive) != 0 ||
                   (playerFilter & (int)PlayerFilter.Dead) != 0 ||
                   (playerFilter & (int)PlayerFilter.Sleeping) != 0;
        }

        /// <summary>
        /// Finds any number of players given a partial name, unique ID, or IP address (case-insensitive, partial names accepted)
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        public virtual IEnumerable<IPlayer> FindPlayers(string partialNameOrIdOrIp, int playerFilter = 0)
        {
            playerFilter = ValidatePlayerFilter(playerFilter);
            // Try by ID first to prevent unnecessary loops
            if ((playerFilter & (int)PlayerFilter.Id) != 0)
            {
                IPlayer idMatch = FindPlayerById(partialNameOrIdOrIp);
                if (idMatch != null)
                {
                    yield return idMatch;
                    yield break;
                }
            }

            IEnumerable<IPlayer> players = (playerFilter & (int)PlayerFilter.Connected) != 0 ? _connectedPlayers.Values : _allPlayers.Values;

            if (ShouldFilter(playerFilter))
            {
                players = Filter(players, playerFilter);
            }

            foreach (IPlayer player in players)
            {
                if (IsPlayerKeyMatch(partialNameOrIdOrIp, player, playerFilter))
                {
                    yield return player;
                }
            }
        }

        /// <summary>
        /// Finds any number of players given a partial name, unique ID, or IP address (case-insensitive, partial names accepted)
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        public virtual IEnumerable<IPlayer> FindPlayers(string partialNameOrIdOrIp, PlayerFilter playerFilter)
        {
            return FindPlayers(partialNameOrIdOrIp, (int)playerFilter);
        }

        public virtual IEnumerable<IPlayer> FindPlayers(int playerFilter = 0)
        {
            IEnumerable<IPlayer> players = (playerFilter & (int)PlayerFilter.Connected) != 0 ? _connectedPlayers.Values : _allPlayers.Values;

            if (ShouldFilter(playerFilter))
            {
                players = Filter(players, playerFilter);
            }

            foreach (IPlayer player in players)
            {
                yield return player;
            }
        }

        public virtual IEnumerable<IPlayer> FindPlayers(PlayerFilter playerFilter)
        {
            return FindPlayers((int)playerFilter);
        }

        #endregion Player Finding

        #region Player Creation

        /// <summary>
        /// Creates or updates implied IPlayer instance when a player joins
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="playerName"></param>
        public virtual void PlayerJoin(string playerId, string playerName)
        {
            if (_allPlayers.TryGetValue(playerId, out IPlayer player))
            {
                player.Name = playerName;
            }
            else
            {
                object[] constructorArgs = ArrayPool.Get(2);
                try
                {
                    constructorArgs[0] = playerId;
                    constructorArgs[1] = playerName;

                    player = PlayerFactory.Make<IPlayer>(_impliedPlayerType, constructorArgs);
                    _allPlayers.Add(playerId, player);
                }
                catch (Exception exception)
                {
                    Logger.Report(Interface.uMod.Strings.Player.CreateFailure, exception);
                }
                finally
                {
                    ArrayPool.Free(constructorArgs);
                }
            }

            Interface.uMod.Auth.Manager.UserReconnect(playerId, playerName, out _);
            AutomaticGrouping(player);
        }

        /// <summary>
        /// Automatically add player to default "player" group
        /// </summary>
        /// <param name="player"></param>
        protected void AutomaticGrouping(IPlayer player)
        {
            if (player == null || !Interface.uMod.Auth.Configuration.Groups.AutomaticGrouping)
            {
                return;
            }

            if (!player.BelongsToGroup(Interface.uMod.Auth.Configuration.Groups.Players))
            {
                player.AddToGroup(Interface.uMod.Auth.Configuration.Groups.Players);
            }
        }

        /// <summary>
        /// Creates a new bound IPlayer instance and sets player as connected
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="playerId"></param>
        /// <param name="gamePlayer"></param>
        public virtual void PlayerConnected<T>(string playerId, T gamePlayer)
        {
            if (_allPlayers.TryGetValue(playerId, out IPlayer player))
            {
                player.Reconnect(gamePlayer);
            }
            else
            {
                object[] constructorArgs = ArrayPool.Get(1);
                try
                {
                    constructorArgs[0] = gamePlayer;
                    player = PlayerFactory.Make<IPlayer>(_impliedPlayerType, constructorArgs);
                }
                finally
                {
                    ArrayPool.Free(constructorArgs);
                }
            }

            if (player != null)
            {
                Interface.uMod.Auth.Manager.UserReconnect(playerId, player.Name, out _);
                if (_allPlayers.ContainsKey(playerId))
                {
                    _allPlayers[playerId] = player;
                }
                else
                {
                    _allPlayers.Add(playerId, player);
                }

                if (_connectedPlayers.ContainsKey(playerId))
                {
                    _connectedPlayers[playerId] = player;
                }
                else
                {
                    _connectedPlayers.Add(playerId, player);
                }
            }
            else
            {
                Logger.Error(Interface.uMod.Strings.Player.CreateFailure);
            }
        }

        /// <summary>
        /// Removes player from connected player list on disconnection
        /// </summary>
        /// <param name="playerId"></param>
        public virtual void PlayerDisconnected(string playerId)
        {
            _connectedPlayers.Remove(playerId);
        }

        #endregion Player Creation

        #region Player Cleanup
        /// <summary>
        /// Purge inactive players
        /// </summary>
        internal void PlayerCleanup(Provider auth)
        {
            if (!auth.Configuration.Players.InactiveCleanup)
            {
                return;
            }

            if (auth.Configuration.Driver == AuthDriver.Database)
            {
                if (auth.Manager is DatabaseAuthManager dbAuthManager)
                {
                    dbAuthManager.PurgeInactivePlayers(auth.Configuration.Players.InactivePeriod)?.Done(
                        delegate()
                        {
                            Logger.Info(Interface.uMod.Strings.Database.PurgeSuccess.Interpolate(("period", auth.Configuration.Players.InactivePeriod)));
                        }, Logger.Report);
                }
            }
            else
            {
                Logger.Warning(Interface.uMod.Strings.Database.PurgeFailed.Interpolate(("driver", Enum.GetName(typeof(AuthDriver), auth.Configuration.Driver)?.ToLower())));
            }
        }
        #endregion
    }
}
