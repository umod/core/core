﻿using System;

namespace uMod.Command
{
    [Serializable]
    public class CommandAlreadyExistsException : CommandException
    {
        public CommandAlreadyExistsException()
        {
        }

        public CommandAlreadyExistsException(string cmd) : base($"Command {cmd} already exists")
        {
        }

        public CommandAlreadyExistsException(string message, Exception inner) : base(message, inner)
        {
        }

        protected CommandAlreadyExistsException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context)
        { }
    }

    [Serializable]
    public class CommandException : Exception
    {
        public CommandException()
        {
        }

        public CommandException(string message) : base(message)
        {
        }

        public CommandException(string message, Exception inner) : base(message, inner)
        {
        }

        protected CommandException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context)
        { }
    }
}
