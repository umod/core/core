﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using uMod.Common;
using uMod.Common.WebSockets;
using uMod.Configuration;
using uMod.Plugins;

namespace uMod.WebSockets
{
    public class Server
    {
        private readonly Apps.WebSockets _client;
        private readonly Plugin _context;

        /// <summary>
        /// Create a websocket server wrapper for the specified plugin
        /// </summary>
        /// <param name="context"></param>
        public Server(Plugin context = null)
        {
            _client = Interface.uMod.Web?.WebSockets?.Application;
            _context = context;
        }

        /// <summary>
        /// Start a websocket server with the specified connection info
        /// </summary>
        /// <param name="info"></param>
        public void Start(ConnectionInfo info)
        {
            var hookDecorator = new Plugins.Decorators.WebSocketsDecorator(_context, info.Name);
            _context.Dispatcher.AddHookDecorator(new HookDispatcher.HookDecorator(hookDecorator));
            hookDecorator.Subscribe();
            _client?.EnqueueServerStart(info);
        }

        /// <summary>
        /// Start a websocket server with a name that matches a connection defined in the global configuration
        /// </summary>
        /// <param name="name"></param>
        public void Start(string name)
        {
            if (Interface.uMod.Web.WebSockets.Configuration.Servers.TryGetValue(name, out IWebServer webServer) &&
                webServer is IWebSocketServer webSocketServer)
            {
                Start(webSocketServer.Connection);
            }
            else
            {
                throw new ArgumentException($"WebSocket server configuration \"{name}\" does not exist");
            }
        }

        /// <summary>
        /// Stop an existing websocket server started with the specified connection
        /// </summary>
        /// <param name="info"></param>
        public void Stop(ConnectionInfo info)
        {
            _client?.EnqueueServerClose(info.Name);
        }

        /// <summary>
        /// Stop an existing websocket server started with the specified name
        /// </summary>
        /// <param name="name"></param>
        public void Stop(string name)
        {
            _client?.EnqueueServerClose(name);
        }
    }
}
