﻿extern alias References;

using System;
using System.Runtime.Serialization;
using References::Newtonsoft.Json;
using uMod.Common;
using uMod.Common.WebSockets;
using uMod.Apps;

namespace uMod.WebSockets
{
    [Serializable]
    public class SocketConnection : IWebSocketConnection
    {
        [JsonProperty]
        public string Id { get; private set; }

        [JsonProperty]
        public string Address { get; private set; }

        [JsonProperty]
        public int Port { get; private set; }

        [JsonProperty]
        public string Path { get; private set; }

        [JsonProperty]
        public WebSocketConnectionState State { get; private set; }

        [JsonProperty]
        public string ConnectionName { get; private set; }

        private int _lastMessageIdentifier = 0;

        internal static Apps.WebSockets Application;

        public SocketConnection()
        {
            if (Application == null)
            {
                Application = Interface.uMod.Web?.WebSockets?.Application;
            }
        }

        public void Close(int code = 1000)
        {
            Application?.EnqueueSocketClose(this, code);
        }

        public void Send(string data)
        {
            Application?.EnqueuePacket(this, data);
        }

        public void Send(byte[] data)
        {
            Application?.EnqueuePacket(this, data);
        }

        public void Send(object data)
        {
            Send(JsonConvert.SerializeObject(data));
        }

        public void Send<T>(T data)
        {
            if (data is Packet)
            {
                Send(JsonConvert.SerializeObject(data));
            }
            else
            {
                PacketScopeAttribute packetAttribute = typeof(T).GetCustomAttribute<PacketScopeAttribute>();
                if (packetAttribute != null)
                {
                    if (_lastMessageIdentifier > 999)
                    {
                        _lastMessageIdentifier = 1;
                    }

                    Send(new Packet()
                    {
                        Identifier = (_lastMessageIdentifier++).ToString(),
                        Message = JsonConvert.SerializeObject(data),
                        Name = ConnectionName,
                        Type = packetAttribute.Type ?? typeof(T).Name
                    } as object);
                }
                else
                {
                    Send(JsonConvert.SerializeObject(data));
                }
            }
        }

        public void Send<T>(Packet packet, T packetData)
        {
            packet.Message = JsonConvert.SerializeObject(packetData);
            Send(packet);
        }
    }
}
