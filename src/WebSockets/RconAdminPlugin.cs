extern alias References;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using References::Newtonsoft.Json;
using uMod.Common;
using uMod.Common.WebSockets;
using uMod.Configuration;
using uMod.Plugins;

namespace uMod.WebSockets
{
    [Info("uMod Web Rcon", "uMod Team & Contributors", "1.0.0")]
    [Description("Provides a basic interface for rcon services")]
    public class RconAdminPlugin : Plugin
    {
        private DateTime StartTime = DateTime.Now;

        #region Server Packets

        [PacketScope("sessions")]
        public class SessionList
        {
            [JsonProperty("session_ids")]
            public string[] SessionIds;
        }

        [Hook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, SessionList connectionList)
        {
            Interface.uMod.Web.RegisterSessions(connectionList.SessionIds);
        }

        [PacketScope("command")]
        public class ServerCommand
        {
            [JsonProperty("command")]
            public string Command;
        }

        /// <summary>
        /// Server command requested
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="serverCmd"></param>
        [Hook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, ServerCommand serverCmd)
        {
            universal.Server.Command(serverCmd.Command);
        }

        [PacketScope("commands")]
        public class ServerCommands
        {
            [JsonProperty("commands")]
            public string[] Commands;
        }

        /// <summary>
        /// Server command requested
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="serverCmd"></param>
        [Hook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, ServerCommands serverCommands)
        {
            foreach (string cmd in serverCommands.Commands)
            {
                if(string.IsNullOrEmpty(cmd))
                {
                    continue;
                }
                universal.Server.Command(cmd);
            }
        }

        public enum ProcessSignalType
        {
            Shutdown = 0,
            Started = 1
        }

        [PacketScope("process.signal")]
        public class ProcessSignal : Packet
        {
            [JsonProperty("signal")]
            public ProcessSignalType SignalType { get; set; }
        }

        [Hook("OnServerInitialized")]
        public void OnServerInitialized()
        {
            BroadcastPacket(new ProcessSignal()
            {
                SignalType = ProcessSignalType.Started
            });
        }

        [Hook("OnServerShutdown")]
        public void OnServerShutdown()
        {
            BroadcastPacket(new ProcessSignal()
            {
                SignalType = ProcessSignalType.Shutdown
            });
        }

        [Hook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, PlayerChat serverChat)
        {
            if (!(serverChat is PlayerChatAdv))
            {
                universal.Server.Broadcast(serverChat.Message, serverChat.Username);
            }
        }

        [Hook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, PlayerChatAdv serverChat)
        {
            string prefix = null;
            if (!string.IsNullOrEmpty(serverChat.Username))
            {
                prefix = serverChat.Username;
            }

            ulong id = 0;

            if (!string.IsNullOrEmpty(serverChat.UserId))
            {
                ulong.TryParse(serverChat.UserId, out id);
            }

            if (string.IsNullOrEmpty(serverChat.TargetId))
            {
                if (id > 0)
                {
                    universal.Server.Broadcast(serverChat.Message, id);
                }
                else
                {
                    universal.Server.Broadcast(serverChat.Message, prefix);
                }
            }
            else
            {
                IPlayer player = Players.FindPlayerById(serverChat.TargetId);
                if (player != null)
                {
                    player.Message(serverChat.Message, prefix);
                }
            }
        }

        [PacketScope("stats")]
        public class ServerStats
        {
            [JsonProperty("ram")]
            public float RamUsage;

            [JsonProperty("framerate")]
            public int FrameRate;

            [JsonProperty("uptime")]
            public string Uptime;
        }

        /// <summary>
        /// Server stats requested
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="serverCmd"></param>
        [Hook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, ServerStats serverStats)
        {
            TimeSpan ts = (DateTime.Now - StartTime);
            Process p = Process.GetCurrentProcess();
            serverStats.RamUsage = p.WorkingSet64 / 1024 / 1024;
            serverStats.FrameRate = universal.Server.FrameRate;
            serverStats.Uptime = ts.ToString();
            connection.Send(serverStats);
        }

        [PacketScope("panels")]
        public class ServerPanels
        {
            [JsonProperty("panels")]
            public Dictionary<string, string> Panels;
        }

        /// <summary>
        /// Server panels requested
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="serverPanels"></param>
        [Hook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, ServerPanels serverPanels)
        {
            serverPanels.Panels = Interface.uMod.Web.GetPanels();
            connection.Send(serverPanels);
        }

        [PacketScope("groups")]
        public class GroupList
        {
            [JsonProperty("groups")]
            public string[] Groups;
        }

        /// <summary>
        /// Group list requested
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="groupList"></param>
        [SimpleHook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, GroupList groupList)
        {
            groupList.Groups = permission.GetGroups().OrderByDescending(x => permission.GetGroupRank(x)).ToArray();
            connection.Send(groupList);
        }

        [PacketScope("groups.delete")]
        public class GroupDelete : GroupList
        {
        }

        /// <summary>
        /// Group list requested
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="groupDelete"></param>
        [SimpleHook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, GroupDelete groupDelete)
        {
            foreach (string group in groupDelete.Groups)
            {
                permission.RemoveGroup(group);
            }
        }

        [PacketScope("groups.search")]
        public class GroupSearch : GroupList
        {
            [JsonProperty("search")]
            public string Search;
        }

        /// <summary>
        /// Group list requested
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="groupSearch"></param>
        [SimpleHook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, GroupSearch groupSearch, Packet packet)
        {
            List<string> groupResults = new List<string>();

            if (groupSearch.Search.IsSteamId())
            {
                string[] playerResults = permission.GetUserGroups(groupSearch.Search);
                if (playerResults != null && playerResults.Length > 0)
                {
                    groupResults.AddRange(playerResults);
                }
            }
            else
            {
                string[] groups = permission.GetGroups().Where(x => x.StartsWith(groupSearch.Search, StringComparison.InvariantCultureIgnoreCase)).ToArray();
                if (groups != null && groups.Length > 0)
                {
                    groupResults.AddRange(groups);
                }

                string[] permissionGroups = permission.GetPermissionGroups(groupSearch.Search);
                if (permissionGroups != null && permissionGroups.Length > 0)
                {
                    groupResults.AddRange(permissionGroups);
                }
            }

            if (groupResults.Count > 0)
            {
                groupSearch.Groups = groupResults?.ToArray();
            }

            connection.Send(packet, groupSearch);
        }

        [PacketScope("group.info")]
        public class GroupInfo
        {
            [JsonProperty("group")]
            public string GroupName;

            [JsonProperty("parent")]
            public string ParentGroup;

            [JsonProperty("rank")]
            public int Rank;

            [JsonProperty("title")]
            public string Title;

            [JsonProperty("permissions")]
            public string[] Permissions;
        }

        /// <summary>
        /// Group info requested
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="groupInfo"></param>
        [Hook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, GroupInfo groupInfo, Packet packet)
        {
            groupInfo.Permissions = permission.GetGroupPermissions(groupInfo.GroupName);

            groupInfo.ParentGroup = permission.GetGroupParent(groupInfo.GroupName);

            groupInfo.Rank = permission.GetGroupRank(groupInfo.GroupName);

            groupInfo.Title = permission.GetGroupTitle(groupInfo.GroupName);

            connection.Send(packet, groupInfo);
        }

        [PacketScope("group.edit")]
        public class GroupEdit : GroupInfo
        {
            [JsonProperty("newname")]
            public string NewName;
        }

        /// <summary>
        /// Group add
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="groupEdit"></param>
        [Hook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, GroupEdit groupEdit)
        {
            if (permission.GroupExists(groupEdit.GroupName))
            {
                string groupName = groupEdit.GroupName;
                if (!string.IsNullOrEmpty(groupEdit.NewName) && !groupEdit.NewName.Equals(groupEdit.GroupName, StringComparison.InvariantCultureIgnoreCase))
                {
                    permission.RenameGroup(groupEdit.GroupName, groupEdit.NewName);
                    groupName = groupEdit.NewName;
                }

                permission.SetGroupParent(groupName, groupEdit.ParentGroup);
                permission.SetGroupRank(groupName, groupEdit.Rank);
                permission.SetGroupTitle(groupName, groupEdit.Title);
            }
            else
            {
                permission.CreateGroup(groupEdit.GroupName, groupEdit.Title, groupEdit.Rank, groupEdit.ParentGroup);
            }
        }

        [PacketScope("group.users")]
        public class GroupUsers
        {
            [JsonProperty("group")]
            public string GroupName;

            [JsonProperty("users")]
            public Dictionary<string, string> Users;
        }

        /// <summary>
        /// Group info requested
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="groupInfo"></param>
        [Hook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, GroupUsers groupUsers)
        {
            string[] playerids = permission.GetUsersInGroup(groupUsers.GroupName);

            groupUsers.Users = new Dictionary<string, string>();
            foreach (string playerid in playerids)
            {
                IPlayer player = Players.FindPlayerById(playerid);
                if (player is IPlayer)
                {
                    groupUsers.Users.Add(player.Id, player.Name);
                }
            }

            connection.Send(groupUsers);
        }

        [PacketScope("permissions")]
        public class PermissionList
        {
            [JsonProperty("permissions")]
            public string[] Permissions;
        }

        /// <summary>
        /// Group list requested
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="permissionList"></param>
        [Hook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, PermissionList permissionList)
        {
            Logger.Debug("Sending permission list");
            permissionList.Permissions = permission.GetPermissions();
            connection.Send(permissionList);
        }

        [PacketScope("plugin.info")]
        public class PluginInfo
        {
            [JsonProperty("plugin")]
            public string PluginName;

            [JsonProperty("permissions")]
            public string[] Permissions;

            [JsonProperty("configuration_files")]
            public string[] ConfigurationFiles;

            [JsonProperty("localization_files")]
            public string[] LocalizationFiles;
        }

        /// <summary>
        /// Permission list requested
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="pluginInfo"></param>
        [Hook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, PluginInfo pluginInfo, Packet packet)
        {
            IPlugin plugin = Interface.uMod.Plugins.Get(pluginInfo.PluginName);
            if (plugin != null)
            {
                pluginInfo.Permissions = permission.GetPermissions(plugin);
                if (plugin is Plugin pluginImpl)
                {
                    pluginInfo.ConfigurationFiles = pluginImpl.Meta.GetConfigurationFiles()?.ToArray();
                    pluginInfo.LocalizationFiles = pluginImpl.Meta.GetLocalizationFiles()?.ToArray();
                }
            }

            connection.Send(packet, pluginInfo);
        }

        #endregion

        #region Player Packets

        [PacketScope("players")]
        public class PlayerList
        {
            [JsonProperty("players")]
            public Dictionary<string, string> Players;

            [JsonProperty("max")]
            public int MaxPlayers;
        }

        /// <summary>
        /// Player list requested
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="playerList"></param>
        [Hook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, PlayerList playerList)
        {
            playerList.Players = Players.Connected.ToDictionary(k => k.Id, v => v.Name);
            playerList.MaxPlayers = universal.Server.MaxPlayers;
            connection.Send(playerList);
        }

        [PacketScope("players/search")]
        public class PlayerSearch
        {
            [JsonProperty("players")]
            public Dictionary<string, string> Players;

            [JsonProperty("permissions")]
            public List<string> Permissions { get; set; }

            [JsonProperty("groups")]
            public List<string> Groups { get; set; }

            [JsonProperty("filter")]
            public int Filter = 0;

            [JsonProperty("search")]
            public string Search;
        }

        /// <summary>
        /// Player search requested
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="playerList"></param>
        [Hook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, PlayerSearch playerList, Packet packet)
        {
            IEnumerable<IPlayer> players = Players.FindPlayers(playerList.Search, playerList.Filter);
            Dictionary<string, string> selectedPlayers = new Dictionary<string, string>();
            if (playerList.Groups != null && playerList.Groups.Count > 0)
            {
                foreach (string group in playerList.Groups)
                {
                    foreach (IPlayer player in players)
                    {
                        if (player.BelongsToGroup(group))
                        {
                            selectedPlayers.Add(player.Id, player.Name);
                        }
                    }
                }
            }
            else if (playerList.Permissions != null && playerList.Permissions.Count > 0)
            {
                foreach (string permission in playerList.Permissions)
                {
                    foreach (IPlayer player in players)
                    {
                        if (player.HasPermission(permission))
                        {
                            selectedPlayers.Add(player.Id, player.Name);
                        }
                    }
                }
            }
            else
            {
                selectedPlayers = players.ToDictionary(k => k.Id, v => v.Name);
            }

            playerList.Players = selectedPlayers;
            connection.Send(packet, playerList);
        }

        public enum PlayerActionEnum
        {
            Kick = 0,
            Ban = 1
        }

        [PacketScope("player.apply")]
        public class PlayerAction
        {
            [JsonProperty("id")]
            public string Id;

            [JsonProperty("action")]
            public PlayerActionEnum Action;

            [JsonProperty("reason")]
            public string Reason;

            [JsonProperty("timespan")]
            public TimeSpan TimeSpan;
        }

        /// <summary>
        /// Player action requested
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="playerAction"></param>
        [Hook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, PlayerAction playerAction)
        {
            IPlayer player;
            switch (playerAction.Action)
            {
                case PlayerActionEnum.Kick:
                    player = Players.FindPlayerById(playerAction.Id);
                    if (player != null)
                    {
                        player.Kick(playerAction.Reason);
                    }
                    break;
                case PlayerActionEnum.Ban:
                    player = Players.FindPlayerById(playerAction.Id);
                    if (player != null)
                    {
                        player.Ban(playerAction.Reason, playerAction.TimeSpan);
                    }
                    break;
            }
        }

        [PacketScope("player.info")]
        public class PlayerInfo : Packet
        {
            [JsonProperty("userid")]
            public string UserId;

            [JsonProperty("username")]
            public string Username;

            [JsonProperty("address")]
            public string Address;

            [JsonProperty("language")]
            public string Language;

            [JsonProperty("ping")]
            public int Ping;

            [JsonProperty("isbanned")]
            public bool IsBanned;

            [JsonProperty("isconnected")]
            public bool IsConnected;

            [JsonProperty("position")]
            public Position Position;

            [JsonProperty("bantimeremaining")]
            public TimeSpan BanTimeRemaining;

            [JsonProperty("groups")]
            public string[] Groups;

            [JsonProperty("permissions")]
            public string[] Permissions;
        }

        [Hook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, PlayerInfo playerInfo, Packet packet)
        {
            var player = Players.FindPlayerById(playerInfo.UserId);
            if (player != null)
            {
                playerInfo.Username = player.Name;
                playerInfo.Address = player.Address;
                playerInfo.Language = player.Language.ThreeLetterISOLanguageName;
                playerInfo.Ping = player.Ping;
                playerInfo.IsBanned = player.IsBanned;
                playerInfo.BanTimeRemaining = player.BanTimeRemaining;
                playerInfo.IsConnected = player.IsConnected;
                playerInfo.Position = player.Position();
                playerInfo.Groups = permission.GetUserGroups(player.Id);
                playerInfo.Permissions = permission.GetUserPermissions(player.Id);

                connection.Send(packet, playerInfo);
            }
        }

        [PacketScope("positions")]
        public class PlayerPositionList
        {
            [JsonProperty("players")]
            public Dictionary<string, Position> Players;
        }

        /// <summary>
        /// Player positions requested
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="playerPositionList"></param>
        [Hook("OnSocketMessage")]
        public void OnSocketMessage(SocketConnection connection, PlayerPositionList playerPositionList)
        {
            playerPositionList.Players = Players.Connected.ToDictionary(k => k.Id, v =>
            {
                Position position = new Position();
                v.Position(out position.X, out position.Y, out position.Z);
                return position;
            });
            connection.Send(playerPositionList);
        }

        [PacketScope("chat")]
        public class PlayerChat : Packet
        {
            [JsonProperty("Username")]
            public string Username;
        }

        [PacketScope("chat.umod")]
        public class PlayerChatAdv : PlayerChat
        {
            [JsonProperty("UserId")]
            public string UserId;

            [JsonProperty("TargetId")]
            public string TargetId;

            [JsonProperty("Channel")]
            public string Channel;
        }

        [Hook("OnPlayerChat")]
        public void OnPlayerChat(IPlayer player, string message, string channel)
        {
            BroadcastPacket(new PlayerChatAdv()
            {
                Message = message,
                Username = player.Name,
                UserId = player.Id,
                Channel = channel
            });
        }

        [PacketScope("player.connected")]
        public class PlayerConnected : Packet
        {
            [JsonProperty("userid")]
            public string UserId;

            [JsonProperty("username")]
            public string Username;
        }

        [PacketScope("player.connected")]
        public class PlayerDisconnected : PlayerConnected
        {
        }

        [Hook("OnPlayerConnected")]
        private void OnPlayerConnected(IPlayer player)
        {
            BroadcastPacket(new PlayerConnected()
            {
                Username = player.Name,
                UserId = player.Id,
            });
        }

        [Hook("OnPlayerDisconnected")]
        private void OnPlayerDisconnected(IPlayer player)
        {
            BroadcastPacket(new PlayerDisconnected()
            {
                Username = player.Name,
                UserId = player.Id,
            });
        }

        #endregion

        private Apps.WebSockets _webSockets;

        [Hook("Loaded")]
        public void Loaded()
        {
            _webSockets = Interface.uMod.Web.WebSockets.Application;
            if (Interface.uMod.Web.Configuration.Servers.TryGetValue("rcon", out IWebServer webServer) && webServer is WebSocketServer webSocketServer)
            {
                if (webSocketServer.Connection != null)
                {
                    WebSocket.Start(webSocketServer.Connection);
                }
            }
        }

        [Hook("Unloaded")]
        public void Unloaded()
        {
            if (Interface.uMod.Web.Configuration.Servers.TryGetValue("rcon", out IWebServer webServer) && webServer is WebSocketServer webSocketServer)
            {
                if (webSocketServer.Connection != null)
                {
                    WebSocket.Stop("rcon");
                }
            }
        }

        private Timer _permissionSyncTimer;

        [Hook("OnPluginLoaded")]
        public void OnPluginLoaded(IPlugin plugin)
        {
            if(plugin == this) return;
            if(!Interface.uMod._plugins.IsInitialized)
                return;
            if(_permissionSyncTimer != null && !_permissionSyncTimer.Destroyed)
            {
                _permissionSyncTimer.Destroy();
            }

            _permissionSyncTimer = timer.Once(30f, SyncPermissions);
        }

        [Hook("OnPluginUnloaded")]
        public void OnPluginUnloaded(IPlugin plugin)
        {
            if(plugin == this) return;
            if(!Interface.uMod._plugins.IsInitialized)
                return;
            if(_permissionSyncTimer != null && !_permissionSyncTimer.Destroyed)
            {
                _permissionSyncTimer.Destroy();
            }

            _permissionSyncTimer = timer.Once(30f, SyncPermissions);
        }

        private void SyncPermissions()
        {
            Broadcast(new PermissionList()
            {
                Permissions = permission.GetPermissions()
            });
        }

        private void Broadcast<T>(T data, string connectionName = null) where T : class
        {
            if (string.IsNullOrEmpty(connectionName))
            {
                connectionName = "rcon";
            }

            PacketScopeAttribute attribute = typeof(T).GetCustomAttribute<PacketScopeAttribute>(true);

            BroadcastPacket(new Packet()
            {
                Message = JsonConvert.SerializeObject(data),
                Type = attribute.Type,
                Name = connectionName
            });
        }

        private void BroadcastPacket<T>(T packet, string connectionName = null) where T : Packet
        {
            if (string.IsNullOrEmpty(connectionName))
            {
                connectionName = "rcon";
            }

            if (string.IsNullOrEmpty(packet.Type))
            {
                PacketScopeAttribute attribute = typeof(T).GetCustomAttribute<PacketScopeAttribute>(true);
                if (attribute != null)
                {
                    packet.Type = attribute.Type.TitleCase();
                }
                else
                {
                    throw new InvalidOperationException("Cannot send packet without type");
                }
            }

            if (string.IsNullOrEmpty(packet.Name))
            {
                packet.Name = connectionName;
            }

            string packetMessage = JsonConvert.SerializeObject(packet);

            _webSockets?.EnqueueBroadcast(connectionName, packetMessage);
        }
    }
}
