﻿namespace uMod.WebSockets
{
    public class Provider
    {
        private Web.Provider _webProvider;
        internal Apps.WebSockets Application;
        internal Configuration.Web Configuration => _webProvider.Configuration;

        internal Provider(Web.Provider webProvider)
        {
            _webProvider = webProvider;
        }

        /// <summary>
        /// Initialize websocket server application
        /// </summary>
        internal void Initialize()
        {
            Configuration.Initialize();
        }
    }
}
