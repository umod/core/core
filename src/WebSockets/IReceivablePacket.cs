﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using uMod.Common;

namespace uMod.WebSockets
{
    public interface IReceivablePacket
    {
        void OnReceived(SocketConnection socketConnection);
    }
}
