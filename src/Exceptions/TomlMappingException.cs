﻿using System;

namespace uMod.Exceptions
{
    public class TomlMappingException : Exception
    {
        public TomlMappingException()
        {
        }

        public TomlMappingException(string message) : base(message)
        {
        }

        public TomlMappingException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
