﻿using System;
using System.IO;

namespace uMod.Exceptions
{
    internal class LegacyConfigFileNotFoundException : FileNotFoundException
    {
        public LegacyConfigFileNotFoundException()
        {
        }

        public LegacyConfigFileNotFoundException(string message) : base(message)
        {
        }

        public LegacyConfigFileNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public LegacyConfigFileNotFoundException(string message, string fileName) : base(message, fileName)
        {
        }

        public LegacyConfigFileNotFoundException(string message, string fileName, Exception innerException) : base(message, fileName, innerException)
        {
        }
    }
}
