﻿using System;

namespace uMod.Exceptions
{
    public class SubstitutionMismatch : Exception
    {
        public SubstitutionMismatch()
        {
        }

        public SubstitutionMismatch(string message) : base(message)
        {
        }

        public SubstitutionMismatch(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
