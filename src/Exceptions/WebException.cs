﻿using System;

namespace uMod.Exceptions
{
    public class WebException : Exception
    {
        public WebException()
        {
        }

        public WebException(string message) : base(message)
        {
        }

        public WebException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
