﻿extern alias References;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using References::Newtonsoft.Json;
using uMod.Configuration;
using uMod.IO;

namespace uMod
{
    /// <summary>
    /// Manages all data files
    /// </summary>
    public class DataFileSystem
    {
        /// <summary>
        /// Get default json serializer settings
        /// </summary>
        /// <returns></returns>
        public static JsonSerializerSettings GetDefaultJsonSerializerSettings()
        {
            JsonSerializerSettings serializerSettings = new JsonSerializerSettings
            {
                ObjectCreationHandling = ObjectCreationHandling.Replace
            };
            serializerSettings.Converters.Add(new JsonKeyValuesConverter());

            return serializerSettings;
        }

        /// <summary>
        /// Gets the directory that this system works in
        /// </summary>
        public string Directory { get; }

        // All currently loaded datafiles
        private readonly Dictionary<string, DynamicConfigFile> _datafiles;

        /// <summary>
        /// Initializes a new instance of the DataFileSystem class
        /// </summary>
        /// <param name="directory"></param>
        public DataFileSystem(string directory)
        {
            Directory = directory;
            _datafiles = new Dictionary<string, DynamicConfigFile>();
        }

        /// <summary>
        /// Get a configuration file by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public DynamicConfigFile GetFile(string name)
        {
            name = PathUtils.Sanitize(name);
            if (!_datafiles.TryGetValue(name, out DynamicConfigFile datafile))
            {
                datafile = new DynamicConfigFile(Path.Combine(Directory, $"{name}.json"));
                _datafiles.Add(name, datafile);
                return datafile;
            }

            return datafile;
        }

        /// <summary>
        /// Check if datafile exists without creating it
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool ExistsDatafile(string name)
        {
            name = PathUtils.Sanitize(name);
            if (!_datafiles.TryGetValue(name, out DynamicConfigFile _))
            {
                return File.Exists(Path.Combine(Directory, $"{name}.json"));
            }

            return true;
        }

        /// <summary>
        /// Gets a datafile
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public DynamicConfigFile GetDatafile(string name)
        {
            DynamicConfigFile datafile = GetFile(name);

            // Does it exist?
            if (datafile.Exists())
            {
                // Load it
                datafile.Load();
            }
            else
            {
                // Just make a new one
                datafile.Save();
            }

            return datafile;
        }

        /// <summary>
        /// Gets data files from path, with optional search pattern
        /// </summary>
        /// <param name="path"></param>
        /// <param name="searchPattern"></param>
        /// <returns></returns>
        public string[] GetFiles(string path = "", string searchPattern = "*")
        {
            return System.IO.Directory.GetFiles(Path.Combine(Directory, path), searchPattern);
        }

        /// <summary>
        /// Saves the specified datafile
        /// </summary>
        /// <param name="name"></param>
        public void SaveDatafile(string name) => GetFile(name).Save();

        /// <summary>
        /// Read a JSON file and convert it to generic object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public T ReadObject<T>(string name)
        {
            if (!ExistsDatafile(name))
            {
                T instance = Interface.uMod.Application.Make<T>();
                //T instance = Activator.CreateInstance<T>();
                WriteObject(name, instance);
                return instance;
            }

            return GetFile(name).ReadObject<T>();
        }

        /// <summary>
        /// Write a generic object to a JSON file
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="object"></param>
        /// <param name="sync"></param>
        public void WriteObject<T>(string name, T @object, bool sync = false) => GetFile(name).WriteObject(@object, sync);

        /// <summary>
        /// Read data files in a batch and send callback
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="callback"></param>
        public void ForEachObject<T>(string name, Action<T> callback)
        {
            string folder = PathUtils.Sanitize(name);
            IEnumerable<DynamicConfigFile> files = _datafiles.Where(d => d.Key.StartsWith(folder)).Select(a => a.Value);
            foreach (DynamicConfigFile file in files)
            {
                callback?.Invoke(file.ReadObject<T>());
            }
        }
    }
}
